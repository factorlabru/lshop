<?php

use yii\db\Migration;

class m170213_170634_create_table_tbl_sections extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tbl_sections}}', [
            'id' => $this->integer(11)->notNull()->append('AUTO_INCREMENT PRIMARY KEY'),
            'parent_id' => $this->integer(11),
            'name' => $this->string(255)->notNull(),
            'url_alias' => $this->string(255),
            'content' => $this->text(),
            'img' => $this->string(255),
            'priority' => $this->integer(11),
            'vis' => $this->smallInteger(1)->notNull()->defaultValue('1'),
            'menu_vis' => $this->smallInteger(1)->notNull()->defaultValue('1'),
            'child_vis' => $this->smallInteger(1)->notNull()->defaultValue('1'),
            'external_link' => $this->string(255),
            'meta_title' => $this->string(255),
            'meta_description' => $this->string(255),
            'meta_keywords' => $this->string(255),
            'h1_tag' => $this->string(255),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime(),
        ], $tableOptions);

    }

    public function safeDown()
    {
        echo "m170213_170634_create_table_tbl_sections cannot be reverted.\n";
        return false;
    }
}
