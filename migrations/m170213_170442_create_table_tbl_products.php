<?php

use yii\db\Migration;

class m170213_170442_create_table_tbl_products extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tbl_products}}', [
            'id' => $this->integer(11)->notNull()->append('AUTO_INCREMENT PRIMARY KEY'),
            'name' => $this->string(255)->notNull(),
            'url_alias' => $this->string(255),
            'price' => $this->integer(11),
            'old_price' => $this->integer(11),
            'code' => $this->string(25),
            'description' => $this->text(),
            'features' => $this->text(),
            'vis' => $this->smallInteger(1)->notNull()->defaultValue('1'),
            'new' => $this->smallInteger(1)->notNull()->defaultValue('0'),
            'category_id' => $this->integer(11),
            'brand_id' => $this->integer(11),
            'meta_title' => $this->string(255),
            'meta_description' => $this->string(255),
            'meta_keywords' => $this->string(255),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime(),
        ], $tableOptions);

    }

    public function safeDown()
    {
        echo "m170213_170442_create_table_tbl_products cannot be reverted.\n";
        return false;
    }
}
