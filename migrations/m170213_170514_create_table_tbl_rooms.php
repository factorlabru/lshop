<?php

use yii\db\Migration;

class m170213_170514_create_table_tbl_rooms extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tbl_rooms}}', [
            'id' => $this->integer(11)->notNull()->append('AUTO_INCREMENT PRIMARY KEY'),
            'name' => $this->string(255)->notNull(),
            'url_alias' => $this->string(255),
            'price_single' => $this->integer(11),
            'description_block' => $this->text(),
            'content' => $this->text()->notNull(),
            'description' => $this->text(),
            'priority' => $this->smallInteger(3),
            'vis' => $this->smallInteger(1)->notNull()->defaultValue('1'),
            'meta_title' => $this->string(255),
            'meta_description' => $this->text(),
            'meta_keywords' => $this->string(255),
            'h1_tag' => $this->string(255),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime(),
        ], $tableOptions);

    }

    public function safeDown()
    {
        echo "m170213_170514_create_table_tbl_rooms cannot be reverted.\n";
        return false;
    }
}
