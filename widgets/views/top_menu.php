<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\helpers\Common;
?>

<style>
    .dropdown:hover .dropdown-menu {
        display: block;
    }
    .dropdown-menu {
        margin-top: 0px;
    }
</style>

<nav class="navbar navbar-inverse navbar-fixed-top demo-navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Навигация</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin">Панель управления</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="/">Главная</a></li>
                <?php if($sections) {?>

                    <?php foreach ($sections as $section) {?>
                        <?php
                        $class = '';
                        if (Common::isInPath(Yii::$app->request->get('path'), $section->url_alias)) {
                            $class = 'active';
                        }?>

                        <?php
                        $dropdown = '';
                        if($section->childrenAll) {
                            $dropdown = ' dropdown';
                        }?>

                        <li class="<?=$class.$dropdown?>">
                            <a href="<?=Url::toRoute([
                                '/section/section/index',
                                'path'=>$section->url_alias])?>">
                                <?=$section->name?>
                            </a>
                            <?php if($dropdown) {?>
                                <ul class="dropdown-menu">
                                    <?php foreach($section->childrenAll as $subsection) {?>
                                        <li>
                                            <a href="<?=Url::toRoute(['/section/section/index', 'path'=>$subsection->getUrlPath()])?>"><?=$subsection->name?></a>
                                        </li>
                                    <?php }?>
                                </ul>
                            <?php }?>
                        </li>

                    <?php }?>
                <?php }?>
            </ul>

            <div class="navbar-form navbar-right">
                <?=\app\modules\cart\widgets\Cart::widget();?>
            </div>
        </div><!--/.nav-collapse -->
    </div>
</nav>