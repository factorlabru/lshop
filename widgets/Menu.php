<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use app\modules\catalog\models\Category;
use app\modules\section\models\Section;


class Menu extends Widget
{
    public $view = 'top_menu';

    public function run()
    {
        $view = $this->view;
        $sections = Section::find()
            ->where(['vis'=>1, 'menu_vis'=>1])
            ->roots()
            ->orderBy('priority')
            ->all();

        return $this->render($view, ['sections'=>$sections]);
    }

    /**
     * Вариант выборки меню без AR.
     * @return array
     */
    private function getMenu()
    {
        $query = "SELECT
                       *
                 FROM
                       tbl_sections
                 WHERE
                       vis='1'
                 AND
                      menu_vis='1'
                 ORDER BY
                       priority, id";

        $sections = Yii::$app->db->createCommand($query)->queryAll();

        $main_sections = [];
        $children_sections = [];

        foreach($sections as $section) {
            if($section['parent_id']) {
                $children_sections[$section['parent_id']][$section['id']] = ["id"=>$section['id'],
                    "name"=>$section['name'],
                    "parent_id"=>$section['parent_id'],
                    "url_alias"=>$section['url_alias']];
            } else {
                $main_sections[$section['id']] = ["id"=>$section['id'],
                    "name"=>$section['name'],
                    "url_alias"=>$section['url_alias']];
            }
        }

        return ['main_sections'=>$main_sections, 'children_sections'=>$children_sections];
    }
}