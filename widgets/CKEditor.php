<?php

namespace app\widgets;

use yii\helpers\ArrayHelper;
use iutbay\yii2kcfinder\KCFinderAsset;
use Yii;


class CKEditor extends \dosamigos\ckeditor\CKEditor
{

    public $enableKCFinder = true;

    /**
     * Registers CKEditor plugin
     */
    protected function registerPlugin()
    {
        if ($this->enableKCFinder) {
            $this->registerKCFinder();
        }

        parent::registerPlugin();
    }

    protected function registerKCFinder()
    {
        $kcfOptions = [
            'disabled' => false,
            'uploadURL' => '/content',
            'uploadDir' => Yii::getAlias('@webroot/content'),
            'access' => [
                'files' => [
                    'upload' => true,
                    'delete' => true,
                    'copy' => true,
                    'move' => true,
                    'rename' => true,
                ],
                'dirs' => [
                    'upload' => true,
                    'create' => true,
                    'delete' => true,
                    'rename' => true,
                ],
            ],
        ];

        $_SESSION['KCFINDER'] = $kcfOptions;

        $register = KCFinderAsset::register($this->view);
        $kcfinderUrl = $register->baseUrl;

        $browseOptions = [
            'filebrowserBrowseUrl' => $kcfinderUrl . '/browse.php?opener=ckeditor&type=files',
            'filebrowserUploadUrl' => $kcfinderUrl . '/upload.php?opener=ckeditor&type=files',
            'filebrowserImageBrowseUrl' => $kcfinderUrl . '/browse.php?opener=ckeditor&type=images',
            'filebrowserImageUploadUrl' => $kcfinderUrl . '/upload.php?opener=ckeditor&type=images',
        ];

        $this->clientOptions = ArrayHelper::merge($browseOptions, $this->clientOptions);
    }
}