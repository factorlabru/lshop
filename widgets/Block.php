<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;


class Block extends Widget
{
    /**
     * @var int
     */
    public $id;

    public function run()
    {
        /** @var \app\modules\block\models\Block $block */

        $id = $this->id;

        //Если, обычный блок
        if($id && is_numeric($id)) {
            $block = \app\modules\block\models\Block::find()
                ->where(['id'=>$id, 'vis'=>1])
                ->one();

            if($block) {
                echo self::makeEditable($block->getEditUrl());
                echo $block->content;
                echo self::makeEditableStopper();
            }
        } else {
            $page_url = parse_url($_SERVER['REQUEST_URI']);
            $page_url = Html::encode($page_url['path']);

            $block = \app\modules\block\models\Block::find()
                ->where(['page_url'=>$page_url, 'vis'=>1])
                ->one();

            if($block) {
                echo self::makeEditable($block->getEditUrl());
                echo $block->content;
                echo self::makeEditableStopper();
            } else {
                echo self::makeEditable(\app\modules\block\models\Block::getCreateUrl());
                echo "<i class='empty_editable'></i>";
                echo self::makeEditableStopper();
            }
        }
    }

    public static function makeEditable($edit_url)
    {
        $data = '';
        if (!Yii::$app->user->isGuest) {
            $data = json_encode([
                'edit_url' => $edit_url,
            ]);
            $data = "<!--we_editable:$data-->";
        }
        return $data;
    }

    public static function makeEditableStopper()
    {
        $data = '';
        if (!Yii::$app->user->isGuest) {
            $data = "<!--we_editable_stopper-->";
        }
        return $data;
    }
}