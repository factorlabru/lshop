<?php
use tests\codeception\_pages\TestimonialPage;

/* @var $scenario Codeception\Scenario */

$I = new AcceptanceTester($scenario);
$I->wantTo('ensure that testimonials works');

$testimonial_page = TestimonialPage::openBy($I);

$I->see('Отзывы');
$I->seeLink('Добавить отзыв');
$I->click('Добавить отзыв');
$I->see('Оставить отзыв');

$I->amGoingTo('submit testimonials form with no data');
$testimonial_page->submit([]);
if (method_exists($I, 'wait')) {
    $I->wait(3); // only for selenium
}
$I->expectTo('see validations errors');
$I->see('Оставить отзыв', 'h1');
$I->see('Необходимо заполнить «ФИО».');
$I->see('Необходимо заполнить «Email».');
$I->see('Необходимо заполнить «Отзыв».');

$I->amGoingTo('submit testimonials form with correct data');
$testimonial_page->submit([
    'name' => 'Name From Test',
    'email' => 'testemail@example.com',
    'message' => 'Test message',
    'verifyCode' => 'testme',
]);
if (method_exists($I, 'wait')) {
    $I->wait(3); // only for selenium
}
$I->dontSeeElement('#testimonial-form');
$I->see('Сообщение отправлено');