<?php

/* @var $scenario Codeception\Scenario */

$I = new AcceptanceTester($scenario);
$I->wantTo('ensure that admin testimonials works');
$I->amOnPage('/admin');
$I->seeInTitle('Вход в панель управления');

$I->amGoingTo('submit login form with correct data');
$I->fillField('LoginForm[username]','admin');
$I->fillField('LoginForm[password]','idkfa');

if (method_exists($I, 'wait')) {
    $I->wait(3); // only for selenium
}
$I->click('Войти');

$I->seeInTitle('CMS | Панель управления');

$I->amGoingTo('testimonials create page');
$I->amOnPage('/admin/testimonials/default/create/');
$I->see('Создание отзыва');

$I->submitForm(
    '#w0',
    [
        'Testimonial' => [
            'created_at' => '2016-12-15',
            'name' => 'Tester',
            'email' => 'test@test.ru',
            'phone' => '755555555',
            'city' => 'Moscow',
            'message' => 'Test testimonial from Codeception',
            'moderation' => true
        ]
    ],
    'save_continue'
);

$I->see('Данные сохранены');