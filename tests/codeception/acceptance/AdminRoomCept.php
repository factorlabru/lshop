<?php

/* @var $scenario Codeception\Scenario */

$I = new AcceptanceTester($scenario);
$I->wantTo('ensure that admin module rooms works');
$I->amOnPage('/admin');
$I->seeInTitle('Вход в панель управления');

$I->amGoingTo('submit login form with correct data');
$I->fillField('LoginForm[username]','admin');
$I->fillField('LoginForm[password]','idkfa');

if (method_exists($I, 'wait')) {
    $I->wait(3); // only for selenium
}
$I->click('Войти');

$I->seeInTitle('CMS | Панель управления');

$I->amGoingTo('room create page');
$I->amOnPage('/admin/rooms/default/create/');
$I->see('Создание номера');

$I->amGoingTo('submit rooms form with wrong data');
$I->fillField('Room[name]','');
$I->fillField('Room[price_single]','12345');
$I->fillField('Room[content]','Test Content From Codeception');
//$I->attachFile('input[name="Room[file_loader][]"]',  'testfile.php');
$I->attachFile('input[type="file"]', 'testfile.php');
$I->click('Сохранить и продолжить');

$I->see('Необходимо заполнить «Название».');
$I->see('Разрешена загрузка файлов только со следующими расширениями: png, jpg, jpeg, gif.');

$I->amGoingTo('submit rooms form with correct data');
$I->fillField('Room[name]','Test Name');
$I->fillField('Room[price_single]','12345');
$I->fillField('Room[content]','Test Content From Codeception');
$I->attachFile('input[type="file"]', 'testimg.jpg');

if (method_exists($I, 'wait')) {
    $I->wait(3); // only for selenium
}
$I->click('Сохранить и продолжить');

$I->see('Данные сохранены');