<?php

namespace tests\codeception\unit\models;

use app\modules\catalog\models\Product;
use app\modules\cart\cart\Cart;
use app\modules\cart\cart\CartItem;
use app\modules\cart\cart\storage\CartStorage;
use Yii;
use yii\codeception\TestCase;
use Codeception\Specify;

class CartTest extends TestCase
{
    use Specify;

    protected function setUp()
    {
        parent::setUp();

        Yii::$container->set(CartStorage::class, function(){
            return new CartStorage(
                null // не используем сессию, там и так все прекрасно.
            );
        });

        Yii::$app->db->createCommand()->delete('{{%cart_products}}', [
            'user_id' => 'test_cookie',
        ])->execute();
    }

    public function testAdd()
    {
        /** @var Cart $cart */
        $cart = Yii::$container->get(Cart::class);

        $product = Product::findOne(1);
        $item = new CartItem($product, 2);

        $cart->add($item);

        $product = Product::findOne(2);
        $item = new CartItem($product, 2);
        $cart->add($item);

        $this->assertEquals(4, $cart->getQuantity());
    }

    public function testSingleQuantity()
    {
        $cart = Yii::$container->get(Cart::class);

        $product = Product::findOne(1);
        $item = new CartItem($product, 3);
        $cart->add($item);

        $id = $item->getId();

        $product = Product::findOne(2);
        $item = new CartItem($product, 5);
        $cart->add($item);

        $this->assertEquals(3, $cart->getItemQuantity($id));
    }

    public function testDelete()
    {
        /** @var Cart $cart */
        $cart = Yii::$container->get(Cart::class);

        $product = Product::findOne(1);

        $item = new CartItem($product, 2);
        $cart->add($item);

        $id = $item->getId();
        $cart->delete($id);

        $this->assertEquals(0, $cart->getItemQuantity($id));
    }

    public function testQuantityException1()
    {
        $this->expectException('InvalidArgumentException');

        $product = Product::findOne(1);
        new CartItem($product, 1.5);
    }

    public function testQuantityException2()
    {
        $this->expectException('InvalidArgumentException');

        $product = Product::findOne(1);
        new CartItem($product, -1);
    }


    public function testQuantityException3()
    {
        $this->expectException('InvalidArgumentException');

        $product = Product::findOne(1);
        $item = new CartItem($product, 1);
        $item->setQuantity(-1);
    }

    public function testQuantityException4()
    {
        $this->expectException('InvalidArgumentException');

        $product = Product::findOne(1);
        $item = new CartItem($product, 1);
        $item->addQuantity(-1);
    }

    public function testQuantityException5()
    {
        $this->expectException('InvalidArgumentException');

        $product = Product::findOne(1);
        $item = new CartItem($product, 1);
        $item->addQuantity(0);
    }


    public function testGetItems()
    {
        /** @var Cart $cart */
        $cart = Yii::$container->get(Cart::class);

        $product = Product::findOne(1);
        $item = new CartItem($product, 3);
        $cart->add($item);

        $product = Product::findOne(2);
        $item2 = new CartItem($product, 2);
        $cart->add($item2);

        $items = $cart->getItems();
        
        $this->assertEquals($item, array_shift($items));
        $this->assertEquals($item2, array_shift($items));
    }


    public function testClear()
    {
        /** @var Cart $cart */
        $cart = Yii::$container->get(Cart::class);

        $product = Product::findOne(1);
        $item = new CartItem($product, 3);
        $cart->add($item);

        $cart->clear();

        $this->assertEquals(0, $cart->getQuantity());
        $this->assertEquals([], $cart->getItems());
    }


}
