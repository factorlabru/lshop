<?php

namespace tests\codeception\_pages;

use yii\codeception\BasePage;

/**
 * Represents contact page
 * @property \AcceptanceTester|\FunctionalTester $actor
 */
class TestimonialPage extends BasePage
{
    public $route = '/testimonials';

    /**
     * @param array $contactData
     */
    public function submit(array $contactData)
    {
        foreach ($contactData as $field => $value) {
            $inputType = $field === 'body' ? 'textarea' : 'input';

            //$this->actor->fillField($inputType . '[name="Testimonial[' . $field . ']"]', $value);
            $this->actor->fillField('Testimonial['.$field.']', $value);
            //$this->actor->fillField($inputType . '[name="Testimonial[' . $field . ']"]', $value);
        }
        $this->actor->click('Отправить');
    }
}
