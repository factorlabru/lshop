<?php

namespace app\helpers;

class Collection extends \yii2mod\collection\Collection
{
    public function get($key, $default = null)
    {
        return \yii\helpers\ArrayHelper::getValue($this->items, $key, $default);
    }
}