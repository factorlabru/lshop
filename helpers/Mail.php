<?php
namespace app\helpers;

use Yii;
use yii\base\Model;

class Mail
{
    public function sendMail($model, $mail_to, $letter_layout='', $subject='Письмо с сайта', $mail_from='')
    {
        $mails = explode(',', $mail_to);

        $mail_from = $mail_from ? $mail_from : 'noreply@'.$_SERVER['SERVER_NAME'];

        foreach ($mails as $mail) {
            Yii::$app->mailer->compose($letter_layout, ['model' => $model])
                ->setFrom($mail_from)
                ->setTo($mail)
                ->setSubject($subject.' '.$_SERVER['SERVER_NAME'])
                ->send();
        }
    }
}