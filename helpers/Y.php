<?php
namespace app\helpers;

use yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\AssetBundle;
class Y
{
    /**
     * @return \yii\console\Application|\yii\web\Application
     */
    public static function app()
    {
        return Yii::$app;
    }

    /**
     * @return \yii\db\Connection
     */
    public static function db()
    {
        return Yii::$app->getDb();
    }

    /**
     * @return \yii\caching\Cache
     */
    public static function cache()
    {
        return Yii::$app->getCache();
    }

    /**
     * @return \yii\console\Request|\yii\web\Request
     */
    public static function request()
    {
        return Yii::$app->getRequest();
    }

    /**
     * @return \yii\console\Response|\yii\web\Response
     */
    public static function response()
    {
        return Yii::$app->getResponse();
    }

    /**
     * @param $name
     * @return null|\yii\base\Module
     */
    public static function module($name)
    {
        return Yii::$app->getModule($name);
    }

    /**
     * @param $name
     * @return null|object
     */
    public static function component($name)
    {
        return Yii::$app->get($name);
    }

    /**
     * @return \yii\i18n\Formatter
     */
    public static function formatter()
    {
        return Yii::$app->formatter;
    }

    /**
     * @return \yii\console\Controller|\yii\web\Controller
     */
    public static function controller()
    {
        return Yii::$app->controller;
    }

    /**
     * @return \yii\base\View|\yii\web\View
     */
    public static function view()
    {
        return Yii::$app->getView();
    }

    /**
     * @return \yii\web\User|null
     */
    public static function user()
    {
        return Yii::$app instanceof \yii\web\Application ? Yii::$app->getUser() : null;
    }

    /**
     * @return \yii\web\Session|null
     */
    public static function session()
    {
        return Yii::$app instanceof \yii\web\Application ? Yii::$app->getSession() : null;
    }

    public static function sessionSet($key, $value)
    {
        Yii::$app->session[$key] = $value;
    }

    public static function sessionGet($key, $defaultValue = null)
    {
        $result = Yii::$app->session[$key];
        if (!$result) {
            return $defaultValue;
        }
        return $result;
    }

    public static function sessionDel($key)
    {
        Yii::$app->session->remove($key);
    }

    /**
     * Returns the Yii::$app->params variable value or $defaultValue if the Yii::$app->params variable does not exist
     *
     * @param string $name the param variable name (could be used dot delimiter for nested variable)
     * Example: variable name 'Post.post_text' will return value at Yii::$app->params['Post']['post_text']
     * @param mixed $defaultValue the default value to be returned when the Yii::$app->params variable does not exist
     * @return mixed
     */
    public static function param($name, $defaultValue = null)
    {
        return ArrayHelper::getValue(Yii::$app->params, $name, $defaultValue);
    }

    /**
     * @param string $name
     * @return AssetBundle the registered asset bundle instance
     */
    public static function assetBundle($name = 'app\assets\AppAsset')
    {
        return Yii::$app->view->registerAssetBundle($name);
    }

    /**
     * Returns the $_GET variable value or $defaultValue if the $_GET variable does not exist
     *
     * @param string $name the $_GET variable name (could be used dot delimiter for nested variable)
     * Example: variable name 'Post.post_text' will return value at $_GET['Post']['post_text']
     * @param mixed $defaultValue the default value to be returned when the $_GET variable does not exist
     * @return mixed
     */
    public static function getGet($name, $defaultValue = null)
    {
        return ArrayHelper::getValue($_GET, $name, $defaultValue);
    }

    /**
     * Returns the $_POST variable value or $defaultValue if the $_POST variable does not exist
     *
     * @param string $name the $_POST variable name (could be used dot delimiter for nested variable)
     * Example: variable name 'Post.post_text' will return value at $_POST['Post']['post_text']
     * @param mixed $defaultValue the default value to be returned when the $_POST variable does not exist
     * @return mixed
     */
    public static function getPost($name, $defaultValue = null)
    {
        return ArrayHelper::getValue($_POST, $name, $defaultValue);
    }

    /**
     * Returns the $_FILES variable value or $defaultValue if the $_FILES variable does not exist
     *
     * @param string $name the $_FILES variable name (could be used dot delimiter for nested variable)
     * Example: variable name 'userfile.name' will return value at $_FILES['userfile']['name']
     * @param mixed $defaultValue the default value to be returned when the $_FILES variable does not exist
     * @return mixed
     */
    public static function getFiles($name, $defaultValue = null)
    {
        return ArrayHelper::getValue($_FILES, $name, $defaultValue);
    }

    public static function getArr($name, $array, $defaultValue = null)
    {
        return ArrayHelper::getValue($array, $name, $defaultValue);
    }

    /**
     * Returns the relative URL for the application
     *
     * @param bool $absolute whether to return an absolute URL. Defaults to false, meaning returning a relative one (@since 1.1.0)
     * @return string
     */
    public static function baseUrl($absolute = false)
    {
        $request = Yii::$app->request;
        return ($absolute ? $request->getHostInfo() : '') . $request->getBaseUrl($absolute);
    }

    public static function isAjax()
    {
        return Yii::$app->request->getIsAjax();
    }

    public static function isPost()
    {
        return Yii::$app->request->getIsPost();
    }

    public static function lang()
    {
        return Yii::$app->language;
    }

    public static function conf($key)
    {
        return Y::param($key); // todo: раделить конфиг и параметры
    }
    
    public static function endJson($data, $options = 320)
    {
        header('Content-Type: application/json;');
        //Yii::$app->response->getHeaders()->set('Content-Type: application/json;');
        echo yii\helpers\Json::encode($data, $options);
        Yii::$app->end();
    }

    public static function end($text = '')
    {
        echo $text;
        Yii::$app->end();
    }

    public static function csrf()
    {
        return Yii::$app->request->csrfToken;
    }

    public static function csrfName()
    {
        return Yii::$app->request->csrfParam;
    }

    public static function cookieSet($name, $value, $expire = 0, $path = '/', $domain = '', $secure = false, $httpOnly = true)
    {
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => $name,
            'value' => $value,
            'expire' => $expire,
            'path' => $path,
            'domain' => $domain,
            'secure' => $secure,
            'httpOnly' => $httpOnly,
        ]));
    }

    public static function cookieGet($name, $defaultValue = null)
    {
        $result = Yii::$app->request->cookies[$name];
        if (empty($result)) {
            $result = $defaultValue;
        }
        return $result;
    }

    public static function cookieDel($name)
    {
        Yii::$app->request->cookies->remove($name);
    }

    public static function notFoundIfEmpty($data, $message = 'Страница не найдена.')
    {
        if (!$data) {
            throw new \yii\web\NotFoundHttpException($message);
        }
    }

    public static function ip()
    {
        return Yii::$app->request->userIP;
    }

    public static function isGuest()
    {
        return Yii::$app->user->isGuest;
    }

    public static function createUrl($route, $params)
    {
        return Url::toRoute(array_merge([$route], $params));
    }

    public static function white($val, array $list, $default = null)
    {
        if(in_array($val, $list)){
            return $val;
        }
        return $default;
    }
}