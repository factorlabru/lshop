<?php


function collect($input = [])
{
    return new \app\helpers\Collection($input);
}

function e($content, $doubleEncode = true)
{
    return \yii\helpers\Html::encode($content, $doubleEncode);
}

function price_format($price)
{
    return number_format($price, 0, " ", " ");
}

function dd()
{
    array_map(function($input){
        \yii\helpers\VarDumper::dump($input, 10, 1);
    }, func_get_args());
    exit;
}

function html_block($id = null)
{
    return \app\widgets\Block::widget(['id' => $id]);
}

function is_active_url($url, $strict = false)
{
    $current_url = Yii::$app->request->pathInfo;
    $url = trim($url, '/') . '/';
    $current_url = trim($current_url, '/') . '/';

    if($strict && $url != $current_url) {
        return false;
    }

    return strpos($current_url, $url, 0) === 0;
}