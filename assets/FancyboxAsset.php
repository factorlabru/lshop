<?php
namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

class FancyboxAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '/web';

    public $css = [
        'js/libs/fancybox/jquery.fancybox.css',
    ];
    public $js = [
        'js/libs/fancybox/jquery.fancybox.pack.js',
    ];
    public $jsOptions = [
        'position' => View::POS_END,
    ];
}