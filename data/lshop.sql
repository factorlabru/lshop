-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Мар 15 2018 г., 11:42
-- Версия сервера: 5.7.21-0ubuntu0.16.04.1
-- Версия PHP: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `shopcms_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_articles`
--

CREATE TABLE `tbl_articles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url_alias` varchar(255) DEFAULT NULL,
  `annotation` text,
  `content` text,
  `img` varchar(255) DEFAULT NULL,
  `vis` tinyint(1) NOT NULL DEFAULT '1',
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `h1_tag` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_articles`
--

INSERT INTO `tbl_articles` (`id`, `name`, `url_alias`, `annotation`, `content`, `img`, `vis`, `meta_title`, `meta_description`, `meta_keywords`, `h1_tag`, `updated_at`, `created_at`) VALUES
(3, 'Типичный гедонизм: адаптация или созерцание?', 'tipichnyy-gedonizm-adaptaciya-ili-sozercanie-3', 'Гедонизм, как принято считать, может быть получен из опыта. Закон исключённого третьего естественно осмысляет трагический гений. Современная ситуация, как следует из вышесказанного, творит трансцендентальный знак. Освобождение, следовательно, создает даосизм. Ощущение мира рассматривается даосизм.', '<p>Гедонизм, как принято считать, может&nbsp;быть&nbsp;получен&nbsp;из&nbsp;опыта. Закон исключённого третьего естественно осмысляет трагический гений. Современная&nbsp;ситуация, как следует из вышесказанного, творит трансцендентальный знак. Освобождение, следовательно, создает даосизм. Ощущение&nbsp;мира рассматривается даосизм.</p>\r\n\r\n<p>Отношение&nbsp;к&nbsp;современности трогательно&nbsp;наивно. Заблуждение трансформирует даосизм. Сомнение амбивалентно индуцирует смысл&nbsp;жизни.</p>\r\n\r\n<p>Предмет&nbsp;деятельности, как следует из вышесказанного, творит трагический гений. Жизнь рассматривается здравый смысл. Структурализм философски осмысляет онтологический дуализм. Искусство, по определению, амбивалентно.</p>\r\n', 'b30cd96fb80d4d54367ad7a52a038b43.jpg', 1, '', '', '', '', '2017-05-12 11:56:08', '2017-02-28 16:15:26'),
(4, 'Трансцендентальный гедонизм — актуальная национальная задача', 'ranscendentalnyy-gedonizm-aktualnaya-nacionalnaya-zadacha', 'Освобождение реально выводит интеллигибельный конфликт. Катарсис порождает и обеспечивает интеллигибельный гедонизм. Отсюда естественно следует, что искусство трогательно наивно. Отвечая на вопрос о взаимоотношении идеального ли и материального ци, Дай Чжень заявлял, что сомнение нетривиально.', '<p>Нечетная функция, как следует из вышесказанного, изоморфна. Поле направлений подчеркивает аксиоматичный даосизм. Атомистика, по определению, ментально заполняет постулат. Пустое подмножество порождает&nbsp;и&nbsp;обеспечивает интеграл от функции, обращающейся в бесконечность вдоль линии. Интерполяция транспонирует криволинейный интеграл.</p>\r\n\r\n<p>Скалярное произведение индуктивно дискредитирует двойной интеграл. Ввиду непрерывности функции f ( x ), начало координат трансформирует конфликт. В общем, генетика подрывает интеграл Фурье.</p>\r\n\r\n<p>Согласно&nbsp;предыдущему, искусство категорически упорядочивает двойной интеграл. Наряду&nbsp;с&nbsp;этим искусство контролирует принцип&nbsp;восприятия, что известно даже школьникам. Надо&nbsp;сказать,&nbsp;что интеграл от функции, обращающейся в бесконечность вдоль линии проецирует мир, отрицая&nbsp;очевидное. Неопределенный интеграл, как следует из вышесказанного, непредвзято порождает&nbsp;и&nbsp;обеспечивает анормальный предел последовательности.</p>\r\n', 'df5da001d03a9885990af983871e3e5e.jpg', 1, '', '', '', '', '2017-05-12 14:17:24', '2017-02-28 16:17:51'),
(5, 'Гетерономная этика как структурализм', 'geteronomnaya-etika-kak-strukturalizm', 'Освобождение реально выводит интеллигибельный конфликт. Катарсис порождает и обеспечивает интеллигибельный гедонизм. Отсюда естественно следует, что искусство трогательно наивно. Отвечая на вопрос о взаимоотношении идеального ли и материального ци, Дай Чжень заявлял, что сомнение нетривиально.троительства к газораспределительной сети.', '<p>Освобождение реально выводит интеллигибельный конфликт. Катарсис порождает и обеспечивает интеллигибельный гедонизм. Отсюда естественно следует, что искусство трогательно наивно. Отвечая на вопрос о взаимоотношении идеального ли и материального ци, Дай Чжень заявлял, что сомнение нетривиально.</p>\r\n', 'aba82269c7c43ec0b951b9667b3600bf.jpg', 1, '', '', '', '', '2017-08-13 23:23:33', '2017-04-06 15:14:30'),
(6, 'fesffesf', 'fesfe', 'sfesf', '', NULL, 1, '', '', '', '', '2017-08-14 16:32:53', '2017-08-14 16:32:49');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_auth_assignment`
--

CREATE TABLE `tbl_auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `tbl_auth_assignment`
--

INSERT INTO `tbl_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '9', 1519983372),
('manager', '10', 1519983359),
('testrole', '11', 1519983347);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_auth_item`
--

CREATE TABLE `tbl_auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `tbl_auth_item`
--

INSERT INTO `tbl_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, 'Администраторы', NULL, NULL, 1458727133, 1458727133),
('admin/default/index', 2, 'Панель управления', NULL, NULL, 1484747715, 1503564648),
('admin/section/default/create', 2, 'Разделы создание', NULL, NULL, 1484815937, 1484829161),
('admin/section/default/index', 2, 'Разделы', NULL, NULL, 1484813681, 1484816151),
('admin/section/default/update', 2, 'Разделы редактирование', NULL, NULL, 1484814141, 1484816095),
('admin/testimonials', 2, 'Отзывы', NULL, NULL, 1484818618, 1484818682),
('manager', 1, 'Менеджер', NULL, NULL, 1458731341, 1503564884),
('tester', 1, 'Тестировщик', NULL, NULL, 1458898498, 1484581515),
('testrole', 1, 'testroleaaa', NULL, NULL, 1484746894, 1484818655);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_auth_item_child`
--

CREATE TABLE `tbl_auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `tbl_auth_item_child`
--

INSERT INTO `tbl_auth_item_child` (`parent`, `child`) VALUES
('manager', 'admin/default/index'),
('testrole', 'admin/default/index'),
('testrole', 'admin/section/default/create'),
('testrole', 'admin/section/default/update'),
('testrole', 'admin/testimonials');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_auth_networks`
--

CREATE TABLE `tbl_auth_networks` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `identity` varchar(255) NOT NULL,
  `network` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_auth_rule`
--

CREATE TABLE `tbl_auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_block`
--

CREATE TABLE `tbl_block` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `page_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vis` tinyint(1) NOT NULL DEFAULT '1',
  `wysiwyg_vis` tinyint(1) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `tbl_block`
--

INSERT INTO `tbl_block` (`id`, `name`, `content`, `page_url`, `vis`, `wysiwyg_vis`, `updated_at`, `created_at`) VALUES
(1, 'test1', '<p><em><strong>Текст сноски</strong></em><img id="gallery12" src="/css/theme/images/gallery.png" style="display:block" title="Тестовая галерея (ID=12)" /></p>\r\n', '', 1, 1, '2017-02-04 14:36:27', NULL),
(2, 'test2', '<p><em><strong><img alt="" src="/content/images/TOpjijj4HQU%284%29.jpg" style="height:306px; width:500px" />еуеыеыу</strong></em></p>\r\n', NULL, 0, 1, NULL, NULL),
(3, '1', '<div class="mega-fucking-block">mega-fucking-block2</div>', '', 1, 1, '2017-01-23 15:07:50', NULL),
(4, 'test page url block', '<p>Hello from the page url block.</p>\r\n', '/testimonials/complete/', 1, 1, '2017-01-17 17:18:42', '2017-01-17 17:15:54');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_booking`
--

CREATE TABLE `tbl_booking` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  `begin_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `adult` tinyint(5) DEFAULT NULL,
  `children` tinyint(5) DEFAULT NULL,
  `additional_info` text,
  `room_id` int(11) DEFAULT NULL,
  `ip` varchar(55) DEFAULT NULL,
  `viewed` tinyint(1) NOT NULL DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_brands`
--

CREATE TABLE `tbl_brands` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url_alias` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `content` text,
  `priority` int(11) DEFAULT NULL,
  `on_main` tinyint(1) NOT NULL DEFAULT '0',
  `vis` tinyint(1) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_brands`
--

INSERT INTO `tbl_brands` (`id`, `name`, `url_alias`, `img`, `content`, `priority`, `on_main`, `vis`, `updated_at`, `created_at`) VALUES
(112, 'Compliment', 'compliment', '19d908da6d0b5d522921885bdf951ef0.jpg', '', 1, 0, 1, '2016-12-06 14:47:16', NULL),
(113, 'Delicates', 'delicates', NULL, '', 2, 0, 1, NULL, NULL),
(114, 'Anastasia', 'anastasia', NULL, '', 3, 1, 1, NULL, NULL),
(115, 'Belle Jardin', 'belle_jardin', NULL, '', 4, 1, 1, NULL, NULL),
(116, 'Black Mask', 'black_mask', NULL, '', 5, 1, 1, NULL, NULL),
(117, 'Dilon', 'dilon', NULL, '', 6, 0, 1, '2016-12-19 11:13:14', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_callbacks`
--

CREATE TABLE `tbl_callbacks` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `ip` varchar(55) DEFAULT NULL,
  `viewed` tinyint(1) NOT NULL DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_cart_products`
--

CREATE TABLE `tbl_cart_products` (
  `id` int(11) NOT NULL,
  `user_id` varchar(50) NOT NULL DEFAULT '0',
  `products` mediumtext,
  `ip` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `tbl_cart_products`
--

INSERT INTO `tbl_cart_products` (`id`, `user_id`, `products`, `ip`, `created_at`, `updated_at`) VALUES
(489, 'test_cookie', '[]', NULL, '2017-08-25 14:23:19', '2017-08-25 14:23:19'),
(490, '550e28724b297c486a508419a5664f17', '{"a3af4ed25fcf0038893ab75fe5c61685":{"product_id":19,"modification_id":null,"options":null,"quantity":"1"}}', '127.0.0.1', '2017-08-28 15:58:28', '2017-08-28 15:58:28'),
(491, 'c76f463ca2ab9473b183589d3f71b74d', '{"a3af4ed25fcf0038893ab75fe5c61685":{"product_id":19,"modification_id":null,"options":null,"quantity":"1"}}', '127.0.0.1', '2017-08-28 16:05:35', '2017-08-31 14:03:51');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_categories`
--

CREATE TABLE `tbl_categories` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `url_alias` varchar(255) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `img` varchar(55) DEFAULT NULL,
  `vis` tinyint(1) NOT NULL DEFAULT '1',
  `priority` tinyint(5) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `h1_tag` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_categories`
--

INSERT INTO `tbl_categories` (`id`, `parent_id`, `name`, `url_alias`, `short_description`, `description`, `img`, `vis`, `priority`, `meta_title`, `meta_description`, `meta_keywords`, `h1_tag`, `updated_at`, `created_at`) VALUES
(1, NULL, 'Труба медная', 'truba-mednaya', NULL, 'Описание', '7e56be2a454d902fed4c4700db4d638b.jpg', 1, 1, '', '', '', '', '2017-03-23 13:04:16', NULL),
(7, 1, 'Труба метрическая', 'metricheskaya', NULL, '', NULL, 1, 1, '', '', '', NULL, '2016-10-31 14:57:55', NULL),
(8, 1, 'Труба алюминиевая', 'alyuminievaya', NULL, '', NULL, 1, 2, '', '', '', NULL, '2016-10-31 14:58:11', NULL),
(12, NULL, 'Электрика', 'elektrika', NULL, '', NULL, 1, NULL, '', '', '', NULL, '2016-10-31 15:06:21', NULL),
(13, NULL, 'Вентиляция', 'ventilyatsiya', NULL, '', NULL, 1, NULL, '', '', '', NULL, '2016-10-31 15:06:37', NULL),
(14, NULL, 'Оборудование', 'oborudovanie', NULL, '', NULL, 1, NULL, '', '', '', NULL, '2016-10-31 15:07:25', NULL),
(15, NULL, 'Инструменты', 'instrumenty', NULL, '', NULL, 1, NULL, '', '', '', NULL, '2016-10-31 15:07:37', NULL),
(16, NULL, 'Расходники', 'rashodniki', NULL, '', NULL, 1, NULL, '', '', '', NULL, '2016-10-31 15:07:46', NULL),
(19, 12, 'Электрика 2.1', 'elektrika_21', NULL, '', NULL, 1, NULL, '', '', '', NULL, '2016-12-05 15:42:39', NULL),
(20, 12, 'Электрика 2.2', 'elektrika_22', NULL, '', NULL, 1, NULL, '', '', '', NULL, '2016-12-05 15:42:52', NULL),
(28, 19, 'Электрика 3.1', 'elektrika-31', NULL, '', NULL, 1, 0, '', '', '', '', '2016-12-13 22:55:14', '2016-12-13 22:55:14');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_cities`
--

CREATE TABLE `tbl_cities` (
  `id` int(11) UNSIGNED NOT NULL,
  `region_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_cities`
--

INSERT INTO `tbl_cities` (`id`, `region_id`, `name`, `updated_at`, `created_at`) VALUES
(3161, 3160, 'Акташ', NULL, NULL),
(3162, 3160, 'Акутиха', NULL, NULL),
(3163, 3160, 'Алейск', NULL, NULL),
(3164, 3160, 'Алтайский', NULL, NULL),
(3165, 3160, 'Баево', NULL, NULL),
(3166, 3160, 'Барнаул', NULL, NULL),
(3167, 3160, 'Белово', NULL, NULL),
(3168, 3160, 'Белокуриха', NULL, NULL),
(3169, 3160, 'Белоярск', NULL, NULL),
(3170, 3160, 'Бийск', NULL, NULL),
(3171, 3160, 'Благовещенск', NULL, NULL),
(3172, 3160, 'Боровлянка', NULL, NULL),
(3173, 3160, 'Бурла', NULL, NULL),
(3174, 3160, 'Бурсоль', NULL, NULL),
(3175, 3160, 'Волчиха', NULL, NULL),
(3176, 3160, 'Горно-Алтайск', NULL, NULL),
(3177, 3160, 'Горняк', NULL, NULL),
(3178, 3160, 'Ельцовка', NULL, NULL),
(3179, 3160, 'Залесово', NULL, NULL),
(3180, 3160, 'Заринск', NULL, NULL),
(3181, 3160, 'Заток', NULL, NULL),
(3182, 3160, 'Змеиногорск', NULL, NULL),
(3183, 3160, 'Камень-на-Оби', NULL, NULL),
(3184, 3160, 'Ключи', NULL, NULL),
(3185, 3160, 'Кош-Агач', NULL, NULL),
(3186, 3160, 'Красногорское', NULL, NULL),
(3187, 3160, 'Краснощеково', NULL, NULL),
(3188, 3160, 'Кулунда', NULL, NULL),
(3189, 3160, 'Кытманово', NULL, NULL),
(3190, 3160, 'Мамонтово', NULL, NULL),
(3191, 3160, 'Новичиха', NULL, NULL),
(3192, 3160, 'Новоалтайск', NULL, NULL),
(3193, 3160, 'Онгудай', NULL, NULL),
(3194, 3160, 'Павловск', NULL, NULL),
(3195, 3160, 'Петропавловское', NULL, NULL),
(3196, 3160, 'Поспелиха', NULL, NULL),
(3197, 3160, 'Ребриха', NULL, NULL),
(3198, 3160, 'Родино', NULL, NULL),
(3199, 3160, 'Рубцовск', NULL, NULL),
(3200, 3160, 'Славгород', NULL, NULL),
(3201, 3160, 'Смоленское', NULL, NULL),
(3202, 3160, 'Солонешное', NULL, NULL),
(3203, 3160, 'Солтон', NULL, NULL),
(3204, 3160, 'Староаллейское', NULL, NULL),
(3205, 3160, 'Табуны', NULL, NULL),
(3206, 3160, 'Тальменка', NULL, NULL),
(3207, 3160, 'Топчиха', NULL, NULL),
(3208, 3160, 'Троицкое', NULL, NULL),
(3209, 3160, 'Турочак', NULL, NULL),
(3210, 3160, 'Тюменцево', NULL, NULL),
(3211, 3160, 'Угловское', NULL, NULL),
(3212, 3160, 'Усть-Калманка', NULL, NULL),
(3213, 3160, 'Усть-Кан', NULL, NULL),
(3214, 3160, 'Усть-Кокса', NULL, NULL),
(3215, 3160, 'Усть-Улаган', NULL, NULL),
(3216, 3160, 'Усть-Чарышская Пристань', NULL, NULL),
(3217, 3160, 'Хабары', NULL, NULL),
(3218, 3160, 'Целинное', NULL, NULL),
(3219, 3160, 'Чарышское', NULL, NULL),
(3220, 3160, 'Шебалино', NULL, NULL),
(3221, 3160, 'Шелаболиха', NULL, NULL),
(3222, 3160, 'Шипуново', NULL, NULL),
(3224, 3223, 'Айгунь', NULL, NULL),
(3225, 3223, 'Архара', NULL, NULL),
(3226, 3223, 'Белогорск', NULL, NULL),
(3227, 3223, 'Благовещенск (Амурская обл.)', NULL, NULL),
(3228, 3223, 'Бурея', NULL, NULL),
(3229, 3223, 'Возжаевка', NULL, NULL),
(3230, 3223, 'Екатеринославка', NULL, NULL),
(3231, 3223, 'Ерофей Павлович', NULL, NULL),
(3232, 3223, 'Завитинск', NULL, NULL),
(3233, 3223, 'Зея', NULL, NULL),
(3234, 3223, 'Златоустовск', NULL, NULL),
(3235, 3223, 'Ивановка', NULL, NULL),
(3236, 3223, 'Коболдо', NULL, NULL),
(3237, 3223, 'Магдагачи', NULL, NULL),
(3238, 3223, 'Новобурейский', NULL, NULL),
(3239, 3223, 'Поярково', NULL, NULL),
(3240, 3223, 'Райчихинск', NULL, NULL),
(3241, 3223, 'Ромны', NULL, NULL),
(3242, 3223, 'Свободный', NULL, NULL),
(3243, 3223, 'Серышево', NULL, NULL),
(3244, 3223, 'Сковородино', NULL, NULL),
(3245, 3223, 'Стойба', NULL, NULL),
(3246, 3223, 'Тамбовка', NULL, NULL),
(3247, 3223, 'Тында', NULL, NULL),
(3248, 3223, 'Шимановск', NULL, NULL),
(3249, 3223, 'Экимчан', NULL, NULL),
(3250, 3223, 'Ядрино', NULL, NULL),
(3252, 3251, 'Амдерма', NULL, NULL),
(3253, 3251, 'Архангельск', NULL, NULL),
(3254, 3251, 'Березник', NULL, NULL),
(3255, 3251, 'Вельск', NULL, NULL),
(3256, 3251, 'Верхняя Тойма', NULL, NULL),
(3257, 3251, 'Волошка', NULL, NULL),
(3258, 3251, 'Вычегодский', NULL, NULL),
(3259, 3251, 'Емца', NULL, NULL),
(3260, 3251, 'Илеза', NULL, NULL),
(3261, 3251, 'Ильинско-Подомское', NULL, NULL),
(3262, 3251, 'Каргополь', NULL, NULL),
(3263, 3251, 'Карпогоры', NULL, NULL),
(3264, 3251, 'Кодино', NULL, NULL),
(3265, 3251, 'Коноша', NULL, NULL),
(3266, 3251, 'Коряжма', NULL, NULL),
(3267, 3251, 'Котлас', NULL, NULL),
(3268, 3251, 'Красноборск', NULL, NULL),
(3269, 3251, 'Лешуконское', NULL, NULL),
(3270, 3251, 'Мезень', NULL, NULL),
(3271, 3251, 'Нарьян-Мар', NULL, NULL),
(3272, 3251, 'Новодвинск', NULL, NULL),
(3273, 3251, 'Няндома', NULL, NULL),
(3274, 3251, 'Онега', NULL, NULL),
(3275, 3251, 'Пинега', NULL, NULL),
(3276, 3251, 'Плесецк', NULL, NULL),
(3277, 3251, 'Северодвинск', NULL, NULL),
(3278, 3251, 'Сольвычегодск', NULL, NULL),
(3279, 3251, 'Холмогоры', NULL, NULL),
(3280, 3251, 'Шенкурск', NULL, NULL),
(3281, 3251, 'Яренск', NULL, NULL),
(3283, 3282, 'Астрахань', NULL, NULL),
(3284, 3282, 'Ахтубинск', NULL, NULL),
(3285, 3282, 'Верхний Баскунчак', NULL, NULL),
(3286, 3282, 'Володарский', NULL, NULL),
(3287, 3282, 'Енотаевка', NULL, NULL),
(3288, 3282, 'Икряное', NULL, NULL),
(3289, 3282, 'Камызяк', NULL, NULL),
(3290, 3282, 'Капустин Яр', NULL, NULL),
(3291, 3282, 'Красный Яр', NULL, NULL),
(3292, 3282, 'Лиман', NULL, NULL),
(3293, 3282, 'Началово', NULL, NULL),
(3294, 3282, 'Харабали', NULL, NULL),
(3295, 3282, 'Черный Яр', NULL, NULL),
(3297, 3296, 'Аксаково', NULL, NULL),
(3298, 3296, 'Амзя', NULL, NULL),
(3299, 3296, 'Аскино', NULL, NULL),
(3300, 3296, 'Баймак', NULL, NULL),
(3301, 3296, 'Бакалы', NULL, NULL),
(3302, 3296, 'Белебей', NULL, NULL),
(3303, 3296, 'Белорецк', NULL, NULL),
(3304, 3296, 'Бижбуляк', NULL, NULL),
(3305, 3296, 'Бирск', NULL, NULL),
(3306, 3296, 'Благовещенск', NULL, NULL),
(3307, 3296, 'Большеустьикинское', NULL, NULL),
(3308, 3296, 'Верхнеяркеево', NULL, NULL),
(3309, 3296, 'Верхние Киги', NULL, NULL),
(3310, 3296, 'Верхние Татышлы', NULL, NULL),
(3311, 3296, 'Верхний Авзян', NULL, NULL),
(3312, 3296, 'Давлеканово', NULL, NULL),
(3313, 3296, 'Дуван', NULL, NULL),
(3314, 3296, 'Дюртюли', NULL, NULL),
(3315, 3296, 'Ермекеево', NULL, NULL),
(3316, 3296, 'Ермолаево', NULL, NULL),
(3317, 3296, 'Зилаир', NULL, NULL),
(3318, 3296, 'Зирган', NULL, NULL),
(3319, 3296, 'Иглино', NULL, NULL),
(3320, 3296, 'Инзер', NULL, NULL),
(3321, 3296, 'Исянгулово', NULL, NULL),
(3322, 3296, 'Ишимбай', NULL, NULL),
(3323, 3296, 'Кананикольское', NULL, NULL),
(3324, 3296, 'Кандры', NULL, NULL),
(3325, 3296, 'Караидель', NULL, NULL),
(3326, 3296, 'Караидельский', NULL, NULL),
(3327, 3296, 'Киргиз-Мияки', NULL, NULL),
(3328, 3296, 'Красноусольский', NULL, NULL),
(3329, 3296, 'Кумертау', NULL, NULL),
(3330, 3296, 'Кушнаренково', NULL, NULL),
(3331, 3296, 'Малояз', NULL, NULL),
(3332, 3296, 'Мелеуз', NULL, NULL),
(3333, 3296, 'Месягутово', NULL, NULL),
(3334, 3296, 'Мраково', NULL, NULL),
(3335, 3296, 'Нефтекамск', NULL, NULL),
(3336, 3296, 'Октябрьский', NULL, NULL),
(3337, 3296, 'Раевский', NULL, NULL),
(3338, 3296, 'Салават', NULL, NULL),
(3339, 3296, 'Сибай', NULL, NULL),
(3340, 3296, 'Старобалтачево', NULL, NULL),
(3341, 3296, 'Старосубхангулово', NULL, NULL),
(3342, 3296, 'Стерлибашево', NULL, NULL),
(3343, 3296, 'Стерлитамак', NULL, NULL),
(3344, 3296, 'Туймазы', NULL, NULL),
(3345, 3296, 'Уфа', NULL, NULL),
(3346, 3296, 'Учалы', NULL, NULL),
(3347, 3296, 'Федоровка', NULL, NULL),
(3348, 3296, 'Чекмагуш', NULL, NULL),
(3349, 3296, 'Чишмы', NULL, NULL),
(3350, 3296, 'Шаран', NULL, NULL),
(3351, 3296, 'Янаул', NULL, NULL),
(3353, 3352, 'Алексеевка', NULL, NULL),
(3354, 3352, 'Белгород', NULL, NULL),
(3355, 3352, 'Борисовка', NULL, NULL),
(3356, 3352, 'Валуйки', NULL, NULL),
(3357, 3352, 'Вейделевка', NULL, NULL),
(3358, 3352, 'Волоконовка', NULL, NULL),
(3359, 3352, 'Грайворон', NULL, NULL),
(3360, 3352, 'Губкин', NULL, NULL),
(3361, 3352, 'Ивня', NULL, NULL),
(3362, 3352, 'Короча', NULL, NULL),
(3363, 3352, 'Красногвардейское', NULL, NULL),
(3364, 3352, 'Новый Оскол', NULL, NULL),
(3365, 3352, 'Ракитное', NULL, NULL),
(3366, 3352, 'Ровеньки', NULL, NULL),
(3367, 3352, 'Старый Оскол', NULL, NULL),
(3368, 3352, 'Строитель', NULL, NULL),
(3369, 3352, 'Чернянка', NULL, NULL),
(3370, 3352, 'Шебекино', NULL, NULL),
(3372, 3371, 'Алтухово', NULL, NULL),
(3373, 3371, 'Белая Березка', NULL, NULL),
(3374, 3371, 'Белые Берега', NULL, NULL),
(3375, 3371, 'Большое Полпино', NULL, NULL),
(3376, 3371, 'Брянск', NULL, NULL),
(3377, 3371, 'Бытошь', NULL, NULL),
(3378, 3371, 'Выгоничи', NULL, NULL),
(3379, 3371, 'Вышков', NULL, NULL),
(3380, 3371, 'Гордеевка', NULL, NULL),
(3381, 3371, 'Дубровка', NULL, NULL),
(3382, 3371, 'Жирятино', NULL, NULL),
(3383, 3371, 'Жуковка', NULL, NULL),
(3384, 3371, 'Злынка', NULL, NULL),
(3385, 3371, 'Ивот', NULL, NULL),
(3386, 3371, 'Карачев', NULL, NULL),
(3387, 3371, 'Клетня', NULL, NULL),
(3388, 3371, 'Климово', NULL, NULL),
(3389, 3371, 'Клинцы', NULL, NULL),
(3390, 3371, 'Кокаревка', NULL, NULL),
(3391, 3371, 'Комаричи', NULL, NULL),
(3392, 3371, 'Красная Гора', NULL, NULL),
(3393, 3371, 'Локоть', NULL, NULL),
(3394, 3371, 'Мглин', NULL, NULL),
(3395, 3371, 'Навля', NULL, NULL),
(3396, 3371, 'Новозыбков', NULL, NULL),
(3397, 3371, 'Погар', NULL, NULL),
(3398, 3371, 'Почеп', NULL, NULL),
(3399, 3371, 'Ржаница', NULL, NULL),
(3400, 3371, 'Рогнедино', NULL, NULL),
(3401, 3371, 'Севск', NULL, NULL),
(3402, 3371, 'Стародуб', NULL, NULL),
(3403, 3371, 'Суземка', NULL, NULL),
(3404, 3371, 'Сураж', NULL, NULL),
(3405, 3371, 'Трубчевск', NULL, NULL),
(3406, 3371, 'Унеча', NULL, NULL),
(3408, 3407, 'Бабушкин', NULL, NULL),
(3409, 3407, 'Багдарин', NULL, NULL),
(3410, 3407, 'Баргузин', NULL, NULL),
(3411, 3407, 'Баянгол', NULL, NULL),
(3412, 3407, 'Бичура', NULL, NULL),
(3413, 3407, 'Выдрино', NULL, NULL),
(3414, 3407, 'Гусиное Озеро', NULL, NULL),
(3415, 3407, 'Гусиноозерск', NULL, NULL),
(3416, 3407, 'Заиграево', NULL, NULL),
(3417, 3407, 'Закаменск', NULL, NULL),
(3418, 3407, 'Иволгинск', NULL, NULL),
(3419, 3407, 'Илька', NULL, NULL),
(3420, 3407, 'Кабанск', NULL, NULL),
(3421, 3407, 'Каменск', NULL, NULL),
(3422, 3407, 'Кижинга', NULL, NULL),
(3423, 3407, 'Курумкан', NULL, NULL),
(3424, 3407, 'Кырен', NULL, NULL),
(3425, 3407, 'Кяхта', NULL, NULL),
(3426, 3407, 'Монды', NULL, NULL),
(3427, 3407, 'Мухоршибирь', NULL, NULL),
(3428, 3407, 'Нижнеангарск', NULL, NULL),
(3429, 3407, 'Орлик', NULL, NULL),
(3430, 3407, 'Петропавловка', NULL, NULL),
(3431, 3407, 'Романовка', NULL, NULL),
(3432, 3407, 'Селенгинск', NULL, NULL),
(3433, 3407, 'Сосново-Озерское', NULL, NULL),
(3434, 3407, 'Турунтаево', NULL, NULL),
(3435, 3407, 'Улан-Удэ', NULL, NULL),
(3436, 3407, 'Хоринск', NULL, NULL),
(3438, 3437, 'Александров', NULL, NULL),
(3439, 3437, 'Андреево', NULL, NULL),
(3440, 3437, 'Анопино', NULL, NULL),
(3441, 3437, 'Бавлены', NULL, NULL),
(3442, 3437, 'Балакирево', NULL, NULL),
(3443, 3437, 'Боголюбово', NULL, NULL),
(3444, 3437, 'Великодворский', NULL, NULL),
(3445, 3437, 'Вербовский', NULL, NULL),
(3446, 3437, 'Владимир', NULL, NULL),
(3447, 3437, 'Вязники', NULL, NULL),
(3448, 3437, 'Городищи', NULL, NULL),
(3449, 3437, 'Гороховец', NULL, NULL),
(3450, 3437, 'Гусевский', NULL, NULL),
(3451, 3437, 'Гусь Хрустальный', NULL, NULL),
(3452, 3437, 'Золотково', NULL, NULL),
(3453, 3437, 'Иванищи', NULL, NULL),
(3454, 3437, 'Камешково', NULL, NULL),
(3455, 3437, 'Карабаново', NULL, NULL),
(3456, 3437, 'Киржач', NULL, NULL),
(3457, 3437, 'Ковров', NULL, NULL),
(3458, 3437, 'Кольчугино', NULL, NULL),
(3459, 3437, 'Красная Горбатка', NULL, NULL),
(3460, 3437, 'Меленки', NULL, NULL),
(3461, 3437, 'Муром', NULL, NULL),
(3462, 3437, 'Петушки', NULL, NULL),
(3463, 3437, 'Покров', NULL, NULL),
(3464, 3437, 'Собинка', NULL, NULL),
(3465, 3437, 'Судогда', NULL, NULL),
(3466, 3437, 'Суздаль', NULL, NULL),
(3467, 3437, 'Юрьев-Польский', NULL, NULL),
(3469, 3468, 'Алексеевская', NULL, NULL),
(3470, 3468, 'Алущевск', NULL, NULL),
(3471, 3468, 'Быково', NULL, NULL),
(3472, 3468, 'Волгоград', NULL, NULL),
(3473, 3468, 'Волжский', NULL, NULL),
(3474, 3468, 'Городище', NULL, NULL),
(3475, 3468, 'Дубовка', NULL, NULL),
(3476, 3468, 'Елань', NULL, NULL),
(3477, 3468, 'Жирновск', NULL, NULL),
(3478, 3468, 'Иловля', NULL, NULL),
(3479, 3468, 'Калач-на-Дону', NULL, NULL),
(3480, 3468, 'Камышин', NULL, NULL),
(3481, 3468, 'Клетский', NULL, NULL),
(3482, 3468, 'Котельниково', NULL, NULL),
(3483, 3468, 'Котово', NULL, NULL),
(3484, 3468, 'Кумылженская', NULL, NULL),
(3485, 3468, 'Ленинск', NULL, NULL),
(3486, 3468, 'Михайловка', NULL, NULL),
(3487, 3468, 'Нехаевский', NULL, NULL),
(3488, 3468, 'Николаевск', NULL, NULL),
(3489, 3468, 'Новоаннинский', NULL, NULL),
(3490, 3468, 'Новониколаевский', NULL, NULL),
(3491, 3468, 'Ольховка', NULL, NULL),
(3492, 3468, 'Палласовка', NULL, NULL),
(3493, 3468, 'Рудня', NULL, NULL),
(3494, 3468, 'Светлый Яр', NULL, NULL),
(3495, 3468, 'Серафимович', NULL, NULL),
(3496, 3468, 'Средняя Ахтуба', NULL, NULL),
(3497, 3468, 'Сталинград', NULL, NULL),
(3498, 3468, 'Старая Полтавка', NULL, NULL),
(3499, 3468, 'Суровикино', NULL, NULL),
(3500, 3468, 'Урюпинск', NULL, NULL),
(3501, 3468, 'Фролово', NULL, NULL),
(3502, 3468, 'Чернышковский', NULL, NULL),
(3504, 3503, 'Бабаево', NULL, NULL),
(3505, 3503, 'Белозерск', NULL, NULL),
(3506, 3503, 'Великий Устюг', NULL, NULL),
(3507, 3503, 'Верховажье', NULL, NULL),
(3508, 3503, 'Вожега', NULL, NULL),
(3509, 3503, 'Вологда', NULL, NULL),
(3510, 3503, 'Вохтога', NULL, NULL),
(3511, 3503, 'Вытегра', NULL, NULL),
(3512, 3503, 'Грязовец', NULL, NULL),
(3513, 3503, 'Кадников', NULL, NULL),
(3514, 3503, 'Кадуй', NULL, NULL),
(3515, 3503, 'Кичменгский Городок', NULL, NULL),
(3516, 3503, 'Липин Бор', NULL, NULL),
(3517, 3503, 'Никольск', NULL, NULL),
(3518, 3503, 'Нюксеница', NULL, NULL),
(3519, 3503, 'Сокол', NULL, NULL),
(3520, 3503, 'Сямжа', NULL, NULL),
(3521, 3503, 'Тарногский Городок', NULL, NULL),
(3522, 3503, 'Тотьма', NULL, NULL),
(3523, 3503, 'Устюжна', NULL, NULL),
(3524, 3503, 'Харовск', NULL, NULL),
(3525, 3503, 'Чагода', NULL, NULL),
(3526, 3503, 'Череповец', NULL, NULL),
(3527, 3503, 'Шексна', NULL, NULL),
(3528, 3503, 'Шуйское', NULL, NULL),
(3530, 3529, 'Анна', NULL, NULL),
(3531, 3529, 'Бобров', NULL, NULL),
(3532, 3529, 'Богучар', NULL, NULL),
(3533, 3529, 'Борисоглебск', NULL, NULL),
(3534, 3529, 'Бутурлиновка', NULL, NULL),
(3535, 3529, 'Верхний Мамон', NULL, NULL),
(3536, 3529, 'Верхняя Хава', NULL, NULL),
(3537, 3529, 'Воробьевка', NULL, NULL),
(3538, 3529, 'Воронеж', NULL, NULL),
(3539, 3529, 'Лиски', NULL, NULL),
(3540, 3529, 'Грибановский', NULL, NULL),
(3541, 3529, 'Давыдовка', NULL, NULL),
(3542, 3529, 'Елань-Коленовский', NULL, NULL),
(3543, 3529, 'Калач', NULL, NULL),
(3544, 3529, 'Кантемировка', NULL, NULL),
(3545, 3529, 'Нижнедевицк', NULL, NULL),
(3546, 3529, 'Новая Усмань', NULL, NULL),
(3547, 3529, 'Новохоперск', NULL, NULL),
(3548, 3529, 'Ольховатка', NULL, NULL),
(3549, 3529, 'Острогожск', NULL, NULL),
(3550, 3529, 'Павловск', NULL, NULL),
(3551, 3529, 'Панино', NULL, NULL),
(3552, 3529, 'Петропавловка', NULL, NULL),
(3553, 3529, 'Поворино', NULL, NULL),
(3554, 3529, 'Подгоренский', NULL, NULL),
(3555, 3529, 'Рамонь', NULL, NULL),
(3556, 3529, 'Репьевка', NULL, NULL),
(3557, 3529, 'Россошь', NULL, NULL),
(3558, 3529, 'Семилуки', NULL, NULL),
(3559, 3529, 'Таловая', NULL, NULL),
(3560, 3529, 'Терновка', NULL, NULL),
(3561, 3529, 'Хохольский', NULL, NULL),
(3562, 3529, 'Эртиль', NULL, NULL),
(3564, 3563, 'Ардатов', NULL, NULL),
(3565, 3563, 'Арзамас', NULL, NULL),
(3566, 3563, 'Арья', NULL, NULL),
(3567, 3563, 'Балахна', NULL, NULL),
(3568, 3563, 'Богородск', NULL, NULL),
(3569, 3563, 'Большереченск', NULL, NULL),
(3570, 3563, 'Большое Болдино', NULL, NULL),
(3571, 3563, 'Большое Козино', NULL, NULL),
(3572, 3563, 'Большое Мурашкино', NULL, NULL),
(3573, 3563, 'Большое Пикино', NULL, NULL),
(3574, 3563, 'Бор', NULL, NULL),
(3575, 3563, 'Бутурлино', NULL, NULL),
(3576, 3563, 'Вад', NULL, NULL),
(3577, 3563, 'Варнавино', NULL, NULL),
(3578, 3563, 'Васильсурск', NULL, NULL),
(3579, 3563, 'Вахтан', NULL, NULL),
(3580, 3563, 'Вача', NULL, NULL),
(3581, 3563, 'Велетьма', NULL, NULL),
(3582, 3563, 'Ветлуга', NULL, NULL),
(3583, 3563, 'Виля', NULL, NULL),
(3584, 3563, 'Вознесенское', NULL, NULL),
(3585, 3563, 'Володарск', NULL, NULL),
(3586, 3563, 'Воротынец', NULL, NULL),
(3587, 3563, 'Ворсма', NULL, NULL),
(3588, 3563, 'Воскресенское', NULL, NULL),
(3589, 3563, 'Выездное', NULL, NULL),
(3590, 3563, 'Выкса', NULL, NULL),
(3591, 3563, 'Гагино', NULL, NULL),
(3592, 3563, 'Гидроторф', NULL, NULL),
(3593, 3563, 'Горбатов', NULL, NULL),
(3594, 3563, 'Горбатовка', NULL, NULL),
(3595, 3563, 'Городец', NULL, NULL),
(3596, 3563, 'Горький', NULL, NULL),
(3597, 3563, 'Дальнее Константиново', NULL, NULL),
(3598, 3563, 'Дзержинск', NULL, NULL),
(3599, 3563, 'Дивеево', NULL, NULL),
(3600, 3563, 'Досчатое', NULL, NULL),
(3601, 3563, 'Заволжье', NULL, NULL),
(3602, 3563, 'Катунки', NULL, NULL),
(3603, 3563, 'Керженец', NULL, NULL),
(3604, 3563, 'Княгинино', NULL, NULL),
(3605, 3563, 'Ковернино', NULL, NULL),
(3606, 3563, 'Красные Баки', NULL, NULL),
(3607, 3563, 'Кстово', NULL, NULL),
(3608, 3563, 'Кулебаки', NULL, NULL),
(3609, 3563, 'Лукоянов', NULL, NULL),
(3610, 3563, 'Лысково', NULL, NULL),
(3611, 3563, 'Навашино', NULL, NULL),
(3612, 3563, 'Нижний Новгород', NULL, NULL),
(3613, 3563, 'Павлово', NULL, NULL),
(3614, 3563, 'Первомайск', NULL, NULL),
(3615, 3563, 'Перевоз', NULL, NULL),
(3616, 3563, 'Пильна', NULL, NULL),
(3617, 3563, 'Починки', NULL, NULL),
(3618, 3563, 'Сергач', NULL, NULL),
(3619, 3563, 'Сеченово', NULL, NULL),
(3620, 3563, 'Сосновское', NULL, NULL),
(3621, 3563, 'Спасское', NULL, NULL),
(3622, 3563, 'Тонкино', NULL, NULL),
(3623, 3563, 'Тоншаево', NULL, NULL),
(3624, 3563, 'Уразовка', NULL, NULL),
(3625, 3563, 'Урень', NULL, NULL),
(3626, 3563, 'Чкаловск', NULL, NULL),
(3627, 3563, 'Шаранга', NULL, NULL),
(3628, 3563, 'Шатки', NULL, NULL),
(3629, 3563, 'Шахунья', NULL, NULL),
(3631, 3630, 'Агвали', NULL, NULL),
(3632, 3630, 'Акуша', NULL, NULL),
(3633, 3630, 'Ахты', NULL, NULL),
(3634, 3630, 'Ачису', NULL, NULL),
(3635, 3630, 'Бабаюрт', NULL, NULL),
(3636, 3630, 'Бежта', NULL, NULL),
(3637, 3630, 'Ботлих', NULL, NULL),
(3638, 3630, 'Буйнакск', NULL, NULL),
(3639, 3630, 'Вачи', NULL, NULL),
(3640, 3630, 'Гергебиль', NULL, NULL),
(3641, 3630, 'Гуниб', NULL, NULL),
(3642, 3630, 'Дагестанские Огни', NULL, NULL),
(3643, 3630, 'Дербент', NULL, NULL),
(3644, 3630, 'Дылым', NULL, NULL),
(3645, 3630, 'Ершовка', NULL, NULL),
(3646, 3630, 'Избербаш', NULL, NULL),
(3647, 3630, 'Карабудахкент', NULL, NULL),
(3648, 3630, 'Карата', NULL, NULL),
(3649, 3630, 'Каспийск', NULL, NULL),
(3650, 3630, 'Касумкент', NULL, NULL),
(3651, 3630, 'Кизилюрт', NULL, NULL),
(3652, 3630, 'Кизляр', NULL, NULL),
(3653, 3630, 'Кочубей', NULL, NULL),
(3654, 3630, 'Кумух', NULL, NULL),
(3655, 3630, 'Курах', NULL, NULL),
(3656, 3630, 'Магарамкент', NULL, NULL),
(3657, 3630, 'Маджалис', NULL, NULL),
(3658, 3630, 'Махачкала', NULL, NULL),
(3659, 3630, 'Мехельта', NULL, NULL),
(3660, 3630, 'Новолакское', NULL, NULL),
(3661, 3630, 'Рутул', NULL, NULL),
(3662, 3630, 'Советское', NULL, NULL),
(3663, 3630, 'Тарумовка', NULL, NULL),
(3664, 3630, 'Терекли-Мектеб', NULL, NULL),
(3665, 3630, 'Тлярата', NULL, NULL),
(3666, 3630, 'Тпиг', NULL, NULL),
(3667, 3630, 'Уркарах', NULL, NULL),
(3668, 3630, 'Хасавюрт', NULL, NULL),
(3669, 3630, 'Хив', NULL, NULL),
(3670, 3630, 'Хунзах', NULL, NULL),
(3671, 3630, 'Цуриб', NULL, NULL),
(3672, 3630, 'Южно-Сухокумск', NULL, NULL),
(3674, 3673, 'Биробиджан', NULL, NULL),
(3676, 3675, 'Архиповка', NULL, NULL),
(3677, 3675, 'Верхний Ландех', NULL, NULL),
(3678, 3675, 'Вичуга', NULL, NULL),
(3679, 3675, 'Гаврилов Посад', NULL, NULL),
(3680, 3675, 'Долматовский', NULL, NULL),
(3681, 3675, 'Дуляпино', NULL, NULL),
(3682, 3675, 'Заволжск', NULL, NULL),
(3683, 3675, 'Заречный', NULL, NULL),
(3684, 3675, 'Иваново', NULL, NULL),
(3685, 3675, 'Иваньковский', NULL, NULL),
(3686, 3675, 'Ильинское-Хованское', NULL, NULL),
(3687, 3675, 'Каминский', NULL, NULL),
(3688, 3675, 'Кинешма', NULL, NULL),
(3689, 3675, 'Комсомольск', NULL, NULL),
(3690, 3675, 'Лух', NULL, NULL),
(3691, 3675, 'Палех', NULL, NULL),
(3692, 3675, 'Пестяки', NULL, NULL),
(3693, 3675, 'Приволжск', NULL, NULL),
(3694, 3675, 'Пучеж', NULL, NULL),
(3695, 3675, 'Родники', NULL, NULL),
(3696, 3675, 'Савино', NULL, NULL),
(3697, 3675, 'Сокольское', NULL, NULL),
(3698, 3675, 'Тейково', NULL, NULL),
(3699, 3675, 'Фурманов', NULL, NULL),
(3700, 3675, 'Шуя', NULL, NULL),
(3701, 3675, 'Южа', NULL, NULL),
(3702, 3675, 'Юрьевец', NULL, NULL),
(3704, 3703, 'Алексеевск', NULL, NULL),
(3705, 3703, 'Алзамай', NULL, NULL),
(3706, 3703, 'Алыгжер', NULL, NULL),
(3707, 3703, 'Ангарск', NULL, NULL),
(3708, 3703, 'Артемовский', NULL, NULL),
(3709, 3703, 'Атагай', NULL, NULL),
(3710, 3703, 'Байкал', NULL, NULL),
(3711, 3703, 'Байкальск', NULL, NULL),
(3712, 3703, 'Балаганск', NULL, NULL),
(3713, 3703, 'Баяндай', NULL, NULL),
(3714, 3703, 'Бирюсинск', NULL, NULL),
(3715, 3703, 'Бодайбо', NULL, NULL),
(3716, 3703, 'Большая Речка', NULL, NULL),
(3717, 3703, 'Большой Луг', NULL, NULL),
(3718, 3703, 'Бохан', NULL, NULL),
(3719, 3703, 'Братск', NULL, NULL),
(3720, 3703, 'Видим', NULL, NULL),
(3721, 3703, 'Витимский', NULL, NULL),
(3722, 3703, 'Вихоревка', NULL, NULL),
(3723, 3703, 'Еланцы', NULL, NULL),
(3724, 3703, 'Ербогачен', NULL, NULL),
(3725, 3703, 'Железногорск-Илимский', NULL, NULL),
(3726, 3703, 'Жигалово', NULL, NULL),
(3727, 3703, 'Забитуй', NULL, NULL),
(3728, 3703, 'Залари', NULL, NULL),
(3729, 3703, 'Звездный', NULL, NULL),
(3730, 3703, 'Зима', NULL, NULL),
(3731, 3703, 'Иркутск', NULL, NULL),
(3732, 3703, 'Казачинское', NULL, NULL),
(3733, 3703, 'Качуг', NULL, NULL),
(3734, 3703, 'Квиток', NULL, NULL),
(3735, 3703, 'Киренск', NULL, NULL),
(3736, 3703, 'Куйтун', NULL, NULL),
(3737, 3703, 'Кутулик', NULL, NULL),
(3738, 3703, 'Мама', NULL, NULL),
(3739, 3703, 'Нижнеудинск', NULL, NULL),
(3740, 3703, 'Оса', NULL, NULL),
(3741, 3703, 'Слюдянка', NULL, NULL),
(3742, 3703, 'Тайшет', NULL, NULL),
(3743, 3703, 'Тулун', NULL, NULL),
(3744, 3703, 'Усолье-Сибирское', NULL, NULL),
(3745, 3703, 'Усть-Илимск', NULL, NULL),
(3746, 3703, 'Усть-Кут', NULL, NULL),
(3747, 3703, 'Усть-Ордынский', NULL, NULL),
(3748, 3703, 'Усть-Уда', NULL, NULL),
(3749, 3703, 'Черемхово', NULL, NULL),
(3750, 3703, 'Шелехов', NULL, NULL),
(3752, 3751, 'Баксан', NULL, NULL),
(3753, 3751, 'Майский', NULL, NULL),
(3754, 3751, 'Нальчик', NULL, NULL),
(3755, 3751, 'Нарткала', NULL, NULL),
(3756, 3751, 'Прохладный', NULL, NULL),
(3757, 3751, 'Советское', NULL, NULL),
(3758, 3751, 'Терек', NULL, NULL),
(3759, 3751, 'Тырныауз', NULL, NULL),
(3760, 3751, 'Чегем-Первый', NULL, NULL),
(3762, 3761, 'Багратионовск', NULL, NULL),
(3763, 3761, 'Балтийск', NULL, NULL),
(3764, 3761, 'Гвардейск', NULL, NULL),
(3765, 3761, 'Гурьевск', NULL, NULL),
(3766, 3761, 'Гусев', NULL, NULL),
(3767, 3761, 'Железнодорожный', NULL, NULL),
(3768, 3761, 'Зеленоградск', NULL, NULL),
(3769, 3761, 'Знаменск', NULL, NULL),
(3770, 3761, 'Калининград', NULL, NULL),
(3771, 3761, 'Кенисберг', NULL, NULL),
(3772, 3761, 'Краснознаменск', NULL, NULL),
(3773, 3761, 'Мамоново', NULL, NULL),
(3774, 3761, 'Неман', NULL, NULL),
(3775, 3761, 'Нестеров', NULL, NULL),
(3776, 3761, 'Озерск', NULL, NULL),
(3777, 3761, 'Полесск', NULL, NULL),
(3778, 3761, 'Правдинск', NULL, NULL),
(3779, 3761, 'Светлогорск', NULL, NULL),
(3780, 3761, 'Светлый', NULL, NULL),
(3781, 3761, 'Славск', NULL, NULL),
(3782, 3761, 'Советск', NULL, NULL),
(3783, 3761, 'Черняховск', NULL, NULL),
(3785, 3784, 'Андреаполь', NULL, NULL),
(3786, 3784, 'Бежецк', NULL, NULL),
(3787, 3784, 'Белый', NULL, NULL),
(3788, 3784, 'Белый Городок', NULL, NULL),
(3789, 3784, 'Березайка', NULL, NULL),
(3790, 3784, 'Бологое', NULL, NULL),
(3791, 3784, 'Васильевский Мох', NULL, NULL),
(3792, 3784, 'Выползово', NULL, NULL),
(3793, 3784, 'Вышний Волочек', NULL, NULL),
(3794, 3784, 'Жарковский', NULL, NULL),
(3795, 3784, 'Западная Двина', NULL, NULL),
(3796, 3784, 'Заречье', NULL, NULL),
(3797, 3784, 'Зубцов', NULL, NULL),
(3798, 3784, 'Изоплит', NULL, NULL),
(3799, 3784, 'Калашниково', NULL, NULL),
(3800, 3784, 'Калинин', NULL, NULL),
(3801, 3784, 'Калязин', NULL, NULL),
(3802, 3784, 'Кашин', NULL, NULL),
(3803, 3784, 'Кесова Гора', NULL, NULL),
(3804, 3784, 'Кимры', NULL, NULL),
(3805, 3784, 'Конаково', NULL, NULL),
(3806, 3784, 'Красный Холм', NULL, NULL),
(3807, 3784, 'Кувшиново', NULL, NULL),
(3808, 3784, 'Лесное', NULL, NULL),
(3809, 3784, 'Лихославль', NULL, NULL),
(3810, 3784, 'Максатиха', NULL, NULL),
(3811, 3784, 'Молоково', NULL, NULL),
(3812, 3784, 'Нелидово', NULL, NULL),
(3813, 3784, 'Оленино', NULL, NULL),
(3814, 3784, 'Осташков', NULL, NULL),
(3815, 3784, 'Пено', NULL, NULL),
(3816, 3784, 'Рамешки', NULL, NULL),
(3817, 3784, 'Ржев', NULL, NULL),
(3818, 3784, 'Сандово', NULL, NULL),
(3819, 3784, 'Селижарово', NULL, NULL),
(3820, 3784, 'Сонково', NULL, NULL),
(3821, 3784, 'Спирово', NULL, NULL),
(3822, 3784, 'Старица', NULL, NULL),
(3823, 3784, 'Тверь', NULL, NULL),
(3824, 3784, 'Торжок', NULL, NULL),
(3825, 3784, 'Торопец', NULL, NULL),
(3826, 3784, 'Фирово', NULL, NULL),
(3828, 3827, 'Аршань', NULL, NULL),
(3829, 3827, 'Каспийский', NULL, NULL),
(3830, 3827, 'Комсомольский', NULL, NULL),
(3831, 3827, 'Малые Дербеты', NULL, NULL),
(3832, 3827, 'Приютное', NULL, NULL),
(3833, 3827, 'Советское', NULL, NULL),
(3834, 3827, 'Троицкое', NULL, NULL),
(3835, 3827, 'Утта', NULL, NULL),
(3836, 3827, 'Цаган-Аман', NULL, NULL),
(3837, 3827, 'Элиста', NULL, NULL),
(3838, 3827, 'Юста', NULL, NULL),
(3839, 3827, 'Яшалта', NULL, NULL),
(3840, 3827, 'Яшкуль', NULL, NULL),
(3842, 3841, 'Бабынино', NULL, NULL),
(3843, 3841, 'Балабаново', NULL, NULL),
(3844, 3841, 'Барятино', NULL, NULL),
(3845, 3841, 'Белоусово', NULL, NULL),
(3846, 3841, 'Бетлица', NULL, NULL),
(3847, 3841, 'Боровск', NULL, NULL),
(3848, 3841, 'Дугна', NULL, NULL),
(3849, 3841, 'Дудоровский', NULL, NULL),
(3850, 3841, 'Думиничи', NULL, NULL),
(3851, 3841, 'Еленский', NULL, NULL),
(3852, 3841, 'Жиздра', NULL, NULL),
(3853, 3841, 'Износки', NULL, NULL),
(3854, 3841, 'Калуга', NULL, NULL),
(3855, 3841, 'Киров', NULL, NULL),
(3856, 3841, 'Козельск', NULL, NULL),
(3857, 3841, 'Кондрово', NULL, NULL),
(3858, 3841, 'Людиново', NULL, NULL),
(3859, 3841, 'Малоярославец', NULL, NULL),
(3860, 3841, 'Медынь', NULL, NULL),
(3861, 3841, 'Мещовск', NULL, NULL),
(3862, 3841, 'Мосальск', NULL, NULL),
(3863, 3841, 'Обнинск', NULL, NULL),
(3864, 3841, 'Перемышль', NULL, NULL),
(3865, 3841, 'Спас-Деменск', NULL, NULL),
(3866, 3841, 'Сухиничи', NULL, NULL),
(3867, 3841, 'Таруса', NULL, NULL),
(3868, 3841, 'Ульяново', NULL, NULL),
(3869, 3841, 'Ферзиково', NULL, NULL),
(3870, 3841, 'Хвастовичи', NULL, NULL),
(3871, 3841, 'Юхнов', NULL, NULL),
(3873, 3872, 'Атласово', NULL, NULL),
(3874, 3872, 'Аянка', NULL, NULL),
(3875, 3872, 'Большерецк', NULL, NULL),
(3876, 3872, 'Ильпырский', NULL, NULL),
(3877, 3872, 'Каменское', NULL, NULL),
(3878, 3872, 'Кировский', NULL, NULL),
(3879, 3872, 'Ключи', NULL, NULL),
(3880, 3872, 'Крапивная', NULL, NULL),
(3881, 3872, 'Мильково', NULL, NULL),
(3882, 3872, 'Никольское', NULL, NULL),
(3883, 3872, 'Оссора', NULL, NULL),
(3884, 3872, 'Палана', NULL, NULL),
(3885, 3872, 'Парень', NULL, NULL),
(3886, 3872, 'Пахачи', NULL, NULL),
(3887, 3872, 'Петропавловск-Камчатский', NULL, NULL),
(3888, 3872, 'Тигиль', NULL, NULL),
(3889, 3872, 'Тиличики', NULL, NULL),
(3890, 3872, 'Усть-Большерецк', NULL, NULL),
(3891, 3872, 'Усть-Камчатск', NULL, NULL),
(3893, 3892, 'Амбарный', NULL, NULL),
(3894, 3892, 'Беломорск', NULL, NULL),
(3895, 3892, 'Валаам', NULL, NULL),
(3896, 3892, 'Вирандозеро', NULL, NULL),
(3897, 3892, 'Гирвас', NULL, NULL),
(3898, 3892, 'Деревянка', NULL, NULL),
(3899, 3892, 'Идель', NULL, NULL),
(3900, 3892, 'Ильинский', NULL, NULL),
(3901, 3892, 'Импалахти', NULL, NULL),
(3902, 3892, 'Калевала', NULL, NULL),
(3903, 3892, 'Кемь', NULL, NULL),
(3904, 3892, 'Кестеньга', NULL, NULL),
(3905, 3892, 'Кондопога', NULL, NULL),
(3906, 3892, 'Костомукша', NULL, NULL),
(3907, 3892, 'Лахденпохья', NULL, NULL),
(3908, 3892, 'Лоухи', NULL, NULL),
(3909, 3892, 'Медвежьегорск', NULL, NULL),
(3910, 3892, 'Муезерский', NULL, NULL),
(3911, 3892, 'Олонец', NULL, NULL),
(3912, 3892, 'Петрозаводск', NULL, NULL),
(3913, 3892, 'Питкяранта', NULL, NULL),
(3914, 3892, 'Повенец', NULL, NULL),
(3915, 3892, 'Пряжа', NULL, NULL),
(3916, 3892, 'Пудож', NULL, NULL),
(3917, 3892, 'Сегежа', NULL, NULL),
(3918, 3892, 'Сортавала', NULL, NULL),
(3919, 3892, 'Софпорог', NULL, NULL),
(3920, 3892, 'Суоярви', NULL, NULL),
(3922, 3921, 'Анжеро-Судженск', NULL, NULL),
(3923, 3921, 'Барзас', NULL, NULL),
(3924, 3921, 'Белово', NULL, NULL),
(3925, 3921, 'Белогорск', NULL, NULL),
(3926, 3921, 'Березовский', NULL, NULL),
(3927, 3921, 'Грамотеино', NULL, NULL),
(3928, 3921, 'Гурьевск', NULL, NULL),
(3929, 3921, 'Ижморский', NULL, NULL),
(3930, 3921, 'Итатский', NULL, NULL),
(3931, 3921, 'Калтан', NULL, NULL),
(3932, 3921, 'Кедровка', NULL, NULL),
(3933, 3921, 'Кемерово', NULL, NULL),
(3934, 3921, 'Киселевск', NULL, NULL),
(3935, 3921, 'Крапивинский', NULL, NULL),
(3936, 3921, 'Ленинск-Кузнецкий', NULL, NULL),
(3937, 3921, 'Мариинск', NULL, NULL),
(3938, 3921, 'Междуреченск', NULL, NULL),
(3939, 3921, 'Мыски', NULL, NULL),
(3940, 3921, 'Новокузнецк', NULL, NULL),
(3941, 3921, 'Осинники', NULL, NULL),
(3942, 3921, 'Прокопьевск', NULL, NULL),
(3943, 3921, 'Промышленная', NULL, NULL),
(3944, 3921, 'Тайга', NULL, NULL),
(3945, 3921, 'Таштагол', NULL, NULL),
(3946, 3921, 'Тисуль', NULL, NULL),
(3947, 3921, 'Топки', NULL, NULL),
(3948, 3921, 'Тяжинский', NULL, NULL),
(3949, 3921, 'Юрга', NULL, NULL),
(3950, 3921, 'Яшкино', NULL, NULL),
(3951, 3921, 'Яя', NULL, NULL),
(3953, 3952, 'Арбаж', NULL, NULL),
(3954, 3952, 'Аркуль', NULL, NULL),
(3955, 3952, 'Белая Холуница', NULL, NULL),
(3956, 3952, 'Богородское', NULL, NULL),
(3957, 3952, 'Боровой', NULL, NULL),
(3958, 3952, 'Верхошижемье', NULL, NULL),
(3959, 3952, 'Зуевка', NULL, NULL),
(3960, 3952, 'Каринторф', NULL, NULL),
(3961, 3952, 'Кикнур', NULL, NULL),
(3962, 3952, 'Кильмезь', NULL, NULL),
(3963, 3952, 'Киров', NULL, NULL),
(3964, 3952, 'Кирово-Чепецк', NULL, NULL),
(3965, 3952, 'Кирс', NULL, NULL),
(3966, 3952, 'Кобра', NULL, NULL),
(3967, 3952, 'Котельнич', NULL, NULL),
(3968, 3952, 'Кумены', NULL, NULL),
(3969, 3952, 'Ленинское', NULL, NULL),
(3970, 3952, 'Луза', NULL, NULL),
(3971, 3952, 'Малмыж', NULL, NULL),
(3972, 3952, 'Мураши', NULL, NULL),
(3973, 3952, 'Нагорск', NULL, NULL),
(3974, 3952, 'Нема', NULL, NULL),
(3975, 3952, 'Нововятск', NULL, NULL),
(3976, 3952, 'Нолинск', NULL, NULL),
(3977, 3952, 'Омутнинск', NULL, NULL),
(3978, 3952, 'Опарино', NULL, NULL),
(3979, 3952, 'Оричи', NULL, NULL),
(3980, 3952, 'Пижанка', NULL, NULL),
(3981, 3952, 'Подосиновец', NULL, NULL),
(3982, 3952, 'Санчурск', NULL, NULL),
(3983, 3952, 'Свеча', NULL, NULL),
(3984, 3952, 'Слободской', NULL, NULL),
(3985, 3952, 'Советск', NULL, NULL),
(3986, 3952, 'Суна', NULL, NULL),
(3987, 3952, 'Тужа', NULL, NULL),
(3988, 3952, 'Уни', NULL, NULL),
(3989, 3952, 'Уржум', NULL, NULL),
(3990, 3952, 'Фаленки', NULL, NULL),
(3991, 3952, 'Халтурин', NULL, NULL),
(3992, 3952, 'Юрья', NULL, NULL),
(3993, 3952, 'Яранск', NULL, NULL),
(3995, 3994, 'Абезь', NULL, NULL),
(3996, 3994, 'Айкино', NULL, NULL),
(3997, 3994, 'Верхняя Инта', NULL, NULL),
(3998, 3994, 'Визинга', NULL, NULL),
(3999, 3994, 'Водный', NULL, NULL),
(4000, 3994, 'Вожаель', NULL, NULL),
(4001, 3994, 'Воркута', NULL, NULL),
(4002, 3994, 'Вуктыл', NULL, NULL),
(4003, 3994, 'Гешарт', NULL, NULL),
(4004, 3994, 'Елецкий', NULL, NULL),
(4005, 3994, 'Емва', NULL, NULL),
(4006, 3994, 'Заполярный', NULL, NULL),
(4007, 3994, 'Ижма', NULL, NULL),
(4008, 3994, 'Инта', NULL, NULL),
(4009, 3994, 'Ираель', NULL, NULL),
(4010, 3994, 'Каджером', NULL, NULL),
(4011, 3994, 'Кажым', NULL, NULL),
(4012, 3994, 'Кожым', NULL, NULL),
(4013, 3994, 'Койгородок', NULL, NULL),
(4014, 3994, 'Корткерос', NULL, NULL),
(4015, 3994, 'Кослан', NULL, NULL),
(4016, 3994, 'Объячево', NULL, NULL),
(4017, 3994, 'Печора', NULL, NULL),
(4018, 3994, 'Сосногорск', NULL, NULL),
(4019, 3994, 'Сыктывкар', NULL, NULL),
(4020, 3994, 'Троицко-Печерск', NULL, NULL),
(4021, 3994, 'Усинск', NULL, NULL),
(4022, 3994, 'Усогорск', NULL, NULL),
(4023, 3994, 'Усть-Кулом', NULL, NULL),
(4024, 3994, 'Усть-Цильма', NULL, NULL),
(4025, 3994, 'Ухта', NULL, NULL),
(4027, 4026, 'Антропово', NULL, NULL),
(4028, 4026, 'Боговарово', NULL, NULL),
(4029, 4026, 'Буй', NULL, NULL),
(4030, 4026, 'Волгореченск', NULL, NULL),
(4031, 4026, 'Галич', NULL, NULL),
(4032, 4026, 'Горчуха', NULL, NULL),
(4033, 4026, 'Зебляки', NULL, NULL),
(4034, 4026, 'Кадый', NULL, NULL),
(4035, 4026, 'Кологрив', NULL, NULL),
(4036, 4026, 'Кострома', NULL, NULL),
(4037, 4026, 'Красное-на-Волге', NULL, NULL),
(4038, 4026, 'Макарьев', NULL, NULL),
(4039, 4026, 'Мантурово', NULL, NULL),
(4040, 4026, 'Нерехта', NULL, NULL),
(4041, 4026, 'Нея', NULL, NULL),
(4042, 4026, 'Островское', NULL, NULL),
(4043, 4026, 'Павино', NULL, NULL),
(4044, 4026, 'Парфентьево', NULL, NULL),
(4045, 4026, 'Поназырево', NULL, NULL),
(4046, 4026, 'Солигалич', NULL, NULL),
(4047, 4026, 'Судиславль', NULL, NULL),
(4048, 4026, 'Сусанино', NULL, NULL),
(4049, 4026, 'Чухлома', NULL, NULL),
(4050, 4026, 'Шарья', NULL, NULL),
(4051, 4026, 'Шемятино', NULL, NULL),
(4053, 4052, 'Абинск', NULL, NULL),
(4054, 4052, 'Абрау-Дюрсо', NULL, NULL),
(4055, 4052, 'Анапа', NULL, NULL),
(4056, 4052, 'Апшеронск', NULL, NULL),
(4057, 4052, 'Армавир', NULL, NULL),
(4058, 4052, 'Архипо-Осиповка', NULL, NULL),
(4059, 4052, 'Афипский', NULL, NULL),
(4060, 4052, 'Ахтырский', NULL, NULL),
(4061, 4052, 'Ачуево', NULL, NULL),
(4062, 4052, 'Белореченск', NULL, NULL),
(4063, 4052, 'Верхнебаканский', NULL, NULL),
(4064, 4052, 'Выселки', NULL, NULL),
(4065, 4052, 'Геленджик', NULL, NULL),
(4066, 4052, 'Гиагинская', NULL, NULL),
(4067, 4052, 'Горячий Ключ', NULL, NULL),
(4068, 4052, 'Джубга', NULL, NULL),
(4069, 4052, 'Динская', NULL, NULL),
(4070, 4052, 'Ейск', NULL, NULL),
(4071, 4052, 'Ильский', NULL, NULL),
(4072, 4052, 'Кабардинка', NULL, NULL),
(4073, 4052, 'Калинино', NULL, NULL),
(4074, 4052, 'Калининская', NULL, NULL),
(4075, 4052, 'Каменномостский', NULL, NULL),
(4076, 4052, 'Каневская', NULL, NULL),
(4077, 4052, 'Кореновск', NULL, NULL),
(4078, 4052, 'Красноармейская', NULL, NULL),
(4079, 4052, 'Краснодар', NULL, NULL),
(4080, 4052, 'Кропоткин', NULL, NULL),
(4081, 4052, 'Крыловская', NULL, NULL),
(4082, 4052, 'Крымск', NULL, NULL),
(4083, 4052, 'Кущевская', NULL, NULL),
(4084, 4052, 'Лабинск', NULL, NULL),
(4085, 4052, 'Лениградская', NULL, NULL),
(4086, 4052, 'Майкоп', NULL, NULL),
(4087, 4052, 'Мостовской', NULL, NULL),
(4088, 4052, 'Новороссийск', NULL, NULL),
(4089, 4052, 'Отрадная', NULL, NULL),
(4090, 4052, 'Павловская', NULL, NULL),
(4091, 4052, 'Приморско-Ахтарск', NULL, NULL),
(4092, 4052, 'Северская', NULL, NULL),
(4093, 4052, 'Славянск-на-Кубани', NULL, NULL),
(4094, 4052, 'Сочи', NULL, NULL),
(4095, 4052, 'Староминская', NULL, NULL),
(4096, 4052, 'Старощербиновская', NULL, NULL),
(4097, 4052, 'Тбилисская', NULL, NULL),
(4098, 4052, 'Темрюк', NULL, NULL),
(4099, 4052, 'Тимашевск', NULL, NULL),
(4100, 4052, 'Тихорецк', NULL, NULL),
(4101, 4052, 'Туапсе', NULL, NULL),
(4102, 4052, 'Тульский', NULL, NULL),
(4103, 4052, 'Усть-Лабинск', NULL, NULL),
(4104, 4052, 'Шовгеновский', NULL, NULL),
(4106, 4105, 'Абаза', NULL, NULL),
(4107, 4105, 'Абакан', NULL, NULL),
(4108, 4105, 'Абан', NULL, NULL),
(4109, 4105, 'Агинское', NULL, NULL),
(4110, 4105, 'Артемовск', NULL, NULL),
(4111, 4105, 'Аскиз', NULL, NULL),
(4112, 4105, 'Ачинск', NULL, NULL),
(4113, 4105, 'Байкит', NULL, NULL),
(4114, 4105, 'Балахта', NULL, NULL),
(4115, 4105, 'Балыкса', NULL, NULL),
(4116, 4105, 'Белый Яр', NULL, NULL),
(4117, 4105, 'Бельтырский', NULL, NULL),
(4118, 4105, 'Бея', NULL, NULL),
(4119, 4105, 'Бискамжа', NULL, NULL),
(4120, 4105, 'Боготол', NULL, NULL),
(4121, 4105, 'Боград', NULL, NULL),
(4122, 4105, 'Богучаны', NULL, NULL),
(4123, 4105, 'Большая Мурта', NULL, NULL),
(4124, 4105, 'Большой Улуй', NULL, NULL),
(4125, 4105, 'Бородино', NULL, NULL),
(4126, 4105, 'Ванавара', NULL, NULL),
(4127, 4105, 'Верхнеимбатск', NULL, NULL),
(4128, 4105, 'Горячегорск', NULL, NULL),
(4129, 4105, 'Дзержинское', NULL, NULL),
(4130, 4105, 'Дивногорск', NULL, NULL),
(4131, 4105, 'Диксон', NULL, NULL),
(4132, 4105, 'Дудинка', NULL, NULL),
(4133, 4105, 'Емельяново', NULL, NULL),
(4134, 4105, 'Енисейск', NULL, NULL),
(4135, 4105, 'Ермаковское', NULL, NULL),
(4136, 4105, 'Заозерный', NULL, NULL),
(4137, 4105, 'Игарка', NULL, NULL),
(4138, 4105, 'Идринское', NULL, NULL),
(4139, 4105, 'Иланский', NULL, NULL),
(4140, 4105, 'Ирбейское', NULL, NULL),
(4141, 4105, 'Казачинское', NULL, NULL),
(4142, 4105, 'Канск', NULL, NULL),
(4143, 4105, 'Каратузское', NULL, NULL),
(4144, 4105, 'Караул', NULL, NULL),
(4145, 4105, 'Кежма', NULL, NULL),
(4146, 4105, 'Козулька', NULL, NULL),
(4147, 4105, 'Копьево', NULL, NULL),
(4148, 4105, 'Краснотуранск', NULL, NULL),
(4149, 4105, 'Красноярск', NULL, NULL),
(4150, 4105, 'Курагино', NULL, NULL),
(4151, 4105, 'Лесосибирск', NULL, NULL),
(4152, 4105, 'Минусинск', NULL, NULL),
(4153, 4105, 'Мотыгино', NULL, NULL),
(4154, 4105, 'Назарово', NULL, NULL),
(4155, 4105, 'Нижний Ингаш', NULL, NULL),
(4156, 4105, 'Новоселово', NULL, NULL),
(4157, 4105, 'Норильск', NULL, NULL),
(4158, 4105, 'Партизанское', NULL, NULL),
(4159, 4105, 'Пировское', NULL, NULL),
(4160, 4105, 'Саяногорск', NULL, NULL),
(4161, 4105, 'Северо-Енисейский', NULL, NULL),
(4162, 4105, 'Тасеево', NULL, NULL),
(4163, 4105, 'Таштып', NULL, NULL),
(4164, 4105, 'Тура', NULL, NULL),
(4165, 4105, 'Туруханск', NULL, NULL),
(4166, 4105, 'Тюхтет', NULL, NULL),
(4167, 4105, 'Ужур', NULL, NULL),
(4168, 4105, 'Усть-Авам', NULL, NULL),
(4169, 4105, 'Уяр', NULL, NULL),
(4170, 4105, 'Хатанга', NULL, NULL),
(4171, 4105, 'Черемушки', NULL, NULL),
(4172, 4105, 'Черногорск', NULL, NULL),
(4173, 4105, 'Шалинское', NULL, NULL),
(4174, 4105, 'Шира', NULL, NULL),
(4175, 4105, 'Шушенское', NULL, NULL),
(4177, 4176, 'Варгаши', NULL, NULL),
(4178, 4176, 'Глядянское', NULL, NULL),
(4179, 4176, 'Далматово', NULL, NULL),
(4180, 4176, 'Каргаполье', NULL, NULL),
(4181, 4176, 'Катайск', NULL, NULL),
(4182, 4176, 'Кетово', NULL, NULL),
(4183, 4176, 'Курган', NULL, NULL),
(4184, 4176, 'Куртамыш', NULL, NULL),
(4185, 4176, 'Лебяжье', NULL, NULL),
(4186, 4176, 'Макушино', NULL, NULL),
(4187, 4176, 'Мишкино', NULL, NULL),
(4188, 4176, 'Мокроусово', NULL, NULL),
(4189, 4176, 'Петухово', NULL, NULL),
(4190, 4176, 'Половинное', NULL, NULL),
(4191, 4176, 'Сафакулево', NULL, NULL),
(4192, 4176, 'Целинное', NULL, NULL),
(4193, 4176, 'Шадринск', NULL, NULL),
(4194, 4176, 'Шатрово', NULL, NULL),
(4195, 4176, 'Шумиха', NULL, NULL),
(4196, 4176, 'Щучье', NULL, NULL),
(4197, 4176, 'Юргамыш', NULL, NULL),
(4199, 4198, 'Альменево', NULL, NULL),
(4200, 4198, 'Белая', NULL, NULL),
(4201, 4198, 'Большое Солдатское', NULL, NULL),
(4202, 4198, 'Глушково', NULL, NULL),
(4203, 4198, 'Горшечное', NULL, NULL),
(4204, 4198, 'Дмитриев-Льговский', NULL, NULL),
(4205, 4198, 'Железногорск', NULL, NULL),
(4206, 4198, 'Золотухино', NULL, NULL),
(4207, 4198, 'Касторное', NULL, NULL),
(4208, 4198, 'Конышевка', NULL, NULL),
(4209, 4198, 'Коренево', NULL, NULL),
(4210, 4198, 'Курск', NULL, NULL),
(4211, 4198, 'Курчатов', NULL, NULL),
(4212, 4198, 'Кшенский', NULL, NULL),
(4213, 4198, 'Льгов', NULL, NULL),
(4214, 4198, 'Мантурово', NULL, NULL),
(4215, 4198, 'Медвенка', NULL, NULL),
(4216, 4198, 'Обоянь', NULL, NULL),
(4217, 4198, 'Поныри', NULL, NULL),
(4218, 4198, 'Пристень', NULL, NULL),
(4219, 4198, 'Прямицыно', NULL, NULL),
(4220, 4198, 'Рыльск', NULL, NULL),
(4221, 4198, 'Суджа', NULL, NULL),
(4222, 4198, 'Тим', NULL, NULL),
(4223, 4198, 'Фатеж', NULL, NULL),
(4224, 4198, 'Хомутовка', NULL, NULL),
(4225, 4198, 'Черемисиново', NULL, NULL),
(4226, 4198, 'Щигры', NULL, NULL),
(4228, 4227, 'Грязи', NULL, NULL),
(4229, 4227, 'Данхов', NULL, NULL),
(4230, 4227, 'Доброе', NULL, NULL),
(4231, 4227, 'Долгоруково', NULL, NULL),
(4232, 4227, 'Елец', NULL, NULL),
(4233, 4227, 'Задонск', NULL, NULL),
(4234, 4227, 'Измалково', NULL, NULL),
(4235, 4227, 'Казинка', NULL, NULL),
(4236, 4227, 'Лебедянь', NULL, NULL),
(4237, 4227, 'Лев Толстой', NULL, NULL),
(4238, 4227, 'Липецк', NULL, NULL),
(4239, 4227, 'Тербуны', NULL, NULL),
(4240, 4227, 'Усмань', NULL, NULL),
(4241, 4227, 'Хлевное', NULL, NULL),
(4242, 4227, 'Чаплыгин', NULL, NULL),
(4244, 4243, 'Анадырь', NULL, NULL),
(4245, 4243, 'Атка', NULL, NULL),
(4246, 4243, 'Балыгычан', NULL, NULL),
(4247, 4243, 'Беринговский', NULL, NULL),
(4248, 4243, 'Билибино', NULL, NULL),
(4249, 4243, 'Большевик', NULL, NULL),
(4250, 4243, 'Ванкарем', NULL, NULL),
(4251, 4243, 'Иульитин', NULL, NULL),
(4252, 4243, 'Кадыкчан', NULL, NULL),
(4253, 4243, 'Лаврентия', NULL, NULL),
(4254, 4243, 'Магадан', NULL, NULL),
(4255, 4243, 'Мыс Шмидта', NULL, NULL),
(4256, 4243, 'Ола', NULL, NULL),
(4257, 4243, 'Омонск', NULL, NULL),
(4258, 4243, 'Омсукчан', NULL, NULL),
(4259, 4243, 'Палатка', NULL, NULL),
(4260, 4243, 'Певек', NULL, NULL),
(4261, 4243, 'Провидения', NULL, NULL),
(4262, 4243, 'Сеймчан', NULL, NULL),
(4263, 4243, 'Синегорье', NULL, NULL),
(4264, 4243, 'Сусуман', NULL, NULL),
(4265, 4243, 'Усть-Белая', NULL, NULL),
(4266, 4243, 'Усть-Омчуг', NULL, NULL),
(4267, 4243, 'Эвенск', NULL, NULL),
(4268, 4243, 'Эгвекинот', NULL, NULL),
(4269, 4243, 'Ягодное', NULL, NULL),
(4271, 4270, 'Волжск', NULL, NULL),
(4272, 4270, 'Дубовский', NULL, NULL),
(4273, 4270, 'Звенигово', NULL, NULL),
(4274, 4270, 'Йошкар-Ола', NULL, NULL),
(4275, 4270, 'Килемары', NULL, NULL),
(4276, 4270, 'Козьмодемьянск', NULL, NULL),
(4277, 4270, 'Куженер', NULL, NULL),
(4278, 4270, 'Мари-Турек', NULL, NULL),
(4279, 4270, 'Медведево', NULL, NULL),
(4280, 4270, 'Морки', NULL, NULL),
(4281, 4270, 'Новый Торьял', NULL, NULL),
(4282, 4270, 'Оршанка', NULL, NULL),
(4283, 4270, 'Параньга', NULL, NULL),
(4284, 4270, 'Сернур', NULL, NULL),
(4285, 4270, 'Советский', NULL, NULL),
(4286, 4270, 'Юрино', NULL, NULL),
(4288, 4287, 'Ардатов', NULL, NULL),
(4289, 4287, 'Атюрьево', NULL, NULL),
(4290, 4287, 'Атяшево', NULL, NULL),
(4291, 4287, 'Большие Березники', NULL, NULL),
(4292, 4287, 'Большое Игнатово', NULL, NULL),
(4293, 4287, 'Выша', NULL, NULL),
(4294, 4287, 'Ельники', NULL, NULL),
(4295, 4287, 'Зубова Поляна', NULL, NULL),
(4296, 4287, 'Инсар', NULL, NULL),
(4297, 4287, 'Кадошкино', NULL, NULL),
(4298, 4287, 'Кемля', NULL, NULL),
(4299, 4287, 'Ковылкино', NULL, NULL),
(4300, 4287, 'Комсомольский', NULL, NULL),
(4301, 4287, 'Кочкурово', NULL, NULL),
(4302, 4287, 'Краснослободск', NULL, NULL),
(4303, 4287, 'Лямбирь', NULL, NULL),
(4304, 4287, 'Ромоданово', NULL, NULL),
(4305, 4287, 'Рузаевка', NULL, NULL),
(4306, 4287, 'Саранск', NULL, NULL),
(4307, 4287, 'Старое Шайгово', NULL, NULL),
(4308, 4287, 'Темников', NULL, NULL),
(4309, 4287, 'Теньгушево', NULL, NULL),
(4310, 4287, 'Торбеево', NULL, NULL),
(4311, 4287, 'Чамзинка', NULL, NULL),
(4313, 4312, 'Абрамцево', NULL, NULL),
(4314, 4312, 'Алабино', NULL, NULL),
(4315, 4312, 'Апрелевка', NULL, NULL),
(4316, 4312, 'Архангельское', NULL, NULL),
(4317, 4312, 'Ашитково', NULL, NULL),
(4318, 4312, 'Бакшеево', NULL, NULL),
(4319, 4312, 'Балашиха', NULL, NULL),
(4320, 4312, 'Барыбино', NULL, NULL),
(4321, 4312, 'Белоомут', NULL, NULL),
(4322, 4312, 'Белые Столбы', NULL, NULL),
(4323, 4312, 'Бородино', NULL, NULL),
(4324, 4312, 'Бронницы', NULL, NULL),
(4325, 4312, 'Быково', NULL, NULL),
(4326, 4312, 'Валуево', NULL, NULL),
(4327, 4312, 'Вербилки', NULL, NULL),
(4328, 4312, 'Верея', NULL, NULL),
(4329, 4312, 'Видное', NULL, NULL),
(4330, 4312, 'Внуково', NULL, NULL),
(4331, 4312, 'Вождь Пролетариата', NULL, NULL),
(4332, 4312, 'Волоколамск', NULL, NULL),
(4333, 4312, 'Вороново', NULL, NULL),
(4334, 4312, 'Воскресенск', NULL, NULL),
(4335, 4312, 'Восточный', NULL, NULL),
(4336, 4312, 'Востряково', NULL, NULL),
(4337, 4312, 'Высоковск', NULL, NULL),
(4338, 4312, 'Голицино', NULL, NULL),
(4339, 4312, 'Деденево', NULL, NULL),
(4340, 4312, 'Дедовск', NULL, NULL),
(4341, 4312, 'Джержинский', NULL, NULL),
(4342, 4312, 'Дмитров', NULL, NULL),
(4343, 4312, 'Долгопрудный', NULL, NULL),
(4344, 4312, 'Домодедово', NULL, NULL),
(4345, 4312, 'Дорохово', NULL, NULL),
(4346, 4312, 'Дрезна', NULL, NULL),
(4347, 4312, 'Дубки', NULL, NULL),
(4348, 4312, 'Дубна', NULL, NULL),
(4349, 4312, 'Егорьевск', NULL, NULL),
(4350, 4312, 'Железнодорожный', NULL, NULL),
(4351, 4312, 'Жилево', NULL, NULL),
(4352, 4312, 'Жуковский', NULL, NULL),
(4353, 4312, 'Загорск', NULL, NULL),
(4354, 4312, 'Загорянский', NULL, NULL),
(4355, 4312, 'Запрудная', NULL, NULL),
(4356, 4312, 'Зарайск', NULL, NULL),
(4357, 4312, 'Звенигород', NULL, NULL),
(4358, 4312, 'Зеленоград', NULL, NULL),
(4359, 4312, 'Ивантеевка', NULL, NULL),
(4360, 4312, 'Икша', NULL, NULL),
(4361, 4312, 'Ильинский', NULL, NULL),
(4362, 4312, 'Истра', NULL, NULL),
(4363, 4312, 'Калининград', NULL, NULL),
(4364, 4312, 'Кашира', NULL, NULL),
(4365, 4312, 'Керва', NULL, NULL),
(4366, 4312, 'Климовск', NULL, NULL),
(4367, 4312, 'Клин', NULL, NULL),
(4368, 4312, 'Клязьма', NULL, NULL),
(4369, 4312, 'Кожино', NULL, NULL),
(4370, 4312, 'Кокошкино', NULL, NULL),
(4371, 4312, 'Коломна', NULL, NULL),
(4372, 4312, 'Колюбакино', NULL, NULL),
(4373, 4312, 'Косино', NULL, NULL),
(4374, 4312, 'Котельники', NULL, NULL),
(4375, 4312, 'Красково', NULL, NULL),
(4376, 4312, 'Красноармейск', NULL, NULL),
(4377, 4312, 'Красногорск', NULL, NULL),
(4378, 4312, 'Краснозаводск', NULL, NULL),
(4379, 4312, 'Красный Ткач', NULL, NULL),
(4380, 4312, 'Крюково', NULL, NULL),
(4381, 4312, 'Кубинка', NULL, NULL),
(4382, 4312, 'Купавна', NULL, NULL),
(4383, 4312, 'Куровское', NULL, NULL),
(4384, 4312, 'Лесной Городок', NULL, NULL),
(4385, 4312, 'Ликино-Дулево', NULL, NULL),
(4386, 4312, 'Лобня', NULL, NULL),
(4387, 4312, 'Лопатинский', NULL, NULL),
(4388, 4312, 'Лосино-Петровский', NULL, NULL),
(4389, 4312, 'Лотошино', NULL, NULL),
(4390, 4312, 'Лукино', NULL, NULL),
(4391, 4312, 'Луховицы', NULL, NULL),
(4392, 4312, 'Лыткарино', NULL, NULL),
(4393, 4312, 'Львовский', NULL, NULL),
(4394, 4312, 'Люберцы', NULL, NULL),
(4395, 4312, 'Малаховка', NULL, NULL),
(4396, 4312, 'Михайловское', NULL, NULL),
(4397, 4312, 'Михнево', NULL, NULL),
(4398, 4312, 'Можайск', NULL, NULL),
(4399, 4312, 'Монино', NULL, NULL),
(4400, 4312, 'Москва', NULL, NULL),
(4401, 4312, 'Муханово', NULL, NULL),
(4402, 4312, 'Мытищи', NULL, NULL),
(4403, 4312, 'Нарофоминск', NULL, NULL),
(4404, 4312, 'Нахабино', NULL, NULL),
(4405, 4312, 'Некрасовка', NULL, NULL),
(4406, 4312, 'Немчиновка', NULL, NULL),
(4407, 4312, 'Новобратцевский', NULL, NULL),
(4408, 4312, 'Новоподрезково', NULL, NULL),
(4409, 4312, 'Ногинск', NULL, NULL),
(4410, 4312, 'Обухово', NULL, NULL),
(4411, 4312, 'Одинцово', NULL, NULL),
(4412, 4312, 'Ожерелье', NULL, NULL),
(4413, 4312, 'Озеры', NULL, NULL),
(4414, 4312, 'Октябрьский', NULL, NULL),
(4415, 4312, 'Опалиха', NULL, NULL),
(4416, 4312, 'Орехово-Зуево', NULL, NULL),
(4417, 4312, 'Павловский Посад', NULL, NULL),
(4418, 4312, 'Первомайский', NULL, NULL),
(4419, 4312, 'Пески', NULL, NULL),
(4420, 4312, 'Пироговский', NULL, NULL),
(4421, 4312, 'Подольск', NULL, NULL),
(4422, 4312, 'Полушкино', NULL, NULL),
(4423, 4312, 'Правдинский', NULL, NULL),
(4424, 4312, 'Привокзальный', NULL, NULL),
(4425, 4312, 'Пролетарский', NULL, NULL),
(4426, 4312, 'Пушкино', NULL, NULL),
(4427, 4312, 'Пущино', NULL, NULL),
(4428, 4312, 'Радовицкий', NULL, NULL),
(4429, 4312, 'Раменское', NULL, NULL),
(4430, 4312, 'Реутов', NULL, NULL),
(4431, 4312, 'Решетниково', NULL, NULL),
(4432, 4312, 'Родники', NULL, NULL),
(4433, 4312, 'Рошаль', NULL, NULL),
(4434, 4312, 'Рублево', NULL, NULL),
(4435, 4312, 'Руза', NULL, NULL),
(4436, 4312, 'Салтыковка', NULL, NULL),
(4437, 4312, 'Северный', NULL, NULL),
(4438, 4312, 'Сергиев Посад', NULL, NULL),
(4439, 4312, 'Серебряные Пруды', NULL, NULL),
(4440, 4312, 'Серпухов', NULL, NULL),
(4441, 4312, 'Солнечногорск', NULL, NULL),
(4442, 4312, 'Солнцево', NULL, NULL),
(4443, 4312, 'Софрино', NULL, NULL),
(4444, 4312, 'Старая Купавна', NULL, NULL),
(4445, 4312, 'Старбеево', NULL, NULL),
(4446, 4312, 'Ступино', NULL, NULL),
(4447, 4312, 'Сходня', NULL, NULL),
(4448, 4312, 'Талдом', NULL, NULL),
(4449, 4312, 'Текстильщик', NULL, NULL),
(4450, 4312, 'Темпы', NULL, NULL),
(4451, 4312, 'Тишково', NULL, NULL),
(4452, 4312, 'Томилино', NULL, NULL),
(4453, 4312, 'Троицк', NULL, NULL),
(4454, 4312, 'Туголесский Бор', NULL, NULL),
(4455, 4312, 'Тучково', NULL, NULL),
(4456, 4312, 'Уваровка', NULL, NULL),
(4457, 4312, 'Удельная', NULL, NULL),
(4458, 4312, 'Успенское', NULL, NULL),
(4459, 4312, 'Фирсановка', NULL, NULL),
(4460, 4312, 'Фосфоритный', NULL, NULL),
(4461, 4312, 'Фрязино', NULL, NULL),
(4462, 4312, 'Фряново', NULL, NULL),
(4463, 4312, 'Химки', NULL, NULL),
(4464, 4312, 'Хорлово', NULL, NULL),
(4465, 4312, 'Хотьково', NULL, NULL),
(4466, 4312, 'Черкизово', NULL, NULL),
(4467, 4312, 'Черноголовка', NULL, NULL),
(4468, 4312, 'Черусти', NULL, NULL),
(4469, 4312, 'Чехов', NULL, NULL),
(4470, 4312, 'Шарапово', NULL, NULL),
(4471, 4312, 'Шатура', NULL, NULL),
(4472, 4312, 'Шатурторф', NULL, NULL),
(4473, 4312, 'Шаховская', NULL, NULL),
(4474, 4312, 'Шереметьевский', NULL, NULL),
(4475, 4312, 'Щелково', NULL, NULL),
(4476, 4312, 'Щербинка', NULL, NULL),
(4477, 4312, 'Электрогорск', NULL, NULL),
(4478, 4312, 'Электросталь', NULL, NULL),
(4479, 4312, 'Электроугли', NULL, NULL),
(4480, 4312, 'Яхрома', NULL, NULL),
(4482, 4481, 'Апатиты', NULL, NULL),
(4483, 4481, 'Африканда', NULL, NULL),
(4484, 4481, 'Верхнетуломский', NULL, NULL),
(4485, 4481, 'Заполярный', NULL, NULL),
(4486, 4481, 'Зареченск', NULL, NULL),
(4487, 4481, 'Зашеек', NULL, NULL),
(4488, 4481, 'Зеленоборский', NULL, NULL),
(4489, 4481, 'Кандалакша', NULL, NULL),
(4490, 4481, 'Кильдинстрой', NULL, NULL),
(4491, 4481, 'Кировск', NULL, NULL),
(4492, 4481, 'Ковдор', NULL, NULL),
(4493, 4481, 'Кола', NULL, NULL),
(4494, 4481, 'Конда', NULL, NULL),
(4495, 4481, 'Мончегорск', NULL, NULL),
(4496, 4481, 'Мурманск', NULL, NULL),
(4497, 4481, 'Мурмаши', NULL, NULL),
(4498, 4481, 'Никель', NULL, NULL),
(4499, 4481, 'Оленегорск', NULL, NULL),
(4500, 4481, 'Полярный', NULL, NULL),
(4501, 4481, 'Североморск', NULL, NULL),
(4502, 4481, 'Умба', NULL, NULL),
(4504, 4503, 'Анциферово', NULL, NULL),
(4505, 4503, 'Батецкий', NULL, NULL),
(4506, 4503, 'Большая Вишера', NULL, NULL),
(4507, 4503, 'Боровичи', NULL, NULL),
(4508, 4503, 'Валдай', NULL, NULL),
(4509, 4503, 'Волот', NULL, NULL),
(4510, 4503, 'Деманск', NULL, NULL),
(4511, 4503, 'Зарубино', NULL, NULL),
(4512, 4503, 'Кресцы', NULL, NULL),
(4513, 4503, 'Любытино', NULL, NULL),
(4514, 4503, 'Малая Вишера', NULL, NULL),
(4515, 4503, 'Марево', NULL, NULL),
(4516, 4503, 'Мошенское', NULL, NULL),
(4517, 4503, 'Новгород', NULL, NULL),
(4518, 4503, 'Окуловка', NULL, NULL),
(4519, 4503, 'Парфино', NULL, NULL),
(4520, 4503, 'Пестово', NULL, NULL),
(4521, 4503, 'Поддорье', NULL, NULL),
(4522, 4503, 'Сольцы', NULL, NULL),
(4523, 4503, 'Старая Русса', NULL, NULL),
(4524, 4503, 'Хвойное', NULL, NULL),
(4525, 4503, 'Холм', NULL, NULL),
(4526, 4503, 'Чудово', NULL, NULL),
(4527, 4503, 'Шимск', NULL, NULL),
(4529, 4528, 'Баган', NULL, NULL),
(4530, 4528, 'Барабинск', NULL, NULL),
(4531, 4528, 'Бердск', NULL, NULL),
(4532, 4528, 'Биаза', NULL, NULL),
(4533, 4528, 'Болотное', NULL, NULL),
(4534, 4528, 'Венгерово', NULL, NULL),
(4535, 4528, 'Довольное', NULL, NULL),
(4536, 4528, 'Завьялово', NULL, NULL),
(4537, 4528, 'Искитим', NULL, NULL),
(4538, 4528, 'Карасук', NULL, NULL),
(4539, 4528, 'Каргат', NULL, NULL),
(4540, 4528, 'Колывань', NULL, NULL),
(4541, 4528, 'Краснозерское', NULL, NULL),
(4542, 4528, 'Крутиха', NULL, NULL),
(4543, 4528, 'Куйбышев', NULL, NULL),
(4544, 4528, 'Купино', NULL, NULL),
(4545, 4528, 'Кыштовка', NULL, NULL),
(4546, 4528, 'Маслянино', NULL, NULL),
(4547, 4528, 'Михайловский', NULL, NULL),
(4548, 4528, 'Мошково', NULL, NULL),
(4549, 4528, 'Новосибирск', NULL, NULL),
(4550, 4528, 'Ордынское', NULL, NULL),
(4551, 4528, 'Северное', NULL, NULL),
(4552, 4528, 'Сузун', NULL, NULL),
(4553, 4528, 'Татарск', NULL, NULL),
(4554, 4528, 'Тогучин', NULL, NULL),
(4555, 4528, 'Убинское', NULL, NULL),
(4556, 4528, 'Усть-Тарка', NULL, NULL),
(4557, 4528, 'Чаны', NULL, NULL),
(4558, 4528, 'Черепаново', NULL, NULL),
(4559, 4528, 'Чистоозерное', NULL, NULL),
(4560, 4528, 'Чулым', NULL, NULL),
(4562, 4561, 'Береговой', NULL, NULL),
(4563, 4561, 'Большеречье', NULL, NULL),
(4564, 4561, 'Большие Уки', NULL, NULL),
(4565, 4561, 'Горьковское', NULL, NULL),
(4566, 4561, 'Знаменское', NULL, NULL),
(4567, 4561, 'Исилькуль', NULL, NULL),
(4568, 4561, 'Калачинск', NULL, NULL),
(4569, 4561, 'Колосовка', NULL, NULL),
(4570, 4561, 'Кормиловка', NULL, NULL);
INSERT INTO `tbl_cities` (`id`, `region_id`, `name`, `updated_at`, `created_at`) VALUES
(4571, 4561, 'Крутинка', NULL, NULL),
(4572, 4561, 'Любинский', NULL, NULL),
(4573, 4561, 'Марьяновка', NULL, NULL),
(4574, 4561, 'Муромцево', NULL, NULL),
(4575, 4561, 'Называевск', NULL, NULL),
(4576, 4561, 'Нижняя Омка', NULL, NULL),
(4577, 4561, 'Нововаршавка', NULL, NULL),
(4578, 4561, 'Одесское', NULL, NULL),
(4579, 4561, 'Оконешниково', NULL, NULL),
(4580, 4561, 'Омск', NULL, NULL),
(4581, 4561, 'Павлоградка', NULL, NULL),
(4582, 4561, 'Полтавка', NULL, NULL),
(4583, 4561, 'Русская Поляна', NULL, NULL),
(4584, 4561, 'Саргатское', NULL, NULL),
(4585, 4561, 'Седельниково', NULL, NULL),
(4586, 4561, 'Таврическое', NULL, NULL),
(4587, 4561, 'Тара', NULL, NULL),
(4588, 4561, 'Тевриз', NULL, NULL),
(4589, 4561, 'Тюкалинск', NULL, NULL),
(4590, 4561, 'Усть-Ишим', NULL, NULL),
(4591, 4561, 'Черлак', NULL, NULL),
(4592, 4561, 'Шербакуль', NULL, NULL),
(4594, 4593, 'Абдулино', NULL, NULL),
(4595, 4593, 'Адамовка', NULL, NULL),
(4596, 4593, 'Айдырлинский', NULL, NULL),
(4597, 4593, 'Акбулак', NULL, NULL),
(4598, 4593, 'Аккермановка', NULL, NULL),
(4599, 4593, 'Асекеево', NULL, NULL),
(4600, 4593, 'Беляевка', NULL, NULL),
(4601, 4593, 'Бугуруслан', NULL, NULL),
(4602, 4593, 'Бузулук', NULL, NULL),
(4603, 4593, 'Гай', NULL, NULL),
(4604, 4593, 'Грачевка', NULL, NULL),
(4605, 4593, 'Домбаровский', NULL, NULL),
(4606, 4593, 'Дубенский', NULL, NULL),
(4607, 4593, 'Илек', NULL, NULL),
(4608, 4593, 'Ириклинский', NULL, NULL),
(4609, 4593, 'Кувандык', NULL, NULL),
(4610, 4593, 'Курманаевка', NULL, NULL),
(4611, 4593, 'Матвеевка', NULL, NULL),
(4612, 4593, 'Медногорск', NULL, NULL),
(4613, 4593, 'Новоорск', NULL, NULL),
(4614, 4593, 'Новосергиевка', NULL, NULL),
(4615, 4593, 'Новотроицк', NULL, NULL),
(4616, 4593, 'Октябрьское', NULL, NULL),
(4617, 4593, 'Оренбург', NULL, NULL),
(4618, 4593, 'Орск', NULL, NULL),
(4619, 4593, 'Первомайский', NULL, NULL),
(4620, 4593, 'Переволоцкий', NULL, NULL),
(4621, 4593, 'Пономаревка', NULL, NULL),
(4622, 4593, 'Саракташ', NULL, NULL),
(4623, 4593, 'Светлый', NULL, NULL),
(4624, 4593, 'Северное', NULL, NULL),
(4625, 4593, 'Соль-Илецк', NULL, NULL),
(4626, 4593, 'Сорочинск', NULL, NULL),
(4627, 4593, 'Ташла', NULL, NULL),
(4628, 4593, 'Тоцкое', NULL, NULL),
(4629, 4593, 'Тюльган', NULL, NULL),
(4630, 4593, 'Шарлык', NULL, NULL),
(4631, 4593, 'Энергетик', NULL, NULL),
(4632, 4593, 'Ясный', NULL, NULL),
(4634, 4633, 'Болхов', NULL, NULL),
(4635, 4633, 'Верховье', NULL, NULL),
(4636, 4633, 'Глазуновка', NULL, NULL),
(4637, 4633, 'Дмитровск-Орловский', NULL, NULL),
(4638, 4633, 'Долгое', NULL, NULL),
(4639, 4633, 'Залегощь', NULL, NULL),
(4640, 4633, 'Змиевка', NULL, NULL),
(4641, 4633, 'Знаменское', NULL, NULL),
(4642, 4633, 'Колпны', NULL, NULL),
(4643, 4633, 'Красная Заря', NULL, NULL),
(4644, 4633, 'Кромы', NULL, NULL),
(4645, 4633, 'Ливны', NULL, NULL),
(4646, 4633, 'Малоархангельск', NULL, NULL),
(4647, 4633, 'Мценск', NULL, NULL),
(4648, 4633, 'Нарышкино', NULL, NULL),
(4649, 4633, 'Новосиль', NULL, NULL),
(4650, 4633, 'Орел', NULL, NULL),
(4651, 4633, 'Покровское', NULL, NULL),
(4652, 4633, 'Сосково', NULL, NULL),
(4653, 4633, 'Тросна', NULL, NULL),
(4654, 4633, 'Хомутово', NULL, NULL),
(4655, 4633, 'Хотынец', NULL, NULL),
(4656, 4633, 'Шаблыкино', NULL, NULL),
(4658, 4657, 'Башмаково', NULL, NULL),
(4659, 4657, 'Беднодемьяновск', NULL, NULL),
(4660, 4657, 'Беково', NULL, NULL),
(4661, 4657, 'Белинский', NULL, NULL),
(4662, 4657, 'Бессоновка', NULL, NULL),
(4663, 4657, 'Вадинск', NULL, NULL),
(4664, 4657, 'Верхозим', NULL, NULL),
(4665, 4657, 'Городище', NULL, NULL),
(4666, 4657, 'Евлашево', NULL, NULL),
(4667, 4657, 'Земетчино', NULL, NULL),
(4668, 4657, 'Золотаревка', NULL, NULL),
(4669, 4657, 'Исса', NULL, NULL),
(4670, 4657, 'Каменка', NULL, NULL),
(4671, 4657, 'Колышлей', NULL, NULL),
(4672, 4657, 'Кондоль', NULL, NULL),
(4673, 4657, 'Кузнецк', NULL, NULL),
(4674, 4657, 'Лопатино', NULL, NULL),
(4675, 4657, 'Малая Сердоба', NULL, NULL),
(4676, 4657, 'Мокшан', NULL, NULL),
(4677, 4657, 'Наровчат', NULL, NULL),
(4678, 4657, 'Неверкино', NULL, NULL),
(4679, 4657, 'Нижний Ломов', NULL, NULL),
(4680, 4657, 'Никольск', NULL, NULL),
(4681, 4657, 'Пачелма', NULL, NULL),
(4682, 4657, 'Пенза', NULL, NULL),
(4683, 4657, 'Русский Камешкир', NULL, NULL),
(4684, 4657, 'Сердобск', NULL, NULL),
(4685, 4657, 'Сосновоборск', NULL, NULL),
(4686, 4657, 'Сура', NULL, NULL),
(4687, 4657, 'Тамала', NULL, NULL),
(4688, 4657, 'Шемышейка', NULL, NULL),
(4690, 4689, 'Барда', NULL, NULL),
(4691, 4689, 'Березники', NULL, NULL),
(4692, 4689, 'Большая Соснова', NULL, NULL),
(4693, 4689, 'Верещагино', NULL, NULL),
(4694, 4689, 'Гайны', NULL, NULL),
(4695, 4689, 'Горнозаводск', NULL, NULL),
(4696, 4689, 'Гремячинск', NULL, NULL),
(4697, 4689, 'Губаха', NULL, NULL),
(4698, 4689, 'Добрянка', NULL, NULL),
(4699, 4689, 'Елово', NULL, NULL),
(4700, 4689, 'Зюкайка', NULL, NULL),
(4701, 4689, 'Ильинский', NULL, NULL),
(4702, 4689, 'Карагай', NULL, NULL),
(4703, 4689, 'Керчевский', NULL, NULL),
(4704, 4689, 'Кизел', NULL, NULL),
(4705, 4689, 'Коса', NULL, NULL),
(4706, 4689, 'Кочево', NULL, NULL),
(4707, 4689, 'Красновишерск', NULL, NULL),
(4708, 4689, 'Краснокамск', NULL, NULL),
(4709, 4689, 'Кудымкар', NULL, NULL),
(4710, 4689, 'Куеда', NULL, NULL),
(4711, 4689, 'Кунгур', NULL, NULL),
(4712, 4689, 'Лысьва', NULL, NULL),
(4713, 4689, 'Ныроб', NULL, NULL),
(4714, 4689, 'Нытва', NULL, NULL),
(4715, 4689, 'Октябрьский', NULL, NULL),
(4716, 4689, 'Орда', NULL, NULL),
(4717, 4689, 'Оса', NULL, NULL),
(4718, 4689, 'Оханск', NULL, NULL),
(4719, 4689, 'Очер', NULL, NULL),
(4720, 4689, 'Пермь', NULL, NULL),
(4721, 4689, 'Соликамск', NULL, NULL),
(4722, 4689, 'Суксун', NULL, NULL),
(4723, 4689, 'Уинское', NULL, NULL),
(4724, 4689, 'Усолье', NULL, NULL),
(4725, 4689, 'Усть-Кишерть', NULL, NULL),
(4726, 4689, 'Чайковский', NULL, NULL),
(4727, 4689, 'Частые', NULL, NULL),
(4728, 4689, 'Чердынь', NULL, NULL),
(4729, 4689, 'Чернореченский', NULL, NULL),
(4730, 4689, 'Чернушка', NULL, NULL),
(4731, 4689, 'Чусовой', NULL, NULL),
(4732, 4689, 'Юрла', NULL, NULL),
(4733, 4689, 'Юсьва', NULL, NULL),
(4735, 4734, 'Анучино', NULL, NULL),
(4736, 4734, 'Арсеньев', NULL, NULL),
(4737, 4734, 'Артем', NULL, NULL),
(4738, 4734, 'Артемовский', NULL, NULL),
(4739, 4734, 'Большой Камень', NULL, NULL),
(4740, 4734, 'Валентин', NULL, NULL),
(4741, 4734, 'Владивосток', NULL, NULL),
(4742, 4734, 'Высокогорск', NULL, NULL),
(4743, 4734, 'Горные Ключи', NULL, NULL),
(4744, 4734, 'Горный', NULL, NULL),
(4745, 4734, 'Дальнегорск', NULL, NULL),
(4746, 4734, 'Дальнереченск', NULL, NULL),
(4747, 4734, 'Зарубино', NULL, NULL),
(4748, 4734, 'Кавалерово', NULL, NULL),
(4749, 4734, 'Каменка', NULL, NULL),
(4750, 4734, 'Камень-Рыболов', NULL, NULL),
(4751, 4734, 'Кировский', NULL, NULL),
(4752, 4734, 'Лазо', NULL, NULL),
(4753, 4734, 'Лесозаводск', NULL, NULL),
(4754, 4734, 'Лучегорск', NULL, NULL),
(4755, 4734, 'Михайловка', NULL, NULL),
(4756, 4734, 'Находка', NULL, NULL),
(4757, 4734, 'Новопокровка', NULL, NULL),
(4758, 4734, 'Ольга', NULL, NULL),
(4759, 4734, 'Партизанск', NULL, NULL),
(4760, 4734, 'Пограничный', NULL, NULL),
(4761, 4734, 'Покровка', NULL, NULL),
(4762, 4734, 'Русский', NULL, NULL),
(4763, 4734, 'Самарга', NULL, NULL),
(4764, 4734, 'Славянка', NULL, NULL),
(4765, 4734, 'Спасск-Дальний', NULL, NULL),
(4766, 4734, 'Терней', NULL, NULL),
(4767, 4734, 'Уссурийск', NULL, NULL),
(4768, 4734, 'Хасан', NULL, NULL),
(4769, 4734, 'Хороль', NULL, NULL),
(4770, 4734, 'Черниговка', NULL, NULL),
(4771, 4734, 'Чугуевка', NULL, NULL),
(4772, 4734, 'Яковлевка', NULL, NULL),
(4774, 4773, 'Бежаницы', NULL, NULL),
(4775, 4773, 'Великие Луки', NULL, NULL),
(4776, 4773, 'Гдов', NULL, NULL),
(4777, 4773, 'Дедовичи', NULL, NULL),
(4778, 4773, 'Дно', NULL, NULL),
(4779, 4773, 'Заплюсье', NULL, NULL),
(4780, 4773, 'Идрица', NULL, NULL),
(4781, 4773, 'Красногородское', NULL, NULL),
(4782, 4773, 'Кунья', NULL, NULL),
(4783, 4773, 'Локня', NULL, NULL),
(4784, 4773, 'Невель', NULL, NULL),
(4785, 4773, 'Новоржев', NULL, NULL),
(4786, 4773, 'Новосокольники', NULL, NULL),
(4787, 4773, 'Опочка', NULL, NULL),
(4788, 4773, 'Остров', NULL, NULL),
(4789, 4773, 'Палкино', NULL, NULL),
(4790, 4773, 'Печоры', NULL, NULL),
(4791, 4773, 'Плюсса', NULL, NULL),
(4792, 4773, 'Порхов', NULL, NULL),
(4793, 4773, 'Псков', NULL, NULL),
(4794, 4773, 'Пустошка', NULL, NULL),
(4795, 4773, 'Пушкинские Горы', NULL, NULL),
(4796, 4773, 'Пыталово', NULL, NULL),
(4797, 4773, 'Себеж', NULL, NULL),
(4798, 4773, 'Струги-Красные', NULL, NULL),
(4799, 4773, 'Усвяты', NULL, NULL),
(4801, 4800, 'Азов', NULL, NULL),
(4802, 4800, 'Аксай', NULL, NULL),
(4803, 4800, 'Алмазный', NULL, NULL),
(4804, 4800, 'Аютинск', NULL, NULL),
(4805, 4800, 'Багаевский', NULL, NULL),
(4806, 4800, 'Батайск', NULL, NULL),
(4807, 4800, 'Белая Калитва', NULL, NULL),
(4808, 4800, 'Боковская', NULL, NULL),
(4809, 4800, 'Большая Мартыновка', NULL, NULL),
(4810, 4800, 'Вешенская', NULL, NULL),
(4811, 4800, 'Волгодонск', NULL, NULL),
(4812, 4800, 'Восход', NULL, NULL),
(4813, 4800, 'Гигант', NULL, NULL),
(4814, 4800, 'Горняцкий', NULL, NULL),
(4815, 4800, 'Гуково', NULL, NULL),
(4816, 4800, 'Донецк', NULL, NULL),
(4817, 4800, 'Донской', NULL, NULL),
(4818, 4800, 'Дубовское', NULL, NULL),
(4819, 4800, 'Жирнов', NULL, NULL),
(4820, 4800, 'Заветное', NULL, NULL),
(4821, 4800, 'Заводской', NULL, NULL),
(4822, 4800, 'Зверево', NULL, NULL),
(4823, 4800, 'Зерноград', NULL, NULL),
(4824, 4800, 'Зимовники', NULL, NULL),
(4825, 4800, 'Кагальницкая', NULL, NULL),
(4826, 4800, 'Казанская', NULL, NULL),
(4827, 4800, 'Каменоломни', NULL, NULL),
(4828, 4800, 'Каменск-Шахтинский', NULL, NULL),
(4829, 4800, 'Кашары', NULL, NULL),
(4830, 4800, 'Коксовый', NULL, NULL),
(4831, 4800, 'Константиновск', NULL, NULL),
(4832, 4800, 'Красный Сулин', NULL, NULL),
(4833, 4800, 'Куйбышево', NULL, NULL),
(4834, 4800, 'Матвеев Курган', NULL, NULL),
(4835, 4800, 'Мигулинская', NULL, NULL),
(4836, 4800, 'Миллерово', NULL, NULL),
(4837, 4800, 'Милютинская', NULL, NULL),
(4838, 4800, 'Морозовск', NULL, NULL),
(4839, 4800, 'Новочеркасск', NULL, NULL),
(4840, 4800, 'Новошахтинск', NULL, NULL),
(4841, 4800, 'Обливская', NULL, NULL),
(4842, 4800, 'Орловский', NULL, NULL),
(4843, 4800, 'Песчанокопское', NULL, NULL),
(4844, 4800, 'Покровское', NULL, NULL),
(4845, 4800, 'Пролетарск', NULL, NULL),
(4846, 4800, 'Ремонтное', NULL, NULL),
(4847, 4800, 'Родионово-Несветайская', NULL, NULL),
(4848, 4800, 'Ростов-на-Дону', NULL, NULL),
(4849, 4800, 'Сальск', NULL, NULL),
(4850, 4800, 'Семикаракорск', NULL, NULL),
(4851, 4800, 'Таганрог', NULL, NULL),
(4852, 4800, 'Тарасовский', NULL, NULL),
(4853, 4800, 'Тацинский', NULL, NULL),
(4854, 4800, 'Усть-Донецкий', NULL, NULL),
(4855, 4800, 'Целина', NULL, NULL),
(4856, 4800, 'Цимлянск', NULL, NULL),
(4857, 4800, 'Чалтырь', NULL, NULL),
(4858, 4800, 'Чертково', NULL, NULL),
(4859, 4800, 'Шахты', NULL, NULL),
(4860, 4800, 'Шолоховский', NULL, NULL),
(4862, 4861, 'Александро-Невский', NULL, NULL),
(4863, 4861, 'Горняк', NULL, NULL),
(4864, 4861, 'Гусь Железный', NULL, NULL),
(4865, 4861, 'Елатьма', NULL, NULL),
(4866, 4861, 'Ермишь', NULL, NULL),
(4867, 4861, 'Заречный', NULL, NULL),
(4868, 4861, 'Захарово', NULL, NULL),
(4869, 4861, 'Кадом', NULL, NULL),
(4870, 4861, 'Касимов', NULL, NULL),
(4871, 4861, 'Кораблино', NULL, NULL),
(4872, 4861, 'Милославское', NULL, NULL),
(4873, 4861, 'Михайлов', NULL, NULL),
(4874, 4861, 'Пителино', NULL, NULL),
(4875, 4861, 'Пронск', NULL, NULL),
(4876, 4861, 'Путятино', NULL, NULL),
(4877, 4861, 'Рыбное', NULL, NULL),
(4878, 4861, 'Ряжск', NULL, NULL),
(4879, 4861, 'Рязань', NULL, NULL),
(4880, 4861, 'Сапожок', NULL, NULL),
(4881, 4861, 'Сараи', NULL, NULL),
(4882, 4861, 'Сасово', NULL, NULL),
(4883, 4861, 'Скопин', NULL, NULL),
(4884, 4861, 'Спас-Клепики', NULL, NULL),
(4885, 4861, 'Спасск-Рязанский', NULL, NULL),
(4886, 4861, 'Старожилово', NULL, NULL),
(4887, 4861, 'Ухолово', NULL, NULL),
(4888, 4861, 'Чучково', NULL, NULL),
(4889, 4861, 'Шацк', NULL, NULL),
(4890, 4861, 'Шилово', NULL, NULL),
(4892, 4891, 'Алексеевка', NULL, NULL),
(4893, 4891, 'Безенчук', NULL, NULL),
(4894, 4891, 'Богатое', NULL, NULL),
(4895, 4891, 'Богатырь', NULL, NULL),
(4896, 4891, 'Большая Глущица', NULL, NULL),
(4897, 4891, 'Большая Черниговка', NULL, NULL),
(4898, 4891, 'Борское', NULL, NULL),
(4899, 4891, 'Волжский', NULL, NULL),
(4900, 4891, 'Жигулевск', NULL, NULL),
(4901, 4891, 'Зольное', NULL, NULL),
(4902, 4891, 'Исаклы', NULL, NULL),
(4903, 4891, 'Камышла', NULL, NULL),
(4904, 4891, 'Кинель', NULL, NULL),
(4905, 4891, 'Кинель-Черкасы', NULL, NULL),
(4906, 4891, 'Клявлино', NULL, NULL),
(4907, 4891, 'Кошки', NULL, NULL),
(4908, 4891, 'Красноармейское', NULL, NULL),
(4909, 4891, 'Куйбышев', NULL, NULL),
(4910, 4891, 'Нефтегорск', NULL, NULL),
(4911, 4891, 'Новокуйбышевск', NULL, NULL),
(4912, 4891, 'Октябрьск', NULL, NULL),
(4913, 4891, 'Отрадный', NULL, NULL),
(4914, 4891, 'Пестравка', NULL, NULL),
(4915, 4891, 'Похвистнево', NULL, NULL),
(4916, 4891, 'Приволжье', NULL, NULL),
(4917, 4891, 'Самара', NULL, NULL),
(4918, 4891, 'Сызрань', NULL, NULL),
(4919, 4891, 'Тольятти', NULL, NULL),
(4920, 4891, 'Хворостянка', NULL, NULL),
(4921, 4891, 'Чапаевск', NULL, NULL),
(4922, 4891, 'Челно-Вершины', NULL, NULL),
(4923, 4891, 'Шентала', NULL, NULL),
(4924, 4891, 'Шигоны', NULL, NULL),
(4926, 4925, 'Александровская', NULL, NULL),
(4927, 4925, 'Бокситогорск', NULL, NULL),
(4928, 4925, 'Большая Ижора', NULL, NULL),
(4929, 4925, 'Будогощь', NULL, NULL),
(4930, 4925, 'Вознесенье', NULL, NULL),
(4931, 4925, 'Волосово', NULL, NULL),
(4932, 4925, 'Волхов', NULL, NULL),
(4933, 4925, 'Всеволожск', NULL, NULL),
(4934, 4925, 'Выборг', NULL, NULL),
(4935, 4925, 'Вырица', NULL, NULL),
(4936, 4925, 'Высоцк', NULL, NULL),
(4937, 4925, 'Гатчина', NULL, NULL),
(4938, 4925, 'Дружная Горка', NULL, NULL),
(4939, 4925, 'Дубровка', NULL, NULL),
(4940, 4925, 'Ефимовский', NULL, NULL),
(4941, 4925, 'Зеленогорск', NULL, NULL),
(4942, 4925, 'Ивангород', NULL, NULL),
(4943, 4925, 'Каменногорск', NULL, NULL),
(4944, 4925, 'Кикерино', NULL, NULL),
(4945, 4925, 'Кингисепп', NULL, NULL),
(4946, 4925, 'Кириши', NULL, NULL),
(4947, 4925, 'Кировск', NULL, NULL),
(4948, 4925, 'Кобринское', NULL, NULL),
(4949, 4925, 'Колпино', NULL, NULL),
(4950, 4925, 'Кронштадт', NULL, NULL),
(4952, 4925, 'Лисий Нос', NULL, NULL),
(4953, 4925, 'Лодейное Поле', NULL, NULL),
(4954, 4925, 'Ломоносов', NULL, NULL),
(4955, 4925, 'Луга', NULL, NULL),
(4956, 4925, 'Павловск', NULL, NULL),
(4957, 4925, 'Парголово', NULL, NULL),
(4958, 4925, 'Петродворец', NULL, NULL),
(4959, 4925, 'Подпорожье', NULL, NULL),
(4960, 4925, 'Приозерск', NULL, NULL),
(4961, 4925, 'Пушкин', NULL, NULL),
(4962, 4925, 'Санкт-Петербург', NULL, NULL),
(4964, 4925, 'Сестрорецк', NULL, NULL),
(4965, 4925, 'Сланцы', NULL, NULL),
(4966, 4925, 'Сосновый Бор', NULL, NULL),
(4967, 4925, 'Тихвин', NULL, NULL),
(4968, 4925, 'Тосно', NULL, NULL),
(4970, 4969, 'Александров Гай', NULL, NULL),
(4971, 4969, 'Аркадак', NULL, NULL),
(4972, 4969, 'Аткарск', NULL, NULL),
(4973, 4969, 'Базарный Карабулак', NULL, NULL),
(4974, 4969, 'Балаково', NULL, NULL),
(4975, 4969, 'Балашов', NULL, NULL),
(4976, 4969, 'Балтай', NULL, NULL),
(4977, 4969, 'Возрождение', NULL, NULL),
(4978, 4969, 'Вольск', NULL, NULL),
(4979, 4969, 'Воскресенское', NULL, NULL),
(4980, 4969, 'Горный', NULL, NULL),
(4981, 4969, 'Дергачи', NULL, NULL),
(4982, 4969, 'Духовницкое', NULL, NULL),
(4983, 4969, 'Екатериновка', NULL, NULL),
(4984, 4969, 'Ершов', NULL, NULL),
(4985, 4969, 'Заречный', NULL, NULL),
(4986, 4969, 'Ивантеевка', NULL, NULL),
(4987, 4969, 'Калининск', NULL, NULL),
(4988, 4969, 'Каменский', NULL, NULL),
(4989, 4969, 'Красноармейск', NULL, NULL),
(4990, 4969, 'Красный Кут', NULL, NULL),
(4991, 4969, 'Лысые Горы', NULL, NULL),
(4992, 4969, 'Маркс', NULL, NULL),
(4993, 4969, 'Мокроус', NULL, NULL),
(4994, 4969, 'Новоузенск', NULL, NULL),
(4995, 4969, 'Новые Бурасы', NULL, NULL),
(4996, 4969, 'Озинки', NULL, NULL),
(4997, 4969, 'Перелюб', NULL, NULL),
(4998, 4969, 'Петровск', NULL, NULL),
(4999, 4969, 'Питерка', NULL, NULL),
(5000, 4969, 'Пугачев', NULL, NULL),
(5001, 4969, 'Ровное', NULL, NULL),
(5002, 4969, 'Романовка', NULL, NULL),
(5003, 4969, 'Ртищево', NULL, NULL),
(5004, 4969, 'Самойловка', NULL, NULL),
(5005, 4969, 'Саратов', NULL, NULL),
(5006, 4969, 'Степное', NULL, NULL),
(5007, 4969, 'Татищево', NULL, NULL),
(5008, 4969, 'Турки', NULL, NULL),
(5009, 4969, 'Хвалынск', NULL, NULL),
(5010, 4969, 'Энгельс', NULL, NULL),
(5012, 5011, 'Абый', NULL, NULL),
(5013, 5011, 'Алдан', NULL, NULL),
(5014, 5011, 'Амга', NULL, NULL),
(5015, 5011, 'Батагай', NULL, NULL),
(5016, 5011, 'Бердигестях', NULL, NULL),
(5017, 5011, 'Беркакит', NULL, NULL),
(5018, 5011, 'Бестях', NULL, NULL),
(5019, 5011, 'Борогонцы', NULL, NULL),
(5020, 5011, 'Верхневилюйск', NULL, NULL),
(5021, 5011, 'Верхнеколымск', NULL, NULL),
(5022, 5011, 'Верхоянск', NULL, NULL),
(5023, 5011, 'Вилюйск', NULL, NULL),
(5024, 5011, 'Витим', NULL, NULL),
(5025, 5011, 'Власово', NULL, NULL),
(5026, 5011, 'Жиганск', NULL, NULL),
(5027, 5011, 'Зырянка', NULL, NULL),
(5028, 5011, 'Кангалассы', NULL, NULL),
(5029, 5011, 'Канкунский', NULL, NULL),
(5030, 5011, 'Ленск', NULL, NULL),
(5031, 5011, 'Майя', NULL, NULL),
(5032, 5011, 'Менкеря', NULL, NULL),
(5033, 5011, 'Мирный', NULL, NULL),
(5034, 5011, 'Нерюнгри', NULL, NULL),
(5035, 5011, 'Нычалах', NULL, NULL),
(5036, 5011, 'Нюрба', NULL, NULL),
(5037, 5011, 'Олекминск', NULL, NULL),
(5038, 5011, 'Покровск', NULL, NULL),
(5039, 5011, 'Сангар', NULL, NULL),
(5040, 5011, 'Саскылах', NULL, NULL),
(5041, 5011, 'Среднеколымск', NULL, NULL),
(5042, 5011, 'Сунтар', NULL, NULL),
(5043, 5011, 'Тикси', NULL, NULL),
(5044, 5011, 'Усть-Мая', NULL, NULL),
(5045, 5011, 'Усть-Нера', NULL, NULL),
(5046, 5011, 'Хандыга', NULL, NULL),
(5047, 5011, 'Хонуу', NULL, NULL),
(5048, 5011, 'Черский', NULL, NULL),
(5049, 5011, 'Чокурдах', NULL, NULL),
(5050, 5011, 'Чурапча', NULL, NULL),
(5051, 5011, 'Якутск', NULL, NULL),
(5053, 5052, 'Александровск-Сахалинский', NULL, NULL),
(5054, 5052, 'Анбэцу', NULL, NULL),
(5055, 5052, 'Анива', NULL, NULL),
(5056, 5052, 'Бошняково', NULL, NULL),
(5057, 5052, 'Быков', NULL, NULL),
(5058, 5052, 'Вахрушев', NULL, NULL),
(5059, 5052, 'Взморье', NULL, NULL),
(5060, 5052, 'Гастелло', NULL, NULL),
(5061, 5052, 'Горнозаводск', NULL, NULL),
(5062, 5052, 'Долинск', NULL, NULL),
(5063, 5052, 'Ильинский', NULL, NULL),
(5064, 5052, 'Катангли', NULL, NULL),
(5065, 5052, 'Корсаков', NULL, NULL),
(5066, 5052, 'Курильск', NULL, NULL),
(5067, 5052, 'Макаров', NULL, NULL),
(5068, 5052, 'Невельск', NULL, NULL),
(5069, 5052, 'Ноглики', NULL, NULL),
(5070, 5052, 'Оха', NULL, NULL),
(5071, 5052, 'Поронайск', NULL, NULL),
(5072, 5052, 'Северо-Курильск', NULL, NULL),
(5073, 5052, 'Смирных', NULL, NULL),
(5074, 5052, 'Томари', NULL, NULL),
(5075, 5052, 'Тымовское', NULL, NULL),
(5076, 5052, 'Углегорск', NULL, NULL),
(5077, 5052, 'Холмск', NULL, NULL),
(5078, 5052, 'Шахтерск', NULL, NULL),
(5079, 5052, 'Южно-Сахалинск', NULL, NULL),
(5081, 5080, 'Алапаевск', NULL, NULL),
(5082, 5080, 'Алтынай', NULL, NULL),
(5083, 5080, 'Арамиль', NULL, NULL),
(5084, 5080, 'Артемовский', NULL, NULL),
(5085, 5080, 'Арти', NULL, NULL),
(5086, 5080, 'Асбест', NULL, NULL),
(5087, 5080, 'Ачит', NULL, NULL),
(5088, 5080, 'Байкалово', NULL, NULL),
(5089, 5080, 'Басьяновский', NULL, NULL),
(5090, 5080, 'Белоярский', NULL, NULL),
(5091, 5080, 'Березовский', NULL, NULL),
(5092, 5080, 'Богданович', NULL, NULL),
(5093, 5080, 'Буланаш', NULL, NULL),
(5094, 5080, 'Верхний Тагил', NULL, NULL),
(5095, 5080, 'Верхняя Пышма', NULL, NULL),
(5096, 5080, 'Верхняя Салда', NULL, NULL),
(5097, 5080, 'Верхняя Синячиха', NULL, NULL),
(5098, 5080, 'Верхняя Сысерть', NULL, NULL),
(5099, 5080, 'Верхняя Тура', NULL, NULL),
(5100, 5080, 'Верхотурье', NULL, NULL),
(5101, 5080, 'Висим', NULL, NULL),
(5102, 5080, 'Волчанск', NULL, NULL),
(5103, 5080, 'Воронцовка', NULL, NULL),
(5104, 5080, 'Гари', NULL, NULL),
(5105, 5080, 'Дегтярск', NULL, NULL),
(5106, 5080, 'Екатеринбург', NULL, NULL),
(5107, 5080, 'Ертарский', NULL, NULL),
(5108, 5080, 'Заводоуспенское', NULL, NULL),
(5109, 5080, 'Зыряновский', NULL, NULL),
(5110, 5080, 'Зюзельский', NULL, NULL),
(5111, 5080, 'Ивдель', NULL, NULL),
(5112, 5080, 'Изумруд', NULL, NULL),
(5113, 5080, 'Ирбит', NULL, NULL),
(5114, 5080, 'Ис', NULL, NULL),
(5115, 5080, 'Каменск-Уральский', NULL, NULL),
(5116, 5080, 'Камышлов', NULL, NULL),
(5117, 5080, 'Карпинск', NULL, NULL),
(5118, 5080, 'Карпунинский', NULL, NULL),
(5119, 5080, 'Качканар', NULL, NULL),
(5120, 5080, 'Кировград', NULL, NULL),
(5121, 5080, 'Краснотурьинск', NULL, NULL),
(5122, 5080, 'Красноуральск', NULL, NULL),
(5123, 5080, 'Красноуфимск', NULL, NULL),
(5124, 5080, 'Кушва', NULL, NULL),
(5125, 5080, 'Михайловск', NULL, NULL),
(5126, 5080, 'Невьянск', NULL, NULL),
(5127, 5080, 'Нижние Серги', NULL, NULL),
(5128, 5080, 'Нижний Тагил', NULL, NULL),
(5129, 5080, 'Нижняя Салда', NULL, NULL),
(5130, 5080, 'Нижняя Тура', NULL, NULL),
(5131, 5080, 'Новая Ляля', NULL, NULL),
(5132, 5080, 'Оус', NULL, NULL),
(5133, 5080, 'Первоуральск', NULL, NULL),
(5134, 5080, 'Полевской', NULL, NULL),
(5135, 5080, 'Пышма', NULL, NULL),
(5136, 5080, 'Ревда', NULL, NULL),
(5137, 5080, 'Реж', NULL, NULL),
(5138, 5080, 'Свердловск', NULL, NULL),
(5139, 5080, 'Североуральск', NULL, NULL),
(5140, 5080, 'Серов', NULL, NULL),
(5141, 5080, 'Сосьва', NULL, NULL),
(5142, 5080, 'Среднеуральск', NULL, NULL),
(5143, 5080, 'Сухой Лог', NULL, NULL),
(5144, 5080, 'Сысерть', NULL, NULL),
(5145, 5080, 'Таборы', NULL, NULL),
(5146, 5080, 'Тавда', NULL, NULL),
(5147, 5080, 'Талица', NULL, NULL),
(5148, 5080, 'Тугулым', NULL, NULL),
(5149, 5080, 'Туринск', NULL, NULL),
(5150, 5080, 'Туринская Слобода', NULL, NULL),
(5152, 5151, 'Алагир', NULL, NULL),
(5153, 5151, 'Ардон', NULL, NULL),
(5154, 5151, 'Беслан', NULL, NULL),
(5155, 5151, 'Бурон', NULL, NULL),
(5156, 5151, 'Владикавказ', NULL, NULL),
(5157, 5151, 'Дигора', NULL, NULL),
(5158, 5151, 'Моздок', NULL, NULL),
(5159, 5151, 'Орджоникидзе', NULL, NULL),
(5160, 5151, 'Чикола', NULL, NULL),
(5162, 5161, 'Велиж', NULL, NULL),
(5163, 5161, 'Верхнеднепровский', NULL, NULL),
(5164, 5161, 'Ворга', NULL, NULL),
(5165, 5161, 'Вязьма', NULL, NULL),
(5166, 5161, 'Гагарин', NULL, NULL),
(5167, 5161, 'Глинка', NULL, NULL),
(5168, 5161, 'Голынки', NULL, NULL),
(5169, 5161, 'Демидов', NULL, NULL),
(5170, 5161, 'Дорогобуж', NULL, NULL),
(5171, 5161, 'Духовщина', NULL, NULL),
(5172, 5161, 'Екимовичи', NULL, NULL),
(5173, 5161, 'Ельня', NULL, NULL),
(5174, 5161, 'Ершичи', NULL, NULL),
(5175, 5161, 'Издешково', NULL, NULL),
(5176, 5161, 'Кардымово', NULL, NULL),
(5177, 5161, 'Красный', NULL, NULL),
(5178, 5161, 'Монастырщина', NULL, NULL),
(5179, 5161, 'Новодугино', NULL, NULL),
(5180, 5161, 'Починок', NULL, NULL),
(5181, 5161, 'Рославль', NULL, NULL),
(5182, 5161, 'Рудня', NULL, NULL),
(5183, 5161, 'Сафоново', NULL, NULL),
(5184, 5161, 'Смоленск', NULL, NULL),
(5185, 5161, 'Сычевка', NULL, NULL),
(5186, 5161, 'Угра', NULL, NULL),
(5187, 5161, 'Хиславичи', NULL, NULL),
(5188, 5161, 'Холм-Жирковский', NULL, NULL),
(5189, 5161, 'Шумячи', NULL, NULL),
(5190, 5161, 'Ярцево', NULL, NULL),
(5192, 5191, 'Александровское', NULL, NULL),
(5193, 5191, 'Арзгир', NULL, NULL),
(5194, 5191, 'Благодарный', NULL, NULL),
(5195, 5191, 'Буденновск', NULL, NULL),
(5196, 5191, 'Георгиевск', NULL, NULL),
(5197, 5191, 'Дивное', NULL, NULL),
(5198, 5191, 'Домбай', NULL, NULL),
(5199, 5191, 'Донское', NULL, NULL),
(5200, 5191, 'Ессентуки', NULL, NULL),
(5201, 5191, 'Иноземцево', NULL, NULL),
(5202, 5191, 'Ипатово', NULL, NULL),
(5203, 5191, 'Карачаевск', NULL, NULL),
(5204, 5191, 'Кисловодск', NULL, NULL),
(5205, 5191, 'Кочубеевское', NULL, NULL),
(5206, 5191, 'Красногвардейское', NULL, NULL),
(5207, 5191, 'Курсавка', NULL, NULL),
(5208, 5191, 'Левокумское', NULL, NULL),
(5209, 5191, 'Минеральные Воды', NULL, NULL),
(5210, 5191, 'Невинномысск', NULL, NULL),
(5211, 5191, 'Нефтекумск', NULL, NULL),
(5212, 5191, 'Новоалександровск', NULL, NULL),
(5213, 5191, 'Новоалександровская', NULL, NULL),
(5214, 5191, 'Новопавловск', NULL, NULL),
(5215, 5191, 'Новоселицкое', NULL, NULL),
(5216, 5191, 'Преградная', NULL, NULL),
(5217, 5191, 'Пятигорск', NULL, NULL),
(5218, 5191, 'Светлоград', NULL, NULL),
(5219, 5191, 'Ставрополь', NULL, NULL),
(5220, 5191, 'Степное', NULL, NULL),
(5221, 5191, 'Теберда', NULL, NULL),
(5222, 5191, 'Усть-Джегута', NULL, NULL),
(5223, 5191, 'Хабез', NULL, NULL),
(5224, 5191, 'Черкесск', NULL, NULL),
(5226, 5225, 'Бондари', NULL, NULL),
(5227, 5225, 'Гавриловка Вторая', NULL, NULL),
(5228, 5225, 'Жердевка', NULL, NULL),
(5229, 5225, 'Знаменка', NULL, NULL),
(5230, 5225, 'Инжавино', NULL, NULL),
(5231, 5225, 'Кирсанов', NULL, NULL),
(5232, 5225, 'Котовск', NULL, NULL),
(5233, 5225, 'Мичуринск', NULL, NULL),
(5234, 5225, 'Мордово', NULL, NULL),
(5235, 5225, 'Моршанск', NULL, NULL),
(5236, 5225, 'Мучкапский', NULL, NULL),
(5237, 5225, 'Петровское', NULL, NULL),
(5238, 5225, 'Пичаево', NULL, NULL),
(5239, 5225, 'Рассказово', NULL, NULL),
(5240, 5225, 'Ржакса', NULL, NULL),
(5241, 5225, 'Староюрьево', NULL, NULL),
(5242, 5225, 'Тамбов', NULL, NULL),
(5243, 5225, 'Токаревка', NULL, NULL),
(5244, 5225, 'Уварово', NULL, NULL),
(5245, 5225, 'Умет', NULL, NULL),
(5247, 5246, 'Агрыз', NULL, NULL),
(5248, 5246, 'Азнакаево', NULL, NULL),
(5249, 5246, 'Аксубаево', NULL, NULL),
(5250, 5246, 'Актюбинский', NULL, NULL),
(5251, 5246, 'Алексеевское', NULL, NULL),
(5252, 5246, 'Альметьевск', NULL, NULL),
(5253, 5246, 'Апастово', NULL, NULL),
(5254, 5246, 'Арск', NULL, NULL),
(5255, 5246, 'Бавлы', NULL, NULL),
(5256, 5246, 'Базарные Матаки', NULL, NULL),
(5257, 5246, 'Балтаси', NULL, NULL),
(5258, 5246, 'Богатые Сабы', NULL, NULL),
(5259, 5246, 'Брежнев', NULL, NULL),
(5260, 5246, 'Бугульма', NULL, NULL),
(5261, 5246, 'Буинск', NULL, NULL),
(5262, 5246, 'Васильево', NULL, NULL),
(5263, 5246, 'Верхний Услон', NULL, NULL),
(5264, 5246, 'Высокая Гора', NULL, NULL),
(5265, 5246, 'Дербешкинский', NULL, NULL),
(5266, 5246, 'Елабуга', NULL, NULL),
(5267, 5246, 'Заинск', NULL, NULL),
(5268, 5246, 'Зеленодольск', NULL, NULL),
(5269, 5246, 'Казань', NULL, NULL),
(5270, 5246, 'Камское Устье', NULL, NULL),
(5271, 5246, 'Карабаш', NULL, NULL),
(5272, 5246, 'Куйбышев', NULL, NULL),
(5273, 5246, 'Кукмод', NULL, NULL),
(5274, 5246, 'Кукмор', NULL, NULL),
(5275, 5246, 'Лаишево', NULL, NULL),
(5276, 5246, 'Лениногорск', NULL, NULL),
(5277, 5246, 'Мамадыш', NULL, NULL),
(5278, 5246, 'Менделеевск', NULL, NULL),
(5279, 5246, 'Мензелинск', NULL, NULL),
(5280, 5246, 'Муслюмово', NULL, NULL),
(5281, 5246, 'Набережные Челны', NULL, NULL),
(5282, 5246, 'Нижнекамск', NULL, NULL),
(5283, 5246, 'Новошешминск', NULL, NULL),
(5284, 5246, 'Нурлат', NULL, NULL),
(5285, 5246, 'Пестрецы', NULL, NULL),
(5286, 5246, 'Рыбная Слобода', NULL, NULL),
(5287, 5246, 'Сарманово', NULL, NULL),
(5288, 5246, 'Старое Дрожжаное', NULL, NULL),
(5289, 5246, 'Тетюши', NULL, NULL),
(5290, 5246, 'Чистополь', NULL, NULL),
(5292, 5291, 'Александровское', NULL, NULL),
(5293, 5291, 'Асино', NULL, NULL),
(5294, 5291, 'Бакчар', NULL, NULL),
(5295, 5291, 'Батурино', NULL, NULL),
(5296, 5291, 'Белый Яр', NULL, NULL),
(5297, 5291, 'Зырянское', NULL, NULL),
(5298, 5291, 'Итатка', NULL, NULL),
(5299, 5291, 'Каргасок', NULL, NULL),
(5300, 5291, 'Катайга', NULL, NULL),
(5301, 5291, 'Кожевниково', NULL, NULL),
(5302, 5291, 'Колпашево', NULL, NULL),
(5303, 5291, 'Кривошеино', NULL, NULL),
(5304, 5291, 'Мельниково', NULL, NULL),
(5305, 5291, 'Молчаново', NULL, NULL),
(5306, 5291, 'Парабель', NULL, NULL),
(5307, 5291, 'Первомайское', NULL, NULL),
(5308, 5291, 'Подгорное', NULL, NULL),
(5309, 5291, 'Стрежевой', NULL, NULL),
(5310, 5291, 'Томск', NULL, NULL),
(5311, 5291, 'Тымск', NULL, NULL),
(5313, 5312, 'Ак-Довурак', NULL, NULL),
(5314, 5312, 'Бай Хаак', NULL, NULL),
(5315, 5312, 'Кызыл', NULL, NULL),
(5316, 5312, 'Самагалтай', NULL, NULL),
(5317, 5312, 'Сарыг-Сеп', NULL, NULL),
(5318, 5312, 'Суть-Холь', NULL, NULL),
(5319, 5312, 'Тоора-Хем', NULL, NULL),
(5320, 5312, 'Туран', NULL, NULL),
(5321, 5312, 'Тээли', NULL, NULL),
(5322, 5312, 'Хову-Аксы', NULL, NULL),
(5323, 5312, 'Чадан', NULL, NULL),
(5324, 5312, 'Шагонар', NULL, NULL),
(5325, 5312, 'Эрзин', NULL, NULL),
(5327, 5326, 'Агеево', NULL, NULL),
(5328, 5326, 'Алексин', NULL, NULL),
(5329, 5326, 'Арсеньево', NULL, NULL),
(5330, 5326, 'Барсуки', NULL, NULL),
(5331, 5326, 'Бегичевский', NULL, NULL),
(5332, 5326, 'Богородицк', NULL, NULL),
(5333, 5326, 'Болохово', NULL, NULL),
(5334, 5326, 'Велегож', NULL, NULL),
(5335, 5326, 'Венев', NULL, NULL),
(5336, 5326, 'Волово', NULL, NULL),
(5337, 5326, 'Горелки', NULL, NULL),
(5338, 5326, 'Донской', NULL, NULL),
(5339, 5326, 'Дубна', NULL, NULL),
(5340, 5326, 'Епифань', NULL, NULL),
(5341, 5326, 'Ефремов', NULL, NULL),
(5342, 5326, 'Заокский', NULL, NULL),
(5343, 5326, 'Казановка', NULL, NULL),
(5344, 5326, 'Кимовск', NULL, NULL),
(5345, 5326, 'Киреевск', NULL, NULL),
(5346, 5326, 'Куркино', NULL, NULL),
(5347, 5326, 'Ленинский', NULL, NULL),
(5348, 5326, 'Новомосковск', NULL, NULL),
(5349, 5326, 'Одоев', NULL, NULL),
(5350, 5326, 'Плавск', NULL, NULL),
(5351, 5326, 'Суворов', NULL, NULL),
(5352, 5326, 'Тула', NULL, NULL),
(5353, 5326, 'Узловая', NULL, NULL),
(5354, 5326, 'Щекино', NULL, NULL),
(5355, 5326, 'Ясногорск', NULL, NULL),
(5357, 5356, 'Абатский', NULL, NULL),
(5358, 5356, 'Аксарка', NULL, NULL),
(5359, 5356, 'Армизонское', NULL, NULL),
(5360, 5356, 'Аромашево', NULL, NULL),
(5361, 5356, 'Бердюжье', NULL, NULL),
(5362, 5356, 'Большое Сорокино', NULL, NULL),
(5363, 5356, 'Вагай', NULL, NULL),
(5364, 5356, 'Викулово', NULL, NULL),
(5365, 5356, 'Винзили', NULL, NULL),
(5366, 5356, 'Голышманово', NULL, NULL),
(5367, 5356, 'Заводопетровский', NULL, NULL),
(5368, 5356, 'Заводоуковск', NULL, NULL),
(5369, 5356, 'Исетское', NULL, NULL),
(5370, 5356, 'Ишим', NULL, NULL),
(5371, 5356, 'Казанское', NULL, NULL),
(5372, 5356, 'Казым-Мыс', NULL, NULL),
(5373, 5356, 'Кондинское', NULL, NULL),
(5374, 5356, 'Красноселькуп', NULL, NULL),
(5375, 5356, 'Лабытнанги', NULL, NULL),
(5376, 5356, 'Мужи', NULL, NULL),
(5377, 5356, 'Надым', NULL, NULL),
(5378, 5356, 'Находка', NULL, NULL),
(5379, 5356, 'Нефтеюганск', NULL, NULL),
(5380, 5356, 'Нижневартовск', NULL, NULL),
(5381, 5356, 'Нижняя Тавда', NULL, NULL),
(5382, 5356, 'Новый Уренгой', NULL, NULL),
(5383, 5356, 'Ноябрьск', NULL, NULL),
(5384, 5356, 'Нягань', NULL, NULL),
(5385, 5356, 'Октябрьское', NULL, NULL),
(5386, 5356, 'Омутинский', NULL, NULL),
(5387, 5356, 'Радужный', NULL, NULL),
(5388, 5356, 'Салехард', NULL, NULL),
(5389, 5356, 'Сладково', NULL, NULL),
(5390, 5356, 'Советский', NULL, NULL),
(5391, 5356, 'Сургут', NULL, NULL),
(5392, 5356, 'Тазовский', NULL, NULL),
(5393, 5356, 'Тарко-Сале', NULL, NULL),
(5394, 5356, 'Тобольск', NULL, NULL),
(5395, 5356, 'Тюмень', NULL, NULL),
(5396, 5356, 'Уват', NULL, NULL),
(5397, 5356, 'Упорово', NULL, NULL),
(5398, 5356, 'Урай', NULL, NULL),
(5399, 5356, 'Ханты-Мансийск', NULL, NULL),
(5400, 5356, 'Юрибей', NULL, NULL),
(5401, 5356, 'Ялуторовск', NULL, NULL),
(5402, 5356, 'Яр-Сале', NULL, NULL),
(5403, 5356, 'Ярково', NULL, NULL),
(5405, 5404, 'Алнаши', NULL, NULL),
(5406, 5404, 'Балезино', NULL, NULL),
(5407, 5404, 'Вавож', NULL, NULL),
(5408, 5404, 'Воткинск', NULL, NULL),
(5409, 5404, 'Глазов', NULL, NULL),
(5410, 5404, 'Грахово', NULL, NULL),
(5411, 5404, 'Дебесы', NULL, NULL),
(5412, 5404, 'Завьялово', NULL, NULL),
(5413, 5404, 'Игра', NULL, NULL),
(5414, 5404, 'Ижевск', NULL, NULL),
(5415, 5404, 'Кама', NULL, NULL),
(5416, 5404, 'Камбарка', NULL, NULL),
(5417, 5404, 'Каракулино', NULL, NULL),
(5418, 5404, 'Кез', NULL, NULL),
(5419, 5404, 'Кизнер', NULL, NULL),
(5420, 5404, 'Киясово', NULL, NULL),
(5421, 5404, 'Красногорское', NULL, NULL),
(5422, 5404, 'Можга', NULL, NULL),
(5423, 5404, 'Сарапул', NULL, NULL),
(5424, 5404, 'Селты', NULL, NULL),
(5425, 5404, 'Сюмси', NULL, NULL),
(5426, 5404, 'Ува', NULL, NULL),
(5427, 5404, 'Устинов', NULL, NULL),
(5428, 5404, 'Шаркан', NULL, NULL),
(5429, 5404, 'Юкаменское', NULL, NULL),
(5430, 5404, 'Якшур-Бодья', NULL, NULL),
(5431, 5404, 'Яр', NULL, NULL),
(5433, 5432, 'Базарный Сызган', NULL, NULL),
(5434, 5432, 'Барыш', NULL, NULL),
(5435, 5432, 'Большое Нагаткино', NULL, NULL),
(5436, 5432, 'Вешкайма', NULL, NULL),
(5437, 5432, 'Глотовка', NULL, NULL),
(5438, 5432, 'Димитровград', NULL, NULL),
(5439, 5432, 'Игнатовка', NULL, NULL),
(5440, 5432, 'Измайлово', NULL, NULL),
(5441, 5432, 'Инза', NULL, NULL),
(5442, 5432, 'Ишеевка', NULL, NULL),
(5443, 5432, 'Канадей', NULL, NULL),
(5444, 5432, 'Карсун', NULL, NULL),
(5445, 5432, 'Кузоватово', NULL, NULL),
(5446, 5432, 'Майна', NULL, NULL),
(5447, 5432, 'Новая Малыкла', NULL, NULL),
(5448, 5432, 'Новоспасское', NULL, NULL),
(5449, 5432, 'Павловка', NULL, NULL),
(5450, 5432, 'Радищево', NULL, NULL),
(5451, 5432, 'Сенгилей', NULL, NULL),
(5452, 5432, 'Старая Кулатка', NULL, NULL),
(5453, 5432, 'Старая Майна', NULL, NULL),
(5454, 5432, 'Сурское', NULL, NULL),
(5455, 5432, 'Тереньга', NULL, NULL),
(5456, 5432, 'Ульяновск', NULL, NULL),
(5457, 5432, 'Чердаклы', NULL, NULL),
(5459, 5458, 'Аксай', NULL, NULL),
(5460, 5458, 'Дарьинское', NULL, NULL),
(5461, 5458, 'Деркул', NULL, NULL),
(5462, 5458, 'Джамбейты', NULL, NULL),
(5463, 5458, 'Джаныбек', NULL, NULL),
(5464, 5458, 'Казталовка', NULL, NULL),
(5465, 5458, 'Калмыково', NULL, NULL),
(5466, 5458, 'Каратобе', NULL, NULL),
(5467, 5458, 'Переметное', NULL, NULL),
(5468, 5458, 'Сайхин', NULL, NULL),
(5469, 5458, 'Уральск', NULL, NULL),
(5470, 5458, 'Федоровка', NULL, NULL),
(5471, 5458, 'Фурманово', NULL, NULL),
(5472, 5458, 'Чапаев', NULL, NULL),
(5474, 5473, 'Амурск', NULL, NULL),
(5475, 5473, 'Аян', NULL, NULL),
(5476, 5473, 'Березовый', NULL, NULL),
(5477, 5473, 'Бикин', NULL, NULL),
(5478, 5473, 'Бира', NULL, NULL),
(5479, 5473, 'Биракан', NULL, NULL),
(5480, 5473, 'Богородское', NULL, NULL),
(5481, 5473, 'Болонь', NULL, NULL),
(5482, 5473, 'Ванино', NULL, NULL),
(5483, 5473, 'Волочаевка Вторая', NULL, NULL),
(5484, 5473, 'Высокогорный', NULL, NULL),
(5485, 5473, 'Вяземский', NULL, NULL),
(5486, 5473, 'Горный', NULL, NULL),
(5487, 5473, 'Гурское', NULL, NULL),
(5488, 5473, 'Дормидонтовка', NULL, NULL),
(5489, 5473, 'Заветы Ильича', NULL, NULL),
(5490, 5473, 'Известковый', NULL, NULL),
(5491, 5473, 'Иннокентьевка', NULL, NULL),
(5492, 5473, 'Комсомольск-на-Амуре', NULL, NULL),
(5493, 5473, 'Ленинское', NULL, NULL),
(5494, 5473, 'Нелькан', NULL, NULL),
(5495, 5473, 'Николаевск-на-Амуре', NULL, NULL),
(5496, 5473, 'Облучье', NULL, NULL),
(5497, 5473, 'Охотск', NULL, NULL),
(5498, 5473, 'Переяславка', NULL, NULL),
(5499, 5473, 'Смидович', NULL, NULL),
(5500, 5473, 'Советская Гавань', NULL, NULL),
(5501, 5473, 'Софийск', NULL, NULL),
(5502, 5473, 'Троицкое', NULL, NULL),
(5503, 5473, 'Тугур', NULL, NULL),
(5504, 5473, 'Хабаровск', NULL, NULL),
(5505, 5473, 'Чегдомын', NULL, NULL),
(5506, 5473, 'Чумикан', NULL, NULL),
(5508, 5507, 'Агаповка', NULL, NULL),
(5509, 5507, 'Аргаяш', NULL, NULL),
(5510, 5507, 'Аша', NULL, NULL),
(5511, 5507, 'Бакал', NULL, NULL),
(5512, 5507, 'Бреды', NULL, NULL),
(5513, 5507, 'Варна', NULL, NULL),
(5514, 5507, 'Верхнеуральск', NULL, NULL),
(5515, 5507, 'Верхний Уфалей', NULL, NULL),
(5516, 5507, 'Еманжелинск', NULL, NULL),
(5517, 5507, 'Златоуст', NULL, NULL),
(5518, 5507, 'Карабаш', NULL, NULL),
(5519, 5507, 'Карталы', NULL, NULL),
(5520, 5507, 'Касли', NULL, NULL),
(5521, 5507, 'Катав-Ивановск', NULL, NULL),
(5522, 5507, 'Копейск', NULL, NULL),
(5523, 5507, 'Коркино', NULL, NULL),
(5524, 5507, 'Кунашак', NULL, NULL),
(5525, 5507, 'Куса', NULL, NULL),
(5526, 5507, 'Кыштым', NULL, NULL),
(5527, 5507, 'Магнитогорск', NULL, NULL),
(5528, 5507, 'Миасс', NULL, NULL),
(5529, 5507, 'Октябрьское', NULL, NULL),
(5530, 5507, 'Пласт', NULL, NULL),
(5531, 5507, 'Сатка', NULL, NULL),
(5532, 5507, 'Сим', NULL, NULL),
(5533, 5507, 'Троицк', NULL, NULL),
(5534, 5507, 'Увельский', NULL, NULL),
(5535, 5507, 'Уйское', NULL, NULL),
(5536, 5507, 'Усть-Катав', NULL, NULL),
(5537, 5507, 'Фершампенуаз', NULL, NULL),
(5538, 5507, 'Чебаркуль', NULL, NULL),
(5539, 5507, 'Челябинск', NULL, NULL),
(5540, 5507, 'Чесма', NULL, NULL),
(5541, 5507, 'Южно-Уральск', NULL, NULL),
(5542, 5507, 'Юрюзань', NULL, NULL),
(5544, 5543, 'Аргун', NULL, NULL),
(5545, 5543, 'Грозный', NULL, NULL),
(5546, 5543, 'Гудермез', NULL, NULL),
(5547, 5543, 'Малгобек', NULL, NULL),
(5548, 5543, 'Назрань', NULL, NULL),
(5549, 5543, 'Наурская', NULL, NULL),
(5550, 5543, 'Ножай-Юрт', NULL, NULL),
(5551, 5543, 'Орджоникидзевская', NULL, NULL),
(5552, 5543, 'Советское', NULL, NULL),
(5553, 5543, 'Урус-Мартан', NULL, NULL),
(5554, 5543, 'Шали', NULL, NULL),
(5556, 5555, 'Агинское', NULL, NULL),
(5557, 5555, 'Аксеново-Зиловское', NULL, NULL),
(5558, 5555, 'Акша', NULL, NULL),
(5559, 5555, 'Александровский Завод', NULL, NULL),
(5560, 5555, 'Амазар', NULL, NULL),
(5561, 5555, 'Арбагар', NULL, NULL),
(5562, 5555, 'Атамановка', NULL, NULL),
(5563, 5555, 'Балей', NULL, NULL),
(5564, 5555, 'Борзя', NULL, NULL),
(5565, 5555, 'Букачача', NULL, NULL),
(5566, 5555, 'Газимурский Завод', NULL, NULL),
(5567, 5555, 'Давенда', NULL, NULL),
(5568, 5555, 'Дарасун', NULL, NULL),
(5569, 5555, 'Дровяная', NULL, NULL),
(5570, 5555, 'Дульдурга', NULL, NULL),
(5571, 5555, 'Жиндо', NULL, NULL),
(5572, 5555, 'Забайкальск', NULL, NULL),
(5573, 5555, 'Итака', NULL, NULL),
(5574, 5555, 'Калга', NULL, NULL),
(5575, 5555, 'Карымское', NULL, NULL),
(5576, 5555, 'Кличка', NULL, NULL),
(5577, 5555, 'Ключевский', NULL, NULL),
(5578, 5555, 'Кокуй', NULL, NULL),
(5579, 5555, 'Краснокаменск', NULL, NULL),
(5580, 5555, 'Красный Чикой', NULL, NULL),
(5581, 5555, 'Кыра', NULL, NULL),
(5582, 5555, 'Моготуй', NULL, NULL),
(5583, 5555, 'Могоча', NULL, NULL),
(5584, 5555, 'Нерчинск', NULL, NULL),
(5585, 5555, 'Нерчинский Завод', NULL, NULL),
(5586, 5555, 'Нижний Часучей', NULL, NULL),
(5587, 5555, 'Оловянная', NULL, NULL),
(5588, 5555, 'Первомайский', NULL, NULL),
(5589, 5555, 'Петровск-Забайкальский', NULL, NULL),
(5590, 5555, 'Приаргунск', NULL, NULL),
(5591, 5555, 'Сретенск', NULL, NULL),
(5592, 5555, 'Тупик', NULL, NULL),
(5593, 5555, 'Улеты', NULL, NULL),
(5594, 5555, 'Хилок', NULL, NULL),
(5595, 5555, 'Чара', NULL, NULL),
(5596, 5555, 'Чернышевск', NULL, NULL),
(5597, 5555, 'Чита', NULL, NULL),
(5598, 5555, 'Шелопугино', NULL, NULL),
(5599, 5555, 'Шилка', NULL, NULL),
(5601, 5600, 'Алатырь', NULL, NULL),
(5602, 5600, 'Аликово', NULL, NULL),
(5603, 5600, 'Батырева', NULL, NULL),
(5604, 5600, 'Буинск', NULL, NULL),
(5605, 5600, 'Вурнары', NULL, NULL),
(5606, 5600, 'Ибреси', NULL, NULL),
(5607, 5600, 'Канаш', NULL, NULL),
(5608, 5600, 'Киря', NULL, NULL),
(5609, 5600, 'Комсомольское', NULL, NULL),
(5610, 5600, 'Красноармейское', NULL, NULL),
(5611, 5600, 'Красные Четаи', NULL, NULL),
(5612, 5600, 'Кугеси', NULL, NULL),
(5613, 5600, 'Мариинский Посад', NULL, NULL),
(5614, 5600, 'Моргауши', NULL, NULL),
(5615, 5600, 'Новочебоксарск', NULL, NULL),
(5616, 5600, 'Порецкое', NULL, NULL),
(5617, 5600, 'Урмары', NULL, NULL),
(5618, 5600, 'Цивильск', NULL, NULL),
(5619, 5600, 'Чебоксары', NULL, NULL),
(5620, 5600, 'Шемурша', NULL, NULL),
(5621, 5600, 'Шумерля', NULL, NULL),
(5622, 5600, 'Ядрин', NULL, NULL),
(5623, 5600, 'Яльчики', NULL, NULL),
(5624, 5600, 'Янтиково', NULL, NULL),
(5626, 5625, 'Андропов', NULL, NULL),
(5627, 5625, 'Берендеево', NULL, NULL),
(5628, 5625, 'Большое Село', NULL, NULL),
(5629, 5625, 'Борисоглебский', NULL, NULL),
(5630, 5625, 'Брейтово', NULL, NULL),
(5631, 5625, 'Бурмакино', NULL, NULL),
(5632, 5625, 'Варегово', NULL, NULL),
(5633, 5625, 'Волга', NULL, NULL),
(5634, 5625, 'Гаврилов Ям', NULL, NULL),
(5635, 5625, 'Данилов', NULL, NULL),
(5636, 5625, 'Любим', NULL, NULL),
(5637, 5625, 'Мышкино', NULL, NULL),
(5638, 5625, 'Некрасовское', NULL, NULL),
(5639, 5625, 'Новый Некоуз', NULL, NULL),
(5640, 5625, 'Переславль-Залесский', NULL, NULL),
(5641, 5625, 'Пошехонье-Володарск', NULL, NULL),
(5642, 5625, 'Ростов', NULL, NULL),
(5643, 5625, 'Рыбинск', NULL, NULL),
(5644, 5625, 'Тутаев', NULL, NULL),
(5645, 5625, 'Углич', NULL, NULL),
(5646, 5625, 'Ярославль', NULL, NULL),
(279078, 2499002, 'Аган', NULL, NULL),
(1923662, 5356, 'Когалым', NULL, NULL),
(1924171, 3251, 'Мирный', NULL, NULL),
(1924532, 4925, 'Шлиссельбург', NULL, NULL),
(1925143, 3761, 'Кёнигсберг', NULL, NULL),
(1929637, 3296, 'Бураево', NULL, NULL),
(1998542, 1998532, 'Майкоп', NULL, NULL),
(1998584, 1998532, 'Адыгейск', NULL, NULL),
(2005016, 5507, 'Снежинск', NULL, NULL),
(2019672, 5326, 'Белев', NULL, NULL),
(2052982, 3529, 'нововоронеж', NULL, NULL),
(2116660, 3703, 'Саянск', NULL, NULL),
(2184457, 5161, 'Десногорск', NULL, NULL),
(2185271, 3563, 'Саров', NULL, NULL),
(2185858, 4052, 'Курганинск', NULL, NULL),
(2316517, 2316497, 'Саяногорск', NULL, NULL),
(2411629, 5356, 'Излучинск', NULL, NULL),
(2413243, 4481, 'Заозерск', NULL, NULL),
(2415620, 2415585, 'Анадырь', NULL, NULL),
(2479040, 4105, 'Шарыпово', NULL, NULL),
(2499035, 2499002, 'Нижневартовск', NULL, NULL),
(2499082, 5080, 'Новоуральск', NULL, NULL),
(2812698, 5356, 'Муравленко', NULL, NULL),
(3118538, 4312, 'Королев', NULL, NULL),
(3138835, 3703, 'Чунский', NULL, NULL),
(3138841, 4312, 'Протвино', NULL, NULL),
(3145145, 3407, 'Таксимо', NULL, NULL),
(3408643, 5507, 'Трехгорный', NULL, NULL),
(3409478, 4105, 'Зеленогорск', NULL, NULL),
(3411630, 3872, 'Елизово', NULL, NULL),
(3411741, 3703, 'Култук', NULL, NULL),
(3473821, 5356, 'Ларьяк', NULL, NULL),
(3474011, 5225, 'Первомайский', NULL, NULL),
(3535020, 5356, 'Губкинский', NULL, NULL),
(3881459, 3371, 'Дятьково', NULL, NULL),
(4041912, 2499002, 'Лянтор', NULL, NULL),
(4093520, 5246, 'Альметьевск', NULL, NULL),
(4093692, 2316497, 'Абакан', NULL, NULL),
(4156345, 2499002, 'Югорск', NULL, NULL),
(4276360, 5356, 'Белоярский', NULL, NULL),
(4285366, 3784, 'Удомля', NULL, NULL),
(4285519, 5507, 'Озерск', NULL, NULL),
(4777963, 5052, 'Южно-Курильск', NULL, NULL),
(4778470, 4481, 'Снежногорск', NULL, NULL),
(4778496, 5080, 'Лесной', NULL, NULL),
(4778522, 2499002, 'Мегион', NULL, NULL),
(4869166, 5191, 'Железноводск', NULL, NULL),
(5019404, 5019394, 'Салехард', NULL, NULL),
(5020365, 5019394, 'Новый Уренгой', NULL, NULL),
(5020595, 4481, 'Полярные Зори', NULL, NULL),
(5020614, 4105, 'Сосновоборск', NULL, NULL),
(5020665, 4891, 'Сургут (Самарская обл.)', NULL, NULL),
(5020681, 2499002, 'Сургут', NULL, NULL),
(5252498, 5019394, 'Муравленко', NULL, NULL),
(5498759, 5019394, 'Губкинский', NULL, NULL),
(5555962, 3407, 'Северобайкальск', NULL, NULL),
(5556050, 3952, 'Вятские Поляны', NULL, NULL),
(5641077, 5019394, 'Пуровск', NULL, NULL),
(5908265, 3675, 'Кохма', NULL, NULL),
(5908547, 5019394, 'Надым', NULL, NULL),
(6113699, 2499002, 'Нефтеюганск', NULL, NULL),
(6454651, 5356, 'Мегион', NULL, NULL),
(6456567, 4105, 'Кодинск', NULL, NULL),
(6711408, 2415585, 'Билибино', NULL, NULL),
(7592215, 4800, 'Егорлыкская', NULL, NULL),
(7592923, 4312, 'Байконур', NULL, NULL),
(7593075, 4312, 'Краснознаменск', NULL, NULL),
(7593225, 2499002, 'Нягань', NULL, NULL),
(7717502, 2499002, 'Излучинск', NULL, NULL),
(7717702, 5019394, 'Ноябрьск', NULL, NULL),
(7718652, 2499002, 'Ханты-Мансийск', NULL, NULL),
(10498697, 5191, 'Солнечнодольск', NULL, NULL),
(10499269, 5019394, 'Тарко', NULL, NULL),
(10499672, 3872, 'Озерновский', NULL, NULL),
(10500023, 4925, 'Коммунар', NULL, NULL),
(10500094, 5191, 'Изобильный', NULL, NULL),
(10500382, 3468, 'Кириллов', NULL, NULL),
(10500476, 4891, 'Красный Яр', NULL, NULL),
(10501002, 5291, 'Северск', NULL, NULL),
(10502714, 4105, ' Железногорск', NULL, NULL),
(10504604, 4925, 'Пикалёво', NULL, NULL),
(10505475, 5191, 'Зеленокумск', NULL, NULL),
(10688837, 2499002, 'Покачи', NULL, NULL),
(10689767, 4734, 'Фокино', NULL, NULL),
(11567319, 2499002, 'Радужный', NULL, NULL),
(11569100, 2499002, 'Урай', NULL, NULL),
(11569394, 2499002, 'Пыть-Ях', NULL, NULL),
(11910622, 2499002, 'Игрим', NULL, NULL),
(11911051, 5356, 'Унъюган', NULL, NULL),
(12364794, 2499002, 'Лангепас', NULL, NULL),
(12370243, 2499002, 'Приобье', NULL, NULL),
(12370942, 3872, 'Вилючинск', NULL, NULL),
(12371093, 5019394, 'Заполярный', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_custom_html`
--

CREATE TABLE `tbl_custom_html` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `position` tinyint(4) NOT NULL COMMENT 'Позиция',
  `html` text COMMENT 'Html-код',
  `vis` tinyint(4) DEFAULT '1',
  `priority` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_custom_html`
--

INSERT INTO `tbl_custom_html` (`id`, `name`, `position`, `html`, `vis`, `priority`) VALUES
(1, 'Счетчики', 3, '<!-- custom html -->', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_delivery_types`
--

CREATE TABLE `tbl_delivery_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `tbl_delivery_types`
--

INSERT INTO `tbl_delivery_types` (`id`, `name`, `updated_at`, `created_at`) VALUES
(1, 'Курьер', '2017-06-08 10:17:08', '2017-06-08 10:17:08'),
(2, 'Служба доставки', '2017-06-08 10:17:19', '2017-06-08 10:17:19');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_faq`
--

CREATE TABLE `tbl_faq` (
  `id` int(11) NOT NULL,
  `question` text,
  `answer` text,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `vis` tinyint(1) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_faq`
--

INSERT INTO `tbl_faq` (`id`, `question`, `answer`, `name`, `email`, `phone`, `priority`, `vis`, `updated_at`, `created_at`) VALUES
(1, 'Почему очевидна не для всех вероятностная логика?', 'Ответ', '', 'test@test.ru', '', 1, 1, '2017-04-07 12:56:21', '2017-04-07 12:55:48'),
(2, 'Почему иллюзорна адаптация?', '', '', 'test@test.ru', '', 2, 1, '2017-04-07 12:57:18', '2017-04-07 12:57:18'),
(3, 'Почему амбивалентно отношение к современности?', 'Ответ', '', 'test@test.ru', '', 3, 1, '2017-04-07 12:57:41', '2017-04-07 12:57:32'),
(4, 'Вопрос *', NULL, 'test', 'fdsf@fsdfds.ru', NULL, 4, 0, '2017-04-11 12:10:38', '2017-04-11 12:10:38'),
(5, 'Вопрос *', NULL, 'test', 'fdsf@fsdfds.ru', NULL, 5, 0, '2017-04-11 12:12:07', '2017-04-11 12:12:07');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_feedback`
--

CREATE TABLE `tbl_feedback` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(55) DEFAULT NULL,
  `message` text NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `viewed` tinyint(1) NOT NULL DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_feedback`
--

INSERT INTO `tbl_feedback` (`id`, `name`, `email`, `phone`, `message`, `ip`, `viewed`, `updated_at`, `created_at`) VALUES
(1, 'Yoshio Holcomb', 'tiwyd@hotmail.com', NULL, 'Illo est totam et consequuntur minus libero quis sequi ut itaque explicabo Ipsum dicta molestiae qui adipisci corporis', '127.0.0.1', 0, '2017-08-29 00:00:00', '2017-08-29 00:00:00'),
(2, 'test', 'maadassda@esfs.ru', NULL, 'fdsfs', '127.0.0.1', 0, '2017-09-12 17:50:33', '2017-09-12 17:50:33'),
(3, 'test', 'maadassda@esfs.ru', NULL, 'fdsfs', '127.0.0.1', 0, '2017-09-12 17:51:43', '2017-09-12 17:51:43'),
(4, 'test', 'maadassda@esfs.ru', NULL, 'fdsfs', '127.0.0.1', 0, '2017-09-12 17:52:22', '2017-09-12 17:52:22'),
(5, 'fdsfsf', 'maadassda@esfs.ru', NULL, 'fdsfsf', '127.0.0.1', 0, '2017-09-12 18:06:01', '2017-09-12 18:06:01'),
(6, 'fdsfsf', 'maadassda@esfs.ru', NULL, 'fdsfsf', '127.0.0.1', 0, '2017-09-12 18:21:33', '2017-09-12 18:21:33');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_galleries`
--

CREATE TABLE `tbl_galleries` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url_alias` varchar(255) NOT NULL,
  `cover` varchar(255) DEFAULT NULL COMMENT 'Обложка',
  `vis` tinyint(1) NOT NULL DEFAULT '1',
  `priority` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_galleries`
--

INSERT INTO `tbl_galleries` (`id`, `name`, `url_alias`, `cover`, `vis`, `priority`, `updated_at`, `created_at`) VALUES
(1, 'Галерея', 'test', NULL, 1, 1, '2017-09-11 18:16:04', '2016-12-28 17:12:58'),
(2, 'Галерея 2', 'test2', NULL, 1, 2, '2017-03-23 10:03:46', '2017-01-23 16:07:27');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_gallery_images`
--

CREATE TABLE `tbl_gallery_images` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_gallery_images`
--

INSERT INTO `tbl_gallery_images` (`id`, `name`, `description`, `link`, `img`, `priority`, `gallery_id`) VALUES
(5, NULL, NULL, NULL, '0dd248ef54ef111edacddc63d9887e2a.jpg', 1, 2),
(21, 'grdrgdg', 'fdsfds', NULL, '1e8559c349368134d65115481a476b8e.jpg', 2, 1),
(22, NULL, NULL, NULL, '37b700019990ba7e004a43af3fc0b46b.jpg', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_image`
--

CREATE TABLE `tbl_image` (
  `id` int(11) NOT NULL,
  `file` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `priority` int(3) NOT NULL,
  `gid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `tbl_image`
--

INSERT INTO `tbl_image` (`id`, `file`, `name`, `link`, `description`, `priority`, `gid`) VALUES
(86, '147619570357fcf5778f4e6.jpg', '147619570357fcf5778f4e6.jpg', '', '', 0, 12),
(87, '14764329225800941a51ee3.jpg', '14764329225800941a51ee3.jpg', '', '', 0, 12),
(92, '147643488958009bc949c96.jpg', '147643488958009bc949c96.jpg', '', '', 2, 14),
(93, '147643488958009bc979197.jpg', '147643488958009bc979197.jpg', '', '', 1, 14),
(95, '14770383685809d120f404c.gif', '14770380205809cfc41c25b.jpg', '', '', 0, 13),
(96, '14770385295809d1c1bf50e.jpg', '14770385295809d1c1bf50e.jpg', '', '', 2, 13),
(97, '14770385305809d1c2c22b8.jpg', '14770385305809d1c2c22b8.jpg', '', '', 0, 13),
(98, '14770385305809d1c2ea229.jpg', '14770385305809d1c2ea229.jpg', '', '', 0, 13),
(99, '14770385315809d1c30e76c.jpg', '14770385315809d1c30e76c.jpg', '', '', 0, 13),
(100, '148000690758371cfb043e5.jpg', '148000690758371cfb043e5.jpg', '', '', 0, 13);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_logs`
--

CREATE TABLE `tbl_logs` (
  `id` int(11) NOT NULL,
  `module` varchar(255) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  `action` varchar(20) DEFAULT NULL,
  `attribute` varchar(255) DEFAULT NULL,
  `changed_data` mediumtext,
  `ip` varchar(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_logs`
--

INSERT INTO `tbl_logs` (`id`, `module`, `model`, `record_id`, `action`, `attribute`, `changed_data`, `ip`, `user_id`, `created_at`) VALUES
(1, 'blog', 'app\\modules\\blog\\models\\Post', 2, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 09:33:11'),
(2, 'blog', 'app\\modules\\blog\\models\\Post', 2, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 09:33:43'),
(3, 'blog', 'app\\modules\\blog\\models\\Post', 2, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 09:34:33'),
(4, 'sidebarblock', 'app\\modules\\sidebarblock\\models\\SidebarBlock', 1, 'create', NULL, NULL, '127.0.0.1', 9, '2017-08-29 09:40:23'),
(5, 'section', 'app\\modules\\section\\models\\Section', 2, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 09:40:51'),
(6, 'section', 'app\\modules\\section\\models\\Section', 2, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 09:40:55'),
(7, 'section', 'app\\modules\\section\\models\\Section', 2, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 09:41:04'),
(8, 'section', 'app\\modules\\section\\models\\Section', 2, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 09:41:11'),
(9, 'order', 'app\\modules\\order\\models\\Order', 1, 'delete', NULL, NULL, '127.0.0.1', 9, '2017-08-29 10:25:00'),
(10, 'blog', 'app\\modules\\blog\\models\\Post', 2, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 10:25:23'),
(11, 'blog', 'app\\modules\\blog\\models\\Post', 2, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 10:25:26'),
(12, 'news', 'app\\modules\\news\\models\\News', 2, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 14:11:49'),
(13, 'news', 'app\\modules\\news\\models\\News', 2, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 14:32:03'),
(14, 'news', 'app\\modules\\news\\models\\News', 2, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 14:32:06'),
(15, 'testimonials', 'app\\modules\\testimonials\\models\\Testimonial', 11, 'create', NULL, NULL, '127.0.0.1', 9, '2017-08-29 14:47:54'),
(16, 'section', 'app\\modules\\section\\models\\Section', 25, 'create', NULL, NULL, '127.0.0.1', 9, '2017-08-29 15:09:46'),
(17, 'section', 'app\\modules\\section\\models\\Section', 25, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 15:09:57'),
(18, 'section', 'app\\modules\\section\\models\\Section', 2, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 16:11:54'),
(19, 'section', 'app\\modules\\section\\models\\Section', 2, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 16:12:04'),
(20, 'admin', 'app\\modules\\admin\\models\\backend\\User', 12, 'delete', NULL, NULL, '127.0.0.1', 9, '2017-08-29 17:01:43'),
(21, 'admin', 'app\\modules\\admin\\models\\backend\\User', 9, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 17:01:55'),
(22, 'admin', 'app\\modules\\admin\\models\\backend\\User', 9, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 17:04:17'),
(23, 'admin', 'app\\modules\\admin\\models\\backend\\User', 9, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 17:08:57'),
(24, 'admin', 'app\\modules\\admin\\models\\backend\\User', 9, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 17:09:35'),
(25, 'admin', 'app\\modules\\admin\\models\\backend\\User', 9, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 17:10:41'),
(26, 'admin', 'app\\modules\\admin\\models\\backend\\User', 9, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 17:20:31'),
(27, 'admin', 'app\\modules\\admin\\models\\backend\\User', 9, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 17:37:59'),
(28, 'admin', 'app\\modules\\admin\\models\\backend\\User', 9, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 17:59:08'),
(29, 'admin', 'app\\modules\\admin\\models\\backend\\User', 9, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 17:59:15'),
(30, 'admin', 'app\\modules\\admin\\models\\backend\\User', 9, 'update', NULL, NULL, '127.0.0.1', 9, '2017-08-29 17:59:51'),
(31, 'news', 'app\\modules\\news\\models\\News', 1, 'update', NULL, NULL, '127.0.0.1', 9, '2017-09-07 15:23:07'),
(32, 'gallery', 'app\\modules\\gallery\\models\\Gallery', 1, 'update', NULL, NULL, '127.0.0.1', 9, '2017-09-07 16:38:15'),
(33, 'gallery', 'app\\modules\\gallery\\models\\Gallery', 1, 'update', NULL, NULL, '127.0.0.1', 9, '2017-09-07 16:38:20'),
(34, 'order', 'app\\modules\\order\\models\\Order', 1, 'delete', NULL, NULL, '127.0.0.1', 9, '2017-09-07 16:38:31'),
(35, 'rooms', 'app\\modules\\rooms\\models\\Room', 3, 'delete', NULL, NULL, '127.0.0.1', 9, '2017-09-07 16:45:11'),
(36, 'testimonials', 'app\\modules\\testimonials\\models\\Testimonial', 11, 'delete', NULL, NULL, '127.0.0.1', 9, '2017-09-07 17:01:45'),
(37, 'rooms', 'app\\modules\\rooms\\models\\Room', 1, 'update', NULL, NULL, '127.0.0.1', 9, '2017-09-07 17:51:41'),
(38, 'gallery', 'app\\modules\\gallery\\models\\Gallery', 1, 'update', NULL, NULL, '127.0.0.1', 9, '2017-09-11 11:48:40'),
(39, 'gallery', 'app\\modules\\gallery\\models\\Gallery', 1, 'update', NULL, NULL, '127.0.0.1', 9, '2017-09-11 11:49:06'),
(40, 'gallery', 'app\\modules\\gallery\\models\\Gallery', 1, 'update', NULL, NULL, '127.0.0.1', 9, '2017-09-11 11:49:15'),
(41, 'gallery', 'app\\modules\\gallery\\models\\Gallery', 1, 'update', NULL, NULL, '127.0.0.1', 9, '2017-09-11 11:51:57'),
(42, 'gallery', 'app\\modules\\gallery\\models\\Gallery', 1, 'update', NULL, NULL, '127.0.0.1', 9, '2017-09-11 11:53:18'),
(43, 'gallery', 'app\\modules\\gallery\\models\\Gallery', 1, 'update', NULL, NULL, '127.0.0.1', 9, '2017-09-11 12:00:29'),
(44, 'gallery', 'app\\modules\\gallery\\models\\Gallery', 1, 'update', NULL, NULL, '127.0.0.1', 9, '2017-09-11 18:16:04'),
(45, 'param', 'app\\modules\\param\\models\\backend\\Param', 3, 'update', NULL, NULL, '127.0.0.1', 9, '2017-09-12 17:49:21'),
(46, 'param', 'app\\modules\\param\\models\\backend\\Param', 3, 'update', NULL, NULL, '127.0.0.1', 9, '2017-09-12 17:52:12'),
(47, 'param', 'app\\modules\\param\\models\\backend\\Param', 3, 'update', NULL, NULL, '127.0.0.1', 9, '2017-09-12 18:05:23'),
(48, 'param', 'app\\modules\\param\\models\\backend\\Param', 3, 'update', NULL, NULL, '127.0.0.1', 9, '2017-09-12 18:05:23'),
(49, 'section', 'app\\modules\\section\\models\\Section', 2, 'update', NULL, NULL, '127.0.0.1', 9, '2017-09-14 15:45:58'),
(50, 'section', 'app\\modules\\section\\models\\Section', 2, 'update', NULL, NULL, '127.0.0.1', 9, '2017-09-14 15:46:12'),
(51, 'section', 'app\\modules\\section\\models\\Section', 2, 'update', NULL, NULL, '127.0.0.1', 9, '2017-09-14 17:52:53'),
(52, 'section', 'app\\modules\\section\\models\\Section', 2, 'update', NULL, NULL, '127.0.0.1', 9, '2017-09-14 17:52:59'),
(53, 'section', 'app\\modules\\section\\models\\Section', 2, 'update', NULL, NULL, '127.0.0.1', 9, '2017-09-14 17:54:28'),
(54, 'admin', 'app\\modules\\admin\\models\\backend\\User', 11, 'update', NULL, '{"E-mail":{"old_val":"support@webelement.ru","new_val":"tester@test.ru"}}', '127.0.0.1', 9, '2018-03-02 12:35:46'),
(55, 'admin', 'app\\modules\\admin\\models\\backend\\User', 10, 'update', NULL, '{"E-mail":{"old_val":"test3@webelement.ru","new_val":"manager@test.ru"}}', '127.0.0.1', 9, '2018-03-02 12:35:59'),
(56, 'admin', 'app\\modules\\admin\\models\\backend\\User', 9, 'update', NULL, '{"E-mail":{"old_val":"info@webelement2.ru","new_val":"skarah@mail.ru"}}', '127.0.0.1', 9, '2018-03-02 12:36:12');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_meta`
--

CREATE TABLE `tbl_meta` (
  `id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `h1_tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `og_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `og_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `og_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `og_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `og_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dc_subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dc_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dc_publisher_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dc_contributor` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `tbl_meta`
--

INSERT INTO `tbl_meta` (`id`, `url`, `meta_title`, `meta_description`, `meta_keywords`, `h1_tag`, `og_title`, `og_description`, `og_image`, `og_type`, `og_url`, `dc_subject`, `dc_description`, `dc_publisher_address`, `dc_contributor`) VALUES
(1, '111111111111111111', '343', '4343', '434', '343', '', '', '', '', '', '', '', '', ''),
(2, '2', '4454', '6456456', '', '', '', '', '', '', '', '', '', '', ''),
(3, 'new', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4, 'old', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_migration`
--

CREATE TABLE `tbl_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_migration`
--

INSERT INTO `tbl_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1457941546),
('m130524_201442_init', 1457941602),
('m140506_102106_rbac_init', 1457941839);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_modules`
--

CREATE TABLE `tbl_modules` (
  `id` int(11) NOT NULL,
  `module` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `menu_vis` tinyint(1) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `bootstrap` tinyint(1) NOT NULL DEFAULT '0',
  `viewed` tinyint(1) NOT NULL DEFAULT '0',
  `priority` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_modules`
--

INSERT INTO `tbl_modules` (`id`, `module`, `name`, `icon`, `menu_vis`, `active`, `bootstrap`, `viewed`, `priority`, `updated_at`, `created_at`) VALUES
(1, 'block', 'Блоки', '<i class="fa fa-object-ungroup"></i>', 1, 1, 0, 0, 4, NULL, '2017-08-23 11:04:39'),
(2, 'booking', 'Бронирование', '<i class="fa fa-key"></i>', 1, 1, 1, 1, 15, NULL, '2017-08-23 11:04:40'),
(3, 'extended', 'Редактирование html, css, js, robots', '', 0, 1, 0, 0, 24, NULL, '2017-08-23 11:04:40'),
(5, 'gallery', 'Галерея', '<i class="fa fa-image"></i>', 1, 1, 1, 0, 12, NULL, '2017-08-23 11:04:40'),
(6, 'param', 'Настройки', '', 0, 1, 0, 0, 23, NULL, '2017-08-23 11:04:40'),
(7, 'section', 'Разделы', '<i class="fa fa-file-text"></i>', 1, 1, 1, 0, 2, NULL, '2017-08-23 11:04:40'),
(8, 'snippet', 'Сниппеты', '', 0, 1, 0, 0, 25, NULL, '2017-08-23 11:04:40'),
(9, 'user', 'Пользователи', '<i class="fa fa-user"></i>', 1, 1, 0, 0, 20, NULL, '2017-08-23 11:04:40'),
(10, 'promo', 'Акции', '<i class="fa fa-heart-o"></i>', 1, 1, 1, 0, 8, NULL, '2017-08-23 11:04:40'),
(11, 'news', 'Новости', '<i class="fa fa-newspaper-o"></i>', 1, 1, 1, 0, 6, NULL, '2017-08-23 11:04:40'),
(12, 'feedback', 'Обратная связь', '<i class="fa fa-comments-o"></i>', 1, 1, 1, 1, 13, NULL, '2017-08-23 11:04:40'),
(13, 'rooms', 'Номера', '<i class="fa fa-bed"></i>', 1, 1, 1, 0, 10, NULL, '2017-08-23 11:04:40'),
(14, 'sidebarblock', 'Боковые блоки', '<i class="fa fa-object-ungroup"></i>', 1, 1, 0, 0, 5, NULL, '2017-08-23 11:04:40'),
(16, 'sitemap', 'Карта сайта', '', 0, 1, 1, 0, 27, NULL, '2017-08-23 11:04:40'),
(17, 'catalog', 'Каталог', '<i class="fa fa-shopping-cart"></i>', 1, 1, 1, 0, 18, NULL, '2017-08-23 11:04:40'),
(18, 'slider', 'Слайдер', '<i class="fa fa-arrows-h"></i>', 1, 1, 0, 0, 3, NULL, '2017-08-23 11:04:40'),
(19, 'callback', 'Обратный звонок', '<i class="fa fa-phone"></i>', 1, 1, 1, 1, 14, NULL, '2017-08-23 11:04:40'),
(20, 'logs', 'Логи', '<i class="fa fa-eye" aria-hidden="true"></i>', 1, 1, 0, 0, 21, NULL, '2017-08-23 11:04:40'),
(21, 'subscription', 'Подписчики', '<i class="fa fa-envelope"></i>', 1, 1, 1, 0, 16, NULL, '2017-08-23 11:04:40'),
(22, 'faq', 'FAQ', '<i class="fa fa-question-circle" aria-hidden="true"></i>', 1, 1, 1, 0, 11, NULL, '2017-08-23 11:04:40'),
(23, 'gitdeploy', 'Git deploy', '', 0, 1, 1, 0, 26, NULL, '2017-08-23 11:04:40'),
(24, 'article', 'Статьи', '<i class="fa fa-commenting-o"></i>', 1, 1, 1, 0, 9, NULL, '2017-08-23 11:04:40'),
(25, 'blog', 'Блог', '<i class="fa fa-commenting-o"></i>', 1, 1, 1, 0, 7, NULL, '2017-08-23 11:04:40'),
(26, 'location', 'Регионы/Города', '<i class="fa fa-map" aria-hidden="true"></i>', 1, 1, 0, 0, 19, NULL, '2017-08-23 11:04:40'),
(27, 'order', 'Заказы', '<i class="fa fa-phone"></i>', 1, 1, 1, 1, 17, NULL, '2017-08-23 11:04:40'),
(28, 'cabinet', 'Личный кабинет', NULL, 0, 1, 1, 0, 22, NULL, NULL),
(31, 'testimonials', 'Отзывы\n', '<i class="fa fa-comment"></i>', NULL, 1, 0, 0, 1, NULL, NULL),
(33, 'yandextranslator', 'Яндекс переводчик', NULL, NULL, 1, 0, 0, 28, NULL, NULL),
(34, 'cart', 'Корзина', NULL, NULL, 1, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_news`
--

CREATE TABLE `tbl_news` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url_alias` varchar(255) DEFAULT NULL,
  `annotation` text,
  `content` text,
  `img` varchar(255) DEFAULT NULL,
  `vis` tinyint(1) NOT NULL DEFAULT '1',
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `h1_tag` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_news`
--

INSERT INTO `tbl_news` (`id`, `name`, `url_alias`, `annotation`, `content`, `img`, `vis`, `meta_title`, `meta_description`, `meta_keywords`, `h1_tag`, `updated_at`, `created_at`) VALUES
(1, 'Эмпирический ряд Тейлора: гипотеза и теории', 'empiricheskiy-ryad-teylora-gipoteza-i-teorii', 'Математическая статистика упорядочивает абсолютно сходящийся ряд. Мир транспонирует абстрактный двойной интеграл. Нормальное распределение выводит стремящийся даосизм. Представляется логичным, что абсолютная погрешность принимает во внимание закон исключённого третьего. Постоянная величина, как принято считать, существенно трансформирует линейно зависимый бабувизм.', '<p>Математическая статистика упорядочивает абсолютно сходящийся ряд. Мир транспонирует абстрактный двойной интеграл. Нормальное распределение выводит стремящийся даосизм. Представляется&nbsp;логичным,&nbsp;что абсолютная погрешность принимает&nbsp;во&nbsp;внимание закон исключённого третьего. Постоянная величина, как принято считать, существенно трансформирует линейно зависимый бабувизм.</p>\r\n\r\n<p>Согласно&nbsp;мнению&nbsp;известных&nbsp;философов, лист Мёбиуса порождает&nbsp;и&nbsp;обеспечивает нормальный интеграл Гамильтона, таким образом сбылась мечта идиота - утверждение полностью доказано. Интеграл по бесконечной области рассматривается Наибольший Общий Делитель (НОД). Галактика,&nbsp;очевидно, транслирует гравитационный парадокс. Несмотря на сложности, векторное поле тривиально. Освобождение недоказуемо. Умножение двух векторов (скалярное), по определению, оспособляет равновероятный бином Ньютона, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения.</p>\r\n', 'dc6ca2f0f56074f5ebd895a0c8f1387f.jpg', 1, '', '', '', '', '2017-09-07 15:23:07', '2016-11-20 00:00:00'),
(2, 'Межатомный экситон: основные моменты', 'mezhatomnyy-eksiton-osnovnye-momenty', 'Можно предположить, что вихрь раскладывает на элементы торсионный катарсис. Заблуждение самопроизвольно. Гегельянство трогательно наивно. Разрыв рассматривается изотопный катарсис, хотя в официозе принято обратное.', '<p>Можно&nbsp;предположить,&nbsp;что вихрь раскладывает&nbsp;на&nbsp;элементы торсионный катарсис. Заблуждение самопроизвольно. Гегельянство трогательно&nbsp;наивно. Разрыв рассматривается изотопный катарсис, хотя&nbsp;в&nbsp;официозе&nbsp;принято&nbsp;обратное.</p>\r\n\r\n<p>Очевидно, что возмущение плотности пространственно порождает&nbsp;и&nbsp;обеспечивает гравитационный парадокс, ломая&nbsp;рамки&nbsp;привычных&nbsp;представлений. Электрон квазипериодично усиливает бабувизм. Пульсар, в первом приближении, творит позитивизм. Гетерогенная структура, как неоднократно наблюдалось при постоянном воздействии ультрафиолетового облучения, изоморфна&nbsp;времени.</p>\r\n', 'b3d8948a498393bde61af05fea18839a.jpg', 1, '', '', '', '', '2017-08-29 14:32:06', '2016-11-21 00:00:00'),
(3, 'Плазменное образование как течение среды', 'plazmennoe-obrazovanie-kak-techenie-sredy', 'Искусство упорядочивает закон внешнего мира, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Итак, ясно, что подмножество естественно создает типичный бабувизм. Бесконечно малая величина очевидна не для всех.', '<p>Искусство упорядочивает закон&nbsp;внешнего&nbsp;мира, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Итак, ясно, что подмножество естественно создает типичный бабувизм. Бесконечно малая величина очевидна&nbsp;не&nbsp;для&nbsp;всех.</p>\r\n\r\n<p>Наибольший Общий Делитель (НОД) преобразует тройной интеграл, учитывая опасность, которую представляли собой писания Дюринга для не окрепшего еще немецкого рабочего движения. Интеграл от функции, имеющий конечный разрыв стабилизирует из ряда вон выходящий метод последовательных приближений. Язык&nbsp;образов в принципе соответствует сложный смысл&nbsp;жизни. Теорема Ферма, следовательно, понимает&nbsp;под&nbsp;собой тригонометрический метод последовательных приближений.</p>\r\n\r\n<p>Идеи гедонизма занимают центральное место в утилитаризме Милля и Бентама, однако связное множество накладывает позитивизм. Интеграл Дирихле категорически порождает&nbsp;и&nbsp;обеспечивает смысл&nbsp;жизни. Реальность,&nbsp;общеизвестно, подрывает знак.</p>\r\n', 'cb44b4c9dd060823a3eb6058c21caa53.jpg', 1, NULL, NULL, NULL, NULL, '2016-11-30 12:27:57', '2016-11-15 00:00:00'),
(5, 'Тахионный гравитационный парадокс глазами современников', 'tahionnyy-gravitacionnyy-paradoks-glazami-sovremennikov', 'Лист Мёбиуса, как принято считать, дискредитирует субъективный закон исключённого третьего, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Матожидание, как следует из вышесказанного, переворачивает возрастающий знак. Искусство, как следует из вышесказанного, не критично.', '<p>Лист Мёбиуса, как принято считать, дискредитирует субъективный закон исключённого третьего, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Матожидание,&nbsp;как&nbsp;следует&nbsp;из&nbsp;вышесказанного, переворачивает возрастающий знак. Искусство,&nbsp;как&nbsp;следует&nbsp;из&nbsp;вышесказанного, не критично.</p>\r\n\r\n<p>Ассоциация изменяет комплексный математический анализ, в итоге приходим к логическому противоречию. Александрийская школа осмысленно подрывает вектор, ломая&nbsp;рамки&nbsp;привычных&nbsp;представлений. Число е,&nbsp;очевидно, индуцирует невероятный даосизм.</p>\r\n\r\n<p>Постоянная величина привлекает закон исключённого третьего, что несомненно приведет нас к истине. Знак основан&nbsp;на&nbsp;тщательном анализе. Функция выпуклая кверху реально раскладывает&nbsp;на&nbsp;элементы принцип&nbsp;восприятия.</p>\r\n', '', 1, NULL, NULL, NULL, NULL, '2016-11-24 00:26:27', '2016-11-10 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_orders`
--

CREATE TABLE `tbl_orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` text,
  `message` text,
  `pay_type_id` smallint(5) DEFAULT NULL,
  `delivery_type_id` smallint(5) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `viewed` tinyint(1) NOT NULL DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `tbl_orders`
--

INSERT INTO `tbl_orders` (`id`, `user_id`, `name`, `phone`, `email`, `address`, `message`, `pay_type_id`, `delivery_type_id`, `city_id`, `region_id`, `total`, `viewed`, `updated_at`, `created_at`) VALUES
(2, NULL, 'Cherokee Ochoa', '+151-53-4015498', 'beqypeq@gmail.com', NULL, 'Proident fugiat ipsum occaecat quas repudiandae commodo in asperiores laboris possimus itaque nostrum ex sit', NULL, NULL, 0, 0, '110.00', 0, '2017-08-30 11:36:05', '2017-08-30 11:36:05'),
(3, NULL, 'Alden Howard', '+546-35-2382237', 'kysex@yahoo.com', NULL, 'Placeat ut nesciunt rem nihil non consequatur Recusandae Et dolor in duis voluptate quis voluptatum', NULL, NULL, 0, 0, '110.00', 0, '2017-08-30 11:37:31', '2017-08-30 11:37:31'),
(4, NULL, 'ddddddd', '+7(767)564-67-33', 'email@tochka.ru', NULL, NULL, 1, 2, 3546, 3529, '90.00', 0, '2018-03-13 03:30:25', '2018-03-13 03:30:25');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_order_products`
--

CREATE TABLE `tbl_order_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `total` int(11) DEFAULT '1',
  `quantity` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_pages`
--

CREATE TABLE `tbl_pages` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url_alias` varchar(255) DEFAULT NULL,
  `content` text,
  `img` varchar(255) NOT NULL,
  `priority` int(11) DEFAULT NULL,
  `vis` tinyint(1) NOT NULL DEFAULT '1',
  `child_vis` tinyint(1) NOT NULL DEFAULT '1',
  `external_link` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `h1_tag` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_params`
--

CREATE TABLE `tbl_params` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `priority` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_params`
--

INSERT INTO `tbl_params` (`id`, `name`, `value`, `title`, `updated_at`, `created_at`, `priority`) VALUES
(2, 'meta_description', '', 'Описание сайта', '2017-08-25 11:24:15', NULL, 4),
(3, 'admin_email', 'skarah@mail.ru', 'Ящик для писем', '2018-03-02 12:17:22', NULL, 8),
(5, 'title_trail', 'Текст который всегда будет добавляться в конец заголовка(title для head)', 'Текст который всегда будет добавляться в конец заголовка(title для head)', '2017-08-25 11:24:15', NULL, 3),
(6, 'meta_keywords', '', 'Ключевые слова', '2017-08-25 11:24:15', NULL, 5),
(10, 'phone', '8 800 555-55-555', 'Телефон', '2017-08-25 11:24:15', NULL, 9),
(12, 'mainpage_title', 'Редактируется в настройках', 'Заголовок. Для главной страницы.', '2017-08-25 11:24:15', NULL, 2),
(13, 'yandex-verification', '', 'Код подтверждения. Яндекс', '2017-08-25 11:24:15', '2017-01-16 16:57:55', 6),
(14, 'google-verification', '', 'Код подтверждения. Google', '2017-08-25 11:24:15', '2017-01-16 17:22:29', 7),
(15, 'admin_site_name', 'На сайт', 'Название сайта в админке', '2017-08-29 15:30:57', '2017-02-05 21:03:52', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_pay_types`
--

CREATE TABLE `tbl_pay_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `priority` tinyint(4) DEFAULT '10'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `tbl_pay_types`
--

INSERT INTO `tbl_pay_types` (`id`, `name`, `updated_at`, `created_at`, `priority`) VALUES
(1, 'Наличные', '2017-06-08 10:14:05', '2017-06-08 10:14:05', NULL),
(2, 'Яндекс деньги', '2017-06-08 10:15:13', '2017-06-08 10:15:13', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_posts`
--

CREATE TABLE `tbl_posts` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url_alias` varchar(255) DEFAULT NULL,
  `annotation` text,
  `content` text,
  `author` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `vis` tinyint(1) NOT NULL DEFAULT '1',
  `img` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `h1_tag` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_posts`
--

INSERT INTO `tbl_posts` (`id`, `name`, `url_alias`, `annotation`, `content`, `author`, `user_id`, `vis`, `img`, `meta_title`, `meta_description`, `meta_keywords`, `h1_tag`, `updated_at`, `created_at`) VALUES
(2, 'Субъективный даосизм: основные моменты', 'subektivnyy-daosizm-osnovnye-momenty', 'Предмет деятельности, конечно, подчеркивает даосизм. Атомистика, следовательно, раскладывает на элементы неоднозначный интеллект. Можно предположить, что абстракция понимает под собой даосизм. Предмет деятельности, по определению, прост. Ощущение мира поразительно.', '<div class="referats__text" style="font-size: 15px; line-height: 1.6; font-family: &quot;Yandex Sans Text Web&quot;, sans-serif; background-color: rgb(255, 255, 255);">\r\n<div class="referats__text" style="line-height: 1.6;">\r\n<p>Предмет&nbsp;деятельности, конечно, подчеркивает даосизм. Атомистика, следовательно, раскладывает&nbsp;на&nbsp;элементы неоднозначный интеллект. Можно&nbsp;предположить,&nbsp;что абстракция понимает&nbsp;под&nbsp;собой даосизм. Предмет&nbsp;деятельности, по определению, прост. Ощущение&nbsp;мира поразительно.</p>\r\n\r\n<p>Позитивизм принимает&nbsp;во&nbsp;внимание закон&nbsp;внешнего&nbsp;мира. Исчисление предикатов раскладывает&nbsp;на&nbsp;элементы примитивный конфликт. Отвечая на вопрос о взаимоотношении идеального ли и материального ци, Дай Чжень заявлял, что закон&nbsp;внешнего&nbsp;мира принимает&nbsp;во&nbsp;внимание гедонизм, ломая&nbsp;рамки&nbsp;привычных&nbsp;представлений.</p>\r\n\r\n<p>Александрийская школа рефлектирует примитивный язык&nbsp;образов, изменяя&nbsp;привычную&nbsp;реальность. Гегельянство нетривиально. Мир нетривиален. Акциденция понимает&nbsp;под&nbsp;собой типичный интеллект.</p>\r\n</div>\r\n\r\n<div style="font-size: 12.8px;">\r\n<div class="referats__share" style="float: right;">\r\n<div class="share i-bem ya-share2 ya-share2_inited share_js_inited" style="line-height: normal;">\r\n<div class="ya-share2__container ya-share2__container_size_m" style="line-height: normal; font-size: 13px;">\r\n<ul style="list-style-type:none">\r\n	<li>&nbsp;</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n', NULL, 1, 1, '2b6662d6e70a249b2ba350a61f11e910.jpg', '', '', '', '', '2017-08-29 10:25:26', '2016-10-05 20:06:13'),
(3, 'Бабувизм как заблуждение', 'babuvizm-kak-zabluzhdenie', 'Созерцание методологически рефлектирует структурализм, хотя в официозе принято обратное. Исчисление предикатов порождает и обеспечивает структурализм. Закон внешнего мира неоднозначен.', '<p>Созерцание создает даосизм. Надстройка осмысляет сложный закон&nbsp;внешнего&nbsp;мира. Моцзы, Сюнъцзы и другие считали, что закон исключённого третьего неоднозначен. По своим философским взглядам Дезами был материалистом и атеистом, последователем Гельвеция, однако дуализм ясен&nbsp;не&nbsp;всем. Интересно&nbsp;отметить,&nbsp;что дедуктивный метод реально преобразует принцип&nbsp;восприятия. Гегельянство непредвзято осмысляет трансцендентальный даосизм, открывая&nbsp;новые&nbsp;горизонты.</p>\r\n\r\n<p>Созерцание методологически рефлектирует структурализм, хотя&nbsp;в&nbsp;официозе&nbsp;принято&nbsp;обратное. Исчисление предикатов порождает&nbsp;и&nbsp;обеспечивает структурализм. Закон&nbsp;внешнего&nbsp;мира неоднозначен.</p>\r\n\r\n<p>Апостериори, импликация естественно творит сложный даосизм, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире. Концепция раскладывает&nbsp;на&nbsp;элементы трансцендентальный дуализм. Заблуждение индуктивно контролирует структурализм, не учитывая мнения&nbsp;авторитетов. Исчисление предикатов дискредитирует принцип&nbsp;восприятия.</p>\r\n\r\n<p>Ассоциация очевидна&nbsp;не&nbsp;для&nbsp;всех. Жизнь, следовательно, естественно выводит гедонизм. Надо&nbsp;сказать,&nbsp;что гений транспонирует сенсибельный конфликт, tertium nоn datur. Дискретность, конечно, решительно осмысляет неоднозначный закон&nbsp;внешнего&nbsp;мира. Гедонизм трансформирует мир. Эсхатологическая идея откровенна.</p>\r\n', NULL, 1, 1, '1fe92969e6ce4c96da0650c3faa087fb.jpg', '', '', '', '', '2017-01-19 15:58:44', '2016-10-09 20:34:27'),
(7, 'Советы велосипедисту: как выбрать подходящий насос', 'sovety-velosipedistu-kak-vybrat-podhodyashchiy-nasos', '', '', NULL, NULL, 1, NULL, '', '', '', '', '2017-01-23 11:55:39', '2017-01-23 11:55:39');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_post_tags`
--

CREATE TABLE `tbl_post_tags` (
  `id` int(11) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_post_tags`
--

INSERT INTO `tbl_post_tags` (`id`, `post_id`, `tag_id`) VALUES
(1, 2, 3),
(3, 2, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_products`
--

CREATE TABLE `tbl_products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url_alias` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `old_price` int(11) DEFAULT NULL,
  `code` varchar(25) DEFAULT NULL,
  `description` text,
  `features` text,
  `vis` tinyint(1) NOT NULL DEFAULT '1',
  `new` tinyint(1) NOT NULL DEFAULT '0',
  `special` tinyint(1) NOT NULL DEFAULT '0',
  `category_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `h1_tag` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_products`
--

INSERT INTO `tbl_products` (`id`, `name`, `url_alias`, `price`, `old_price`, `code`, `description`, `features`, `vis`, `new`, `special`, `category_id`, `brand_id`, `meta_title`, `meta_description`, `meta_keywords`, `h1_tag`, `updated_at`, `created_at`) VALUES
(18, 'Название продукта4', 'nazvanie-produkta4', 220, NULL, '', 'Акциденция, на первый взгляд, отображает экстремум функции. Используя таблицу интегралов элементарных функций, получим: струя допускает ультрафиолетовый газ.', '<p>Акциденция, на первый взгляд, отображает экстремум функции. Используя таблицу интегралов элементарных функций, получим: струя допускает ультрафиолетовый газ. Интересно&nbsp;отметить,&nbsp;что реальная&nbsp;власть индуцирует комплексный разрыв функции, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире. Непосредственно из законов сохранения следует, что расслоение последовательно. Отношение&nbsp;к&nbsp;современности отталкивает барионный структурализм. Вещество небезынтересно создает магнит, генерируя периодические импульсы синхротронного излучения.</p>\r\n\r\n<p>Вселенная, исключая очевидный случай, устойчиво отражает изоморфный смысл&nbsp;жизни. Отсюда&nbsp;естественно&nbsp;следует,&nbsp;что сверхпроводник ясен&nbsp;не&nbsp;всем. Теорема, по данным астрономических наблюдений, индуцирует осциллятор.</p>\r\n', 1, 1, 0, 7, NULL, '', '', '', NULL, '2017-08-10 15:09:00', '2016-12-09 15:57:36'),
(19, 'Название продукта5', 'nazvanie-produkta5', 110, NULL, '', 'Акциденция, на первый взгляд, отображает экстремум функции. Используя таблицу интегралов элементарных функций, получим: струя допускает ультрафиолетовый газ. ', '<p>Акциденция, на первый взгляд, отображает экстремум функции. Используя таблицу интегралов элементарных функций, получим: струя допускает ультрафиолетовый газ. Интересно&nbsp;отметить,&nbsp;что реальная&nbsp;власть индуцирует комплексный разрыв функции, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире. Непосредственно из законов сохранения следует, что расслоение последовательно. Отношение&nbsp;к&nbsp;современности отталкивает барионный структурализм. Вещество небезынтересно создает магнит, генерируя периодические импульсы синхротронного излучения.</p>\r\n\r\n<p>Вселенная, исключая очевидный случай, устойчиво отражает изоморфный смысл&nbsp;жизни. Отсюда&nbsp;естественно&nbsp;следует,&nbsp;что сверхпроводник ясен&nbsp;не&nbsp;всем. Теорема, по данным астрономических наблюдений, индуцирует осциллятор.</p>\r\n', 1, 0, 0, 7, NULL, '', '', '', NULL, '2016-12-26 21:45:09', '2016-12-09 15:57:36'),
(22, 'Название продукта8', 'nazvanie-produkta8', 90, NULL, '', 'Реальность решительно творит дедуктивный метод. Вселенная иллюзорна. Освобождение представляет собой из ряда вон выходящий закон исключённого третьего.', '<p>Культ джайнизма включает в себя поклонение Махавире и другим тиртханкарам, поэтому реальность индуктивно порождает&nbsp;и&nbsp;обеспечивает даосизм, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире. Отношение&nbsp;к&nbsp;современности, как следует из вышесказанного, раскладывает&nbsp;на&nbsp;элементы сенсибельный дуализм. Отношение&nbsp;к&nbsp;современности индуктивно подрывает сложный гравитационный парадокс, открывая&nbsp;новые&nbsp;горизонты. Согласно&nbsp;предыдущему, дедуктивный метод индуцирует принцип&nbsp;восприятия.</p>\r\n\r\n<p>Здравый смысл непредвзято понимает&nbsp;под&nbsp;собой данный смысл&nbsp;жизни. Свобода порождена&nbsp;временем. Галактика нетривиальна.</p>\r\n', 1, 1, 0, 7, NULL, '', '', '', NULL, '2016-12-13 12:47:50', '2016-12-09 15:57:36'),
(23, 'Название продукта22', 'nazvanie-produkta9', 190, NULL, '', 'Вселенная, исключая очевидный случай, устойчиво отражает изоморфный смысл жизни. Отсюда естественно следует, что сверхпроводник ясен не всем. Теорема, по данным астрономических наблюдений, индуцирует осциллятор.', '<p>Акциденция, на первый взгляд, отображает экстремум функции. Используя таблицу интегралов элементарных функций, получим: струя допускает ультрафиолетовый газ. Интересно&nbsp;отметить,&nbsp;что реальная&nbsp;власть индуцирует комплексный разрыв функции, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире. Непосредственно из законов сохранения следует, что расслоение последовательно. Отношение&nbsp;к&nbsp;современности отталкивает барионный структурализм. Вещество небезынтересно создает магнит, генерируя периодические импульсы синхротронного излучения.</p>\r\n\r\n<p>Вселенная, исключая очевидный случай, устойчиво отражает изоморфный смысл&nbsp;жизни. Отсюда&nbsp;естественно&nbsp;следует,&nbsp;что сверхпроводник ясен&nbsp;не&nbsp;всем. Теорема, по данным астрономических наблюдений, индуцирует осциллятор.</p>\r\n', 1, 1, 0, 7, NULL, '', '', '', NULL, '2016-12-13 11:42:37', '2016-12-09 15:57:36'),
(24, 'Название продукта10', 'nazvanie-produkta10', 700, NULL, '', 'Созерцание, как принято считать, дискредитирует напряженный гений. Надо сказать, что ощущение мира философски подчеркивает смысл жизни. Ощущение мира философски подчеркивает здравый смысл. Автоматизация, по определению, раскладывает на элементы онтологический дедуктивный метод.', '<p>Отвечая на вопрос о взаимоотношении идеального ли и материального ци, Дай Чжень заявлял, что умножение двух векторов (скалярное) масштабирует бабувизм, при этом, вместо 13 можно взять любую другую константу. Гомогенная среда, в рамках ограничений классической механики, трансформирует вихревой бабувизм. Созерцание создает бабувизм. Интеграл по поверхности творит изоморфный закон исключённого третьего. Интересно&nbsp;отметить,&nbsp;что экстремум функции осмысляет закон&nbsp;внешнего&nbsp;мира.</p>\r\n', 1, 0, 0, 7, NULL, '', '', '', NULL, '2016-12-13 11:44:13', '2016-12-09 15:57:36');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_product_images`
--

CREATE TABLE `tbl_product_images` (
  `id` int(11) NOT NULL,
  `img` varchar(50) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `priority` tinyint(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_product_images`
--

INSERT INTO `tbl_product_images` (`id`, `img`, `product_id`, `priority`) VALUES
(7, '94130fc32dbe53b1b9cf40fb06282fb8.jpg', 24, 1),
(8, '4706df2715190498fb42f2fe599dc920.jpg', 24, 2),
(9, '23f0877d1c42cf3822deeaea7169503f.jpg', 24, 3),
(15, '750a604cc5311854425676f22d24a805.jpg', 19, 1),
(18, 'c7c65764cf9588e5ab39308feec2208d.jpg', 22, 1),
(19, 'ccbc11c3b112225627e2976c731c8b39.jpg', 18, 1),
(20, 'a3dc7f751fd6ad2f091c9e6f9a6d5993.jpg', 18, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_promo`
--

CREATE TABLE `tbl_promo` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url_alias` varchar(255) DEFAULT NULL,
  `description` text,
  `content` text,
  `img` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `vis` tinyint(1) NOT NULL DEFAULT '1',
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `h1_tag` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_promo`
--

INSERT INTO `tbl_promo` (`id`, `name`, `url_alias`, `description`, `content`, `img`, `file`, `vis`, `meta_title`, `meta_description`, `meta_keywords`, `h1_tag`, `updated_at`, `created_at`) VALUES
(1, 'Оздоровительные путевки!', 'ozdorovitelnye-putevki', 'С 1 ноября по 31 декабря 2014 года санаторий «Рога и копыта» предлагает оздоровительные путевки по минимальной стоимости &mdash; от 2000 рублей!', '<div class="right dashed1" style="\r\n    border: 2px dashed #FFD941;\r\n    padding: 16px;\r\n    float: right;\r\n    width: 35%;\r\n    margin-left: 20px;\r\n                                  \r\n    padding-top: 11px;\r\n    margin-bottom: 20px;\r\n">\r\n<h3>Оздоровительная путевка включает:</h3>\r\nпроживание, питание, обследование и перечень процедур по программе &laquo;Оздоровительная&raquo;.</div>\r\n\r\n<div style="\r\n    display: inline-block;  \r\n    margin-bottom: 20px;\r\n    width: 54%;\r\n    float: left;\r\n">\r\n<h3>С 1 ноября по 31 декабря 2014 года санаторий &laquo;Буковая Роща&raquo; предлагает оздоровительные путевки по минимальной стоимости &mdash; от 2000 рублей!</h3>\r\n</div>\r\n\r\n<hr />\r\n<h2>Перечень медицинских услуг по программе &laquo;Оздоровительная&raquo;</h2>\r\n\r\n<table class="tableclass">\r\n	<tbody>\r\n		<tr>\r\n			<th rowspan="2" style="text-align:center">№</th>\r\n			<th rowspan="2" style="text-align:center">Перечень мед. услуг</th>\r\n			<th colspan="3" style="text-align:center">Количество процедур</th>\r\n		</tr>\r\n		<tr>\r\n			<th style="text-align:center">14 дней</th>\r\n		</tr>\r\n		<tr>\r\n			<td>1</td>\r\n			<td>Прием лечащего врача-терапевта или гастроэнтеролога</td>\r\n			<td>2</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2</td>\r\n			<td>Прием медсестры</td>\r\n			<td>2</td>\r\n		</tr>\r\n		<tr>\r\n			<td>3</td>\r\n			<td>Анализ крови клинический</td>\r\n			<td>1</td>\r\n		</tr>\r\n		<tr>\r\n			<td>4</td>\r\n			<td>Анализ мочи клинический</td>\r\n			<td>1</td>\r\n		</tr>\r\n		<tr>\r\n			<td>5</td>\r\n			<td>Электрокардиография (ЭКГ)</td>\r\n			<td>1</td>\r\n		</tr>\r\n		<tr>\r\n			<td>6</td>\r\n			<td>Питьевое лечение минер. водой 3 р. в день</td>\r\n			<td>14</td>\r\n		</tr>\r\n		<tr>\r\n			<td>7</td>\r\n			<td>Диетическое питание трехразовое</td>\r\n			<td>14</td>\r\n		</tr>\r\n		<tr>\r\n			<td>8</td>\r\n			<td>Терренкур</td>\r\n			<td>14</td>\r\n		</tr>\r\n		<tr>\r\n			<td>9</td>\r\n			<td>Аппаратная физиотерапия</td>\r\n			<td>6</td>\r\n		</tr>\r\n		<tr>\r\n			<td>10</td>\r\n			<td>Микроклизмы с отваром трав и масляные</td>\r\n			<td>6</td>\r\n		</tr>\r\n		<tr>\r\n			<td>11</td>\r\n			<td>Ингаляции</td>\r\n			<td>6</td>\r\n		</tr>\r\n		<tr>\r\n			<td>12</td>\r\n			<td>Тренажерный зал (30 мин)</td>\r\n			<td>14</td>\r\n		</tr>\r\n		<tr>\r\n			<td>13</td>\r\n			<td>Бассейн (60 мин)</td>\r\n			<td>10</td>\r\n		</tr>\r\n		<tr>\r\n			<td>14</td>\r\n			<td>Неотложная медицинская помощь*</td>\r\n			<td>+</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<div class="footnote" style="\r\n    margin-top: 13px;\r\n    padding: 20px;\r\n">\r\n<p>*Оказание неотложной помощи входит в стоимость санаторно&mdash;курортной путевки.<br />\r\nДальнейшее медикаментозное лечение осуществляется за счет пациента.</p>\r\n</div>\r\n', '91c23975ee0aab32925aca6c1d5c2147.jpg', '', 1, NULL, NULL, NULL, NULL, '2016-11-30 12:15:38', '2016-11-24 14:55:46'),
(2, 'Бронируйте путевки', 'broniruyte-putevki', 'С 10 марта при бронировании путевки за месяц до заезда &mdash; скидка 20 %. При бронировании за 2 месяца до заезда &mdash; скидка 25 %. ', '<div class="review noava block850" style="width: 22%;float: right;padding: 0 32px 14px 59px;">\r\n<div class="author"><strong>За подробной информацией обращайтесь в отдел реализации путевок.</strong></div>\r\n</div>\r\n\r\n<div style="\r\n    display: inline-block;  \r\n    margin-bottom: 20px;\r\n    width: 65%;\r\n    float: left;\r\n">\r\n<ul>\r\n	<li>\r\n	<h3>С 10 марта при бронировании путевки за месяц до заезда - скидка 20%;</h3>\r\n	</li>\r\n	<li>\r\n	<h3>При бронировании за 2 месяца до заезда - скидка 25%.</h3>\r\n	</li>\r\n</ul>\r\n</div>\r\n\r\n<hr />\r\n<h3>За подробной информацией обращайтесь в отдел реализации путевок.</h3>\r\n', 'cf8a45019fe7befa21300d7bed987a2f.jpg', NULL, 1, '', '', '', '', '2017-03-26 22:06:15', '2016-11-24 15:02:04'),
(4, 'Положение о дисконтной программе', 'polozhenie-o-diskontnoy-programme', 'Дисконтная программа ООО «Рога и копыта» предназначена для постоянных клиентов в виде предоставления им льготных условий на санаторно-курортные путевки, предлагаемые ООО «Буковая роща».', '<div class="right dashed1" style="\r\n    border: 2px dashed #FFD941;\r\n    padding: 20px;\r\n    float: right;\r\n    width: 40%;\r\n    margin-left: 20px;\r\n">\r\n<p>Карта постоянного гостя не является кредитной, платежной или банковской.</p>\r\n</div>\r\n\r\n<h3>Дисконтная программа ООО &laquo;Буковая роща&raquo; предназначена для постоянных клиентов в виде предоставления им льготных условий на санаторно-курортные путевки, предлагаемые ООО &laquo;Буковая роща&raquo;.</h3>\r\n\r\n<div class="footnote" style="\r\n    margin-top: 4px;\r\n">\r\n<h4>Действие системы скидок на покупателей, которые не предъявили свою карту постоянного гостя, не распространяется.</h4>\r\n</div>\r\n\r\n<ol>\r\n	<li><strong>Общие положения:</strong></li>\r\n	<li>Действие настоящей системы скидок распространяется на покупателей санаторно-курортных путевок в ООО &laquo;Буковая роща&raquo;.</li>\r\n	<li>Дисконтная программа предусматривает скидки для постоянных клиентов по &laquo;карте постоянного гостя&raquo;.</li>\r\n	<li>Карта постоянного гостя действует при расчетах с физическими лицами. Действие карты не распространяется на юридические лица.</li>\r\n	<li>Карта постоянного гостя является собственностью ООО &laquo;Буковая роща&raquo;.</li>\r\n	<li>ООО &laquo;Буковая роща&raquo; оставляет за собой право вносить изменения в правила работы с картой постоянного гостя с предварительным уведомлением клиентов не менее чем за 30 дней до вступления в силу решения об изменении условий выдачи или условий предоставления скидок по картам постоянного гостя, если эти условия ограничивают ранее продекларированные права гостя.</li>\r\n	<li>Карта постоянного гостя имеет индивидуальный номер, является именной и не может передаваться другому лицу для последующего использования. &nbsp;</li>\r\n	<li>Карта постоянного гостя дает возможность приобрести санаторно-курортные путевки для близких людей (до 3-х человек) со скидкой, соответствующей размеру скидки предъявленной карты, при одновременном проживании с участником программы.</li>\r\n</ol>\r\n\r\n<ol>\r\n	<li><strong>Порядок получения дисконтной карты.</strong></li>\r\n	<li>Карта постоянного гостя выдается и действует для физического лица, совершившего покупку санаторно-курортной путевки ООО &laquo;Буковая роща&raquo;,продолжительностью не менее 14 дней.</li>\r\n	<li>Обладателем карты постоянного гостя становится Покупатель, сделавший покупку санаторно-курортной путевки в 3-ий раз без предъявления какой-либо другой карты постоянного гостя за наличный, безналичный расчет в ООО &laquo;Буковая роща&raquo;.</li>\r\n	<li>Для получения карты постоянного гостя необходимо заполнить персональную анкету-заявление (Приложение 1).</li>\r\n	<li>Карта постоянного гостя находится у Покупателя, каждая имеет индивидуальный номер, указанный на ней.</li>\r\n	<li>При утрате карту постоянного гостя можно восстановить в том случае, если была заполнена персональная анкета-заявление.</li>\r\n	<li>Одному лицу может принадлежать только одна карта постоянного гостя.</li>\r\n	<li><strong>Порядок пользования картой постоянного гостя.</strong></li>\r\n	<li>Для получения скидки, предусмотренной настоящей системой скидок, покупатель при покупке санаторно-курортной путевки обязательно должен предъявить карту постоянного гостя до момента оплаты, либо одновременно с оплатой в кассе ООО &laquo;Буковая роща&raquo;.</li>\r\n</ol>\r\n', '4bf70af09fd78fe0e3cefbb791d5e1b0.jpg', NULL, 1, NULL, NULL, NULL, NULL, '2016-12-28 00:43:15', '2016-11-24 15:03:28');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_regions`
--

CREATE TABLE `tbl_regions` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL DEFAULT '',
  `priority` int(11) UNSIGNED NOT NULL DEFAULT '99',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_regions`
--

INSERT INTO `tbl_regions` (`id`, `name`, `priority`, `created_at`, `updated_at`) VALUES
(3160, 'Алтайский край', 99, NULL, NULL),
(3223, 'Амурская обл.', 99, NULL, NULL),
(3251, 'Архангельская обл.', 99, NULL, NULL),
(3282, 'Астраханская обл.', 99, NULL, NULL),
(3296, 'Башкортостан(Башкирия)', 99, NULL, NULL),
(3352, 'Белгородская обл.', 99, NULL, NULL),
(3371, 'Брянская обл.', 99, NULL, NULL),
(3407, 'Бурятия', 99, NULL, NULL),
(3437, 'Владимирская обл.', 99, NULL, NULL),
(3468, 'Волгоградская обл.', 99, NULL, NULL),
(3503, 'Вологодская обл.', 99, NULL, NULL),
(3529, 'Воронежская обл.', 99, NULL, NULL),
(3563, 'Нижегородская (Горьковская)', 99, NULL, NULL),
(3630, 'Дагестан', 99, NULL, NULL),
(3673, 'Еврейская обл.', 99, NULL, NULL),
(3675, 'Ивановская обл.', 99, NULL, NULL),
(3703, 'Иркутская обл.', 99, NULL, NULL),
(3751, 'Кабардино-Балкария', 99, NULL, NULL),
(3761, 'Калининградская обл.', 99, NULL, NULL),
(3784, 'Тверская обл.', 99, NULL, NULL),
(3827, 'Калмыкия', 99, NULL, NULL),
(3841, 'Калужская обл.', 99, NULL, NULL),
(3872, 'Камчатская обл.', 99, NULL, NULL),
(3892, 'Карелия', 99, NULL, NULL),
(3921, 'Кемеровская обл.', 99, NULL, NULL),
(3952, 'Кировская обл.', 99, NULL, NULL),
(3994, 'Коми', 99, NULL, NULL),
(4026, 'Костромская обл.', 99, NULL, NULL),
(4052, 'Краснодарский край', 99, NULL, NULL),
(4105, 'Красноярский край', 99, NULL, NULL),
(4176, 'Курганская обл.', 99, NULL, NULL),
(4198, 'Курская обл.', 99, NULL, NULL),
(4227, 'Липецкая обл.', 99, NULL, NULL),
(4243, 'Магаданская обл.', 99, NULL, NULL),
(4270, 'Марий Эл', 99, NULL, NULL),
(4287, 'Мордовия', 99, NULL, NULL),
(4312, 'Москва и Московская обл.', 1, NULL, NULL),
(4481, 'Мурманская обл.', 99, NULL, NULL),
(4503, 'Новгородская обл.', 99, NULL, NULL),
(4528, 'Новосибирская обл.', 99, NULL, NULL),
(4561, 'Омская обл.', 99, NULL, NULL),
(4593, 'Оренбургская обл.', 99, NULL, NULL),
(4633, 'Орловская обл.', 99, NULL, NULL),
(4657, 'Пензенская обл.', 99, NULL, NULL),
(4689, 'Пермская обл.', 99, NULL, NULL),
(4734, 'Приморский край', 99, NULL, NULL),
(4773, 'Псковская обл.', 99, NULL, NULL),
(4800, 'Ростовская обл.', 99, NULL, NULL),
(4861, 'Рязанская обл.', 99, NULL, NULL),
(4891, 'Самарская обл.', 99, NULL, NULL),
(4925, 'Санкт-Петербург и область', 2, NULL, NULL),
(4969, 'Саратовская обл.', 99, NULL, NULL),
(5011, 'Саха (Якутия)', 99, NULL, '2017-03-23 10:52:09'),
(5052, 'Сахалин', 99, NULL, NULL),
(5080, 'Свердловская обл.', 99, NULL, NULL),
(5151, 'Северная Осетия', 99, NULL, NULL),
(5161, 'Смоленская обл.', 99, NULL, NULL),
(5191, 'Ставропольский край', 99, NULL, NULL),
(5225, 'Тамбовская обл.', 99, NULL, NULL),
(5246, 'Татарстан', 99, NULL, NULL),
(5291, 'Томская обл.', 99, NULL, NULL),
(5312, 'Тува (Тувинская Респ.)', 99, NULL, NULL),
(5326, 'Тульская обл.', 99, NULL, NULL),
(5356, 'Тюменская обл.', 99, NULL, NULL),
(5404, 'Удмуртия', 99, NULL, NULL),
(5432, 'Ульяновская обл.', 99, NULL, NULL),
(5458, 'Уральская обл.', 99, NULL, NULL),
(5473, 'Хабаровский край', 99, NULL, NULL),
(5507, 'Челябинская обл.', 99, NULL, NULL),
(5543, 'Чечено-Ингушетия', 99, NULL, NULL),
(5555, 'Читинская обл.', 99, NULL, NULL),
(5600, 'Чувашия', 99, NULL, NULL),
(5625, 'Ярославская обл.', 99, NULL, NULL),
(10227, 'Крым', 99, NULL, NULL),
(1998532, 'Адыгея', 99, NULL, NULL),
(2316497, 'Хакасия', 99, NULL, NULL),
(2415585, 'Чукотский АО', 99, NULL, NULL),
(2499002, 'Ханты-Мансийский АО', 99, NULL, NULL),
(5019394, 'Ямало-Ненецкий АО', 99, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_rooms`
--

CREATE TABLE `tbl_rooms` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url_alias` varchar(255) DEFAULT NULL,
  `price_single` int(11) DEFAULT NULL,
  `description_block` text,
  `content` text NOT NULL,
  `description` text,
  `priority` tinyint(3) DEFAULT NULL,
  `vis` tinyint(1) NOT NULL DEFAULT '1',
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` text,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `h1_tag` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_rooms`
--

INSERT INTO `tbl_rooms` (`id`, `name`, `url_alias`, `price_single`, `description_block`, `content`, `description`, `priority`, `vis`, `meta_title`, `meta_description`, `meta_keywords`, `h1_tag`, `updated_at`, `created_at`) VALUES
(1, 'Стандартный номер', 'standartnyiynomer', 1000, '<div class="b-block-gray">\r\n            <div class="b-block-gray-inner">\r\n                <div class="b-block-gray-right">\r\n                    <h4>Дополнительно</h4>\r\n\r\n                    <ul>\r\n                        <li>Скидка 20 % детям до 5 лет</li>\r\n                        <li>Скидка 10 % детям с 5 до 14 лет</li>\r\n                        <li>Дополнительное место &mdash; 10 %</li>\r\n                        <li>Уборка в номере 1 раз в день</li>\r\n                        <li>Расчетный час &mdash; 8:00</li>\r\n                    </ul>\r\n                </div><!--.pull-right-->\r\n\r\n                <div class="b-block-gray-left">\r\n\r\n                    <h4>Что входит в стоимость проживания?</h4>\r\n\r\n                    <div class="b-block-icon">\r\n                        <i class="b-icon treatment pull-left"></i>\r\n\r\n                        <div class="b-block-icon-text">\r\n                            Процедуры в соответствии с одной из<br>\r\n                            <a href="#">лечебных программ</a>\r\n                        </div><!--.b-block-icon-text-->\r\n\r\n                        <div class="clearfix"></div>\r\n                    </div><!--.b-block-icon-->\r\n\r\n                    <div class="b-block-icon">\r\n                        <i class="b-icon food pull-left"></i>\r\n\r\n                        <div class="b-block-icon-text">\r\n                            Двухразовое диетическое питание в<br>\r\n                            <a href="#">столовой санатория</a>\r\n                        </div><!--.b-block-icon-text-->\r\n\r\n                        <div class="clearfix"></div>\r\n                    </div><!--.b-block-icon-->\r\n\r\n                </div><!--.b-block-gray-left-->\r\n\r\n                <div class="clearfix"></div>\r\n            </div><!--.b-block-gray-inner-->\r\n        </div>', '<p>При погружении в жидкий кислород созерцание вертикально отражает структурализм, открывая&nbsp;новые&nbsp;горизонты. Искусство создает даосизм. Зеркало категорически испускает позитивистский гидродинамический удар. Атом ясен&nbsp;не&nbsp;всем. Тело преобразует институциональный электрон.</p>\r\n\r\n<p>Возмущение плотности конфокально рассматривается барионный объект. Любовь, согласно традиционным представлениям, ментально поглощает ролевой онтогенез речи. Акциденция преобразует квазар. Гомеостаз, в представлении Морено, транспонирует интеллект. Зеркало отрицательно заряжено. Эриксоновский гипноз ускоряет автоматизм.</p>\r\n', 'Идеи гедонизма занимают центральное место в утилитаризме Милля и Бентама, однако дедуктивный метод контролирует интеллект. Искусство рефлектирует гедонизм. Единственной космической субстанцией Гумбольдт считал материю, наделенную внутренней активностью, несмотря на это свобода заполняет позитивизм', 2, 1, '', '', '', '', '2017-09-07 17:51:40', NULL),
(5, 'Test Name', 'test-name', 12345, NULL, 'Test Content From Codeception', '', 4, 1, NULL, NULL, NULL, NULL, '2016-12-28 00:43:29', '2016-12-18 22:26:03');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_room_images`
--

CREATE TABLE `tbl_room_images` (
  `id` int(11) NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  `img` varchar(50) DEFAULT NULL,
  `priority` tinyint(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_room_images`
--

INSERT INTO `tbl_room_images` (`id`, `room_id`, `img`, `priority`) VALUES
(9, 1, 'f4fd463f52947eec5e97c43963ea0346.jpg', 1),
(22, 5, '9fe43b89dee675de70065db2409cca68.jpg', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_sections`
--

CREATE TABLE `tbl_sections` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `url_alias` varchar(255) DEFAULT NULL,
  `content` mediumtext,
  `img` varchar(255) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `vis` tinyint(1) NOT NULL DEFAULT '1',
  `menu_vis` tinyint(1) NOT NULL DEFAULT '1',
  `child_vis` tinyint(1) NOT NULL DEFAULT '1',
  `external_link` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `h1_tag` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_sections`
--

INSERT INTO `tbl_sections` (`id`, `parent_id`, `name`, `url_alias`, `content`, `img`, `priority`, `vis`, `menu_vis`, `child_vis`, `external_link`, `meta_title`, `meta_description`, `meta_keywords`, `h1_tag`, `updated_at`, `created_at`) VALUES
(2, NULL, 'Услуги', 'uslugi', '<p>Бабувизм выводит трагический знак. Даосизм раскладывает&nbsp;на&nbsp;элементы неоднозначный гедонизм. Предмет&nbsp;деятельности нетривиален.</p>\r\n\r\n<p>Гегельянство трансформирует сложный смысл&nbsp;жизни. Моцзы, Сюнъцзы и другие считали, что акциденция решительно рассматривается данный бабувизм. Даосизм прост. Априори, катарсис решительно подрывает интеллигибельный гравитационный парадокс, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Освобождение реально заполняет сложный даосизм. Априори, здравый смысл раскладывает&nbsp;на&nbsp;элементы сенсибельный гедонизм.</p>\r\n', '1417fa9529cf758de4debe308ee1c099.jpg', 2, 1, 1, 1, NULL, '', '', '', '', '2017-09-14 17:54:28', '2016-12-26 18:42:27'),
(3, NULL, 'Контакты', 'contacts', '', NULL, 3, 1, 1, 1, NULL, '', '', '', '', '2017-04-11 12:03:05', '2016-12-26 18:48:18'),
(11, 13, 'Номера', 'rooms', '', NULL, 0, 1, 1, 1, NULL, '', '', '', '', '2017-01-31 23:46:48', '2017-01-31 16:42:34'),
(13, NULL, 'Пример модулей', 'dlyatesta', '<p>Гегельянство выводит экситон вне зависимости от предсказаний самосогласованной теоретической модели явления. Вещество вертикально стабилизирует расширяющийся кристалл. Струя, следовательно, искажает расширяющийся конфликт. Исследователями из разных лабораторий неоднократно наблюдалось, как сверхпроводник решительно отклоняет нестационарный разрыв. Катарсис амбивалентно оспособляет коллапсирующий принцип&nbsp;восприятия, ломая&nbsp;рамки&nbsp;привычных&nbsp;представлений.</p>\r\n\r\n<p>При наступлении резонанса струя принципиально неизмерима. Единственной космической субстанцией Гумбольдт считал материю, наделенную внутренней активностью, несмотря на это отношение&nbsp;к&nbsp;современности непредсказуемо. Заблуждение виртуально. Поверхность, как следует из вышесказанного, непредвзято рассматривается барионный закон&nbsp;внешнего&nbsp;мира. Экситон, по определению, творит принцип&nbsp;восприятия.</p>\r\n', NULL, 12, 1, 1, 1, NULL, '', '', '', '', '2017-02-09 23:56:19', '2017-01-31 23:42:59'),
(14, 13, 'Новости', 'news', '', NULL, 0, 1, 1, 1, NULL, '', '', '', '', '2017-02-01 00:24:55', '2017-01-31 23:45:09'),
(15, 13, 'Отзывы', 'testimonials', '', NULL, 10, 1, 1, 1, NULL, '', '', '', '', '2017-02-09 23:55:45', '2017-02-01 00:01:52'),
(17, 13, 'Акции', 'promo', '', NULL, 50, 1, 1, 1, NULL, '', '', '', '', '2017-02-09 23:59:42', '2017-02-01 11:11:22'),
(18, 13, 'Обратная связь', 'feedback', '', NULL, 22, 1, 1, 1, NULL, '', '', '', '', '2017-02-09 23:56:02', '2017-02-01 12:26:06'),
(23, NULL, 'Каталог', 'catalog', '', '0c822837e72d2e8c928ab75b87774602.jpg', 51, 1, 1, 1, NULL, '', '', '', '', '2017-03-23 12:59:13', '2017-02-11 23:01:50'),
(24, NULL, 'FAQ', 'faq', '', NULL, 52, 1, 1, 1, NULL, '', '', '', '', '2017-08-24 15:18:20', '2017-03-27 10:31:14'),
(25, 13, 'Блог', 'blog', '', NULL, 53, 1, 1, 1, NULL, '', '', '', '', '2017-08-29 15:09:57', '2017-08-29 15:09:46');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_sections_history`
--

CREATE TABLE `tbl_sections_history` (
  `id` int(11) NOT NULL,
  `model_id` int(11) DEFAULT NULL,
  `model_name` varchar(255) DEFAULT NULL,
  `history_hash` varchar(35) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `url_alias` varchar(255) DEFAULT NULL,
  `content` text,
  `img` varchar(255) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `vis` tinyint(1) NOT NULL DEFAULT '1',
  `menu_vis` tinyint(1) NOT NULL DEFAULT '1',
  `child_vis` tinyint(1) DEFAULT NULL,
  `external_link` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `h1_tag` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_sections_history`
--

INSERT INTO `tbl_sections_history` (`id`, `model_id`, `model_name`, `history_hash`, `parent_id`, `name`, `url_alias`, `content`, `img`, `priority`, `vis`, `menu_vis`, `child_vis`, `external_link`, `meta_title`, `meta_description`, `meta_keywords`, `h1_tag`, `updated_at`, `created_at`) VALUES
(9, 2, 'app\\modules\\section\\models\\Section', NULL, NULL, 'Услуги', 'uslugi', '<p>Бабувизм выводит трагический знак. Даосизм раскладывает&nbsp;на&nbsp;элементы неоднозначный гедонизм. Предмет&nbsp;деятельности нетривиален.</p>\r\n\r\n<p>Гегельянство трансформирует сложный смысл&nbsp;жизни. Моцзы, Сюнъцзы и другие считали, что акциденция решительно рассматривается данный бабувизм. Даосизм прост. Априори, катарсис решительно подрывает интеллигибельный гравитационный парадокс, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Освобождение реально заполняет сложный даосизм. Априори, здравый смысл раскладывает&nbsp;на&nbsp;элементы сенсибельный гедонизм.</p>\r\n', NULL, 2, 1, 1, 1, NULL, '', '', '', 'H1 Tag', '2017-09-14 15:45:58', '2016-12-26 18:42:27'),
(10, 2, 'app\\modules\\section\\models\\Section', NULL, NULL, 'Услуги', 'uslugi', '<p>Бабувизм выводит трагический знак. Даосизм раскладывает&nbsp;на&nbsp;элементы неоднозначный гедонизм. Предмет&nbsp;деятельности нетривиален.</p>\r\n\r\n<p>Гегельянство трансформирует сложный смысл&nbsp;жизни. Моцзы, Сюнъцзы и другие считали, что акциденция решительно рассматривается данный бабувизм. Даосизм прост. Априори, катарсис решительно подрывает интеллигибельный гравитационный парадокс, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Освобождение реально заполняет сложный даосизм. Априори, здравый смысл раскладывает&nbsp;на&nbsp;элементы сенсибельный гедонизм.</p>\r\n', NULL, 2, 1, 1, 1, NULL, '', '', '', '', '2017-09-14 15:46:12', '2016-12-26 18:42:27'),
(11, 2, 'app\\modules\\section\\models\\Section', NULL, NULL, 'Услуги', 'uslugi', '<p>Бабувизм выводит трагический знак. Даосизм раскладывает&nbsp;на&nbsp;элементы неоднозначный гедонизм. Предмет&nbsp;деятельности нетривиален.</p>\r\n\r\n<p>Гегельянство трансформирует сложный смысл&nbsp;жизни. Моцзы, Сюнъцзы и другие считали, что акциденция решительно рассматривается данный бабувизм. Даосизм прост. Априори, катарсис решительно подрывает интеллигибельный гравитационный парадокс, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Освобождение реально заполняет сложный даосизм. Априори, здравый смысл раскладывает&nbsp;на&nbsp;элементы сенсибельный гедонизм.</p>\r\n', '', 2, 1, 1, 1, NULL, '', '', '', '', '2017-09-14 17:52:53', '2016-12-26 18:42:27'),
(12, 2, 'app\\modules\\section\\models\\Section', NULL, NULL, 'Услуги', 'uslugi', '<p>Бабувизм выводит трагический знак. Даосизм раскладывает&nbsp;на&nbsp;элементы неоднозначный гедонизм. Предмет&nbsp;деятельности нетривиален.</p>\r\n\r\n<p>Гегельянство трансформирует сложный смысл&nbsp;жизни. Моцзы, Сюнъцзы и другие считали, что акциденция решительно рассматривается данный бабувизм. Даосизм прост. Априори, катарсис решительно подрывает интеллигибельный гравитационный парадокс, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Освобождение реально заполняет сложный даосизм. Априори, здравый смысл раскладывает&nbsp;на&nbsp;элементы сенсибельный гедонизм.</p>\r\n', '', 2, 1, 1, 1, NULL, '', '', '', '', '2017-09-14 17:52:59', '2016-12-26 18:42:27'),
(13, 2, 'app\\modules\\section\\models\\Section', NULL, NULL, 'Услуги', 'uslugi', '<p>Бабувизм выводит трагический знак. Даосизм раскладывает&nbsp;на&nbsp;элементы неоднозначный гедонизм. Предмет&nbsp;деятельности нетривиален.</p>\r\n\r\n<p>Гегельянство трансформирует сложный смысл&nbsp;жизни. Моцзы, Сюнъцзы и другие считали, что акциденция решительно рассматривается данный бабувизм. Даосизм прост. Априори, катарсис решительно подрывает интеллигибельный гравитационный парадокс, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Освобождение реально заполняет сложный даосизм. Априори, здравый смысл раскладывает&nbsp;на&nbsp;элементы сенсибельный гедонизм.</p>\r\n', '1417fa9529cf758de4debe308ee1c099.jpg', 2, 1, 1, 1, NULL, '', '', '', '', '2017-09-14 17:54:28', '2016-12-26 18:42:27');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_section_sidebar_blocks`
--

CREATE TABLE `tbl_section_sidebar_blocks` (
  `id` int(11) NOT NULL,
  `section_id` int(11) DEFAULT NULL,
  `sidebar_block_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_sidebar_blocks`
--

CREATE TABLE `tbl_sidebar_blocks` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content` text,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `priority` tinyint(4) DEFAULT '50'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_sidebar_blocks`
--

INSERT INTO `tbl_sidebar_blocks` (`id`, `name`, `content`, `updated_at`, `created_at`, `priority`) VALUES
(1, 'Боковой блок', '', '2017-08-29 09:40:23', '2017-08-29 09:40:23', 50);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_slider`
--

CREATE TABLE `tbl_slider` (
  `id` int(11) NOT NULL,
  `vis` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `link` varchar(255) DEFAULT NULL,
  `button_text` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `priority` tinyint(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_slider`
--

INSERT INTO `tbl_slider` (`id`, `vis`, `name`, `title`, `description`, `link`, `button_text`, `img`, `priority`, `updated_at`, `created_at`) VALUES
(1, 1, 'Нвазние слайда', 'Заголовок', '', '', '', 'ff5630c7fe517066df4ef0e61e354e26.jpg', 1, '2017-08-23 09:16:35', '2016-12-25 23:41:15'),
(2, 1, 'Нвазние слайда2', '', '', '', '', NULL, 2, '2017-08-23 09:16:35', '2016-12-26 19:02:51'),
(3, 1, 'Нвазние слайда3', '', '', '', '', NULL, 3, '2017-08-22 20:34:22', '2016-12-26 19:04:06');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_snippet`
--

CREATE TABLE `tbl_snippet` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `tbl_snippet`
--

INSERT INTO `tbl_snippet` (`id`, `name`, `text`) VALUES
(1, 'Сноска', '<p><em><strong>Текст сноски</strong></em></p>\r\n'),
(2, 'Блок с картинкой', '<p><img alt="" src="/web/content/images/eb6.jpg" style="height:453px; width:604px" /></p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_subscriptions`
--

CREATE TABLE `tbl_subscriptions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `ip` varchar(55) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_subscriptions`
--

INSERT INTO `tbl_subscriptions` (`id`, `name`, `email`, `phone`, `ip`, `updated_at`, `created_at`) VALUES
(3, 'testes', 'fdsfs@ff.ru', NULL, '127.0.0.1', '2017-04-06 17:30:32', '2017-04-06 00:00:00'),
(4, 'testes', 'fdsfs2@ff.ru', NULL, '127.0.0.1', '2017-04-06 17:30:42', '2017-04-06 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_tags`
--

CREATE TABLE `tbl_tags` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url_alias` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_tags`
--

INSERT INTO `tbl_tags` (`id`, `name`, `url_alias`, `updated_at`, `created_at`) VALUES
(1, 'PHP', 'php', NULL, NULL),
(2, 'curl', 'curl', NULL, NULL),
(3, 'javascript', 'javascript', NULL, NULL),
(4, 'Верстка', 'verstka', NULL, NULL),
(5, 'SEO', 'seo', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_testimonials`
--

CREATE TABLE `tbl_testimonials` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `message` text,
  `ip` varchar(55) DEFAULT NULL,
  `moderation` tinyint(1) NOT NULL DEFAULT '1',
  `vis` tinyint(1) NOT NULL DEFAULT '0',
  `viewed` tinyint(1) NOT NULL DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_testimonials`
--

INSERT INTO `tbl_testimonials` (`id`, `name`, `email`, `phone`, `city`, `message`, `ip`, `moderation`, `vis`, `viewed`, `updated_at`, `created_at`) VALUES
(1, 'Виктор Пелевин', 'pelevin@webelement.ru', '8 (555) 555-55-55', 'Москва', 'Мир индуктивно контролирует из ряда вон выходящий знак. По своим философским взглядам Дезами был материалистом и атеистом, последователем Гельвеция, однако искусство поразительно. Культ джайнизма включает в себя поклонение Махавире и другим тиртханкарам, поэтому сомнение нетривиально. Закон внешнего мира принимает во внимание неоднозначный гений. Конфликт прост.', NULL, 1, 1, 1, '2017-02-07 00:56:23', '2016-11-11'),
(8, 'Антон Павлович Чехов', 'chechov@webelement.ru', '8 (555) 555-55-55', 'Таганрог', 'Искусство творит трагический мир. Знак, по определению, рефлектирует принцип восприятия, ломая рамки привычных представлений. Сомнение, конечно, амбивалентно. Гений, как следует из вышесказанного, подчеркивает напряженный дуализм.', NULL, 1, 1, 1, '2017-03-23 10:11:07', '2016-11-14'),
(10, 'Михаил Юрьевич Лермонтов', 'admin@host-kmv.ru', NULL, NULL, 'fdsfsf', '127.0.0.1', 1, 1, 1, '2017-04-07 00:34:29', '2017-02-27');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `shop_start_bonus_points` int(11) NOT NULL DEFAULT '0',
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_sign_in` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` int(11) NOT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email_verification_token`, `email`, `status`, `shop_start_bonus_points`, `role`, `last_sign_in`, `current_sign_in_ip`, `last_sign_in_ip`, `updated_at`, `created_at`) VALUES
(9, 'admin', '8T4zpYY9nDydj1G5cr-HymDd52Aq_9kE', '$2y$13$yJaTm.Jbo3cimR5eNe7H4OV.DEYi8WgsKNrx5Z299RCDoyfLUbNba', NULL, NULL, 'skarah@mail.ru', 10, 0, 'admin', '2018-03-02 12:35:12', '127.0.0.1', '127.0.0.1', 1519983372, 1457085560),
(10, 'manager', '', '$2y$13$6wpukqhCXAWwuk7rhunRPeW/Ui0/tM44ZgZ79MuQ.AVllqBwUDe7a', NULL, NULL, 'manager@test.ru', 10, 0, 'manager', NULL, NULL, '', 1519983359, 1457941234),
(11, 'tester', '', '$2y$13$FGP4Ax6MAIj8FI4N1S8YX..WlQ1iwjrEV3n8ZQ/cHsP/oynXjNrsi', NULL, NULL, 'tester@test.ru', 10, 0, '', NULL, NULL, '', 1519983346, 1459176908);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_user_wishlist`
--

CREATE TABLE `tbl_user_wishlist` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_user_wishlist`
--

INSERT INTO `tbl_user_wishlist` (`id`, `user_id`, `product_id`) VALUES
(4, 9, 19);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `tbl_articles`
--
ALTER TABLE `tbl_articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url_alias` (`url_alias`),
  ADD KEY `vis` (`vis`),
  ADD KEY `created_at` (`created_at`);

--
-- Индексы таблицы `tbl_auth_assignment`
--
ALTER TABLE `tbl_auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Индексы таблицы `tbl_auth_item`
--
ALTER TABLE `tbl_auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Индексы таблицы `tbl_auth_item_child`
--
ALTER TABLE `tbl_auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `tbl_auth_networks`
--
ALTER TABLE `tbl_auth_networks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `identity` (`identity`),
  ADD KEY `network` (`network`);

--
-- Индексы таблицы `tbl_auth_rule`
--
ALTER TABLE `tbl_auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `tbl_block`
--
ALTER TABLE `tbl_block`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vis` (`vis`),
  ADD KEY `page_url` (`page_url`);

--
-- Индексы таблицы `tbl_booking`
--
ALTER TABLE `tbl_booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_id` (`room_id`),
  ADD KEY `created_at` (`created_at`),
  ADD KEY `viewed` (`viewed`);

--
-- Индексы таблицы `tbl_brands`
--
ALTER TABLE `tbl_brands`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url_alias` (`url_alias`),
  ADD KEY `vis` (`vis`),
  ADD KEY `on_main` (`on_main`),
  ADD KEY `priority` (`priority`);

--
-- Индексы таблицы `tbl_callbacks`
--
ALTER TABLE `tbl_callbacks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_at` (`created_at`),
  ADD KEY `viewed` (`viewed`);

--
-- Индексы таблицы `tbl_cart_products`
--
ALTER TABLE `tbl_cart_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `tbl_categories`
--
ALTER TABLE `tbl_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url_alias` (`url_alias`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `priority` (`priority`),
  ADD KEY `vis` (`vis`);

--
-- Индексы таблицы `tbl_cities`
--
ALTER TABLE `tbl_cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `region_id` (`region_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `tbl_custom_html`
--
ALTER TABLE `tbl_custom_html`
  ADD PRIMARY KEY (`id`),
  ADD KEY `priority` (`priority`),
  ADD KEY `vis` (`vis`);

--
-- Индексы таблицы `tbl_delivery_types`
--
ALTER TABLE `tbl_delivery_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tbl_faq`
--
ALTER TABLE `tbl_faq`
  ADD PRIMARY KEY (`id`),
  ADD KEY `priority` (`priority`),
  ADD KEY `vis` (`vis`),
  ADD KEY `created_at` (`created_at`);

--
-- Индексы таблицы `tbl_feedback`
--
ALTER TABLE `tbl_feedback`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_at` (`created_at`),
  ADD KEY `viewed` (`viewed`);

--
-- Индексы таблицы `tbl_galleries`
--
ALTER TABLE `tbl_galleries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url_alias` (`url_alias`),
  ADD KEY `vis` (`vis`),
  ADD KEY `priority` (`priority`);

--
-- Индексы таблицы `tbl_gallery_images`
--
ALTER TABLE `tbl_gallery_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `priority` (`priority`),
  ADD KEY `gallery_id` (`gallery_id`);

--
-- Индексы таблицы `tbl_image`
--
ALTER TABLE `tbl_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `priority` (`priority`),
  ADD KEY `gid` (`gid`);

--
-- Индексы таблицы `tbl_logs`
--
ALTER TABLE `tbl_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_at` (`created_at`);

--
-- Индексы таблицы `tbl_meta`
--
ALTER TABLE `tbl_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `url` (`url`);

--
-- Индексы таблицы `tbl_migration`
--
ALTER TABLE `tbl_migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `tbl_modules`
--
ALTER TABLE `tbl_modules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `module` (`module`),
  ADD KEY `priority` (`priority`),
  ADD KEY `active` (`active`),
  ADD KEY `menu_vis` (`menu_vis`);

--
-- Индексы таблицы `tbl_news`
--
ALTER TABLE `tbl_news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url_alias` (`url_alias`),
  ADD KEY `created_at` (`created_at`),
  ADD KEY `vis` (`vis`);

--
-- Индексы таблицы `tbl_orders`
--
ALTER TABLE `tbl_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pay_type_id` (`pay_type_id`),
  ADD KEY `delivery_type_id` (`delivery_type_id`),
  ADD KEY `viewed` (`viewed`),
  ADD KEY `created_at` (`created_at`),
  ADD KEY `email` (`email`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `tbl_order_products`
--
ALTER TABLE `tbl_order_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `tbl_pages`
--
ALTER TABLE `tbl_pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url_alias` (`url_alias`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `vis` (`vis`),
  ADD KEY `priority` (`priority`);

--
-- Индексы таблицы `tbl_params`
--
ALTER TABLE `tbl_params`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `priority` (`priority`);

--
-- Индексы таблицы `tbl_pay_types`
--
ALTER TABLE `tbl_pay_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `priority` (`priority`);

--
-- Индексы таблицы `tbl_posts`
--
ALTER TABLE `tbl_posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url_alias` (`url_alias`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `updated_at` (`updated_at`),
  ADD KEY `vis` (`vis`),
  ADD KEY `created_at` (`created_at`);

--
-- Индексы таблицы `tbl_post_tags`
--
ALTER TABLE `tbl_post_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_tbl_post_tags_tbl_posts` (`post_id`),
  ADD KEY `FK_tbl_post_tags_tbl_tags` (`tag_id`);

--
-- Индексы таблицы `tbl_products`
--
ALTER TABLE `tbl_products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url_alias` (`url_alias`),
  ADD KEY `brand_id` (`brand_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `price` (`price`),
  ADD KEY `vis` (`vis`),
  ADD KEY `new` (`new`),
  ADD KEY `created_at` (`created_at`);

--
-- Индексы таблицы `tbl_product_images`
--
ALTER TABLE `tbl_product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `priority` (`priority`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `tbl_promo`
--
ALTER TABLE `tbl_promo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url_alias` (`url_alias`),
  ADD KEY `vis` (`vis`),
  ADD KEY `created_at` (`created_at`);

--
-- Индексы таблицы `tbl_regions`
--
ALTER TABLE `tbl_regions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `priority` (`priority`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `tbl_rooms`
--
ALTER TABLE `tbl_rooms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url_alias` (`url_alias`),
  ADD KEY `price_single` (`price_single`),
  ADD KEY `vis` (`vis`),
  ADD KEY `created_at` (`created_at`),
  ADD KEY `priority` (`priority`);

--
-- Индексы таблицы `tbl_room_images`
--
ALTER TABLE `tbl_room_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `priority` (`priority`),
  ADD KEY `room_id` (`room_id`);

--
-- Индексы таблицы `tbl_sections`
--
ALTER TABLE `tbl_sections`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url_alias` (`url_alias`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `vis` (`vis`),
  ADD KEY `menu_vis` (`menu_vis`),
  ADD KEY `priority` (`priority`);

--
-- Индексы таблицы `tbl_sections_history`
--
ALTER TABLE `tbl_sections_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `model_id` (`model_id`),
  ADD KEY `created_at` (`created_at`);

--
-- Индексы таблицы `tbl_section_sidebar_blocks`
--
ALTER TABLE `tbl_section_sidebar_blocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_tbl_section_sidebar_blocks_tbl_sections` (`section_id`),
  ADD KEY `FK_tbl_section_sidebar_blocks_tbl_sidebar_blocks` (`sidebar_block_id`);

--
-- Индексы таблицы `tbl_sidebar_blocks`
--
ALTER TABLE `tbl_sidebar_blocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `priority` (`priority`);

--
-- Индексы таблицы `tbl_slider`
--
ALTER TABLE `tbl_slider`
  ADD PRIMARY KEY (`id`),
  ADD KEY `priority` (`priority`),
  ADD KEY `vis` (`vis`);

--
-- Индексы таблицы `tbl_snippet`
--
ALTER TABLE `tbl_snippet`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tbl_subscriptions`
--
ALTER TABLE `tbl_subscriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_at` (`created_at`);

--
-- Индексы таблицы `tbl_tags`
--
ALTER TABLE `tbl_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `url_alias` (`url_alias`);

--
-- Индексы таблицы `tbl_testimonials`
--
ALTER TABLE `tbl_testimonials`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vis` (`vis`),
  ADD KEY `created_at` (`created_at`),
  ADD KEY `viewed` (`viewed`),
  ADD KEY `moderation` (`moderation`);

--
-- Индексы таблицы `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`),
  ADD UNIQUE KEY `email_verivication_token` (`email_verification_token`),
  ADD KEY `status` (`status`);

--
-- Индексы таблицы `tbl_user_wishlist`
--
ALTER TABLE `tbl_user_wishlist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_tbl_shop_wishlist_tbl_user` (`user_id`),
  ADD KEY `FK_tbl_shop_wishlist_tbl_products` (`product_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `tbl_articles`
--
ALTER TABLE `tbl_articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `tbl_auth_networks`
--
ALTER TABLE `tbl_auth_networks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `tbl_block`
--
ALTER TABLE `tbl_block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `tbl_booking`
--
ALTER TABLE `tbl_booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `tbl_brands`
--
ALTER TABLE `tbl_brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;
--
-- AUTO_INCREMENT для таблицы `tbl_callbacks`
--
ALTER TABLE `tbl_callbacks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `tbl_cart_products`
--
ALTER TABLE `tbl_cart_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=492;
--
-- AUTO_INCREMENT для таблицы `tbl_categories`
--
ALTER TABLE `tbl_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT для таблицы `tbl_cities`
--
ALTER TABLE `tbl_cities`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12371094;
--
-- AUTO_INCREMENT для таблицы `tbl_custom_html`
--
ALTER TABLE `tbl_custom_html`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `tbl_delivery_types`
--
ALTER TABLE `tbl_delivery_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `tbl_faq`
--
ALTER TABLE `tbl_faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `tbl_feedback`
--
ALTER TABLE `tbl_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `tbl_galleries`
--
ALTER TABLE `tbl_galleries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `tbl_gallery_images`
--
ALTER TABLE `tbl_gallery_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблицы `tbl_image`
--
ALTER TABLE `tbl_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT для таблицы `tbl_logs`
--
ALTER TABLE `tbl_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT для таблицы `tbl_meta`
--
ALTER TABLE `tbl_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `tbl_modules`
--
ALTER TABLE `tbl_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT для таблицы `tbl_news`
--
ALTER TABLE `tbl_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `tbl_orders`
--
ALTER TABLE `tbl_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `tbl_order_products`
--
ALTER TABLE `tbl_order_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `tbl_pages`
--
ALTER TABLE `tbl_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `tbl_params`
--
ALTER TABLE `tbl_params`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `tbl_pay_types`
--
ALTER TABLE `tbl_pay_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `tbl_posts`
--
ALTER TABLE `tbl_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `tbl_post_tags`
--
ALTER TABLE `tbl_post_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `tbl_products`
--
ALTER TABLE `tbl_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT для таблицы `tbl_product_images`
--
ALTER TABLE `tbl_product_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблицы `tbl_promo`
--
ALTER TABLE `tbl_promo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `tbl_regions`
--
ALTER TABLE `tbl_regions`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5019395;
--
-- AUTO_INCREMENT для таблицы `tbl_rooms`
--
ALTER TABLE `tbl_rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `tbl_room_images`
--
ALTER TABLE `tbl_room_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблицы `tbl_sections`
--
ALTER TABLE `tbl_sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT для таблицы `tbl_sections_history`
--
ALTER TABLE `tbl_sections_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `tbl_section_sidebar_blocks`
--
ALTER TABLE `tbl_section_sidebar_blocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `tbl_sidebar_blocks`
--
ALTER TABLE `tbl_sidebar_blocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `tbl_slider`
--
ALTER TABLE `tbl_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `tbl_snippet`
--
ALTER TABLE `tbl_snippet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `tbl_subscriptions`
--
ALTER TABLE `tbl_subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `tbl_tags`
--
ALTER TABLE `tbl_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `tbl_testimonials`
--
ALTER TABLE `tbl_testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `tbl_user_wishlist`
--
ALTER TABLE `tbl_user_wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `tbl_auth_assignment`
--
ALTER TABLE `tbl_auth_assignment`
  ADD CONSTRAINT `tbl_auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `tbl_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tbl_auth_item`
--
ALTER TABLE `tbl_auth_item`
  ADD CONSTRAINT `tbl_auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `tbl_auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tbl_auth_item_child`
--
ALTER TABLE `tbl_auth_item_child`
  ADD CONSTRAINT `tbl_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `tbl_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `tbl_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tbl_auth_networks`
--
ALTER TABLE `tbl_auth_networks`
  ADD CONSTRAINT `FK_tbl_auth_networks_tbl_user` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tbl_cities`
--
ALTER TABLE `tbl_cities`
  ADD CONSTRAINT `tbl_cities_ibfk_1` FOREIGN KEY (`region_id`) REFERENCES `tbl_regions` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tbl_post_tags`
--
ALTER TABLE `tbl_post_tags`
  ADD CONSTRAINT `FK_tbl_post_tags_tbl_posts` FOREIGN KEY (`post_id`) REFERENCES `tbl_posts` (`id`),
  ADD CONSTRAINT `FK_tbl_post_tags_tbl_tags` FOREIGN KEY (`tag_id`) REFERENCES `tbl_tags` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_sections_history`
--
ALTER TABLE `tbl_sections_history`
  ADD CONSTRAINT `tbl_sections_history_ibfk_1` FOREIGN KEY (`model_id`) REFERENCES `tbl_sections` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tbl_section_sidebar_blocks`
--
ALTER TABLE `tbl_section_sidebar_blocks`
  ADD CONSTRAINT `FK_tbl_section_sidebar_blocks_tbl_sections` FOREIGN KEY (`section_id`) REFERENCES `tbl_sections` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `FK_tbl_section_sidebar_blocks_tbl_sidebar_blocks` FOREIGN KEY (`sidebar_block_id`) REFERENCES `tbl_sidebar_blocks` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Ограничения внешнего ключа таблицы `tbl_user_wishlist`
--
ALTER TABLE `tbl_user_wishlist`
  ADD CONSTRAINT `FK_tbl_shop_wishlist_tbl_products` FOREIGN KEY (`product_id`) REFERENCES `tbl_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_tbl_shop_wishlist_tbl_user` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
