<?php
namespace app\behaviors;
use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\db\BaseActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yii\validators\Validator;
use app\helpers\Common;
use app\modules\logs\models\Log;
class LogBehavior extends Behavior
{
    const ACTION_CREATE = "create";
    const ACTION_UPDATE = "update";
    const ACTION_DELETE = "delete";
    const ACTION_VIEW = "view";
    const ACTION_DELETE_IMAGE = "ajaxdeleteimage";
    const ACTION_AJAX_EDITABLE = "ajaxeditable";
    const ACTION_MULTI_DELETE = "ajaxdeletegriditems";
    public $excluded_attributes = [
        'updated_at'
    ];
    public $event;
    /**
     * @var array - contains your custom actions
     */
    public $actions = [
        self::ACTION_CREATE,
        self::ACTION_UPDATE,
        self::ACTION_DELETE,
        self::ACTION_DELETE_IMAGE,
        self::ACTION_AJAX_EDITABLE,
        self::ACTION_MULTI_DELETE,
        //self::ACTION_VIEW,
    ];
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'eventHandler',
            ActiveRecord::EVENT_AFTER_UPDATE => 'eventHandler',
            ActiveRecord::EVENT_BEFORE_DELETE => 'eventHandler',
            //ActiveRecord::EVENT_AFTER_FIND => 'eventHandler',
        ];
    }
    public function eventHandler($event)
    {
        $action = \Yii::$app->requestedAction->id;
        $this->event = $event;
        if(in_array($action, $this->actions)) {
            $model = $this->owner;
            if($action == self::ACTION_DELETE_IMAGE || $action == self::ACTION_AJAX_EDITABLE) {
                $action = 'update';
            }
            if($action == self::ACTION_MULTI_DELETE ) {
                $action = 'delete';
            }
            $this->saveLog($model->className(), $model->id, $action);
        }
    }
    private function saveLog($model_name, $record_id, $action)
    {
        $model = $this->owner;
        $log = new Log();
        $log->module = $this->getModuleName($model_name);
        $log->model = $model_name;
        $log->record_id = $record_id;
        $log->action = $action;
        $log->changed_data = $action == 'update' ? $this->getChangedAttributes() : '';
        $log->user_id = Yii::$app->user->id;
        $log->ip = Yii::$app->request->userIP;
        $log->created_at = date('Y-m-d H:i:s');
        if($log->validate()) {
            $log->save();
        } else {
            print_r($log->getErrors());
            die();
        }
    }
    private function getModuleName($model_name)
    {
        $pieces = explode('\\', $model_name);
        return isset($pieces[2]) ? $pieces[2] : '';
    }
    /**
     * @param array $diff
     *
     * @return array
     */
    private function applyExclude(array $diff)
    {
        foreach ($this->excluded_attributes as $attr) {
            unset($diff[$attr]);
        }
        return $diff;
    }
    /**
     * @param array $diff
     *
     * @return array
     */
    private function setLabels(array $diff)
    {
        /**
         * @var ActiveRecord $owner
         */
        $owner = $this->owner;
        foreach ($diff as $attr => $msg) {
            unset($diff[$attr]);
            $diff[$owner->getAttributeLabel($attr)] = $msg;
        }
        return $diff;
    }
    public function getChangedAttributes()
    {
        $changed_attributes = $this->event->changedAttributes;
        $diff = [];
        foreach ($changed_attributes as $attr_name => $attr_val) {
            $new_attr_val = $this->owner->getAttribute($attr_name);
            if ($new_attr_val != $attr_val) {
                if ($attr_val == '') {
                    $attr_val = 'null';
                }
                if ($new_attr_val == '') {
                    $new_attr_val = 'null';
                }
                $diff[$attr_name] = ['old_val'=>$attr_val, 'new_val'=>$new_attr_val];
            }
        }
        $diff = $this->applyExclude($diff);
        if ($diff) {
            $diff = $this->setLabels($diff);
        }
        return json_encode($diff);
    }
}