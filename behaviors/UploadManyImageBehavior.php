<?php

namespace app\behaviors;

use Yii;

class UploadManyImageBehavior extends UploadManyFilesBehavior
{

    public $related_form_data_field = 'RelatedField';

    public function afterUpdate()
    {
        if(Yii::$app->request->post($this->related_form_data_field)) {

            $related_model_name = $this->getRelatedModelName();

            foreach(Yii::$app->request->post($this->related_form_data_field) as $id=>$item) {
                $related_model = $related_model_name::findOne($id);
                if($related_model) {
                    foreach ($item as $attr => $val) {
                        if ($val) {
                            $related_model->{$attr} = $val;
                        }
                    }
                    $related_model->save();
                }
            }
        }

        parent::afterUpdate();
    }


    public function mainImg($profile = 'thumb')
    {
        $model = $this->owner;

        if(isset($model->{$this->relation}[0])) {
            $main_image = $model->{$this->relation}[0];
            return $main_image->img($profile);
        }

        return false;
    }

    /**
     * @return string
     * @deprecated используйте сразу $model->img() у нужного изображения
     * @see \app\behaviors\UploadImageBehavior::img()
     */
    public function getImagePath()
    {
        return (new $this->model_name)->getImagePath();
    }

}