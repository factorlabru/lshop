!function($){
    $(document).ready(function() {



        /*
         один ajax обработчик на множество форм. если нужно будет что-то специфичное, можно создать сколько угодно
         */
        $('body').on('submit', '.js_ajax_form', function() {
            var $this = $(this);
            if($this.data('form_loading')){
                return false;
            }
            $this.data('form_loading', 'true');


            $('.overlay-loader').show();


            var $cur_form = $(this);

            var data = {
                anti_spam: 'anti_spam',
                anti_spam2: Math.random().toString(36).slice(2) //это поле чтобы запутать спамеров
            };

            data[js_vars.csrf_name] = js_vars.csrf;

            $(this).ajaxSubmit({
                complete: function(){
                    $this.data('form_loading', false);
                    $('.overlay-loader').hide();
                },
                success: function(response, statusText, xhr, $form){
                    var form_scope;
                    var success_modal = $form.data('success_modal');
                    var is_reset = $form.data('reset_form');


                    if(!$form.data('form_scope')){
                        form_scope = $form;
                    } else {
                        form_scope = $($form.data('form_scope'));
                    }



                    if(response.status == 1){
                        $('.js_submit_errors_container', form_scope).hide();
                        $('.return-success', form_scope).show();
                        $('.js_captcha_image', form_scope).click();
                        $('.error', form_scope).removeClass('error');

                        $form.trigger('success_send');

                        var js_pop_modal = $cur_form.closest('.js_pop_modal');

                        if(js_pop_modal.arcticmodal){
                            $cur_form.closest('.js_pop_modal').arcticmodal('close');
                        }

                        if(is_reset){
                            $form.resetForm();
                        }

                        if(success_modal){
                            $(success_modal).arcticmodal();
                        }

                        //alert(response.redirect);

                        if(response.redirect){
                            window.location.href = response.redirect;
                        }
                        //window.location.href = $form.data('success_url');

                    } else {
                        processErrors(response, statusText, xhr, $form, form_scope);

                        $('.return-success', form_scope).hide();

                        $('.js_captcha_image').click();
                    }
                }
                ,dataType:  'json'
                ,data: data
            });

            return false;
        });



    });



    /*
     Это общая для всех форм обработка и вывод вернувшихся ошибок
     */
    function processErrors(response, statusText, xhr, $form, form_scope)
    {
        $('.error_text', form_scope).empty();
        $('.error_text', form_scope).hide();
        $('.js_submit_errors_container', form_scope).hide();

        $('.error', form_scope).removeClass('error');

        var errors = "";

        var arr_errors = response.errors;

        for(var model_name in arr_errors){
            if(!arr_errors.hasOwnProperty(model_name)){
                continue;
            }

            var errors_form_model = arr_errors[model_name];

            for(var field_name in errors_form_model){
                if(!errors_form_model.hasOwnProperty(field_name)){
                    continue;
                }

                var field_class = model_name + '_' + field_name;
                var error_text_class = field_class + '_error';
                var first_error_text = errors_form_model[field_name][0];
                var field_selector = '[name="' + model_name + '[' + field_name + ']"]';

                var field_error = $('.'+error_text_class);

                if($form.data('auto_add_errors') == true){
                    if((field_error.length == 0)){
                        var new_div = $('<div></div>');
                        new_div.addClass(error_text_class);
                        new_div.addClass('error_text');
                        $(new_div).insertAfter(field_selector);

                        field_error = $('.'+error_text_class);
                    }
                }

                $(field_error, form_scope).append(first_error_text);
                $(field_error, form_scope).show();

                $(field_selector, form_scope).addClass('error');


                for(var error_index in errors_form_model[field_name]) {
                    if (!errors_form_model[field_name].hasOwnProperty(error_index)) {
                        continue;
                    }

                    errors += errors_form_model[field_name][error_index] + "<br>";
                }

                field_error = false;
            }
        }

        if($form.data('show_all_errors') != false){
            $('.js_submit_errors', form_scope).html(errors);
            $('.js_submit_errors_container', form_scope).show();
        }

    }
}(jQuery);