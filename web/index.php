<?php

//comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

if (YII_DEBUG) {
    ini_set('display_errors','On');
    error_reporting(E_ALL);
} else {
    ini_set('display_errors', 'Off');
    error_reporting(0);
}

//defined('YII_ENV_TEST') or define('YII_ENV_TEST', 'dev');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = yii\helpers\ArrayHelper::merge(
    //require(__DIR__ . '/../config/common.php'),
    [],
    require(__DIR__ . '/../config/web.php')
    //require(__DIR__ . '/../config/module.php')
);

require(__DIR__ . '/../components/Application.php');

(new app\components\Application($config))->run();