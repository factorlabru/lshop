
CKEDITOR.plugins.add('backup', {
    init: function (editor) {

        editor.on('instanceReady', function (e) {
            var select = 0;
            var backup_dumps_name = 'backup_dumps_' + editor.name + '_';
            var backup_keys_name = 'backup_keys_' + editor.name;
            var just_loaded = 1;
            var backup_keys = {};
            var old_text = '';
            var change_timeout = 0;


            function init(){
                if (localStorage.getItem(backup_keys_name) === null) {
                    localStorage.setItem(backup_keys_name, '{}');
                }

                editor.on('change', onChange);
                $('#' + editor.name).on('change', onChange);

                var new_div = $('<div>').attr('style', 'float:right;');
                new_div.html(
                    'Бэкапы: <select id="backuper_' + editor.name + '"></select>&nbsp;' +
                    '<input type="button" value="Удалить" onclick="CKEDITOR.instances[\'' + editor.name + '\'].delete_backup(); return false;"/>&nbsp;' +
                    '<input type="button" value="Удалить везде" onclick="CKEDITOR.instances[\'' + editor.name + '\'].delete_backup_all(); return false;"/>' +
                    '');

                new_div.on('change', function () {
                    editor.restore();
                });

                editor.on('mode', function() {
                    if(this.mode === 'source'){
                        new_div.hide()
                    } else {
                        new_div.show();
                    }
                });

                var editor_id = editor.ui.spaceId ? editor.ui.spaceId("bottom") : 'cke_bottom_' + editor.name;
                var new_node = new CKEDITOR.dom.node(new_div.get(0));

                CKEDITOR.document.getById(editor_id).append(new_node);
                select = CKEDITOR.document.getById('backuper_' + editor.name);

                editor.backup();
                if (localStorage.getItem(backup_keys_name)) {
                    backup_keys = JSON.parse(localStorage.getItem(backup_keys_name));
                }
                updateSelect();
            }


            function onChange()
            {
                clearTimeout(change_timeout);
                change_timeout = setTimeout(function () {
                    editor.backup();
                    just_loaded = false;
                }, 1000);
            }

            function format_number(number) {
                var n = new Date(parseInt(number));
                var frm = function (num) {
                    if (num < 10) num = '0' + num;
                    return num;
                };
                //Исправление бага с месяцами в бекапах в ckeditor.
                var month_n = n.getMonth() + 1;
                //return frm(n.getFullYear()) + '-' + frm(n.getMonth()) + '-' + frm(n.getDay()) + ' ' + frm(n.getHours()) + ':' + frm(n.getMinutes()) + ':' + frm(n.getSeconds());
                return frm(n.getFullYear()) + '-' + frm(month_n) + '-' + frm(n.getDay()) + ' ' + frm(n.getHours()) + ':' + frm(n.getMinutes()) + ':' + frm(n.getSeconds());
            }

            function updateSelect() {
                var options = '<option> ---------------------------- </option>';
                for (var date_key in backup_keys){
                    if(backup_keys.hasOwnProperty(date_key)){
                        options += '<option value="' + date_key + '">' + format_number(date_key) + '</option>';
                    }
                }
                select.setHtml(options);
            }

            editor.delete_backup = function () {
                if (confirm('Вы уверены, что хотите удалить весь бекап?')) {

                    for (var date_key in backup_keys){
                        if(backup_keys.hasOwnProperty(date_key)){
                            localStorage.removeItem(backup_dumps_name + date_key);
                        }
                    }
                    localStorage.setItem(backup_keys_name, '{}');
                    backup_keys = {};
                    updateSelect();
                }
            };

            editor.delete_backup_all = function () {
                if (confirm('Вы уверены, что хотите удалить все бэкапы?')) {
                    localStorage.clear();
                    localStorage.setItem(backup_keys_name, '{}');
                    backup_keys = {};
                    updateSelect();
                }
            };

            editor.backup = function () {

                var now = new Date().getTime();
                var text = editor.getSnapshot();

                backup_keys = JSON.parse(localStorage.getItem(backup_keys_name));

                var last_text = '';
                var keys = Object.keys( backup_keys );
                var last_key = keys[keys.length - 1];
                if(keys.length){
                    last_text = localStorage.getItem(backup_dumps_name + last_key);
                }

                if (text !== '' && text !== '<br>') {
                    if (last_text !== text && text !== old_text) {

                        backup_keys[now] = 1;

                        try {
                            localStorage.setItem(backup_dumps_name + now, text);
                            localStorage.setItem(backup_keys_name, JSON.stringify(backup_keys));
                        } catch (e) {
                            if (e.code === 22) { // QUOTA_EXCEEDED_ERR
                                if(confirm('Локальное хранилище с бэкапами ckeditor переполнено. Очистить?')){
                                    editor.delete_backup_all();
                                }
                            }
                        }

                        if(!just_loaded && old_text && keys.length > 1 && now - last_key < 1000 * 60 * 5){
                            delete backup_keys[last_key];
                            localStorage.removeItem(backup_dumps_name + last_key);
                        }
                        updateSelect();
                    }
                }


                old_text = text;
            };

            editor.restore = function () {
                var text = editor.getSnapshot();
                var backup_key = select.getValue();
                var backup_text = localStorage.getItem(backup_dumps_name + backup_key);
                if (backup_text !== null) {
                    if((text === '' || confirm('Вы уверены, что хотите заменить имеющийся текст, текстом из бекапа?'))){
                        editor.loadSnapshot(backup_text);
                    }
                }
            };

            init();
        });
    }
});