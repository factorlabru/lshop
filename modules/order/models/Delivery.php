<?php

namespace app\modules\order\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class Delivery
{

    const POST_INDEX = 347900;
    const RUSSIAN_POST_DELIVERY = 2;
    const SELF_PICKUP_DELIVERY = 4;
    const INDIVIDUAL_DELIVERY = 5;

    public $products;
    public $zipcode;
    public $weight;

    public function __construct($zipcode=false, $products=false)
    {
        $this->products = $products;
        $this->zipcode = $zipcode;
    }

    /**
     * rp_1class - Отправление 1-м классом.
     * rp_main - Обычное отправление.
     *
     * @param $zipcode
     * @param $products
     * @return mixed
     */
    public function russianPost($zipcode, $products)
    {
        $cost = 0;
        $weight = $this->countWeight($products);
        $weight = $this->convertWeightToKg($weight);
        //$url = 'http://russianpostcalc.ru/api_v1.php?apikey=f5f3cf6455b15e426ec3cfd756d4b2c2&method=calc&from_index=' . Delivery::ROSTOV_POST_INDEX . '&to_index=' . $zipcode . '&weight=' . $weight;
        //$result = CJSON::decode(file_get_contents($url));
        $result['calc'][1]['cost'] = 3330;
        if(isset($result['calc'][1]['cost'])) {
            $cost = $result['calc'][1]['cost'];
            if(!$cost && isset($result['calc'][0]['cost'])) {
                $cost = $result['calc'][0]['cost'];
            }
        }

        return $cost;
    }

    public function convertWeightToKg($weight)
    {
        return $weight / 1000;
    }

    public function countWeight()
    {
        $products = $this->products;
        $weight = 0;
        foreach($products as $product) {
            $weight += $product['weight']*$product['quantity'];
        }

        return $this->convertWeightToKg($weight);
    }

    public static function deliveryTypes($type=false, $region_id=false)
    {
        $types = [
            1 => "СДЭК курьер",
            2 => "Почта России",
            //3=>"Курьером",
            4=>"Самовывоз",
            //5=>"Индивидуальный способ доставки",
            6=>"СДЭК самовывоз",
        ];

        if($region_id && $region_id == Delivery::ROSTOVSKAYA_OBLAST_ID) {
            $types[5] = "Индивидуальный способ доставки";
        }

        return isset($types[$type]) ? $types[$type] : $types;
    }

}
