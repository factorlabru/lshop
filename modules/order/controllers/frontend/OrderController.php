<?php

namespace app\modules\order\controllers\frontend;

use app\modules\order\models\DeliveryType;
use app\modules\order\models\PayType;
use Yii;
use app\modules\order\models\Order;
use app\modules\order\models\OrderSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\Controller;
use app\modules\location\models\Region;

/**
 * Order controller for the `order` module
 */
class OrderController extends Controller
{
    public function actionCheckout()
    {
        $this->setAllMeta(['title'=>'Оформление заказа']);

        $products = Yii::$app->cart->getProducts();

        if(!$products) {
            return $this->redirect('/cart/');
        }

        $order = new Order();
        if($order->load(Yii::$app->request->post(), 'Order') ) {

            $order->total = Yii::$app->cart->countTotalPrice();
            if($order->save()) {

                $mail = (new \app\helpers\Mail())->sendMail($order,
                    Yii::$app->params['admin_email'],
                    '@modules/order/mails/letter',
                    'Заказ с сайта');

                $order->saveProducts($order->id, Yii::$app->cart->getProducts());

                if(isset($order->pay_type_id) && $order->pay_type_id==1) {
                    return $this->render('yakassa', [
                        'order' => $order,
                    ]);
                }else {
                    return $this->redirect(['/order/complete']);
                }

            }
        }

        $regions = Region::getAll();
        $delivery_types = DeliveryType::getAll();
        $pay_types = PayType::getAll();


        return $this->render('checkout', [
            'products'=>$products,
            'order'=>$order,
            'regions'=>$regions,
            'cities'=>array(),
            'delivery_types'=>$delivery_types,
            'pay_types'=>$pay_types,
        ]);
    }

    public function actionComplete()
    {
        return $this->render('complete');
    }
}