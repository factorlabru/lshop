<?php

namespace app\modules\order\controllers\frontend;

use yii\filters\VerbFilter;
use app\components\Controller;
use Yii;


class YaKassaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'order-check' => ['post'],
                    'payment-notification' => ['post'],
                ],
            ]
        ];
    }

    public function actions()
    {
        return [
            'order-check' => [
                'class' => 'app\components\yakassa\actions\CheckOrderAction',
                'beforeResponse' => function ($request) {
                    /**
                     * @var \yii\web\Request $request
                     */
                    $invoice_id = (int) $request->post('orderNumber');
                    Yii::warning("Кто-то хотел купить несуществующую подписку! InvoiceId: {$invoice_id}", Yii::$app->yakassa->logCategory);
                    return false;
                }
            ],
            'payment-notification' => [
                'class' => 'app\components\yakassa\actions\PaymentAvisoAction',
                'beforeResponse' => function ($request) {
                    /**
                     * @var \yii\web\Request $request
                     */
                }
            ],
        ];
    }
}

/*
Using widget is simple. You have to implement 2 interfaces. First is OrderInterface for your order model, to pass sum and id to form. Second is Customer interface, to pass customer id and pre-fill phone and email if exist.

echo kroshilin\yakassa\widgets\Payment::widget([
    'order' => $order,
    'userIdentity' => Yii::$app->user->identity,
	'data' => ['customParam' => 'value'],
	'paymentType' => ['PC' => 'Со счета в Яндекс.Деньгах', 'AC' => 'С банковской карты']
]);

 */