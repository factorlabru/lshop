<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\order\models\DeliveryType */

$this->title = 'Редактирование типа доставки';
$this->params['breadcrumbs'][] = ['label' => 'Типы доставки', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="delivery-type-update content">
    <div class="box">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
