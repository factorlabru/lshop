<div class="cart-grid-body col-xs-12 col-md-8">

    <h1>Оплатить заказ</h1>
    <?php
    echo \kroshilin\yakassa\widgets\Payment::widget([
        'order' => $order,
        'userIdentity' => Yii::$app->user->identity,
        'data' => ['customParam' => 'value'],
        'paymentType' => ['PC' => 'Со счета в Яндекс.Деньгах', 'AC' => 'С банковской карты']
    ]);
    ?>
</div>