<?php

namespace app\modules\order;

return [
    'params' => [
        '_bcModule' => 'Магазин',
        '_bcIcon' => '<i class="fa fa-calculator"></i>',
        '_items' => [
                [
                    'icon' => '<i class="fa fa-opencart"></i>',
                    'label' => 'Заказы',
                    'url' => '/admin/order',
                    'viewed'=>true,
                    'model'=>'\app\modules\order\models\Order',
                ],
                [
                    'icon' => '<i class="fa fa-truck"></i>',
                    'label' => 'Доставка',
                    'url' => '/admin/order/deliverytype',
                ],
                [
                    'icon' => '<i class="fa fa-dollar"></i>',
                    'label' => 'Оплата',
                    'url' => '/admin/order/paytype',
                ],

            ],
        ],
    ];