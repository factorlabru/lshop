<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use yii\widgets\Pjax;
use app\modules\admin\widgets\grid\LinkColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\booking\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::$app->getModule(\Yii::$app->controller->module->id)->module_name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booking-index content">
    <div class="box">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?=\app\modules\admin\widgets\GridMultiDelete::widget()?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\CheckboxColumn'],
                ['class' => 'yii\grid\SerialColumn'],
                'name',
                'phone',
                'email:email',
                [
                    'attribute' => 'viewed',
                    'value'=>function($model) {
                        return $model->viewed == 1 ? 'Да' : 'Нет';
                    },
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'viewed',
                        [1=>'Да', 0=>'Нет'],
                        ['class'=>'form-control','prompt' => 'Все']
                    ),
                ],
                [
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created_at',
                        'pluginOptions' => ['format' => 'yyyy-mm-dd']
                    ]),
                    'attribute' => 'created_at',
                    'format' => ['date', 'php:Y-m-d'],
                ],

                ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {delete}{link}'],
            ],
        ]); ?>
    </div>
</div>
