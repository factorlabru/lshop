<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
?>

<?php $url = Url::toRoute([
        '/rooms/room/view',
        'url_alias'=>$room->url_alias,
        'path'=>'rooms'
]);?>

<div class="flatInfo">
    <div class="pic">
        <a target="_blank" href="<?=$url?>">
            <img src="<?=$room->mainImg('thumb');?>">
        </a>
    </div>

    <div class="text">
        <div class="title">
            <a target="_blank" href="<?=$url?>"><?=$room->name?></a>
        </div>
        <p></p>
        <p><?=$room->description?></p>
        <p></p>
    </div>
</div>