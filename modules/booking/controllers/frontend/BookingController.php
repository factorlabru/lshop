<?php

namespace app\modules\booking\controllers\frontend;

use app\components\Controller;
use app\modules\rooms\widgets\Rooms;
use app\modules\section\models\Section;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\modules\booking\models\Booking;
use app\modules\rooms\models\Room;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use app\components\NumericCaptcha;


/**
 * Booking controller for the `booking` module
 */
class BookingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'app\components\NumericCaptcha',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex($path)
    {
        $section = Section::getByPath($path);

        if(!$section) {
            throw new \yii\web\NotFoundHttpException('Страница не найдена.');
        }

        $this->current_section = $section->id;

        $model = new Booking();
        $rooms = Room::find()
            ->where(['vis'=>1])
            ->orderBy('priority')
            ->all();

        $room_id = ArrayHelper::getValue(Yii::$app->request->post('Booking'), 'room_id');
        if($room_id) {
            $room = Room::find()->where(['vis'=>1, 'id'=>$room_id])->one();
            if($room) {
                $model->current_room = $room;
            }
        } else {
            $model->current_room = $rooms[0];
        }

        if($model->load(Yii::$app->request->post())) {
            $model->begin_date = $model->formatDate(ArrayHelper::getValue(Yii::$app->request->post('Booking'), 'begin_date'));
            $model->end_date = $model->formatDate(ArrayHelper::getValue(Yii::$app->request->post('Booking'), 'end_date'));

            if($model->save()) {
                $mail = (new \app\helpers\Mail())->sendMail($model,
                    Yii::$app->params['admin_email'],
                    '@modules/booking/mails/letter',
                    'Заявка на бронирование с сайта');

                $model->sendMail($model, $mail_to=Yii::$app->params['admin_email']);

                return $this->redirect(['/booking/complete']);
            }
        }

        $this->setMetaByModel($section);
        $this->setBreadcrumbs($section->makeBreadcrumbs());

        return $this->render('index', [
            'model'=>$model,
            'rooms'=>$rooms,
        ]);
    }

    public function actionAjaxLoadRoom()
    {
        $result = true;
        $html = '';
        $room_id = Yii::$app->request->post('room_id');
        if($room_id) {
            $room = Room::find()->where(['vis'=>1, 'id'=>$room_id])->one();
            if($room) {
                $html = Yii::$app->controller->renderPartial('_ajax_load_room', ['room'=>$room]);
            }
        }

        return \yii\helpers\Json::encode(['result'=>$result, 'html'=>$html]);
    }

    public function actionComplete()
    {
        $this->setAllMeta(['title'=>'Сообщение отправлено']);

        return $this->render('complete');
    }
}