<?php
return [
    'params' => [
        '_bcModule' => 'Каталог',
        '_bcIcon' => '<i class="fa fa-shopping-cart"></i>',
        '_items' => [
            [
                'icon' => '',
                'label' => 'Товары',
                'url' => '/admin/catalog/product',
            ],
            [
                'icon' => '',
                'label' => 'Категории',
                'url' => '/admin/catalog/category',
            ],
        ]
    ],
];