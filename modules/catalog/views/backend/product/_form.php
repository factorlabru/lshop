<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\widgets\CKEditor as CKEditor;
use app\modules\admin\widgets\ImageRender;
use app\modules\catalog\models\Brand;
use app\modules\catalog\models\Category;
use app\modules\admin\widgets\ImageBlocks;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form content">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(Yii::$app->session->getFlash('save')) {
        echo Alert::widget([
            'options' => ['class' => 'alert-success',],
            'body' => Yii::$app->session->getFlash('save'),
        ]);
    }?>

    <?= $form->field($model, 'vis')->checkbox() ?>

    <?= $form->field($model, 'new')->checkbox() ?>

    <?= $form->field($model, 'brand_id')->dropDownList(ArrayHelper::map(Brand::find()->all(), 'id', 'name'), ['prompt'=>'Выберите значение']); ?>

    <?= $form->field($model, 'category_id')->dropDownList(Category::treeList(), ['prompt'=>'Выберите значение']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php if(Yii::$app->moduleIsActive('yandextranslator')) {
        echo \app\modules\yandextranslator\widgets\UrlTranslate::widget([
            'source_id' => Html::getInputId($model, 'name'),
            'target_id' => Html::getInputId($model, 'url_alias'),
        ]);
    } ?>

    <?php
    $path = '';
    if(!$model->isNewRecord) {
        $path = \app\helpers\Common::getPathLink($model->getUrl());
    }
    echo $form->field($model, 'url_alias', [
        'template' => '{label}
                <div class="col-sm-12 nopadding inline-input">
                    {input} '.$path.'
                    {error}{hint}
                </div>'
    ])->textInput(['maxlength' => true, 'class'=>$model->url_alias ? 'form-control' : 'form-control translit-to'])?>


    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'old_price')->textInput() ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= \app\modules\admin\widgets\CKEditorWidget::widget([
        'form'=>$form,
        'model'=>$model,
        'attr'=>'features',
        'textarea_id'=>'product-features',
        'gallery_widget'=>false,
    ]); ?>

    <?= $form->field($model, 'file_loader[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>

    <?=ImageBlocks::widget([
        'data'=>$model->images,
    ]);?>

    <?= $this->render('@admin_layouts/chunks/_meta_form', [
        'model' => $model,
        'form' => $form,
    ]); ?>

    <?= $this->render('@admin_layouts/chunks/_save_buttons', [
        'model' => $model,
        'form' => $form,
    ]); ?>

    <?php ActiveForm::end(); ?>

</div>
