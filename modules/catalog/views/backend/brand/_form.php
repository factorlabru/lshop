<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\widgets\CKEditor as CKEditor;
use app\modules\admin\widgets\ImageRender;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\Brand */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="brand-form content">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(Yii::$app->session->getFlash('save')) {
        echo Alert::widget([
            'options' => ['class' => 'alert-success',],
            'body' => Yii::$app->session->getFlash('save'),
        ]);
    }?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php if(Yii::$app->moduleIsActive('yandextranslator')) {
        echo \app\modules\yandextranslator\widgets\UrlTranslate::widget([
            'source_id' => Html::getInputId($model, 'name'),
            'target_id' => Html::getInputId($model, 'url_alias'),
        ]);
    } ?>

    <?= $form->field($model, 'url_alias')->textInput(['maxlength' => true]) ?>

    <div class="col-md-10 nopadding">
        <?= $form->field($model, 'content')->widget(CKEditor::className(), [
            'options' =>['rows' => 10,],
            'preset' => 'full'
        ]) ?>
    </div>

    <?= \app\modules\admin\widgets\CKEditorWidgets::widget([
        'textarea_id'=>'brand-content',
        'gallery_widget'=>false
    ])?>

    <?= $form->field($model, 'priority')->textInput(['class'=>'form-control m-mini']) ?>

    <?= $form->field($model, 'img')->fileInput() ?>

    <?php if($model->img) {
        echo ImageRender::widget([
            'id'=>$model->id,
            'attribute'=>'img',
            'img_url'=>$model->img(),
        ]);
    }?>

    <?= $this->render('@admin_layouts/chunks/_save_buttons', [
        'model' => $model,
        'form' => $form,
    ]); ?>

    <?php ActiveForm::end(); ?>

</div>
