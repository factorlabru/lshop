<?php
namespace app\modules\catalog\widgets;

use yii\base\Widget;
use app\modules\catalog\models\Product;

/**
 * Виджет выводит товары, исходя из заданных параметров.
 *
 * Class ProductsByParam
 * @package app\modules\catalog\widgets
 */
class ProductsByParam extends Widget
{
    /**
     * @var string
     */
    public $view = 'products';

    /**
     * @var string
     */
    public $param = 'new';

    /**
     * @var int
     */
    public $param_val = 1;

    /**
     * @var int
     *
     * Количество выводимых записей.
     */
    public $limit = 9;

    /**
     * @var string
     *
     * Тип сортировки.
     */
    public $order = 'id';

    public function run()
    {
        $products = Product::find()
            ->with('category')
            ->where([$this->param => $this->param_val])
            ->orderBy($this->order)
            ->all();

        return $this->render($this->view, ['products'=>$products]);
    }
}