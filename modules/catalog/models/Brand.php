<?php

namespace app\modules\catalog\models;

use app\behaviors\UploadImageBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%brands}}".
 *
 * @mixin UploadImageBehavior
 *
 * @property integer $id
 * @property string $name
 * @property string $url_alias
 * @property string $img
 * @property string $content
 * @property integer $priority
 * @property integer $on_main
 * @property string $updated_at
 * @property string $created_at
 */
class Brand extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'slug' => [
                'class' => 'app\behaviors\Slug',
                'in_attribute' => 'name',
                'out_attribute' => 'url_alias',
                'translit' => true
            ],
            [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'img',
                'scenarios' => ['create', 'update'],
                'extensions' => 'png, jpg, jpeg, gif',
                'path' => '@webroot/content/brands',
                'url' => '@web/content/brands',
                'generate_new_name'=>true,
                'thumbs' => [
                    'thumb' => [
                        'width' => 335,
                        'height'=>210,
                        'quality' => 90,
                        'mode'=>\Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%brands}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'trim'],
            [['name'], 'required'],
            [['content'], 'string'],
            [['priority', 'vis', 'on_main'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['name', 'url_alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'url_alias' => 'Url Alias',
            'img' => 'Картинка',
            'content' => 'Контент',
            'priority' => 'Приоритет',
            'vis' => 'Показывать',
            'on_main' => 'On Main',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }
}
