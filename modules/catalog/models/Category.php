<?php

namespace app\modules\catalog\models;

use app\behaviors\AdjacencyListBehavior;
use app\behaviors\UploadImageBehavior;
use app\modules\section\models\Section;
use Yii;
use yii\db\ActiveRecord;
use paulzi\adjacencyList\AdjacencyListQueryTrait;
use app\modules\catalog\models\CategoryQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;


/**
 * This is the model class for table "{{%categories}}".
 *
 * @mixin AdjacencyListBehavior
 * @mixin UploadImageBehavior
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property string $url_alias
 * @property string $short_description
 * @property string $description
 * @property string $img
 * @property integer $vis
 * @property integer $priority
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $h1_tag
 * @property string $updated_at
 * @property string $created_at
 */
class Category extends \yii\db\ActiveRecord
{
    public $url_path;

    /** @var  Section */
    public static $_catalog_section;

    public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'slug' => [
                'class' => 'app\behaviors\Slug',
                'in_attribute' => 'name',
                'out_attribute' => 'url_alias',
                'translit' => true
            ],
            'image' => [
                'class' => \app\behaviors\UploadImageBehavior::className(),
                'attribute' => 'img',
                'scenarios' => ['create', 'update'],
                'path' => '@webroot/content/catalog',
                'url' => '@web/content/catalog',
                'generate_new_name'=>true,
                'thumbs' => [
                    'thumb' => [
                        'width' => 335,
                        'height'=>210,
                        'quality' => 90,
                        'mode'=>\Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                ],
            ],
            [
                'class' => \app\behaviors\AdjacencyListBehavior::className(),
                'sortable'=>['sortAttribute'=>'priority']
            ],
        ];
    }

    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%categories}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'vis', 'priority'], 'integer'],
            [['name'], 'required'],
            [['name'], 'trim'],
            [['short_description', 'description'], 'string'],
            [['updated_at', 'created_at'], 'safe'],
            ['img', 'image', 'extensions' => 'png, jpg, jpeg, gif', 'on' => ['create', 'update']],
            [['name', 'url_alias', 'meta_title', 'meta_description', 'meta_keywords', 'h1_tag'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Родительская категория',
            'name' => 'Название',
            'url_alias' => 'Url Alias',
            'short_description' => 'Краткое описание',
            'description' => 'Описание',
            'img' => 'Картинка',
            'vis' => 'Показывать',
            'priority' => 'Приоритет',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'h1_tag' => 'H1 Tag',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Список категорий
     */
    public static function treeList()
    {
        return self::makeTree(self::find()->roots()->all());
    }

    protected static function makeTree($items, $level = 0)
    {
        if ($level > 20) throw new \LogicException();

        return array_reduce($items, function ($list, $item) use ($level) {
            $list[$item['id']] = str_repeat('¦ -', $level) . $item['name'];
            $sublist = self::makeTree($item->children, $level + 1);
            return array_replace($list, $sublist);
        }, []);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }

    /**
     * Получение категории по пути.
     * Например: /catalog/copper_pipe/majdanpek/
     *
     *
     * @param $path
     * @return mixed
     */
    public static function getByPath($path)
    {
        $model = new Category();

        return $model->findByPath($path);
    }

    /**
     * @return null|Section
     */
    public static function getCatalogSection()
    {
        if(!self::$_catalog_section){
            self::$_catalog_section = Section::find()->where([
                'url_alias' => 'catalog'
            ])->one();
        }

        return self::$_catalog_section;
    }

    public function getUrl()
    {
        //$catalog_section = self::getCatalogSection();

        return Url::toRoute([
            '/catalog/catalog/category',
            //'path' => $catalog_section->getUrlPath(),
            'path' => 'catalog',
            'category' => $this->getUrlPath()
        ]);
    }
}
