<?php
namespace app\modules\subscription;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                '<path:([\w_\/-]+\/)?subscription>/<_a:[\w-]+>' => 'subscription/subscription/<_a>',
            ]
        );
    }
}