<?php
//подключение assets(js/css) сallback'а
\app\modules\subscription\assets\SubscriptionAsset::register($this);

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
?>

<div id="subscription-container">
    <?php $form = ActiveForm::begin([
        'id'=>'subscription-form',
        'enableAjaxValidation' => false,
        'enableClientValidation'=>false,
        //'options'=>['class'=>'remove-bottom login-form'],
        'fieldConfig' => [
            'template' => "{input}{error}",
        ]
    ]);?>
        <label>
            <?=$form->field($model, 'name')->textInput(['class'=>'subscription-widget__field', 'placeholder'=>'ФИО *', 'required'=>true])
                ->label(false);?>
        </label>
        <label>
            <?=$form->field($model, 'email')->textInput(['class'=>'subscription-widget__field', 'placeholder'=>'Email *', 'required'=>true])
                ->label(false);?>
        </label>
        <input type="submit" value="Отправить">
    <?php ActiveForm::end()?>
</div>