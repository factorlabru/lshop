//subscription
jQuery("#subscription-form").on('submit', function(e){

    e.preventDefault();
    e.stopImmediatePropagation();

    var error = "",
        form = jQuery(this).closest("form"),
        data = form.serialize(),
        name = jQuery.trim(jQuery("#subscription-name").val()),
        email = jQuery.trim(jQuery("#subscription-email").val()),
        form = jQuery('#subscription-form').serialize();

    if(!name) {
        error = true;
        jQuery("#subscription-name").addClass("error-field");
    }

    if(!email) {
        error = true;
        jQuery("#subscription-email").addClass("error-field");
    }

    if(!error) {
        jQuery.ajax({
            type: 'POST',
            url: "/subscription/index/",
            data: data,
            success: function (data) {
                if(data) {
                    data = jQuery.parseJSON(data);
                    if(data.result) {
                        location.href = '/subscription/complete/';
                    } else {
                        alert("При отпраке сообщения произошла ошибка");
                    }

                }
            }
        });
    }

    return false;
});