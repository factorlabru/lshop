<?php
namespace app\modules\subscription\assets;

use yii\web\AssetBundle;

class SubscriptionAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/subscription/assets';
    public $baseUrl = '/web';

    public $js = [
        'js/subscription.js',
    ];

    public $publishOptions = [
        'forceCopy' => true
    ];
}