<?php
namespace app\modules\rooms;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                /*Rooms*/
                '<path:([\w_\/-]+\/)?rooms>/page/<page:[0-9]+>'=>'rooms/room/index',
                [
                    'pattern' => '<path:([\w_\/-]+\/)?rooms>/<url_alias:[-a-zA-Z0-9_]+>',
                    'route' => 'rooms/room/view',
                    'suffix' => '.html',
                ],
                '<path:([\w_\/-]+\/)?rooms>' => 'rooms/room/index',
                /*Rooms*/
            ]
        );
    }
}