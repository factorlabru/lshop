<?php
namespace app\modules\rooms\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use app\modules\rooms\models\Room;

/**
 * Виджет выводит номера.
 *
 * Class Rooms
 * @package app\modules\rooms\widgets
 */
class Rooms extends Widget
{
    /**
     * @var int
     */
    public $not_id = 0;

    /**
     * @var string
     */
    public $view = 'similar_rooms';

    /**
     * @var int
     *
     * Количество выводимых записей.
     */
    public $limit = 3;

    /**
     * @var string
     *
     * Тип сортировки.
     */
    public $order = 'created_at DESC';

    public $title = 'Похожие номера';

    public function run()
    {
        $rooms = Room::find()
            ->where('vis=1 AND id != :id', ['id'=>$this->not_id])
            ->limit((int)$this->limit)
            ->orderBy($this->order)
            ->all();

        return $this->render($this->view, [
            'rooms'=>$rooms,
            'title'=>$this->title,
        ]);
    }
}