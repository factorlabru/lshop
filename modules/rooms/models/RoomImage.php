<?php

namespace app\modules\rooms\models;

use app\behaviors\UploadImageBehavior;
use Yii;

/**
 * This is the model class for table "tbl_room_images".
 *
 * @mixin UploadImageBehavior
 *
 * @property integer $id
 * @property integer $room_id
 * @property string $img
 * @property integer $priority
 */
class RoomImage extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            'image' => [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'img',
                'scenarios' => ['default'],
                'extensions' => 'png, jpg, jpeg, gif',
                'thumbs'=> [
                    'thumb' => [
                        'width' => 150,
                        'height'=>90,
                        'quality' => 90,
                        'mode'=>\Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                    'thumb_big' => [
                        'width' => 590,
                        'height'=>410,
                        'quality' => 90,
                        'mode'=>\Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                ],
                'generate_new_name'=>true,
                'path' => '@webroot/content/rooms',
                'url' => '@web/content/rooms',
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_room_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['room_id', 'priority'], 'integer'],
            //[['img'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'room_id' => 'Room ID',
            'img' => 'Img',
            'priority' => 'Priority',
        ];
    }
}
