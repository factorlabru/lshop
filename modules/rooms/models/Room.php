<?php

namespace app\modules\rooms\models;

use app\behaviors\UploadManyImageBehavior;
use app\modules\section\models\Section;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Expression;
use Yii;

/**
 * This is the model class for table "tbl_rooms".
 *
 * @mixin UploadManyImageBehavior
 *
 * @property integer $id
 * @property string $name
 * @property string $url_alias
 * @property integer $price_single
 * @property string $description_block
 * @property string $content
 * @property string $description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $h1_tag
 * @property integer $priority
 * @property string $updated_at
 * @property string $created_at
 */
class Room extends \yii\db\ActiveRecord
{
    public $file_loader;
    public static $_rooms_section;

    public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'slug' => [
                'class' => 'app\behaviors\Slug',
                'in_attribute' => 'name',
                'out_attribute' => 'url_alias',
                'translit' => true
            ],
            'PriorityBehavior' => [
                'class' => 'app\behaviors\PriorityBehavior',
            ],
            [
                'class' => \app\behaviors\UploadManyImageBehavior::className(),
                'attribute' => 'file_loader',
                'behavior_name' => 'image',
                'relation' => 'images',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_rooms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'content'], 'required'],
            [['name'], 'trim'],
            [['price_single', 'priority'], 'integer'],
            [['description_block', 'content', 'description'], 'string'],
            [['updated_at', 'created_at'], 'safe'],
            ['file_loader', 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, gif', 'maxFiles' => 10],
            [['name', 'url_alias', 'meta_title', 'meta_description', 'meta_keywords', 'h1_tag'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'url_alias' => 'Url Alias',
            'price_single' => 'Цена',
            'description_block' => 'Блок описания',
            'content' => 'Контент',
            'description' => 'Описание',
            'file_loader' => 'Фото номера',
            'vis' => 'Показывать',
            'priority' => 'Приоритет',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'h1_tag' => 'H1 Tag',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Получение картинок номера.
     * @return ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(RoomImage::className(), ['room_id' => 'id'])->orderBy(['priority'=>SORT_ASC]);
    }

    /**
     * @return mixed|Section
     */
    public static function getRoomsSection()
    {
        if(!self::$_rooms_section){
            self::$_rooms_section = Section::find()->where([
                'url_alias' => 'rooms'
            ])->one();
        }

        return self::$_rooms_section;
    }

    public function getUrl()
    {
        $rooms_section = self::getRoomsSection();

        $url = \yii\helpers\Url::toRoute([
            '/rooms/room/view',
            'path' => $rooms_section->getUrlPath(),
            'url_alias'=>$this->url_alias
        ]);

        return $url;
    }
}
