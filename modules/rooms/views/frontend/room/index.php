<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php if($rooms) {?>

    <?php foreach($rooms as $room) {?>
        <div class="col-md-12">
            <h3>
                <a href="<?=Url::toRoute(['room/view',
                    'path'=>Yii::$app->request->get('path'),
                    'url_alias'=>$room->url_alias,
                ]);?>"><?=$room->name?></a>
            </h3>
            <?php if($room->mainImg()) {?>
                <a href="<?=$room->getUrl();?>">
                    <img src="<?=$room->mainImg();?>" alt="">
                </a>
            <?php }?>

            <div>
                <?=$room->description?>
            </div>
        </div>
        <hr>
    <?php }?>

<?php }?>
