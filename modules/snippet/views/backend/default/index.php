<?php

use app\modules\snippet\models\SearchSnippet;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\admin\widgets\grid\LinkColumn;
/* @var $this yii\web\View */
/* @var $searchModel SearchSnippet */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сниппеты';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">


        <p>
            <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Добавить запись', ['create'], ['class' => 'btn btn-success']) ?>
            <?=\app\modules\admin\widgets\GridMultiDelete::widget()?>
        </p>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'],
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            [
                'class' => LinkColumn::className(),
                'attribute' => 'name',
            ],

            ['class' => 'yii\grid\ActionColumn','template'=>'{update}  {delete}',],
        ],
    ]); ?>
<?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

