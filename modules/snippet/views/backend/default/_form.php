<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\widgets\CKEditor;
/* @var $this yii\web\View */
/* @var $model app\modules\snippet\models\Snippet */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-body">
    <div class="form-group">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div>
        <?= \app\modules\admin\widgets\CKEditorWidget::widget([
            'form'=>$form,
            'model'=>$model,
            'attr'=>'text',
            'textarea_id'=>'text-content',
            'gallery_widget'=>false,
        ]); ?>
    </div>

    <?= $this->render('@admin_layouts/chunks/_save_buttons', [
        'model' => $model,
        'form' => $form,
    ]); ?>

    <?php ActiveForm::end(); ?>
    </div>
</div>
