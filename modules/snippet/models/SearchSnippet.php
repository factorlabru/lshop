<?php

namespace app\modules\snippet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\snippet\models\Snippet;

/**
 * SearchSnippet represents the model behind the search form about `app\modules\snippet\models\backend\Snippet`.
 */
class SearchSnippet extends Snippet
{
    /**
     * @inheritdoc

     */
    public function search($params)
    {
        $query = Snippet::find();
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'name' => SORT_ASC,
                ]
            ],
        ]);


        if($this->load($params)) {

            // adjust the query by adding the filters

            $query->andFilterWhere(['like', 'name', $this->name]);

        }

        return $provider;
    }
}
