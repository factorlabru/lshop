<?php

namespace app\modules\snippet;

use Yii;
/**
 * snippet module definition class
 */
class Module extends \app\modules\admin\Module
{
    public $module_name = 'Сниппеты';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        \Yii::configure($this, require(__DIR__ . '/config.php'));

        // custom initialization code goes here
    }
}
