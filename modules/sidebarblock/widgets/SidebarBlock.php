<?php
namespace app\modules\sidebarblock\widgets;

use app\modules\section\models\Section;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Виджет выводит боковые блоки.
 *
 * Class SidebarBlock
 * @package app\modules\sidebarblock\widgets
 */
class SidebarBlock extends widget
{
    public $current_item;

    public function run()
    {
        $section = false;
        $current_item = $this->current_item;
        $section = Section::find()->where(['vis'=>1, 'id'=>$current_item])->one();

        return $this->render('sidebar_blocks', ['section'=>$section]);
    }
}