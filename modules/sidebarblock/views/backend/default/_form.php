<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\widgets\CKEditor as CKEditor;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model app\modules\sidebarblock\models\SidebarBlock */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sidebar-block-form content">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(Yii::$app->session->getFlash('save')) {
        echo Alert::widget([
            'options' => ['class' => 'alert-success',],
            'body' => Yii::$app->session->getFlash('save'),
        ]);
    }?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= \app\modules\admin\widgets\CKEditorWidget::widget([
        'form'=>$form,
        'model'=>$model,
        'attr'=>'content',
        'textarea_id'=>'sidebarblock-content',
        'gallery_widget'=>false,
    ]); ?>

    <?= $this->render('@admin_layouts/chunks/_save_buttons', [
        'model' => $model,
        'form' => $form,
    ]); ?>

    <?php ActiveForm::end(); ?>

</div>
