<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\sidebarblock\models\SidebarBlock */

$this->title = 'Создание бокового блока';
$this->params['breadcrumbs'][] = ['label' => 'Боковые блоки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sidebar-block-create content">
    <div class="box">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
