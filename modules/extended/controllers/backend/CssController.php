<?php

namespace app\modules\extended\controllers\backend;

use Yii;
use app\modules\admin\components\AdminController;

/**
 * Default controller for the `extendedstyle` module
 */
class CssController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->setbcAction('Расширенные стили');

        $file_path = Yii::getAlias('@webroot/ext/extended.css');

        if (!file_exists($file_path)) {
            $fp = fopen($file_path, "w");
            fwrite($fp, "/* Extended css styles */");
            fclose ($fp);
        }

        $file = file_get_contents($file_path);

        $content = Yii::$app->request->post('content');
        if(Yii::$app->request->post('content')) {
            file_put_contents($file_path, $content);
            \Yii::$app->getSession()->setFlash('save', 'Данные сохранены.');

            return $this->redirect('/admin/extended/css/');
        }

        return $this->render('index', ['file'=>$file]);
    }
}
