<?php

namespace app\modules\extended\controllers\backend;

use app\modules\admin\actions\AjaxDeleteGridItemsAction;
use app\modules\admin\actions\AjaxEditableAction;
use app\modules\extended\models\CustomHtml;
use app\modules\extended\models\SearchCustomHtml;
use Yii;
use yii\web\NotFoundHttpException;
use app\modules\admin\components\AdminController;

/**
 * DefaultController implements the CRUD actions for Faq model.
 */
class HtmlController extends AdminController
{

    public function actions()
    {
        $model_name = CustomHtml::className();

        return [
            'ajax-editable' => [
                'class' => AjaxEditableAction::className(),
                'model' => $model_name,
            ],
            'ajax-delete-grid-items' => [
                'class' => AjaxDeleteGridItemsAction::className(),
                'model' => $model_name,
            ],
        ];
    }

    /**
     * Lists all Faq models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchCustomHtml();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Faq model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Faq model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CustomHtml();
        $model->vis = 1;
        $model->html = '<!-- custom html -->';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->moduleRedirect($model->id);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Faq model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->moduleRedirect($model->id);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Faq model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Faq model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomHtml the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustomHtml::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
