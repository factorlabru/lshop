<?php

namespace app\modules\extended;

/**
 * extendedjs module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\extended\controllers';

    public $module_name = 'Расширенные скрипты и стили';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        //\Yii::configure($this, require(__DIR__ . '/config.php'));
        // custom initialization code goes here
    }
}
