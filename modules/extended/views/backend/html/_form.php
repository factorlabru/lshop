<?php

use app\components\View;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\faq\models\Faq */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faq-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vis')->checkbox() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'position')->dropDownList([
            View::POS_HEAD => 'Перед </head>',
            View::POS_BEGIN => 'После <body>',
            View::POS_END => 'Перед </body>',
    ], ['prompt'=>'Выберите значение']); ?>

    <?= $form->field($model, 'html')->textarea([
        'id' => 'code',
        'rows' => 6,
        'style' => 'height: 350px; width: 100%;',
    ]) ?>


    <?php
    \app\modules\admin\assets\CodemirrorAsset::register($this);

    $script = <<< JS
     CodeMirror.commands.autocomplete = function(cm) {
        cm.showHint({hint: CodeMirror.hint.anyword});
    }
    var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
        lineNumbers: true,
        extraKeys: {"Ctrl-Space": "autocomplete"}
    });
JS;
    $this->registerJs($script, yii\web\View::POS_READY);

    ?>

    <?= $this->render('@admin_layouts/chunks/_save_buttons', [
        'model' => $model,
        'form' => $form,
    ]); ?>

    <?php ActiveForm::end(); ?>

</div>
