<?php

use app\modules\admin\helpers\XEditableHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use yii\widgets\Pjax;
use app\modules\admin\widgets\grid\LinkColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\faq\models\FaqSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Расширенный HTML';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-index content">

    <div class="box">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <div>
            <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Добавить запись', ['create'], ['class' => 'btn btn-success']) ?>

            <?=\app\modules\admin\widgets\GridMultiDelete::widget()?>
        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => LinkColumn::className(),
                    'attribute' => 'name',
                ],

                // 'phone',
                XEditableHelper::buildEditPriority(),
                XEditableHelper::buildYesNo('vis'),

                ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}{link}'],
            ],
        ]); ?>
    </div>
</div>
