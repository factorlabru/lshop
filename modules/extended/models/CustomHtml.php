<?php

namespace app\modules\extended\models;

use Yii;

/**
 * This is the model class for table "tbl_custom_html".
 *
 * @property integer $id
 * @property integer $position
 * @property integer $vis
 * @property integer $priority
 * @property string $html
 */
class CustomHtml extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_custom_html';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position', 'name'], 'required'],
            [['position', 'vis', 'priority'], 'integer'],
            [['html'], 'string'],
            [['name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position' => 'Позиция',
            'priority' => 'Сортировка',
            'name' => 'Пояснение',
            'vis' => 'Отображать',
            'html' => 'Html-код',
        ];
    }
}
