<?php
return [
    'params' => [
        '_bcModule' => 'Регионы/Города',
        '_bcIcon' => '<i class="fa fa-map"></i>',
        '_items' => [
            [
                'icon' => '',
                'label' => 'Регионы',
                'url' => '/admin/location/region',
            ],
            [
                'icon' => '',
                'label' => 'Города',
                'url' => '/admin/location/city',
            ],
        ]
    ],
];