<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Редактирование города';
$this->params['breadcrumbs'][] = ['label' => 'Города', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="city-update content">
    <div class="box">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>