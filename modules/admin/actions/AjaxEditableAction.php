<?php
namespace app\modules\admin\actions;

use Yii;
use yii\base\Action;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class AjaxEditableAction extends Action
{
    public $model;

    public function run()
    {
        if(Yii::$app->request->post('pk') && is_numeric(Yii::$app->request->post('pk'))) {

            $attr = Yii::$app->request->post('name');
            $model = $this->model;
            $model = $model::findOne(Yii::$app->request->post('pk'));
            if($model) {
                $model->$attr = Yii::$app->request->post('value');
                if($model->validate()) {
                    $model->save();
                } else {
                    $message = ArrayHelper::getValue(array_values($model->getFirstErrors()), 0, 'Ошибка');
                    throw new \RuntimeException($message);
                }
            }
        } else {
            throw new NotFoundHttpException(404);
        }
    }
}