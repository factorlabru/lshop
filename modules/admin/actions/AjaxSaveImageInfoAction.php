<?php
namespace app\modules\admin\actions;

use Yii;
use yii\base\Action;
use yii\web\NotFoundHttpException;

class AjaxSaveImageInfoAction extends Action
{
    public $model;

    public function run()
    {
        if(Yii::$app->request->post('id') && is_numeric(Yii::$app->request->post('id'))) {

            $id = Yii::$app->request->post('id');
            $result = false;
            $model = $this->model;
            !is_string($model) || $model = new $model;

            if(Yii::$app->request->post('RelatedField')[$id]) {

                foreach(Yii::$app->request->post('RelatedField')[$id] as $key=>$val) {
                    $model = $model::findOne(Yii::$app->request->post('id'));
                    if($model) {
                        $model->{$key} = $val;
                    }
                    $model->save();
                }

                $result = true;
            }

            return \yii\helpers\Json::encode(['result'=>$result]);
        }

        throw new NotFoundHttpException('Страница не найдена.');
    }
}