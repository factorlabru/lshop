<?php
/**
 * Action удаляет картину.
 */
namespace app\modules\admin\actions;

use Yii;
use yii\base\Action;
use yii\web\NotFoundHttpException;

class AjaxDeleteImageAction extends Action
{
    /**
     * Основная модель.
     * @var
     */
    public $model;

    /**
     * Связанная модель. Например RoomImage. Необзательный параметр.
     * @var
     */
    public $related_model;

    public $id;

    public $attribute = 'img';

    public function run()
    {
        if (Yii::$app->request->post('id')) {
            //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $this->id =  Yii::$app->request->post('id');
            $this->attribute =  Yii::$app->request->post('attribute') ? Yii::$app->request->post('attribute') : $this->attribute;

            if(!$this->related_model) {
                $result = $this->deleteSingleImage();
            } else {
                $result = $this->deleteMultiImage();
            }

            return \yii\helpers\Json::encode(['result' => $result]);
        }

        throw new NotFoundHttpException('Страница не найдена.');
    }

    private function deleteSingleImage()
    {
        $result = false;
        $model = $this->model;
        $model = $model::findOne($this->id);
        if ($model) {
            if ($model->{$this->attribute}) {
                $model->{$this->attribute} = null;
            }
            $model->save();

            $result = true;
        }

        return $result;
    }

    private function deleteMultiImage()
    {
        $result = false;
        $model = $this->model;
        $related_model = $this->related_model;

        $related_model = $related_model::findOne($this->id);
        if($related_model) {

            $model->deleteFile($related_model->{$this->attribute});
            $related_model->delete();

            $result = true;
        }

        return $result;
    }
}