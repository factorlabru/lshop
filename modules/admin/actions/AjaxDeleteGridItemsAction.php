<?php
/**
 * Action удаляет значения выбранных чекбоксами из gridview.
 */
namespace app\modules\admin\actions;

use Yii;
use yii\base\Action;
use yii\web\NotFoundHttpException;

class AjaxDeleteGridItemsAction extends Action
{
    public $model;

    /**
     * Если подключено поведение для работы с adjacency list.
     *
     * @var
     */
    public $adjacency;

    public function run()
    {
        if(Yii::$app->request->post('selected_items') && is_array(Yii::$app->request->post('selected_items'))) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $model = $this->model;
            $selected_items = [];
            foreach (Yii::$app->request->post('selected_items') as $item) {
                $selected_items[] = intval($item);

                $record = $model::findOne($item);
                if($record) {
                    if($this->adjacency) {
                        $record->deleteWithChildren();
                    } else {
                        $record->delete();
                    }
                }
            }

            //$model::deleteAll('id IN ('.implode(',', $selected_items).')');

            $result = true;

            return \yii\helpers\Json::encode(['result'=>$result]);
        }

        throw new NotFoundHttpException('Страница не найдена.');
    }
}