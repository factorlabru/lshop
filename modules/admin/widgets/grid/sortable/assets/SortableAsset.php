<?php

namespace app\modules\admin\widgets\grid\sortable\assets;

use yii\web\AssetBundle;

class SortableAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/admin/widgets/grid/sortable/assets/files';

    public $js = [
        'js/jquery.binding.js',
        'js/sortable-widgets.js',
    ];

    public $css = [
        'css/sortable-widgets.css',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
    ];
}
