<?php

namespace app\modules\admin\widgets\grid\sortable;

use app\modules\admin\widgets\grid\sortable\assets\SortableAsset;
use yii\helpers\Html;
use yii\web\View;

class SortableColumn extends \yii\grid\Column
{
    public $url;
    public $headerOptions = ['style' => 'width: 30px;'];

    public function init()
    {
        $request = \Yii::$app->request->get($this->grid->filterModel->formName(), []);
        $sort = \Yii::$app->request->get('sort');
        $request = array_filter(array_values($request));

        // отключим сортировку, если заполнены данные запроса
        if(!empty($request) || $sort){
            $this->visible = false;
        }
        SortableAsset::register($this->grid->view);
        $this->grid->view->registerJs('initSortableWidgets("' . $this->url . '", "'.$this->grid->id.'");', View::POS_READY, 'sortable');
    }

    protected function renderDataCellContent($model, $key, $index)
    {
        return Html::tag('div', '&#9776;', [
            'class' => 'sortable-widget-handler '
        ]);
    }
}
