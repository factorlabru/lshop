<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\admin\widgets\backend;


use Yii;

use \app\modules\block\models\Block;

class DashboardWidgetThird extends \yii\bootstrap\Widget
{

    public function init()
    {
        parent::init();
    }
	
	public function run()
	{
		$model = Block::find()->all();
		return $this->render('dashboard_widget_third',['blocks' => $model]
		);
	}

}
