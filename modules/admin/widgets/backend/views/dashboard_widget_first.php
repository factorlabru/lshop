<a href="/admin/<?=$moduleName?>/">
	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-<?=$color?>">
			<div class="inner">
				<h3><?=$count?></h3>

				<p><?=$title?></p>
			</div>
			<div class="icon">
				<i class="ion ion-<?=$icon?>"></i>
			</div>
			<span class="small-box-footer">Подробно <i class="fa fa-arrow-circle-right"></i></span>
		</div>
	</div>
</a>