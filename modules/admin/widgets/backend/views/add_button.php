<?php 
use app\modules\admin\widgets\backend\AddButton;
use yii\helpers\Html;
?>

<p>
	<?=Html::a('<i class="glyphicon glyphicon-plus"></i> Добавить запись', ['create'], ['class' => 'btn btn-success']) ?>
</p>