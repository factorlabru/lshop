<div class="col-md-6">
<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Блоки</h3>

		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<div class="table-responsive">
			<table class="table no-margin">
				<thead>
				<tr>
					<th>Название</th>
					<th>Системное название</th>
					<th>Включен/Выключен</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($blocks as $key => $block) { ?>
				<tr>
					<td><a href="/admin/block/default/update?id=<?= $block->id ?>" title="Редактировать блок"><?= $block->name ?></a></td>
					<td><?= $block->title ?></td>
					<td><span class="label label-<?= ($block->vis?'success':'danger'); ?>"><?= ($block->vis?'Включен':'Выключен'); ?></span></td>
				</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
		<!-- /.table-responsive -->
	</div>
	<!-- /.box-body -->
	<div class="box-footer text-center">
		<a href="/admin/block/default/index" class="uppercase">Перейти к блокам</a>
	</div>
	<!-- /.box-footer -->
</div>
<!-- /.box -->
	</div>