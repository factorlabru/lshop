<?php
use yii\helpers\Html;
?>

<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить и закрыть', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'value'=>'Update and close', 'name'=>'submit']) ?>
<?= ( !$model->isNewRecord ? Html::submitButton('Сохранить и продолжить', ['class' => 'btn btn-primary', 'style'=>'margin-left:10px', 'value'=>'Update and continue', 'name'=>'submit']) : '' ); ?>




