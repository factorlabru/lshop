<?php


namespace app\modules\admin\widgets\backend;


use Yii;

use \app\modules\gallery\models\backend\Gallery;
use \app\modules\gallery\models\backend\Image;

class DashboardWidgetSecond extends \yii\bootstrap\Widget
{

    public function init()
    {
        parent::init();
    }
	
	public function run()
	{
		$model=Gallery::find()->all();
		return $this->render('dashboard_widget_second',['gallery' => $model]);
	}
	

	



	
}
