<?php
namespace app\modules\admin\widgets;

use Yii;
use yii\base\Widget;
use app\modules\snippet\models\Snippet;
use app\modules\gallery\models\Gallery;

class CKEditorWidget extends Widget
{
    /**
     * Параметр вкл/выкл wysiwyg
     * @var bool
     */
    public $vis = true;

    public $model;

    public $form;

    public $container_class='col-md-10 nopadding';

    public $attr;

    /**
     * Параметр для вывода сниппетов и галерей.
     * @var
     */
    public $textarea_id;

    /**
     * @var bool
     */
    public $snippet_widget=true;

    /**
     * @var bool
     */
    public $gallery_widget=true;

    public function run()
    {
        $snippets = false;
        $galleries = false;

        if($this->snippet_widget) {
            $snippets = Snippet::find()
                ->orderBy('name')
                ->all();
        }

        if($this->gallery_widget) {
            $galleries = Gallery::find()
                ->orderBy('name')
                ->all();
        }

        $vis = $this->vis;
        //еще один способ выключить wysiwyg-редактор, передать GET-параметр wysiwig=off
        if(Yii::$app->request->get('wysiwyg') && Yii::$app->request->get('wysiwyg') == 'off') {
            $vis = false;
        }

        return $this->render('ckeditor_widget', [
            'container_class'=>$this->container_class,
            'model'=>$this->model,
            'form'=>$this->form,
            'attr'=>$this->attr,
            'snippets'=>$snippets,
            'galleries'=>$galleries,
            'textarea_id'=>$this->textarea_id,
            'vis'=>$vis
        ]);
    }
}