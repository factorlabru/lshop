<?php
use kartik\sortable\Sortable;
?>

<?php
$widget_id = $this->context->getId();

$script = <<< JS
      !function($){

        var widget_class = '.js-sortable-' + '{$widget_id}'

        function updatePriority()
        {
            var priority = 1;
            $(".elm-priority", widget_class).each(function(index, el) {
                $(el).val(priority);
                priority++;
            });
        }

        updatePriority();


        $(function(){
            $(".js-sortable-" + '{$widget_id}').on('priority_updated', function(){
                updatePriority();
            });
        });

        $(document).on('click', widget_class + ' .del-item', function(event) {
            var id = $(this).closest(".files-block__item").data('id');

            if (confirm("Удалить картинку?")) {
                $(widget_class + ' div[data-id='+id+']').parent().remove();

                $.ajax({
                    type: 'POST',
                    url: "$request_del_url",
                    data: {id: id},
                    success: function (data) {
                        if(data) {
                            data = $.parseJSON(data);
                        }
                    }
                });
            }

            return false;
        });

        $(document).on('click', widget_class + ' .save-item', function(event) {
            var id = $(this).closest(".files-block__item").data('id'),
                fields = $(this).closest(".files-block__item").find('.form-control'),
                data = fields.serialize(),
                data = data+'&id='+id;

            $('.saved-info').remove();

            $('div[data-id='+id+']', widget_class).parent().prepend('<div class="saved-info">Информация сохранена</div>');

            $.ajax({
                type: 'POST',
                url: "$request_save_url",
                data: data,
                success: function (data) {
                    if(data) {
                        data = $.parseJSON(data);
                    }
                }
            });


            return false;
        });
    }(jQuery);

JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>

<?php
if($data) {
    $items = [];
    foreach($data as $id=>$item) {
        $name_block = '';
        $description_block = '';

        if($item->hasAttribute('name')) {
            $name_block = '<div class="files-block__field">
                                Название: <input type="text" value="'.$item->name.'" class="elm-name form-control" name="RelatedField['.$item->id.'][name]">
                            </div>
                            <br>';
        }

        if($item->hasAttribute('description')) {
            $description_block = ' <div class="files-block__field">
                                        Описание: <textarea type="text" class="elm-description form-control" name="RelatedField['.$item->id.'][description]">'.$item->description.'</textarea>
                                    </div>
                                    <br>';
        }

        $items[] = ['content'=>
            '<div class="col-md-12 files-block__item" data-id="'.$item->id.'">
                <div class="col-md-9" style="padding-right: 0px">
                    <a href="' . $item->img('') . '"><img src="' . $item->img('thumb') . '" width="200"></a>
                    '.$name_block.'
                    '.$description_block.'
                    <div class="files-block__field">
                        Приоритет: <input type="text" value="" class="elm-priority m-mini form-control" name="RelatedField['.$item->id.'][priority]">
                    </div>
                </div>

                <div class="col-md-3 controls-group">
                     <div class="controls-group__item">
                         <a data-attribute="del-item" type="button" class="btn btn-danger del-item">
                            <i class="fa fa-trash"></i>
                         </a>
                     </div>

                    <div class="controls-group__item">
                        <button data-attribute="save-item" type="button" class="btn btn-success save-item">
                            <i class="fa fa-save"></i>
                        </button>
                    </div>
                </div>
            </div>'
        ];
    }

    echo Sortable::widget([
        'type'=>'grid',
        'items'=>$items,
        'options' => [
            'class' => 'js-sortable-' . $widget_id
        ],
        'pluginEvents' => [
            'sortupdate' => 'function() { 
                $(".js-sortable-" + "'.$widget_id.'").trigger("priority_updated")
            }',
        ]
    ]);
} ?>
<div class="clearfix"></div>
