<?php
$token = Yii::$app->request->getCsrfToken();
$bundle = \app\modules\admin\assets\backend\AppAsset::register($this);
$script = <<< JS

    $(document).on('click', '.add-snippet__block', function(event) {
        CKEDITOR.instances['$textarea_id'].insertHtml('aaaaa');
        return false;
    });

    $(document).on('click', '.snippets-block a', function(event) {
       
        //CKEDITOR.instances['$textarea_id'].destroy()
        
        var id = $(this).data('id');
      
       $.ajax({
            type: 'POST',
            url: '/admin/snippet/default/ajax-load-snippet/',
            data: {id:id, _csrf:"$token"},
            success: function (data) {
                if(data) {
                    data = $.parseJSON(data);
                    if(data.snippet) {
                        CKEDITOR.instances['$textarea_id'].insertHtml(data.snippet);
                    }
                }
            }
        });

        return false;

    });
    
    $(document).on('click', '.galleries-block a', function(event) {
        
        var id = $(this).data('id'),
        gallery_html = '' +
         '<img id="inner-gallery'+id+'" src="$bundle->baseUrl/images/gallery.png" style="display:block" />';
           
        CKEDITOR.instances['$textarea_id'].insertHtml(gallery_html);
      
        return false;
    });

JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>
<div class="col-md-2">
    <?php if($snippets) {?>
        <div class="accordion-group" style="margin-top: 190px">
            <div class="accordion-heading">
                <i class="glyphicon glyphicon-arrow-left"></i>
                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#snippets-data">
                    Сниппеты
                </a>
            </div>
            <div id="snippets-data" class="accordion-body collapse">
                <div class="snippets-block">
                    <ul>
                        <?php foreach ($snippets as $snippet) {?>
                            <li>
                                <a href="#" data-id="<?=$snippet->id?>"><?=$snippet->name?></a>
                            </li>
                        <?php }?>
                    </ul>
                </div>
            </div>
        </div>
    <?php }?>

    <?php if($galleries) {?>
        <div class="accordion-group">
            <div class="accordion-heading">
                <i class="glyphicon glyphicon-arrow-left"></i>
                <a class="accordion-toggle collapsed" data-toggle="collapse" href="#galleries-data">
                    Галереи
                </a>
            </div>
            <div id="galleries-data" class="accordion-body collapse">
                <div class="galleries-block">
                    <ul>
                        <?php foreach ($galleries as $gallery) {?>
                            <li>
                                <a href="#" data-id="<?=$gallery->id?>"><?=$gallery->name?></a>
                            </li>
                        <?php }?>
                    </ul>
                </div>
            </div>
        </div>
    <?php }?>
</div>

<div class="clearfix"></div>