<?php
use app\widgets\CKEditor as CKEditor;
use yii\helpers\Html;

$textarea_id = Html::getInputId($model, $attr) . '-' . $model->id
?>

<div class="<?= $container_class ?>">
    <?php
    if ($vis) {
        echo $form->field($model, $attr)->widget(CKEditor::className(), [
            'options' => [
                'rows' => 10,
                'id' => $textarea_id,
            ],
            'preset' => 'full',
            'clientOptions' => [
                'customConfig' => '/ext/ckeditor/config.js',
            ]
        ]);
    } else {
        echo $form->field($model, $attr)->textarea(['rows' => 6]);
    } ?>
</div>


<?php
$token = Yii::$app->request->getCsrfToken();
$bundle = \app\modules\admin\assets\backend\AppAsset::register($this);


$script = <<< JS

    $(document).on('click', '#snippets-data_$textarea_id .snippets-block a', function(event) {
       
        //CKEDITOR.instances['$textarea_id'].destroy()
        
        var id = $(this).data('id'),
        container_id = $(this).closest('div').data('container');
     
       $.ajax({
            type: 'POST',
            url: '/admin/snippet/default/ajax-load-snippet/',
            data: {id:id, _csrf:"$token"},
            success: function (data) {
                if(data) {
                    data = $.parseJSON(data);
                    if(data.snippet) {
                        CKEDITOR.instances[container_id].insertHtml(data.snippet);
                    }
                }
            }
        });

        return false;

    });
    
    $(document).on('click', '#galleries-data_$textarea_id .galleries-block a', function(event) {
        
        var id = $(this).data('id'),
        gallery_html = '' +
         '<img id="inner-gallery'+id+'" src="$bundle->baseUrl/images/gallery.png" style="display:block" />';
           
        CKEDITOR.instances['$textarea_id'].insertHtml(gallery_html);
      
        return false;
    });

JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>

<?php if ($vis) { ?>
    <div class="col-md-2">
        <?php if ($snippets) { ?>
            <div class="accordion-group" style="margin-top: 190px">
                <div class="accordion-heading">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                    <a class="accordion-toggle collapsed" data-toggle="collapse"
                       href="#snippets-data_<?= $textarea_id ?>">
                        Сниппеты
                    </a>
                </div>
                <div id="snippets-data_<?= $textarea_id ?>" class="accordion-body collapse">
                    <div class="snippets-block" data-container="<?= $textarea_id ?>">
                        <ul>
                            <?php foreach ($snippets as $snippet) { ?>
                                <li>
                                    <a href="#" data-id="<?= $snippet->id ?>"><?= $snippet->name ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        <?php } ?>

        <?php if ($galleries) { ?>
            <div class="accordion-group">
                <div class="accordion-heading">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                    <a class="accordion-toggle collapsed" data-toggle="collapse"
                       href="#galleries-data_<?= $textarea_id ?>">
                        Галереи
                    </a>
                </div>
                <div id="galleries-data_<?= $textarea_id ?>" class="accordion-body collapse">
                    <div class="galleries-block" data-container="<?= $textarea_id ?>">
                        <ul>
                            <?php foreach ($galleries as $gallery) { ?>
                                <li>
                                    <a href="#" data-id="<?= $gallery->id ?>"><?= $gallery->name ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } ?>

<div class="clearfix"></div>