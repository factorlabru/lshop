<?php
use yii\helpers\Html;
?>

<?php
$script = <<< JS

     
        if(!$("input[type='checkbox'][name='selection[]']").length) {
            $('.select-on-check-all').attr('disabled', 'true')
        }
    
        $(document).on('click', '.grid-view input[type=checkbox]', function(event) {
        var exist = false,
            selected_items = $('#w0').yiiGridView('getSelectedRows');

        $('.del-selected-items').addClass('disabled');
        $( ".grid-view input[type=checkbox]").each(function( index ) {
            if($(this).is(':checked')) {
                exist = true;
            }
        });

        if(exist) {
            $('.del-selected-items').removeClass('disabled');
        }
    });

    $(document).on('click', '.del-selected-items', function(event) {
        if(!confirm('Вы действительно хотите удалить данные?')){
            return false;
        }

        var selected_items = $('#w0').yiiGridView('getSelectedRows');
        if(selected_items.length) {
            $.ajax({
                type: 'POST',
                url: "$request_del_url",
                data: {selected_items: selected_items},
                success: function (data) {
                    if(data) {
                        data = $.parseJSON(data);

                        if(data.result) {
                            location.reload();
                        }
                    }
                }
            });
        } else {
            alert('Записи не выбраны или отсутствуют');
        }

        return false;
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>

&nbsp;
<?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', ['create'], ['class' => 'del-selected-items btn btn-danger disabled']) ?>
