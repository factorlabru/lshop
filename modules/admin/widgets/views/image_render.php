<?php
use yii\helpers\Html;
?>

<div class="col-md-6">
    <?php
    $script = <<< JS
        $(document).on('click', '.del-$attribute', function(event) {

            if (confirm("Удалить картинку?")) {
                jQuery.ajax({
                    type: 'POST',
                    url: "$request_url",
                    data: {id: "$id",  attribute:"$attribute"},
                    success: function (data) {
                        if(data) {
                            data = $.parseJSON(data);
    
                            $("a[data-attribute=$attribute]").parent().remove();
                        }
                    }
                });
            }
            return false;
        });
JS;
    $this->registerJs($script, yii\web\View::POS_READY);
    ?>

    <a data-attribute="<?=$attribute?>"
       type="button"
       class="btn btn-danger del-<?=$attribute?>">
        <i class="fa fa-trash"></i>
    </a>

    <div class="col-md-6">
        <?= Html::img($img_url, ['class' => 'img-thumbnail']); ?>
    </div>

</div>
<div class="clear"></div>
<br>
