<?php
namespace app\modules\admin\widgets;

use cebe\markdown\inline\UrlLinkTrait;
use Yii;
use yii\base\Widget;
use yii\helpers\Url;

class ImageRender extends Widget
{
    /**
     * id - записи.
     * @var int
     */
    public $id;

    /**
     * Урл картинки.
     * @var string
     */
    public $img_url;

    /**
     * Маршрут на который идет запрос.
     * @var string
     */
    public $request_url = ['ajax-main-delete-image'];

    /**
     * Атрибут в модели, который будет меняться.
     * @var string
     */
    public $attribute='img';

    public function run()
    {
        $request_url = Url::to($this->request_url);

        return $this->render('image_render', [
            'id'=>$this->id,
            'img_url'=>$this->img_url,
            'request_url'=>$request_url,
            'attribute'=>$this->attribute,
        ]);
    }
}
