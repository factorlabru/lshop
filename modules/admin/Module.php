<?php

namespace app\modules\admin;


class Module extends \yii\base\Module
{

    public $_order = '';
    public $_bcModule = '';
    public $_bcIcon = '';
    public $_items = '';

    public $controllerNamespace = 'app\modules\admin\controllers\backend';//'common\modules\user\controllers';

    public function init()
    {
        //$this->layoutPath = '@app/modules/admin/views/backend/default';
        $this->layout = 'main';

    }
}
