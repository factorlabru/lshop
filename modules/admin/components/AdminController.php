<?php
namespace app\modules\admin\components;

use app\components\AccessBehavior;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\HttpException;
use yii\helpers\Url;
use yii\filters\VerbFilter;

class AdminController extends Controller
{

    public $layout = '@admin_layouts/main.php';

    /**
     * Название контроллера - используется в хлебных крошках
     */
    public $_bcController = '';

    /**
     * Название экшена - используется в хлебных крошках
     */
    public $_bcAction = '';

    /**
     * Порядок отображения в левом меню - используется в виджете Menu
     */
    public $_order = '';

    /**
     * Отображение кнопки "Добавить" на странице списка элементов (обычно actionIndex)
     */
    public $_enableCreate = true;

    public function setbcAction($name)
    {
        $this->_bcAction = $name;
    }

    public function getbcAction()
    {
        return $this->_bcAction;
    }

    public function moduleRedirect($id)
    {
        if(Yii::$app->request->post('save_close')) {
            return $this->redirect(['index']);
        }

        return $this->redirect(['update', 'id'=>$id]);
    }


    public function behaviors()
    {
        return [
            'as AccessBehavior' => [
                'class' => AccessBehavior::className(),
                //'redirect_url' => '/',
                'login_url' => Yii::$app->user->loginUrl,
                'allowedRules' => [
                    'admin/default/login',
                    'admin/default/logout',
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Редирект на экшен index текущего контроллера
     */
    public function goToIndex(){
        $this->redirect(['index']);
    }

    public function actionDevmodeon()
    {
        Yii::$app->session->set('devmode',true);
        $this->goToIndex();
    }

    public function actionDevmodeoff()
    {
        Yii::$app->session->set('devmode',false);
        $this->goToIndex();
    }

    public function beforeAction($action)
    {
        if (!Yii::$app->user->can('admin') && !Yii::$app->user->can('admin/default/index') && (($action->id != 'login') && ($this->module->id != 'user'))) {
            $this->redirect(Url::toRoute('@login'));
        }

        return parent::beforeAction($action);
    }
}
