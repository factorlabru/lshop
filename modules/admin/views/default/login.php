<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<div id="wrapper" class="col-md-4 col-md-offset-4 vertical-align-parent">
    <div class="vertical-align-child">
        <div class="panel">
            <div class="panel-heading text-center"></div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'enableClientValidation' => false,
                    'enableAjaxValidation' => false,
                    'fieldConfig' => [
                        'template' => '{input}',
                    ],
                ]); ?>
                    <div class="form-group field-loginform-username required">
                        <div>
                            <?= $form->field($model, 'username')
                                ->textInput([
                                    "autofocus"=>true,
                                    "class"=>"form-control",
                                    'placeholder'=>'Логин',
                                    "id"=>"user_login",
                                    "size"=>"20",
                                ])?>
                        </div>
                        <div class="errors"><?= Html::error($model, 'username'); ?></div>
                    </div>
                    <div class="form-group field-loginform-password required">
                        <div>
                            <?= $form->field($model, 'password')
                                ->passwordInput([
                                    "class"=>"form-control",
                                    'placeholder'=>'Пароль',
                                    "id"=>"user_password"
                                ])?>
                        </div>
                        <div class="errors"><?= Html::error($model, 'password'); ?></div>
                    </div>
                    <div class="col-md-6">
                        <div class="forgetmenot">
                            <div class="remeberme-checkbox-container">
                                <?= $form->field($model, 'rememberMe')->checkbox(["id"=>"rememberme"])->label(false) ?>
                            </div>
                            <div class="remeberme-label-container">
                                <label for="rememberme">
                                    &nbsp;Запомнить
                                </label>
                            </div>
                        </div>
                    </div>

                <div class="col-md-6 no-right-padding">
                    <button type="submit" class="btn btn-lg btn-primary btn-block">Войти</button>
                </div>
                <div class="clearfix"></div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>