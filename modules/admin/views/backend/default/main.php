<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\modules\admin\assets\backend\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
//use app\modules\admin\widgets\backend\Breadcrumbs;
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;
use app\modules\admin\widgets\backend\Menu;

$bundle = AppAsset::register($this);
$js = <<< 'SCRIPT'
/* To initialize BS3 tooltips set this below */
$(function () { 
    $("[data-toggle='tooltip']").tooltip(); 
});;
/* To initialize BS3 popovers set this below */
$(function () { 
    $("[data-toggle='popover']").popover(); 
});
SCRIPT;
// Register tooltip/popover initialization javascript
$this->registerJs($js);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title).' | Панель управления'?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-red sidebar-mini">
<?php $this->beginBody() ?>
<div class="wrapper">
	<header class="main-header">
		<a href="/" class="logo">
			<span class="logo-mini"><i class="fa fa-home"></i></span>
			<span class="logo-lg"><?=isset(Yii::$app->params['admin_site_name']) ? Yii::$app->params['admin_site_name'] : '' ?></span>
		</a>
		<nav class="navbar navbar-static-top" role="navigation">
			<a href="#" class="sidebar-toggle left-sidebar__control" data-toggle="offcanvas" role="button">
				<span class="sr-only">Навигация</span>
			</a>
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav right-navbar__container">
                    <li>
                        <a title="Сбросить кеш" href="/admin/clearcache/"><i class="fa fa-recycle"></i></a>
                    </li>
                    <li class="<?=\Yii::$app->controller->module->id == 'modulemanager' ? 'active' : ''?>">
                        <a title="Менеджер модулей" href="/admin/modulemanager/"><i class="fa fa-cubes"></i></a>
                    </li>
                    <li class="<?=\Yii::$app->controller->module->id == 'snippet' ? 'active' : ''?>">
                        <a title="Сниппеты" href="/admin/snippet/default/index"><i class="fa fa-code"></i></a>
                    </li>
                    <li class="<?=\Yii::$app->controller->module->id == 'block' ? 'active' : ''?>">
                        <a title="Блоки" href="/admin/block/default/index"><i class="fa fa-object-ungroup"></i></a>
                    </li>
                    <li class="<?= is_active_url('/admin/extended/html/') ? 'active' : ''?>">
                        <a title="Расширенный html" href="/admin/extended/html/"><i class="fa fa-code-fork"></i></a>
                    </li>
                    <li class="<?= is_active_url('/admin/extended/css/') ? 'active' : ''?>">
                        <a title="Расширенные стили" href="/admin/extended/css/"><i class="fa fa-file-code-o"></i></a>
                    </li>
                    <li class="<?= is_active_url('/admin/extended/robots/') ? 'active' : ''?>">
                        <a title="Редактирование robots.txt" href="/admin/extended/robots/"><i class="fa fa-file-text-o"></i></a>
                    </li>
                    <li  class="<?=\Yii::$app->controller->module->id == 'param' ? 'active' : ''?>">
                        <a title="Настройки сайта" href="/admin/param/default/index"><i class="fa fa-gears"></i></a>
                    </li>
					<li>
						<?php if(Yii::$app->user->isGuest) {
							echo Html::a('Вход',Url::toRoute('/admin/default/login'));
						} else {
							echo Html::a('Выход (' . Yii::$app->user->identity->username . ')',Url::toRoute('/admin/default/logout'));
						}?>
					</li>
					<li>
						<a href="#" data-toggle="control-sidebar"><i class="fa fa-list"></i></a>
					</li>
				</ul>
			</div>
		</nav>
	</header>
	<aside class="main-sidebar">
		<section class="sidebar">
			<?= Menu::widget(); ?>
		</section>
	</aside>
  <div class="content-wrapper">
	<section class="content-header">
		<div class="breadcrumbs">
			<?= Breadcrumbs::widget([
				'homeLink' => ['label' => 'Главная', 'url' => '/admin'],
				'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
			]) ?>
		</div>

		<h1><?= Html::encode($this->title) ?></h1>
    </section>
    <?php echo $content; ?>
  </div>
  <footer class="main-footer">
    <strong>&copy; <?=date('Y')?></strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->

    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li class="active">
            <a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-file-text-o"></i></a>
        </li>
        <?php if(Yii::$app->moduleIsActive('catalog')) {?>
            <li>
                <a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-shopping-cart"></i></a>
            </li>
        <?php }?>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="control-sidebar-home-tab">
		  <?= \app\modules\section\widgets\SectionTree::widget(); ?>
      </div>

      <?php if(Yii::$app->moduleIsActive('catalog')) {?>
          <div class="tab-pane" id="control-sidebar-settings-tab">
              <?= \app\modules\catalog\widgets\CategoryTree::widget(); ?>
          </div>
      <?php }?>
    </div>
  </aside>
  <div class="control-sidebar-bg"></div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
