<?php
use yii\helpers\Html;
use app\components\View;

/** @var View $this */
?>

<div class="clear"></div>

<div id="stick-mark"></div>
<div class="form-group save-buttons">
    <div class="simple-save_buttons">
        <?= Html::submitButton('Сохранить и закрыть', ['name' => 'save_close', 'value'=>'save_close', 'class'=>'btn btn-primary']) ?>
        <?= Html::submitButton('Сохранить и продолжить', ['name'=>'save_continue', 'value'=>'save_continue', 'class'=>'btn btn-primary']) ?>
    </div>

    <div class="icons-save_buttons">
        <button title="Сохранить и закрыть" type="submit" class="btn btn-primary" name="save_close" value="save_close">
            <i class="glyphicon glyphicon-floppy-open"  style="font-size:25px"></i>
        </button>
        <br><br>
        <button title="Сохранить и продолжить" type="submit" id="mybtn" class="btn btn-primary" name="save_continue" value="save_continue">
            <i class="glyphicon glyphicon-floppy-save" style="font-size:25px"></i>
        </button>
    </div>
</div>


<?php
$JS = <<<JS
 // если сделали изменения в форме и закрываем/обновлняем окно, то выдавать предупреждение
    !function($){
        var unloader_setup = function(){
            window.onbeforeunload = function(e) {
                var dialogText = 'Возможно, внесенные изменения не сохранятся.';
                e.returnValue = dialogText;
                return dialogText;
            };
            unloader_setup = function(){};
        };

        $('input, textarea, select').on('change input', function(){
            unloader_setup();
        })
        $('form').on('submit', function(){
            window.onbeforeunload = null;
        });
    }(jQuery);
JS;

$this->registerJs($JS, View::POS_END);

?>