<?php
namespace app\modules\feedback;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                '<path:([\w_\/-]+\/)?(feedback|contacts)>' => 'feedback/feedback/index',
                '<path:([\w_\/-]+\/)?(feedback|contacts)>/<_a:[\w-]+>' => 'feedback/feedback/<_a>',
            ]
        );
    }
}