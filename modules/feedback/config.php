<?php

namespace app\modules\feedback;

return [
    'params' => [
        '_bcModule' => 'Обратная связь',
        'viewed'=>true,
        'model'=>'\app\modules\feedback\models\Feedback',
    ],
];