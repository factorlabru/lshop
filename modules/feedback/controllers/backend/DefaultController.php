<?php

namespace app\modules\feedback\controllers\backend;

use app\modules\admin\actions\AjaxDeleteGridItemsAction;
use Yii;
use app\modules\feedback\models\Feedback;
use app\modules\feedback\models\FeedbackSearch;
use yii\web\NotFoundHttpException;
use app\modules\admin\components\AdminController;

/**
 * DefaultController implements the CRUD actions for Feedback model.
 */
class DefaultController extends AdminController
{

    public function actions()
    {
        $model_name = \app\modules\feedback\models\Feedback::className();

        return [
            'ajax-delete-grid-items' => [
                'class' => AjaxDeleteGridItemsAction::className(),
                'model' => $model_name,
            ],
        ];
    }

    /**
     * Lists all Feedback models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FeedbackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Feedback model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $model->updateAttributes([
            'viewed' => 1
        ]);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Feedback model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Feedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Feedback the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Feedback::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
