<h1>Главная</h1>

<?php
$ip = $_SERVER['REMOTE_ADDR'];
$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
echo $details->city;
?>
<?=\app\modules\slider\widgets\Slider::widget();?>

<?=\app\widgets\Block::widget(['id'=>1]);?>


<h2>Новинки</h2>

<?=\app\modules\catalog\widgets\ProductsByParam::widget([
    'param'=>'new',
    'limit'=>8,
    'order'=>'RAND()',
    'view'=>'new_products'
]);?>
<div style="clear: both"></div>
<br>
<h2>Спецпредложение</h2>
<?=\app\modules\catalog\widgets\ProductsByParam::widget([
    'param'=>'special',
    'limit'=>8,
    'order'=>'RAND()',
    'view'=>'special_products'
]);?>
