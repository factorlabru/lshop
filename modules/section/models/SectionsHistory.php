<?php

namespace app\modules\section\models;

use Yii;

/**
 * This is the model class for table "tbl_sections_history".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $model_id
 * @property string $model_name
 * @property string $name
 * @property string $url_alias
 * @property string $content
 * @property string $img
 * @property integer $priority
 * @property integer $vis
 * @property integer $menu_vis
 * @property integer $child_vis
 * @property string $external_link
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $h1_tag
 * @property string $updated_at
 * @property string $created_at
 */
class SectionsHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_sections_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'model_id', 'priority', 'vis', 'menu_vis', 'child_vis'], 'integer'],
            [['name'], 'required'],
            [['content'], 'string'],
            [['updated_at', 'created_at'], 'safe'],
            [['model_name', 'name', 'url_alias', 'img', 'external_link', 'meta_title', 'meta_description', 'meta_keywords', 'h1_tag'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_id' => 'Model ID',
            'model_name' => 'Model Name',
            'parent_id' => 'Родительский раздел',
            'name' => 'Название',
            'url_alias' => 'Url Alias',
            'content' => 'Контент',
            'img' => 'Картинка',
            'priority' => 'Приоритет',
            'vis' => 'Показывать',
            'menu_vis' => 'Показывать в меню',
            'child_vis' => 'Показывать дочерние разделы',
            'external_link' => 'Внешняя ссылка',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'h1_tag' => 'H1 Tag',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }
}
