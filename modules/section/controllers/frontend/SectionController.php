<?php
namespace app\modules\section\controllers\frontend;

use app\modules\section\models\Section;
use app\components\Controller;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use app\modules\gallery\models\InsertGallery;

/**
 * Section controller for the `section` module
 */
class SectionController extends Controller
{
    public function actionIndex($path)
    {
        //$this->layout = '@app/views/layouts/common.php';

        $model = (new Section())->findByPath($path, $strict = false) or $this->fail();

        if($model->getUrlPath() != $path){
            $this->redirect($model->getUrl());
        }

        $this->current_section = $model->id;
        $this->current_item = $model->id;

        $model = InsertGallery::parseData($model);

        $this->setMetaByModel($model);
        $this->setBreadcrumbs($model->makeBreadcrumbs());

        return $this->render('index', ['model'=>$model]);
    }
}