<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\widgets\CKEditor as CKEditor;
use app\modules\admin\widgets\ImageRender;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model app\modules\section\models\Section*/
/* @var $form yii\widgets\ActiveForm */
?>

<div class="section-form content">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(Yii::$app->session->getFlash('save')) {
        echo Alert::widget([
            'options' => ['class' => 'alert-success',],
            'body' => Yii::$app->session->getFlash('save'),
        ]);
    }?>

    <?php if(Yii::$app->session->getFlash('restore')) {
        echo Alert::widget([
            'options' => ['class' => 'alert-success',],
            'body' => Yii::$app->session->getFlash('restore'),
        ]);
    }?>

    <?php if($model->sectionHistory) {?>
        <div class="col-md-12 text-right">
            <a href="/admin/section/timemachine/<?=$model->id?>/index/">
                <i class="fa fa-history" aria-hidden="true"></i>
                Предыдущие версии
            </a>
        </div>
    <?php }?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'vis')->checkbox() ?>

    <?php //echo $form->field($model, 'child_vis')->checkbox(); ?>

    <?= $form->field($model, 'menu_vis')->checkbox() ?>

    <?= $form->field($model, 'parent_id')->dropDownList(\app\modules\section\models\Section::treeList(), ['prompt'=>'Выберите значение']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'class'=>'form-control translit-from']) ?>

    <?php if(Yii::$app->moduleIsActive('yandextranslator')) {
        echo \app\modules\yandextranslator\widgets\UrlTranslate::widget([
            'source_id' => Html::getInputId($model, 'name'),
            'target_id' => Html::getInputId($model, 'url_alias'),
        ]);
    } ?>

    <?php
    $path = '';
    if(!$model->isNewRecord) {
        $path = \app\helpers\Common::getPathLink($model->getUrl());
    }

    echo $form->field($model, 'url_alias', [
        'template' => '{label}
                <div class="col-sm-12 nopadding inline-input">
                    {input} ' . $path  . ' 
                    {error}{hint}
                </div>'
    ])->textInput(['maxlength' => true, 'class'=>$model->url_alias ? 'form-control' : 'form-control translit-to']);?>

    <?= \app\modules\admin\widgets\CKEditorWidget::widget([
            'form'=>$form,
            'model'=>$model,
            'attr'=>'content',
            'textarea_id'=>'section-content',
    ]); ?>

    <?= $form->field($model, 'priority')->textInput(['class'=>'form-control m-mini']) ?>

    <?php
    //Если, подключен модуль боковые блоки.
    if(Yii::$app->moduleIsActive('sidebarblock')) {?>
        <div class="many-to-many_container">
            <?= $form->field($model, 'sidebarblocks')
                ->checkboxList(ArrayHelper::map(\app\modules\sidebarblock\models\SidebarBlock::find()->all(), 'id', 'name'))
                ->label('Боковые блоки') ?>
        </div>
    <?php }?>

    <?= $form->field($model, 'img')->fileInput() ?>

    <?php if($model->img) {
        echo ImageRender::widget([
            'id'=>$model->id,
            'attribute'=>'img',
            'img_url'=>$model->img(),
        ]);
    }?>

    <?= $this->render('@admin_layouts/chunks/_meta_form', [
        'model' => $model,
        'form' => $form,
    ]); ?>

    <?= $this->render('@admin_layouts/chunks/_save_buttons', [
        'model' => $model,
        'form' => $form,
    ]); ?>


    <?php ActiveForm::end(); ?>

</div>
