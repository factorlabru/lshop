<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\section\models\Section */

$this->title = 'Создание раздела';
$this->params['breadcrumbs'][] = ['label' => 'Разделы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="section-create content">
    <div class="box">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
