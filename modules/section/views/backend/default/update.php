<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\section\models\Section */

$this->title = 'Редактирование раздела "'.Html::encode($model->name).'"';
$this->params['breadcrumbs'][] = ['label' => 'Разделы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="section-update content">
    <div class="box">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
