<?php

use app\modules\admin\helpers\XEditableHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\admin\widgets\grid\LinkColumn;
use mcms\xeditable\XEditableColumn;
use app\modules\section\models\Section;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\section\models\SectionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::$app->getModule(\Yii::$app->controller->module->id)->module_name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index content">
    <div class="box">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <div>
            <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Добавить запись', ['create'], ['class' => 'btn btn-success']) ?>
            <?=\app\modules\admin\widgets\GridMultiDelete::widget()?>
        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\CheckboxColumn'],
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => LinkColumn::className(),
                    'attribute' => 'name',
                ],
                'url_alias',

                XEditableHelper::buildListColumn(Section::treeList(), 'parent_id', ['' => '------']),

                XEditableHelper::buildEditPriority(),

                XEditableHelper::buildYesNo('vis'),
                XEditableHelper::buildYesNo('menu_vis'),


                [
                    'header'=>'&nbsp;',
                    'format'=>'html',
                    'value'=>function($model) {
                        return \app\helpers\Common::getPathLink($model->getUrl());
                    },
                    'attribute'=>'url_path',
                    'contentOptions' =>['class' => 'text-center'],
                ],
                ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}{link}'],
            ],
        ]); ?>
    </div>
</div>
