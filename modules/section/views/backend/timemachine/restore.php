<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\widgets\CKEditor as CKEditor;
use app\modules\admin\widgets\ImageRender;
use kartik\date\DatePicker;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model app\modules\section\models\Section */

$this->title = 'Сохраненная копия раздела "'.Html::encode($model->name).'"';
$this->params['breadcrumbs'][] = ['label' => 'История записей раздела ', 'url' => ['index', 'id'=>$model->model_id]];
$this->params['breadcrumbs'][] = 'Сохраненная копия';
?>
<div class="section-update content">
    <div class="box">
        <div class="section-form content">

            <?php $form = ActiveForm::begin(); ?>

            <?php if(Yii::$app->session->getFlash('save')) {
                echo Alert::widget([
                    'options' => ['class' => 'alert-success',],
                    'body' => Yii::$app->session->getFlash('save'),
                ]);
            }?>

            <?= $form->errorSummary($model); ?>

            <?= $form->field($model, 'vis')->checkbox() ?>

            <?php //echo $form->field($model, 'child_vis')->checkbox(); ?>

            <?= $form->field($model, 'menu_vis')->checkbox() ?>

            <?= $form->field($model, 'parent_id')->dropDownList(\app\modules\section\models\Section::treeList(), ['prompt'=>'Выберите значение']); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'url_alias')->textInput(['maxlength' => true]) ?>

            <?php //echo $form->field($model, 'external_link')->textInput(['maxlength' => true]); ?>

            <div class="col-md-10 nopadding">
                <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                    'options' =>['rows' => 10,],
                    'preset' => 'full'
                ]) ?>
            </div>

            <?= \app\modules\admin\widgets\CKEditorWidgets::widget([
                'textarea_id'=>'sectionshistory-content',
            ])?>

            <?= $form->field($model, 'priority')->textInput(['class'=>'form-control m-mini']) ?>

            <?php /*if($model->img) {
                echo ImageRender::widget([
                    'id'=>$model->id,
                    'attribute'=>'img',
                    'img_url'=>$model->img(),
                ]);
            }*/?>

            <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'meta_description')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'h1_tag')->textInput(['maxlength' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton('Восстановить эту версию', ['class'=>'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
