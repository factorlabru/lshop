<?php
use yii\helpers\Html;
?>

<div style="width: 310px">
    <?php
    use execut\widget\TreeView;

    $onSelect = new \yii\web\JsExpression(<<<JS
function (undefined, item) {
   //location.href = item.href;
    console.log(item);
}

JS
    );


    $script = <<< JS

        $(document).on('click', '.sections-tree-item', function(event) {
            event.preventDefault;
   
            var id = $(this).data('id');
            location.href = '/admin/section/'+id+'/update/';
            return false;

         });

        $(document).on('click', '.list-group-item .delete-sections-tree__item', function(event) {
            event.preventDefault;
                
            var id = $(this).data('id'),
                token = $('meta[name=csrf-token]').prop('content');
    
           if (confirm("Удалить раздел?")) {
                $.ajax({
                    type: 'POST',
                    url: "/admin/section/"+id+"/delete/",
                    data: {id: id,_csrf:token},
                    success: function (data) {
                        console.log('Delete data');
                    }
                });    
           }
           
            return false;
         });
        
        $(document).on('click', '.list-group-item .add-sections-tree__item', function(event) {
            event.preventDefault;
            
             var id = $(this).data('id');
            location.href = '/admin/section/default/create/?id='+id;
            return false;
         });

JS;

    $this->registerJs($script, yii\web\View::POS_READY);

    echo Html::beginTag('div', ['class'=>'sections-tree__container']);
    echo TreeView::widget([
        'data' => $items,
        'size' => TreeView::SIZE_MIDDLE,
        'header' => 'Разделы',
        'searchOptions' => [
            'inputOptions' => [
                'placeholder' => 'Поиск'
            ],
        ],
        'clientOptions' => [
            //'onNodeSelected' => $onSelect,
            'selectedBackColor' => '#00a65a',
            'borderColor' => '#222d32',
            'backColor'=>'#222d32',
            'onhoverColor'=>'#00a65a',
            'showTags'=>true
        ],
    ]);
    echo Html::endTag('div');
    ?>
</div>

<!--<a class="btn btn-success" href="/admin/section/default/create/"><i class="glyphicon glyphicon-plus"></i> Добавить раздел</a>-->