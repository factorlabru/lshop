<?php
namespace app\modules\section;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                '<path:[\w_\/-]+>'=>'section/section/index',
            ]
        );
    }
}