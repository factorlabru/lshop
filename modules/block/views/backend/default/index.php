<?php

use app\modules\admin\helpers\XEditableHelper;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\admin\widgets\backend\AddButton;
use app\modules\admin\widgets\grid\LinkColumn;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\block\models\SearchBlock */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Блоки';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
    <div class="box">
        <p>
            <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Добавить запись', ['create'], ['class' => 'btn btn-success']) ?>
            <?=\app\modules\admin\widgets\GridMultiDelete::widget()?>
        </p>

        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\CheckboxColumn'],
                'id',
                [
                    'class' => LinkColumn::className(),
                    'attribute' => 'name',
                ],
                XEditableHelper::buildYesNo('vis'),

                ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}{link}'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</section>
