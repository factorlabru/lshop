<?php

namespace app\modules\block;


use Yii;
/**
 * block module definition class
 */

class Module extends \app\modules\admin\Module
{
    public $module_name = 'Блоки';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        \Yii::configure($this, require(__DIR__ . '/config.php'));

        // custom initialization code goes here
    }
}
