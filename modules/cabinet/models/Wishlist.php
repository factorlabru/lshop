<?php

namespace app\modules\cabinet\models;

use Webmozart\Assert\Assert;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $product_id
 */
class Wishlist extends ActiveRecord
{
    /**
     * @param $user_id
     * @param $product_id
     * @return static
     */
    public static function create($user_id, $product_id)
    {
        Assert::notEmpty($user_id);
        Assert::notEmpty($product_id);

        $item = new static();
        $item->user_id = $user_id;
        $item->product_id = $product_id;
        return $item;
    }


    public static function tableName()
    {
        return '{{%user_wishlist}}';
    }
}