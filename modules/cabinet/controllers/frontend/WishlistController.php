<?php

namespace app\modules\cabinet\controllers\frontend;


use app\helpers\Common;
use app\modules\admin\models\backend\User;
use app\modules\cabinet\repositories\UserRepository;
use app\modules\catalog\models\Product;
use Webmozart\Assert\Assert;
use Yii;
use yii\base\Module;

class WishlistController extends BaseCabinetController
{
    /**
     * @var UserRepository
     */
    private $user_repository;

    public function __construct($id, Module $module, UserRepository $user_repository, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->user_repository = $user_repository;
    }

    public function actionAdd()
    {
        if(!Yii::$app->request->isAjax){
            $this->fail(403, 'Обращение только через ajax');
        }

        $id = Yii::$app->request->post('id');

        Assert::integerish($id);
        Assert::greaterThan($id, 0);

        $product = Product::findOne($id) or $this->fail();

        /** @var User $user */
        $user = Yii::$app->getUser()->getIdentity();

        $wishistProducts = $user->wishlistProducts;
        $wishistProducts[] = $product;
        $user->wishlistProducts = $wishistProducts;

        $this->user_repository->save($user);

        Common::endJson([
            'result' => true,
        ]);
    }


    public function actionDelete()
    {
        if(!Yii::$app->request->isAjax){
            $this->fail(403, 'Обращение только через ajax');
        }

        $id = Yii::$app->request->post('id');

        Assert::integerish($id);
        Assert::greaterThan($id, 0);

        /** @var User $user */
        $user = Yii::$app->getUser()->getIdentity();

        $wishlist = $user->getWishlist()->where([
            'product_id' => $id,
        ])->one() or $this->fail();

        $wishlist->delete();

        Common::endJson([
            'result' => true,
        ]);
    }


}