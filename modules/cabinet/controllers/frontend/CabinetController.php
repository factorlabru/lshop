<?php

namespace app\modules\cabinet\controllers\frontend;


use app\modules\admin\models\backend\User;
use Yii;
use yii\base\Module;

use app\modules\cabinet\services\UserService;



class CabinetController extends BaseCabinetController
{
    private $user_service;

    public function __construct($id, Module $module, UserService $user_service, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->user_service = $user_service;
    }


    public function actionIndex()
    {
        $this->setMetaByModel(['name' => 'Личный кабинет',]);
        $this->setBreadcrumbs($this->view->h1_tag);

        return $this->render('index');
    }

    public function actionWishlist()
    {
        $this->setMetaByModel(['name' => 'Избранные товары',]);
        $this->setBreadcrumbs($this->view->h1_tag);

        /** @var User $user */
        $user = Yii::$app->getUser()->getIdentity();

        $products = $user->wishlistProducts;

        return $this->render('wishlist', [
            'products' => $products,
        ]);
    }

}
