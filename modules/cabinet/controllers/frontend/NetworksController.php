<?php

namespace app\modules\cabinet\controllers\frontend;


use Yii;
use yii\authclient\AuthAction;
use yii\authclient\ClientInterface;
use yii\base\Module;

use app\modules\cabinet\services\UserService;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class NetworksController extends BaseCabinetController
{
    private $user_service;

    public function __construct($id, Module $module, UserService $user_service, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->user_service = $user_service;
    }

    public function actions()
    {
        return [
            'auth' => [
                'class' => AuthAction::class,
                'successCallback' => [$this, 'onAuthSuccess'],
                'successUrl' => Url::to(['cabinet/index']),
            ],
            'attach' => [
                'class' => AuthAction::class,
                'successCallback' => [$this, 'onAttachSuccess'],
                'successUrl' => Url::to(['cabinet/index']),
            ],
        ];
    }


    public function onAuthSuccess(ClientInterface $client)
    {
        $network = $client->getId();
        $attributes = $client->getUserAttributes();
        $identity = ArrayHelper::getValue($attributes, 'id');

        try {
            $user = $this->user_service->authNetwork($network, $identity);
            Yii::$app->user->login($user, 3600 * 24 * 30);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', 'Ошибка при авторизации');
        }
    }

    public function onAttachSuccess(ClientInterface $client)
    {
        $network = $client->getId();
        $attributes = $client->getUserAttributes();
        $identity = ArrayHelper::getValue($attributes, 'id');

        try {
            $this->user_service->attach(Yii::$app->user->id, $network, $identity);
            Yii::$app->session->setFlash('success', 'Социальная сеть успешно прикреплена!');
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', 'Ошибка при прикреплении социальной сети. Возможно она уже прикреплена.');
        }
    }


}
