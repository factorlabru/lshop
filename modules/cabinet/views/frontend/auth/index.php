<section id="content" class="page-content page-cms page-cms-1">
    <div class="row">
        <div class="col-md-8">
            <header class="page-header">
                <h1><?= $this->h1_tag ?></h1>
            </header>

            Добро пожаловать в личный кабинет!

            Пользователь: <?= Yii::$app->user->id ?>


            <div class="row">
                <h3>Прикрепить социальную сеть</h3>
                <div class="row">
                    <?= yii\authclient\widgets\AuthChoice::widget([
                        'baseAuthUrl' => ['networks/attach'],
                    ]); ?>
                </div>
            </div>

            <?= Yii::$app->session->getFlash('success') ?>
            <?= Yii::$app->session->getFlash('error') ?>

        </div>

        <?=$this->render('_sidebar');?>

    </div>
</section>