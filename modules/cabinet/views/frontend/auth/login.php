<?php

use yii\widgets\ActiveForm;

?>


<div class="login_form__wrapper">
    <div class="container">
        <div class="login_form_content">
            <h1><?= $this->h1_tag ?></h1>

            <div class="row">
                <div class="left_block">
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => false,
                        'fieldConfig' => [
                            'template' => "{input}{error}{label}",
                            'options' => [
                                'class' => 'form-group'
                                //'tag'=>false
                            ],
                            'errorOptions' => ['class' => 'error-message'],
                        ]
                    ]); ?>

                    <?= $form->field($model, 'username')
                        ->textInput([
                            "autofocus"=>true,
                        ])?>

                    <?= $form->field($model, 'password')
                        ->passwordInput([
                            "class"=>"form-control",
                            "id"=>"user_password"
                        ])?>

						<div class="agreement-block">
						<label class="agreement_checkbox"><input class="js_agreement_checkbox" type="checkbox" value="1" name="agree" checked=""><span class="personal-date-sitebar">Я даю согласие на обработку моих <a target="_blank" href="/useragreement/">персональных данных</a>.</span></label>
						</div>
						
                        <div class="form-group">
                            <input type="submit" class="js_submit_button" value="Войти" > <a href="<?= \yii\helpers\Url::to(['forget']) ?>" class="forget">Забыли пароль?</a>
                        </div>


                    <?php ActiveForm::end(); ?>

                    <?= yii\authclient\widgets\AuthChoice::widget([
                        'baseAuthUrl' => ['/cabinet/networks/auth']
                    ]); ?>

                </div>

                <?=$this->render('_sidebar');?>

            </div>
        </div>
    </div>
</div>