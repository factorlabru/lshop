<?php

use yii\widgets\ActiveForm;

?>



<div class="login_form__wrapper">
    <div class="container">
        <div class="login_form_content">
            <h1><?= $this->h1_tag ?></h1>

            <div class="row">
                <div class="left_block">
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => false,
                        'fieldConfig' => [
                            'template' => "{input}{error}{label}",
                            'options' => [
                                'class' => 'form-group'
                                //'tag'=>false
                            ],
                            'errorOptions' => ['class' => 'error-message'],
                        ]
                    ]); ?>

                    <?= $form->field($model, 'username')
                        ->textInput([
                            "autofocus"=>true,
                            "class"=>"form-control",
                            "id"=>"user_login",
                            "size"=>"20",
                        ])?>

                    <div class="form-group">
                        <input type="submit" value="Восстановить" >
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>

                <?=$this->render('_sidebar');?>

            </div>
        </div>
    </div>
</div>