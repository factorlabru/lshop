<?php
namespace app\modules\cabinet;

use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                'cabinet' => 'cabinet/cabinet/index',
                'cabinet/<action:(login|reg|logout)>' => 'cabinet/auth/<action>',
                'cabinet/<action:(forget|forget-sent|reset|reset-success)>' => 'cabinet/auth/<action>',
                'cabinet/<action:(reg-activation|email-verification)>' => 'cabinet/auth/<action>',
                'cabinet/networks/<action:(attach|auth)>' => 'cabinet/networks/<action>',
                'cabinet/wishlist/<action:(add|delete)>' => 'cabinet/wishlist/<action>',
                'cabinet/wishlist' => 'cabinet/cabinet/wishlist',
            ]
        );
    }
}