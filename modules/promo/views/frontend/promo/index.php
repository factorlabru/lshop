<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\helpers\Common;
?>

<h1><?=\app\helpers\Common::pageTitle($section)?></h1>

<?php if($promo) { ?>

    <?php foreach($promo as $promo_item) {?>

        <?php echo Html::beginTag('div');?>

        <h2>
            <a href="<?= $promo_item->getUrl() ?>">
                <?=$promo_item->name;?>
            </a>
        </h2>
        <div><?=Common::formatDate($promo_item->created_at);?></div>
        <br>

        <?php if($promo_item->img) {?>
            <div>
                <a href="<?= $promo_item->getUrl() ?>">
                    <img src="<?=$promo_item->img()?>" alt="">
                </a>
            </div>
        <?php }?>
        <br>
        <div>
            <?=$promo_item->description;?>
        </div>
        <br>

        <div class="entry">
            <a href="<?= $promo_item->getUrl() ?>">Читать далее →</a>
        </div>
        <div class="clearfix"></div>

        <?php echo Html::endTag('div');?>

    <?php }?>

<?php }?>

<div class="pagination">
    <?php
    echo \app\widgets\LinkPager::widget([
        'pagination' => $pages,
        'nextPageLabel'=>'вперед →',
        'prevPageLabel'=>'← назад'
    ]); ?>
</div>