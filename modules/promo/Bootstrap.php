<?php
namespace app\modules\promo;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                '<path:([\w_\/-]+\/)?promo>/page/<page:[0-9]+>'=>'promo/promo/index',
                [
                    'pattern' => '<path:([\w_\/-]+\/)?promo>/<url_alias:[-a-zA-Z0-9_]+>',
                    'route' => 'promo/promo/view',
                    'suffix' => '.html',
                ],
                '<path:([\w_\/-]+\/)?promo>' => 'promo/promo/index',
            ]
        );
    }
}