<?php

namespace app\modules\promo\models;

use app\behaviors\UploadImageBehavior;
use app\modules\section\models\Section;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use Yii;

/**
 * This is the model class for table "tbl_promo".
 *
 * @mixin UploadImageBehavior
 *
 * @property integer $id
 * @property string $name
 * @property string $url_alias
 * @property string $description
 * @property string $content
 * @property string $img
 * @property integer $vis
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $h1_tag
 * @property string $updated_at
 * @property string $created_at
 */
class Promo extends \yii\db\ActiveRecord
{
    const PAGE_SIZE = 5;
    public static $_promo_section;

    public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'slug' => [
                'class' => 'app\behaviors\Slug',
                'in_attribute' => 'name',
                'out_attribute' => 'url_alias',
                'translit' => true
            ],
            'image' => [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'img',
                'scenarios' => ['create', 'update'],
                'extensions' => 'png, jpg, jpeg, gif',
                'path' => '@webroot/content/promo',
                'url' => '@web/content/promo',
                'generate_new_name'=>true,
                'thumbs' => [
                    'thumb' => [
                        'width' => 230,
                        'height'=>170,
                        'quality' => 90,
                        'mode'=>\Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_promo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'trim'],
            [['description', 'content'], 'string'],
            [['vis'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            //['img', 'file'],
            [['name', 'url_alias', 'meta_title', 'meta_description', 'meta_keywords', 'h1_tag'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'url_alias' => 'Url Alias',
            'description' => 'Описание',
            'content' => 'Контент',
            'img' => 'Картинка',
            'vis' => 'Показывать',
            'updated_at' => 'Дата обновления',
            'created_at' => 'Дата создания',
        ];
    }


    /**
     * @return mixed|Section
     */
    public static function getPromoSection()
    {
        if(!self::$_promo_section){
            self::$_promo_section = Section::find()->where([
                'url_alias' => 'promo'
            ])->one();
        }

        return self::$_promo_section;
    }

    public function getUrl()
    {
        $promo_section = self::getPromoSection();

        $url = \yii\helpers\Url::toRoute([
            '/promo/promo/view',
            'path' => $promo_section->getUrlPath(),
            'url_alias'=>$this->url_alias
        ]);

        return $url;
    }
}
