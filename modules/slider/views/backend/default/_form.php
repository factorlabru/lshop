<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\bootstrap\Alert;
use app\widgets\CKEditor as CKEditor;
use app\modules\admin\widgets\ImageRender;

/* @var $this yii\web\View */
/* @var $model app\modules\testimonials\models\Testimonial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="testimonial-form content">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(Yii::$app->session->getFlash('save')) {
        echo Alert::widget([
            'options' => ['class' => 'alert-success',],
            'body' => Yii::$app->session->getFlash('save'),
        ]);
    }?>

    <?= $form->field($model, 'vis')->checkbox() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'button_text')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'priority')->textInput(['class'=>'form-control m-mini']) ?>

    <?= $form->field($model, 'img')->fileInput(['accept' => 'image/*']) ?>

    <?php if($model->img) {
        echo ImageRender::widget([
            'id'=>$model->id,
            'attribute'=>'img',
            'img_url'=>$model->img(),
        ]);
    }?>

    <?= $this->render('@admin_layouts/chunks/_save_buttons', [
        'model' => $model,
        'form' => $form,
    ]); ?>

    <?php ActiveForm::end(); ?>

</div>
