<?php
namespace app\modules\slider\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use app\modules\slider\models\Slider as SliderModel;

/**
 * Виджет вывводит слайдер.
 *
 * Class Slider
 * @package common\widgets
 */
class Slider extends Widget
{
    /**
     * @var string
     */
    public $view = 'slider';

    /**
     * @var string
     *
     * Тип сортировки.
     */
    public $order = 'priority';

    public function run()
    {
        $slider = SliderModel::find()
            ->where(['vis'=>1])
            ->orderBy($this->order)
            ->all();

        return $this->render($this->view, ['slider'=>$slider]);
    }
}