<?php

namespace app\modules\callback;

return [
    'params' => [
        '_bcModule' => 'Обратный звонок',
        'viewed'=>true,
        'model'=>'\app\modules\callback\models\Callback',
    ],
];