<?php
//подключение assets(js/css) сallback'а
\app\modules\callback\assets\CallbackAsset::register($this);

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
?>

<div id="callmeback" class="dropdown dropdown-tip dropdown-anchor-right">
    <div class="dropdown-panel">

        <?php $form = ActiveForm::begin([
            'id'=>'callback-form',
            'enableAjaxValidation' => false,
            'enableClientValidation'=>false,
            'options'=>['class'=>'remove-bottom login-form'],
            'fieldConfig' => [
                'template' => "{input}{error}",
            ]
        ]);?>
            <label>
                <?=$form->field($model, 'name')->textInput(['class'=>'inline remove-bottom', 'placeholder'=>'имя', 'required'=>true])
                    ->label(false);?>
            </label>
            <label>
                <?=$form->field($model, 'phone')->input('tel', ['class'=>'inline remove-bottom phone-mask', 'placeholder'=>'телефон', 'required'=>true]);?>
            </label>
            <input type="submit" value="Перезвоните мне" class="remove-bottom full-width">
        <?php ActiveForm::end()?>
    </div>
</div>
