<?php
namespace app\modules\callback\widgets;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * Виджет для вывода обратного звонка.
 *
 * Class Callback
 * @package app\modules\callback\widgets
 */
class Callback extends Widget
{
    public function run()
    {
        $model = new \app\modules\callback\models\Callback();

        return $this->render('callback_form', ['model'=>$model]);
    }
}