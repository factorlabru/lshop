<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\callback\models\Callback */

$this->title = "Обратный звонок";
$this->params['breadcrumbs'][] = ['label' => 'Обратный звонок', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="callback-view content">

    <b>Дата создания:</b><br />
    <?php echo Html::encode(\app\helpers\Common::formatDate($model->created_at))?><br /><br />

    <b>Имя:</b><br />
    <?php echo Html::encode($model->name)?><br /><br />

    <?php if($model->phone) { ?>
        <b>Телефон:</b><br />
        <?php echo Html::encode($model->phone)?><br /><br />
    <?php }?>

    <b>IP:</b><br />
    <?php echo Html::encode($model->ip)?><br /><br />

</div>
