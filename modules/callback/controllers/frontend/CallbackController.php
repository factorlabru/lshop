<?php
namespace app\modules\callback\controllers\frontend;

use app\components\Controller;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\modules\callback\models\Callback;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use app\components\NumericCaptcha;

class CallbackController extends Controller
{

    public function actionIndex()
    {
        if(Yii::$app->request->post('Callback')['name'] && Yii::$app->request->post('Callback')['phone']) {
            $model = new Callback();
            $result = false;

            if($model->load(Yii::$app->request->post()) && $model->save()) {

                $mail = (new \app\helpers\Mail())->sendMail($model,
                    Yii::$app->params['admin_email'],
                    '@modules/callback/mails/letter',
                    'Обратный звонок');

                $result = true;
            }

            return \yii\helpers\Json::encode(['result'=>$result]);
        } else {
            throw new NotFoundHttpException('Страница не найдена.');
        }
    }

    public function actionComplete()
    {
        $this->setAllMeta(['title'=>'Сообщение отправлено']);

        return $this->render('complete');
    }
}