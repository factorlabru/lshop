<?php
namespace app\modules\callback\assets;

use yii\web\AssetBundle;

class CallbackAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/callback/assets';
    public $baseUrl = '/web';

    public $js = [
        'js/callback.js',
    ];

    public $publishOptions = [
        'forceCopy' => true
    ];
}