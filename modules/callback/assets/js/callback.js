//callback
jQuery("#callback-form").on('submit', function(e){

    e.preventDefault();
    e.stopImmediatePropagation();

    var error = "",
        form = jQuery(this).closest("form"),
        data = form.serialize(),
        name = jQuery.trim(jQuery("#callback-name").val()),
        phone = jQuery.trim(jQuery("#callback-phone").val()),
        form = jQuery('#callback-form').serialize();

    if(!name) {
        error = true;
        jQuery("#callback-name").addClass("error-field");
    }

    if(!phone) {
        error = true;
        jQuery("#callback-phone").addClass("error-field");
    }

    if(!error) {
        jQuery.ajax({
            type: 'POST',
            url: "/callback/index/",
            data: data,
            success: function (data) {
                if(data) {
                    data = jQuery.parseJSON(data);
                    if(data.result) {
                        location.href = '/callback/complete/';
                    } else {
                        alert("При отпраке сообщения произошла ошибка");
                    }

                }
            }
        });
    }

    return false;
});