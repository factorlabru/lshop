<?php

namespace app\modules\logs\models;

use Yii;

/**
 * This is the model class for table "tbl_logs".
 *
 * @property integer $id
 * @property integer $record_id
 * @property string $module
 * @property string $model
 * @property string $action
 * @property string $attribute
 * @property string $changed_data
 * @property string $ip
 * @property integer $user_id
 * @property string $created_at
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'record_id'], 'integer'],
            [['created_at', 'changed_data'], 'safe'],
            [['model', 'module', 'attribute'], 'string', 'max' => 255],
            [['action', 'ip'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'record_id' => 'ID записи',
            'module' => 'Модуль',
            'model' => 'Модель',
            'action' => 'Действие',
            'attribute' => 'Attribute',
            'changed_data' => 'Changed data',
            'ip' => 'IP',
            'user_id' => 'Пользователь',
            'created_at' => 'Дата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\modules\admin\models\backend\User::className(), ['id'=>'user_id']);
    }

    public function getActions($action=false)
    {
        $actions = ['create'=>'Создание', 'update'=>'Редактирование', 'delete'=>'Удаление'];

        return isset($actions[$action]) ? $actions[$action] : $actions;
    }


}
