<?php

namespace app\modules\gallery\models;

use app\behaviors\UploadImageBehavior;
use Yii;

/**
 * This is the model class for table "tbl_gallery_images".
 *
 * @mixin UploadImageBehavior
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $link
 * @property string $img
 * @property integer $priority
 * @property integer $gallery_id
 */
class GalleryImage extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            'image' => [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'img',
                'scenarios' => ['default'],
                'extensions' => 'png, jpg, jpeg, gif',
                'thumbs'=> [
                    'thumb_big' => [
                        'width' => 960,
                        'height' => 720,
                        'quality' => 90,
                        'mode'=>\Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                    'thumb' => [
                        'width' => 150,
                        'height'=>90,
                        'quality' => 90,
                        'mode'=>\Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                ],
                'generate_new_name'=>true,
                'path' => '@webroot/content/gallery',
                'url' => '@web/content/gallery',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_gallery_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['priority', 'gallery_id'], 'integer'],
            [['name', 'description', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'link' => 'Link',
            'img' => 'Img',
            'priority' => 'Priority',
            'gallery_id' => 'Gallery ID',
        ];
    }
}
