<?php

namespace app\modules\gallery;

use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                [
                    'pattern' => '<path:([\w_\/-]+\/)?gallery>/<url_alias:[-a-zA-Z0-9_]+>',
                    'route' => 'gallery/gallery/view',
                    'suffix' => '.html',
                ],
                //'<path:[\w_\/-]+>/gallery' => 'gallery/gallery/index',
                '<path:([\w_\/-]+\/)?gallery>' => 'gallery/gallery/index',
            ]
        );
    }
}