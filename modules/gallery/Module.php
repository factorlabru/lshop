<?php

namespace app\modules\gallery;

use Yii;

/**
 * gallery module definition class
 */
class Module extends \app\modules\admin\Module
{

    public $module_name = 'Галереи';
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        \Yii::configure($this, require(__DIR__ . '/config.php'));
    }
}
