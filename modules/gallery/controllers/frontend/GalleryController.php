<?php

namespace app\modules\gallery\controllers\frontend;

use app\components\Controller;
use Yii;
use app\modules\gallery\models\Gallery;
use app\modules\section\models\Section;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

class GalleryController extends Controller
{
    /*public $layout = '@app/views/layouts/common.php';

    public function actionIndex($path)
    {
        $section = Section::getByPath($path);

        if(!$section) {
            throw new \yii\web\NotFoundHttpException('Страница не найдена.');
        }

        $this->current_section = $section->id;

        $galleries = Gallery::find()
            ->where(['vis'=>1])
            ->orderBy('priority')
            ->all();


         $this->setMetaByModel($section);

         $this->setBreadcrumbs($section->makeBreadcrumbs());

        return $this->render('index', ['galleries'=>$galleries]);
    }

    public function actionView($path, $url_alias)
    {
        $section = Section::getByPath($path);

        if(!$section) {
            throw new \yii\web\NotFoundHttpException('Страница не найдена.');
        }

        $model = Gallery::find()->where('url_alias=:url_alias AND vis=1', [':url_alias'=> $url_alias])->one();

        if(!$model) {
            throw new \yii\web\NotFoundHttpException('Страница не найдена.');
        }

        $this->current_section = $section->id;
        $this->current_item = $model->id;


         $this->setMetaByModel($model);
         $this->setBreadcrumbs($section->makeBreadcrumbs(), $model->name);

        return $this->render('view', [
            'model'=>$model,
        ]);
    }*/
}