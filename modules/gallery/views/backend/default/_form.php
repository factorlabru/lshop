<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\admin\widgets\ImageBlocks;

/* @var $this yii\web\View */
/* @var $model app\modules\gallery\models\Gallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallery-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vis')->checkbox() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'priority')->textInput(['class'=>'form-control m-mini']) ?>

    <?php $model->mainImg($attribute='images')?>

    <?= $form->field($model, 'file_loader[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>

    <?=ImageBlocks::widget([
        'data'=>$model->images,
    ]);?>

    <?= $this->render('@admin_layouts/chunks/_save_buttons', [
        'model' => $model,
        'form' => $form,
    ]); ?>

    <?php ActiveForm::end(); ?>

</div>
