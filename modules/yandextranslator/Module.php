<?php

namespace app\modules\yandextranslator;

/**
 * yandextranslator module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\yandextranslator\controllers';
    public $module_name = 'Яндекс переводчик';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
