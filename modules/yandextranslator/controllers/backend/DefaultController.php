<?php

namespace app\modules\yandextranslator\controllers\backend;

use yii;
use yii\web\Controller;
use yii\helpers\Inflector;

/**
 * Default controller for the `yandextranslator` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * TODO: Использовать TransliterHelper
     * @return mixed
     */
    public function actionAjaxUrlTranslate()
    {
        $query = http_build_query(array(
            'text' => Yii::$app->request->get('text'),
            'lang' => 'ru-en',
        ));
        //$url = 'http://webelement.ru/translate-api.php?' . $query;

        $response = json_decode(file_get_contents($url), 1);

        $response['slug'] = Inflector::slug(trim($response['text'][0]), '-', true );

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $response;
    }
}
