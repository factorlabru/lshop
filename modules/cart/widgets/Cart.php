<?php
namespace app\modules\cart\widgets;

use yii\base\Widget;
use yii\helpers\Html;

/**
 *
 * Class Cart
 * @package app\modules\cart\widgets
 */
class Cart extends Widget
{
    public function run()
    {
       $total_products = \Yii::$app->cart->countProducts();
       $total_price = \Yii::$app->cart->countTotalPrice();

        return $this->render('cart', [
            'total_products'=>$total_products,
            'total_price'=>$total_price,
        ]);
    }
}