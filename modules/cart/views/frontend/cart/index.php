<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
?>

<div class="cart-grid row">

    <h1 class="h1">Корзина</h1>

    <div class="col-xs-12 col-lg-12">

        <div class="cart-container">
            <?php if($products) {
                echo $this->render('_cart_table', ['products'=>$products]);
            } else {?>
                <div class="empty-cart__text">Вы еще не добавили товары в корзину.</div>
            <?php }?>
        </div>
    </div>
</div>