<?php
namespace app\modules\cart\controllers\frontend;

use app\components\Controller;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\modules\order\models\Order;
use app\modules\catalog\models\Product;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

class CartController extends Controller
{
    public function actionIndex()
    {
        //$this->layout = '@app/views/layouts/products.php';

        $this->setAllMeta(['title'=>'Корзина']);

        $products = Yii::$app->cart->getProducts();

        $order = new Order();
        if($order->load(Yii::$app->request->post()) ) {
            $order->total = Yii::$app->cart->countTotalPrice();
            if($order->save()) {
                $order->sendMail($order, $mail_to=Yii::$app->params['admin_email']);

                $order->saveProducts($order->id, Yii::$app->cart->getProducts());

                return $this->redirect(['/order/complete']);
            }
        }

        return $this->render('index', [
            'products'=>$products,
            'order'=>$order,
        ]);
    }

    /**
     * Добавление в корзину.
     */
    public function actionAdd()
    {
        if(Yii::$app->request->isPost
        && is_numeric(Yii::$app->request->post('product_id'))
        && is_numeric(Yii::$app->request->post('quantity'))
        ){
            $product = Product::findOne(Yii::$app->request->post('product_id'));
            if($product) {
                Yii::$app->cart->add($product, Yii::$app->request->post('quantity'));
                $total_count = Yii::$app->cart->countProducts();
                $total_price = Yii::$app->cart->countTotalPrice();

                echo json_encode(array(
                    'result'=>true,
                    'total_count'=>$total_count,
                    'total_price'=>$total_price,
                ));
            }
        } else {
            throw new \yii\web\NotFoundHttpException('Страница не найдена.');
        }
    }



    public function actionAjaxloadcities()
    {
        if (isset($_POST['region_id'])) {

            $html = "<option value=''>Выберите значение</option>";

            if($_POST['region_id']) {
                $region_id = intval(Yii::$app->request->post('region_id'));

                $query = "SELECT * FROM tbl_cities WHERE region_id=" . $region_id;
                $cities = Yii::$app->db->createCommand($query)->queryAll();

                foreach ($cities as $city) {
                    $html .= '<option value="' . $city['id'] . '">' . $city['name'] . '</option>';
                }
            }

            echo $html;

        } else {
            throw new \yii\web\NotFoundHttpException('Страница не найдена.');
        }

    }


    public function actionAjaxloaddeliveryprice()
    {
        if (isset($_POST['city_id']) && $_POST['city_id']) {

            ////////////////////// TODO тут будет расчет доставки используя API
            echo intval(Yii::$app->request->post('city_id'));
            //////////////////////////////////////////////////////////////////

        } else {
            throw new \yii\web\NotFoundHttpException('Страница не найдена.');
        }

    }

    /**
     * Удаление товара из корзины.
     */
    public function actionDelete()
    {
        if(Yii::$app->request->isPost && is_numeric(Yii::$app->request->post('product_id'))){

            $product = Product::findOne(Yii::$app->request->post('product_id'));
            if($product) {
                Yii::$app->cart->delete($product);
                $total_count = Yii::$app->cart->countProducts();
                $total_price = Yii::$app->cart->countTotalPrice();

                echo json_encode(array(
                    'result'=>true,
                    'total_count'=>$total_count,
                    'total_price'=>$total_price,
                ));
            }
        } else {
            throw new \yii\web\NotFoundHttpException('Страница не найдена.');
        }
    }

    /**
     * Пересчет товаров в корзине.
     */
    public function actionRecount()
    {
        if(Yii::$app->request->isPost
            && is_numeric(Yii::$app->request->post('product_id'))
            && is_numeric(Yii::$app->request->post('quantity'))
        ){
            $product = Product::findOne(Yii::$app->request->post('product_id'));
            if($product) {

                Yii::$app->cart->add($product, Yii::$app->request->post('quantity'));

                $total_count = Yii::$app->cart->countProducts();
                $new_price = Yii::$app->cart->countProductPrice($product->id);
                $total_price = Yii::$app->cart->countTotalPrice();

                echo json_encode(array(
                    'result'=>true,
                    'total_count'=>$total_count,
                    'new_price'=>$new_price,
                    'total_price'=>$total_price,
                ));
            }
        } else {
            throw new \yii\web\NotFoundHttpException('Страница не найдена.');
        }
    }

    /**
     * Очистка всей корзины.
     */
    public function actionClear()
    {
        Yii::$app->cart->clear();
    }
}