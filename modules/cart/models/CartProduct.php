<?php

namespace app\modules\cart\models;

use Yii;

/**
 * This is the model class for table "tbl_cart_products".
 *
 * @property integer $id
 * @property string $user_id
 * @property integer $product_id
 * @property integer $total
 * @property string $products
 * @property string $ip
 * @property string $created_at
 */
class CartProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_cart_products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'total'], 'integer'],
            [['products'], 'string'],
            [['created_at'], 'safe'],
            [['user_id'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 55],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'product_id' => 'Product ID',
            'total' => 'Total',
            'products' => 'Products',
            'ip' => 'Ip',
            'created_at' => 'Created At',
        ];
    }
}
