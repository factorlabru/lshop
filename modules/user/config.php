<?php
namespace app\modules\user;

return [ 'params'=>
    [
    '_order' => '2',
    '_bcModule' => 'Пользователи',
    '_bcIcon' => '<i class="fa fa-user"></i>',
    '_items' => [
        [
            'icon' => '<i class="fa fa-users"></i>',
            'label' => 'Список пользователей',
            'url' => '/admin/user/default/index',
        ],
        [
            'icon' => '<i class="fa fa-user-secret"></i>',
            'label' => 'Роли пользователей',
            'url' => '/admin/user/access/role',
        ],
        [
            'icon' => '<i class="fa fa-user-plus"></i>',
            'label' => 'Права роли',
            'url' => '/admin/user/access/permission',
        ],
    ]
    ]
];