<?php 
namespace app\modules\user\models\backend;

use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\base\Module;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use Yii;
use developeruz\db_rbac\interfaces\UserRbacInterface;

/**
 * This is the model class for table "tbl_user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property string $role
 * @property string $last_sign_in
 * @property string $current_sign_in_ip
 * @property string $last_sign_in_ip
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends ActiveRecord implements IdentityInterface, UserRbacInterface
{ 

	public $password_field;
    public $role_auth;
	
	const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
	
	public static function tableName()
    {
        return '{{%user}}';
    }
	
	public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            TimestampBehavior::className(),
        ];
    }

    public function afterFind()
    {
        $roles = array_keys(Yii::$app->authManager->getRolesByUser($this->id));
		if (!empty($roles)) {
			$this->role_auth = $roles[0];
		}
        return parent::afterFind();
    }
	
	 public static function primaryKey()
    {
        return array('id');
    }
	
	public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'username' => 'Имя пользователя', 
            'password_field' => 'Пароль',
            'email' => 'E-mail',
            'status' => 'Статус',
            'role_auth' => 'Роль',
            'last_sign_in' => 'Последний вход',
            'current_sign_in_ip' => 'Текущий IP',
            'last_sign_in_ip' => 'Последний вход был с IP',
        );
    }
	
	public function rules()
	{
		return [
			[['username', 'email', 'status', 'password_hash'], 'required'],
			[['username', 'email', 'password_field', 'role_auth'], 'safe'],
			['password_field', 'required', 'on' => 'create'],
			['email', 'email'],
            [['current_sign_in_ip', 'last_sign_in_ip'], 'string', 'max' => 20],
			['email', 'unique'],
			['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
		];
	}
	
	public function search($params)
	{
		$query = User::find();
        $provider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 25,
			],
			'sort' => [
				'defaultOrder' => [
					'username' => SORT_ASC,
				]
			],
		]);


        if($this->load($params)) {

            // adjust the query by adding the filters

            $query->andFilterWhere([
                'id' => $this->id,
                'status' => $this->status,
            ]);

            $query->andFilterWhere(['like', 'username', $this->username])
                ->andFilterWhere(['like', 'email', $this->email])
                ->andFilterWhere(['like', 'status', $this->status]);

        }

		return $provider;
	}

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
	
	public function beforeValidate() 
	{
		if (!empty($this->password_field)) {
			$this->setPassword($this->password_field);
		}
		return parent::beforeValidate();
	}

    public function getStatuses()
    {
        return [self::STATUS_DELETED => 'Не активен', self::STATUS_ACTIVE => 'Активен'];
    }

    public function getUserName()
    {
        return $this->username;
    }

    public static function logUserAccess($username, $id=null)
    {
        $user = static::findByUsername($username);

        $user->last_sign_in = date("Y-m-d H:i:s");
        $user->last_sign_in_ip = $user->current_sign_in_ip;
        $user->current_sign_in_ip = Yii::$app->request->userIP;
        $user->save();
    }
}

