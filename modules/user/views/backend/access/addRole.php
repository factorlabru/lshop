<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Новая роль';
$this->params['breadcrumbs'][] = ['label' => 'Управление ролями', 'url' => ['role']];
$this->params['breadcrumbs'][] = 'Новая роль';
?>
<div class="news-index content">
    <div class="box">
        <div class="links-form">
            <?php if (!empty($error)) { ?>
                <div class="error-summary">
                    <?php echo implode('<br>', $error); ?>
                </div>
            <?php  } ?>
            <?php $form = ActiveForm::begin(); ?>

            <div class="form-group">
                <?= Html::label('Название роли'); ?>
                <?= Html::textInput('name', '', ['class'=>'form-control']); ?>
                <?= '* только латинские буквы, цифры и _ -'; ?>
            </div>

            <div class="form-group">
                <?= Html::label('Текстовое описание'); ?>
                <?= Html::textInput('description', '', ['class'=>'form-control']); ?>
            </div>

            <div class="form-group">
                <?= Html::label('Разрешенные доступы'); ?>
                <?= Html::checkboxList('permissions', null, $permissions, ['separator' => '<br>']); ?>
            </div>

            <?= $this->render('@admin_layouts/chunks/_save_buttons', [
                //'model' => $model,
                'form' => $form,
            ]); ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>