<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Редактирование роли: ' . ' ' . $role->name;
$this->params['breadcrumbs'][] = ['label' => 'Управление ролями', 'url' => ['role']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="news-index content">

    <div class="links-form box">

        <?php if (!empty($error)) {?>
            <div class="error-summary">
                <?php echo implode('<br>', $error); ?>
            </div>
        <?php } ?>

        <?php $form = ActiveForm::begin(); ?>

        <div class="form-group">
            <?= Html::label('Название роли'); ?>
            <?= Html::textInput('name', $role->name); ?>
        </div>

        <div class="form-group">
            <?= Html::label('Текстовое описание'); ?>
            <?= Html::textInput('description', $role->description); ?>
        </div>

        <div class="form-group">
            <?= Html::label('Разрешенные доступы'); ?>
            <?= Html::checkboxList('permissions', $role_permit, $permissions, ['separator' => '<br>']); ?>
        </div>

        <?= $this->render('@admin_layouts/chunks/_save_buttons', [
            //'model' => $model,
            'form' => $form,
        ]); ?>

        <?php ActiveForm::end(); ?>

    </div>
</div>
