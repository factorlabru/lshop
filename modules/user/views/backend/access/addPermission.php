<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Новое правило';
$this->params['breadcrumbs'][] = ['label' => 'Правила доступа', 'url' => ['permission']];
$this->params['breadcrumbs'][] = 'Новое правило';
?>
<div class="news-index content">
    <div class="box">
        <div class="links-form">
            <?php if (!empty($error)) { ?>
                <div class="error-summary">
                    <?php echo implode('<br>', $error); ?>
                </div>
            <?php } ?>

            <?php $form = ActiveForm::begin(); ?>

            <div class="form-group">
                <?= Html::label('Текстовое описание'); ?>
                <?= Html::textInput('description', '', ['class'=>'form-control']); ?>
            </div>

            <div class="form-group">
                <?= Html::label('Разрешенный доступ'); ?>
                <?= Html::textInput('name', '', ['class'=>'form-control']); ?>

            </div>
            <div>
                <b>Пример</b>
                <p>
                admin/rooms - доступ ко всем action'ам контроллера default (create, update и т.д.)
                <br>
                Если вместо admin/rooms задать admin/rooms/default/index, то доступ будет только к action'у index. У create и update будет forbidden.
                </p>
            </div>

            <?= $this->render('@admin_layouts/chunks/_save_buttons', [
                //'model' => $model,
                'form' => $form,
            ]); ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
