<?php

namespace app\modules\testimonials\models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use Yii;

/**
 * This is the model class for table "tbl_testimonials".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $city
 * @property string $message
 * @property string $ip
 * @property integer $moderation
 * @property integer $vis
 * @property integer $viewed
 * @property string $updated_at
 * @property string $created_at
 */
class Testimonial extends \yii\db\ActiveRecord
{
    public $verifyCode;
    const PAGE_SIZE = 5;

    public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $this->ip = Yii::$app->request->userIP;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_testimonials';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'message'], 'required'],
            [['message'], 'string'],
            ['email', 'email'],
            [['moderation', 'vis', 'viewed'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [
                'verifyCode',
                'captcha',
                'captchaAction' => '/testimonials/testimonial/captcha',
                'skipOnEmpty'=>!Yii::$app->user->isGuest
            ],
            [['ip'], 'string', 'max' => 55],
            [['name', 'email', 'phone', 'city'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'email' => 'Email',
            'phone' => 'Телефон',
            'city' => 'Город',
            'message' => 'Отзыв',
            'ip' => 'IP',
            'viewed' => 'Запись просмотрена',
            'moderation' => 'На модерации',
            'vis' => 'Показывать',
            'updated_at' => 'Updated At',
            'created_at' => 'Дата создания',
        ];
    }

    /**
     * @inheritdoc
     * @return TestimonialsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TestimonialsQuery(get_called_class());
    }

    public function sendMail($model, $mail_to, $mail_from='', $subject='Отзыв с сайта')
    {
        $mail_from = $mail_from ? $mail_from : 'noreply@'.$_SERVER['SERVER_NAME'];

        Yii::$app->mailer->compose('@modules/testimonials/mails/letter', ['model' => $model])
            ->setFrom($mail_from)
            ->setTo($mail_to)
            ->setSubject($subject.' '.$_SERVER['SERVER_NAME'])
            ->send();
    }
}
