<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model app\modules\testimonials\models\Testimonial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="testimonial-form content">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(Yii::$app->session->getFlash('save')) {
        echo Alert::widget([
            'options' => ['class' => 'alert-success',],
            'body' => Yii::$app->session->getFlash('save'),
        ]);
    }?>

    <?= $form->field($model, 'vis')->checkbox() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>

    <?php
    echo $form->field($model, 'created_at')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Дата создания отзыва'],
        'value' => date('Y-m-d'),
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-m-dd'
        ]
    ]);?>

    <?= $this->render('@admin_layouts/chunks/_save_buttons', [
        'model' => $model,
        'form' => $form,
    ]); ?>

    <?php ActiveForm::end(); ?>

</div>