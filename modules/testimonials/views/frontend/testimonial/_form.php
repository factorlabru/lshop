<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
?>

<div class="col-md-8">
    <?php $form = ActiveForm::begin([
        'id'=>'testimonial_form',
        'fieldConfig' => [
            'template' => "{input}{error}",
            'options' => [
                //'tag'=>false
            ]
        ]
    ]);?>

        <?= $form->errorSummary($model); ?>

        <div class="form-group">
            <label>ФИО *</label>
            <?=$form->field($model, 'name')->textInput(['class'=>'form-control'])->label(false);?>
        </div>

        <div class="form-group">
            <label>Email *</label>
            <?=$form->field($model, 'email')->textInput(['class'=>'form-control'])->label(false);?>
        </div>

        <div class="form-group">
            <label>Отзыв *</label>
            <?=$form->field($model, 'message')->textarea(['cols'=>70, 'rows'=>10]);?>
        </div>

        <div class="form-group">
            <label>Проверочный код *</label>
            <?=$form->field($model, 'verifyCode')->widget(Captcha::className(), [
                'captchaAction' => '/testimonials/testimonial/captcha',
                'template' => '<div class="row"><div class="col-lg-6">{input}</div><div class="col-lg-3">{image}</div></div>',
            ]) ?>
        </div>

        <button type="submit" class="btn btn-default">Отправить</button>
    <?php ActiveForm::end()?>
</div>