<?php
namespace app\modules\testimonials;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                '<path:([\w_\/-]+\/)?(testimonials|otzyvy)>/page/<page:[0-9]+>'=>'testimonials/testimonial/index',
                '<path:([\w_\/-]+\/)?(testimonials|otzyvy)>' => 'testimonials/testimonial/index',
                'testimonials/captcha' => 'testimonials/testimonial/captcha',
                '<path:([\w_\/-]+\/)?(testimonials|otzyvy)>/<_a:[\w-]+>' => 'testimonials/testimonial/<_a>',
            ]
        );
    }
}
