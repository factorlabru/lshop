<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\admin\widgets\backend\AddButton;
use app\modules\admin\widgets\grid\LinkColumn;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box">
        <?= AddButton::widget(); ?>
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => \app\modules\admin\widgets\grid\sortable\SortableColumn::className(),
                    'url' => \yii\helpers\Url::toRoute(['ajax-sorting'])
                ],
                [
                    'class' => LinkColumn::className(),
                    'attribute' => 'title',
                ],
                [
                    'class' => LinkColumn::className(),
                    'attribute' => 'name',
                ],
                \app\modules\admin\helpers\XEditableHelper::buildEditField('value', 'textarea'),
                ['class' => 'yii\grid\ActionColumn','template'=>'{update}  {delete}',],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</section>