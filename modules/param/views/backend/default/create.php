<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\param\models\backend\Param */

$this->title = 'Создание настройки';
$this->params['breadcrumbs'][] = ['label' => 'Настройки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
    <div class="box">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</section>