<?php

namespace app\modules\param\controllers\backend;

use app\modules\admin\actions\AjaxEditableAction;
use app\modules\admin\widgets\grid\sortable\AjaxGridSortableAction;
use Yii;
use app\modules\admin\components\AdminController;
use app\modules\param\models\backend\Param;
use app\modules\param\models\backend\SearchParam;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for Param model.
 */
class DefaultController extends AdminController
{

    public function actions()
    {
        $model_name = Param::className();

        return [
            'ajax-editable' => [
                'class' => AjaxEditableAction::className(),
                'model' => $model_name,
            ],
            'ajax-sorting' => [
                'class' =>  AjaxGridSortableAction::className(),
                'modelClass' => $model_name,
            ],
        ];
    }

    /**
     * Lists all Param models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->setbcAction('Список настроек');
        $searchModel = new SearchParam();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Param model.
     * @param integer $id
     * @return mixed
     */
    //public function actionView($id)
    //{
    //    return $this->render('view', [
    //        'model' => $this->findModel($id),
    //    ]);
    //}

    /**
     * Creates a new Param model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->setbcAction('Добавление настройки');
        $model = new Param();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->moduleRedirect($model->id);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Param model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->setbcAction('Изменение настройки');
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->moduleRedirect($model->id);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Param model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Param model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Param the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Param::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
