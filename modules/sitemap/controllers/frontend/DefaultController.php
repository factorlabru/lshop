<?php

namespace app\modules\sitemap\controllers\frontend;

use Yii;
use app\components\Controller;
use yii\caching\TagDependency;
use app\modules\section\models\Section;

/**
 * Default controller for the `sitemap` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        if (!$xml_sitemap = Yii::$app->cache->get('sitemap')) {
            $urls = [];
            $sitemap_pages = [];

            $sitemap_pages[] = Section::find()->where(['vis'=>1])->all();

            if(Yii::$app->moduleIsActive('catalog')) {
                $sitemap_pages[] = \app\modules\catalog\models\Category::find()->where(['vis'=>1])->all();
                $sitemap_pages[] = \app\modules\catalog\models\Product::find()->where(['vis'=>1])->all();
            }

            if(Yii::$app->moduleIsActive('promo')) {
                $sitemap_pages[] = \app\modules\promo\models\Promo::find()->where(['vis'=>1])->all();
            }

            if(Yii::$app->moduleIsActive('news')) {
                $sitemap_pages[] = \app\modules\news\models\News::find()->where(['vis'=>1])->all();
            }

            if(Yii::$app->moduleIsActive('rooms')) {
                $sitemap_pages[] = \app\modules\rooms\models\Room::find()->where(['vis'=>1])->all();
            }

            $models = array_merge(...$sitemap_pages);

            /** @var Section[]|mixed $models */
            foreach ($models as $item) {
                $urls[] = $item->getUrl();
            }

            $xml_sitemap = $this->renderPartial('index', [
                'host' => Yii::$app->request->hostInfo,
                'urls' => $urls,
            ]);

            Yii::$app->cache->set(__CLASS__ . 'sitemap_xml', $xml_sitemap, 3600*12, new TagDependency([
                'tags' => [
                    Section::className(),
                    \app\modules\catalog\models\Category::className(),
                    \app\modules\catalog\models\Product::className(),
                    \app\modules\promo\models\Promo::className(),
                    \app\modules\news\models\News::className(),
                    \app\modules\rooms\models\Room::className(),
                ],
            ]));
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
        header('Content-Type: application/xml');
        echo $xml_sitemap;
    }
}
