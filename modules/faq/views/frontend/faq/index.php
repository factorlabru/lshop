<?php
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use app\helpers\Common;
?>

<h1><?=\app\helpers\Common::pageTitle($section)?></h1>

<?=$section->content;?>

<div class="col-sm-8">
    <?php if($faq) { ?>

        <?php foreach($faq as $item) {?>

            <?php echo Html::beginTag('div', ['class'=>'faq-item']);?>

            <h2>
                <?=$item['name']?>
            </h2>
            <span><?=Common::formatDate($item['created_at'])?></span> <br><br>

            <div class="entry">
                <b>Вопрос:</b> <br>
                <?=Html::encode($item['question'])?>
            </div>
            <div class="clearfix"></div>

            <?php if($item['answer']) {?>
                <br>
                <div class="entry">
                    <b>Ответ:</b> <br>
                    <?=Html::encode($item['answer'])?>
                </div>
                <hr>
                <div class="clearfix"></div>
            <?php }?>

            <?php echo Html::endTag('div');?>

        <?php }?>

        <div class="pagination">
            <?php
            echo \app\widgets\LinkPager::widget([
                'pagination' => $pages,
                'nextPageLabel'=>'вперед →',
                'prevPageLabel'=>'← назад'
            ]); ?>
        </div>
    <?php }?>

    <div>
        <?=$this->render('_form', ['model'=>$model]);?>
    </div>
</div>
