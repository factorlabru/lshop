<?php
namespace app\modules\faq;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                '<path:([\w_\/-]+\/)?faq>/page/<page:[0-9]+>'=>'faq/faq/index',
                '<path:([\w_\/-]+\/)?faq>' => 'faq/faq/index',
                'faq/captcha' => 'faq/faq/captcha',
                '<path:([\w_\/-]+\/)?faq>/<_a:[\w-]+>' => 'faq/faq/<_a>',
            ]
        );
    }
}
