<?php

namespace app\modules\faq\controllers\frontend;


use app\components\Controller;
use Yii;
use app\modules\faq\models\Faq;
use yii\data\Pagination;


/**
 * Faq controller for the `faq` module
 */
class FaqController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                //'class' => 'yii\captcha\CaptchaAction',
                'class' => 'app\components\NumericCaptcha',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex($path)
    {
        $section = \app\modules\section\models\Section::getByPath($path);

        if(!$section) {
            throw new \yii\web\NotFoundHttpException('Страница не найдена.');
        }
        $this->current_section = $section->id;


        $query = Faq::find()->where('vis = 1');
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(),
            'pageSize'=>Faq::PAGE_SIZE, 'defaultPageSize'=>Faq::PAGE_SIZE]);

        $faq = $query->offset($pages->offset)
            ->orderBy('created_at DESC')
            ->limit($pages->limit)
            ->all();


        $model = new Faq();

        if($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->sendMail($model, $mail_to=Yii::$app->params['admin_email']);

            $mail = (new \app\helpers\Mail())->sendMail($model,
                Yii::$app->params['admin_email'],
                '@modules/faq/mails/letter',
                'Вопрос с сайта');

            return $this->redirect(['/faq/complete']);
        }

        $this->setMetaByModel($section);
        $this->setBreadcrumbs($section->makeBreadcrumbs());

        return $this->render('index', [
            'faq' => $faq,
            'pages' => $pages,
            'section'=>$section,
            'model'=>$model,
        ]);
    }

    public function actionComplete()
    {
        $this->setMetaByModel(['name'=>'Сообщение отправлено']);
        $this->setBreadcrumbs($this->view->h1_tag);

        return $this->render('complete');
    }
}