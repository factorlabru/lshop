<?php

namespace app\modules\extendedstyle;

/**
 * extendedstyle module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\extendedstyle\controllers';

    public $module_name = 'Расширенные стили';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        \Yii::configure($this, require(__DIR__ . '/config.php'));
        // custom initialization code goes here
    }
}
