<?php

namespace app\modules\extendedstyle\controllers\backend;

use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\admin\components\AdminController;

/**
 * Default controller for the `extendedstyle` module
 */
class DefaultController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->setbcAction('Расширенные стили');

        $file_path = Yii::getAlias('@app/web/ext/extended.css');

        if (!file_exists($file_path)) {
            $fp = fopen($file_path, "w");
            fwrite($fp, "/* Extended css styles */");
            fclose ($fp);
        }

        $file = file_get_contents($file_path);

        if(Yii::$app->request->post('extended_style')) {
            $fp = fopen($file_path, "w");
            $data = str_replace(array("<script>","</script>, javascript:"), "", trim($_POST["extended_style"]));
            fwrite($fp, $data);
            fclose($fp);

            \Yii::$app->getSession()->setFlash('save', 'Данные сохранены.');

            return $this->redirect('/admin/extendedstyle/');
        }

        return $this->render('index', ['file'=>$file]);
    }
}
