<?php
namespace app\modules\news\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use app\modules\news\models\News;

/**
 * Виджет выводит новости.
 *
 * Class NewsWidget
 * @package app\modules\news\widgets
 */
class NewsWidget extends Widget
{
    /**
     * @var string
     */
    public $view = 'news';

    /**
     * @var int
     *
     * Количество выводимых записей.
     */
    public $limit = 3;

    /**
     * @var string
     *
     * Тип сортировки.
     */
    public $order = 'created_at DESC';

    public function run()
    {
        $news = News::find(["vis='1'"])
            ->limit((int)$this->limit)
            ->orderBy($this->order)
            ->all();

        return $this->render($this->view, ['news'=>$news]);
    }
}