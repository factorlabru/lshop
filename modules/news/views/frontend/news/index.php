<?php
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use app\helpers\Common;
?>

<h1><?=\app\helpers\Common::pageTitle($section)?></h1>


<?php if($news) { ?>

    <?php foreach($news as $news_item) {?>

        <?php echo Html::beginTag('div');?>

        <h2>
            <a href="<?= $news_item->getUrl(); ?>">
                <?=$news_item->name;?>
            </a>
        </h2>
        <div><?=Common::formatDate($news_item->created_at);?></div>

        <?php if($news_item->img) {?>
            <div>
                <a href="<?= $news_item->getUrl(); ?>">
                    <img src="<?= $news_item->img() ?>" alt="">
                </a>
            </div>
        <?php }?>

        <br>
        <div>
            <?=$news_item->annotation;?>
        </div>
        <br>

        <div class="entry">

            <p><a href="<?= $news_item->getUrl() ?>#more"
                  class="more-link"><em>Читать далее →</em></a></p>
        </div>
        <div class="clearfix"></div>

        <?php echo Html::endTag('div');?>

    <?php }?>

<?php }?>

<div class="pagination">
    <?php
    echo \app\widgets\LinkPager::widget([
        'pagination' => $pages,
        'nextPageLabel'=>'вперед →',
        'prevPageLabel'=>'← назад'
    ]); ?>
</div>