<?php

namespace app\modules\blog\controllers\frontend;

use app\modules\section\models\Section;
use app\modules\blog\models\Tag;
use app\components\Controller;
use app\modules\blog\models\Post;
use yii\data\Pagination;


/**
 * Default controller for the `post` module
 */
class PostController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex($path)
    {
        $section = Section::getByPath($path) or $this->fail();

        $this->current_section = $section->id;

        $query = Post::find();
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(),
            'pageSize'=>5, 'defaultPageSize'=>5]);

        $posts = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $this->setMetaByModel($section);

        $this->setBreadcrumbs($section->makeBreadcrumbs());

        return $this->render('index', [
            'posts' => $posts,
            'pages' => $pages,
            'section'=>$section,
        ]);
    }


    public function actionView($url_alias, $path)
    {
        $section = Section::getByPath($path) or $this->fail();
        $this->current_section = $section->id;

        $post = Post::find()->where('url_alias=:url_alias AND vis=1', [':url_alias'=> $url_alias])->one();

        if(!$post) {
            throw new \yii\web\NotFoundHttpException();
        }

        $this->current_item = $post->id;

        $this->setMetaByModel($post);
        $this->setBreadcrumbs($section->makeBreadcrumbs(true), $post->name);

        return $this->render('view', ['post'=>$post]);
    }

    public function actionTag($tag_url, $path)
    {
        $section = Section::getByPath($path) or $this->fail();
        $this->current_section = $section->id;

        $tag = Tag::find()->where('url_alias=:tag_url', [':tag_url'=>$tag_url])->one();
        if(!$tag) {
            throw new \yii\web\NotFoundHttpException();
        }

        $query = Post::find()->with(['tags', 'user'])
            ->joinWith('tags')
            ->where("tbl_tags.url_alias=:tag", [':tag'=>$tag->url_alias]);

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize'=>5, 'defaultPageSize'=>5
        ]);

        $posts = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $this->setMetaByModel(['name' => $tag->name]);
        $this->setBreadcrumbs($section->makeBreadcrumbs(true), $tag->name);

        return $this->render('index', [
            'posts'=>$posts,
            'pages'=>$pages,
        ]);
    }
}
