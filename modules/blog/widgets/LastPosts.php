<?php
namespace app\modules\blog\widgets;

use yii\base\Widget;
use app\modules\blog\models\Post;

/**
 * Виджет выводит последние посты.
 *
 * Class LastPostsWidget
 * @package app\modules\blog\widgets
 */
class LastPosts extends Widget
{
    /**
     * @var int
     */
    public $limit = 5;

    /**
     * @var string
     */
    public $order = 'created_at DESC';

    public function run()
    {
        $last_posts = Post::find(['vis=1'])->limit($this->limit)->orderBy($this->order)->all();

        return $this->render('last_posts', ['last_posts'=>$last_posts]);
    }
}