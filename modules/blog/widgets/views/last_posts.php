<?php
use yii\helpers\Html;
?>

<?php if($last_posts) {

    echo Html::beginTag('h2');
    echo "Свежие записи";
    echo Html::endTag('h2');

    echo Html::beginTag('ul');
    foreach($last_posts as $post) {
        echo Html::beginTag('li');
        echo Html::a($post->name, ['post/view', 'url_alias'=>$post->url_alias]);
        echo Html::endTag('li');
    }
    echo Html::endTag('ul');
    ?>
<?php }?>