<?php
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use app\helpers\Common;
use app\modules\blog\widgets\Tags;
?>

<h1><?=\app\helpers\Common::pageTitle($section)?></h1>

<div class="col-md-8">
    <?php if($posts) { ?>

        <?php foreach($posts as $post) {?>

            <?php echo Html::beginTag('div');?>

            <div class="post-date">
                <?=Common::numericDate($post->created_at);?>
            </div>

            <h2>
                <a href="<?= $post->getUrl() ?>"><?=$post->name;?></a>
            </h2>

            <?php if($post->img) {?>
                <div>
                    <a href="<?= $post->getUrl() ?>">
                        <img src="<?=$post->img()?>" alt="">
                    </a>
                </div>
            <?php }?>

            <div class="entry">
                <?=$post->annotation;?>

                <p><a href="<?= $post->getUrl() ?>#more"
                      class="more-link"><em>Читать далее →</em></a></p>
            </div>
            <div class="clearfix"></div>

            <?php echo Html::endTag('div');?>

        <?php }?>

    <?php }?>

    <div class="pagination">
        <?php
        echo \app\widgets\LinkPager::widget([
            'pagination' => $pages,
            'nextPageLabel'=>'вперед →',
            'prevPageLabel'=>'← назад'
        ]); ?>
    </div>
</div>

<div class="col-md-4">
    <?=Tags::widget()?>
</div>