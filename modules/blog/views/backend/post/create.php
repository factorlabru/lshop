<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Создание поста';
$this->params['breadcrumbs'][] = ['label' => 'Посты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="section-create content">
    <div class="box">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>