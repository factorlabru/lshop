<?php

namespace app\modules\blog\models;

use Yii;

/**
 * This is the model class for table "tbl_post_categories".
 *
 * @property integer $id
 * @property integer $post_id
 * @property integer $category_id
 */
class PostCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_post_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'category_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'category_id' => 'Category ID',
        ];
    }
}
