<?php
namespace app\modules\gitdeploy;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                'gitdeploy' => 'gitdeploy/default/index',
            ]
            /*[
                'pattern' => 'gitdeploy',
                'route' => 'gitdeploy/default/index',
                'suffix' => '.php',
            ]*/
        );
    }
}