<?php

namespace app\modules\modulemanager\models;

use Yii;

/**
 * This is the model class for table "tbl_modules".
 *
 * @property integer $id
 * @property string $module
 * @property string $name
 * @property string $icon
 * @property integer $priority
 * @property integer $menu_vis
 * @property integer $active
 * @property integer $bootstrap
 * @property integer $viewed
 * @property string $model
 * @property string $updated_at
 * @property string $created_at
 */
class Module extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_modules';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['priority', 'menu_vis', 'active', 'bootstrap', 'viewed'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['module', 'name', 'icon'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'module' => 'Модуль',
            'name' => 'Название',
            'icon' => 'Иконка',
            'bootstrap' => 'Автозагрузка',
            'priority' => 'Приоритет',
            'menu_vis' => 'Отображение в меню',
            'active' => 'Активирован',
            'updated_at' => 'Дата редактирования',
            'created_at' => 'Дата создания',
        ];
    }
}
