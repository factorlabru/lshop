<?php
/**
 * Модель для работы с настройками модудулей.
 */

namespace app\modules\modulemanager\models;

use Yii;
use yii\helpers\Html;
use app\modules\modulemanager\models\Module;

class ModuleManager
{
    public $core_modules = [
        'admin', 'main', 'modulemanager',
        'user', 'block', 'snippet', 'param',
        'meta', 'section', 'extendedstyle', 'extendedjs'
    ];



    public function __construct()
    {
    }

    public function getModulesList()
    {
        $core_modules = $this->core_modules;

        $modules_records = Module::find()->all();

        $modules = [];

        foreach ($modules_records as $one) {
            $modules[$one->module] = ['params' => []];
        }

        return $modules;
    }


    /**
     * Получение активных модулей.
     */
    public function getActiveModules()
    {
        $modules_records = Module::find()->where(['active' => 1])->all();
        $active_modules = [];

        foreach ($modules_records as $module) {
            if ($module->active) {
                $active_modules[$module->module] = $module->attributes;
            }
        }

        return $active_modules;
    }

    /**
     * Проверяет, есть у модуля Bootstrap.php
     *
     * @param $module_name
     * @return int - возвращаться должно, именно числовое значение.
     */
    private function checkBootstrap($module_name)
    {
        if (is_file(Yii::getAlias('@app/modules/' . $module_name . '/Bootstrap.php'))) {
            return 1;
        }

        return 0;
    }


}