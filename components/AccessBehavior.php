<?php


namespace app\components;


use Yii;

class AccessBehavior extends \developeruz\db_rbac\behaviors\AccessBehavior
{
    public $allowedRules = [];

    public function interception($event)
    {
        $route = Yii::$app->getRequest()->resolve();
        if(in_array($route[0], $this->allowedRules)){
            return;
        }
        parent::interception($event);
    }
}

