<?php

namespace app\components;

use Yii;
use yii\web\User;

class WebUser extends User
{

    public $authKeyParam = '__authKey';

    /**
     * При восстановлении данных из сессии проверяем authKey и делаем logout() если устарел
     */
    protected function renewAuthStatus()
    {
        parent::renewAuthStatus();

        if($this->identity){
            $session = Yii::$app->getSession();
            if($session->getHasSessionId() || $session->getIsActive()){
                $authKey = $session->get($this->authKeyParam);
                /* @var $class \yii\web\IdentityInterface */
                if(!$this->identity->validateAuthKey($authKey)){
                    $this->logout();
                }
            }
        }
    }


    /**
     * При смене пользователя записываем в сессию его authKey
     */
    public function switchIdentity($identity, $duration = 0)
    {
        parent::switchIdentity($identity, $duration);

        $session = Yii::$app->getSession();
        $session->remove($this->authKeyParam);
        if($identity){
            $session->set($this->authKeyParam, $identity->getAuthKey());
        }
    }

}