<?php
/**
 * Переопределеный класс представления.
 * Выводим html из модуля extended в нужных местах шаблона.
 */
namespace app\components;

class View extends \yii\web\View
{

    public $h1_tag;
    private $html = [];

    public function registerHtml($html, $position, $key = null)
    {
        if($key){
            $this->html[$position][$key] = $html;
        } else {
            $this->html[$position][] = $html;
        }
    }

    private function processHtml($position)
    {
        $lines = [];
        if (!empty($this->html[$position])) {
            $lines[] = implode("\n", $this->html[$position]);
        }
        return empty($lines) ? '' : implode("\n", $lines);
    }

    protected function renderBodyEndHtml($ajaxMode)
    {
        $html = parent::renderBodyEndHtml($ajaxMode);
        return $html . $this->processHtml(self::POS_END);
    }

    protected function renderHeadHtml()
    {
        $html = parent::renderHeadHtml();
        return $html . $this->processHtml(self::POS_HEAD);
    }

    protected function renderBodyBeginHtml()
    {
        $html = parent::renderBodyBeginHtml();
        return $html . $this->processHtml(self::POS_BEGIN);
    }
}