<?php
/**
 * Определение и подключение модулей.
 */
namespace app\components;

use app\modules\modulemanager\models\Module;
use Yii;
use yii\helpers\ArrayHelper;

class Application extends \yii\web\Application
{

    public $active_modules;
    public $modules_config;

    /** @inheritdoc */
    public function init()
    {
        $this->enableModules();

        parent::init();
    }

    private function enableModules()
    {
        $active_modules = $this->active_modules = $this->getActiveModules();
        $modules_config = $this->modules_config = $this->getModulesConfig();

        $bootstrap = [];

        if ($active_modules) {
            foreach ($active_modules as $module_name => $module) {
                if(!isset($modules_config[$module_name])){
                    continue;
                }
                if ($module['active']) {
                    $modules_backend[$module_name] = [
                        'class' => 'app\modules\\' . $module_name . '\Module',
                        'controllerNamespace' => 'app\modules\\' . $module_name . '\controllers\backend',
                        'viewPath' => '@app/modules/' . $module_name . '/views/backend',
                    ];
                    $modules_frontend[$module_name] = [
                        'class' => 'app\modules\\' . $module_name . '\Module',
                        'controllerNamespace' => 'app\modules\\' . $module_name . '\controllers\frontend',
                        'viewPath' => '@app/modules/' . $module_name . '/views/frontend',
                    ];


                    if(is_file(Yii::getAlias('@app/modules/'.$module_name.'/Bootstrap.php'))){
                        if ($module_name != 'section') {
                            $bootstrap[] = 'app\modules\\' . $module_name . '\Bootstrap';
                        }
                    }
                }
            }
        }

        array_push($bootstrap, 'app\modules\section\Bootstrap');

        $modules_backend['modulemanager'] = [
            'class' => 'app\modules\modulemanager\Module',
            'controllerNamespace' => 'app\modules\modulemanager\controllers\backend',
            'viewPath' => '@app/modules/modulemanager/views/backend',
        ];
        $modules_frontend['main'] = ['class' => 'app\modules\main\Module'];

        $modules_backend['admin'] = [
            'class' => 'app\modules\admin\Module',
            'modules' => $modules_backend
        ];

        $modules = array_merge($modules_backend, $modules_frontend);

        $modules = ArrayHelper::merge($modules, $modules_config);

        $this->setModules($modules);
        $this->bootstrap = array_merge($this->bootstrap, $bootstrap);

    }

    /**
     * @return mixed
     */
    private function getActiveModules()
    {
        //TODO: добавить кеширование и убрать AR
        $cms_modules = Module::find()
            ->where(['active' => 1])
            ->indexBy('module')
            ->asArray()
            ->all();

        return $cms_modules;
    }

    private function getModulesConfig()
    {
        $dir = dirname(__DIR__) . '/modules';
        $dh  = opendir($dir);
        $modules = [];

        while (false !== ($moduleName = readdir($dh))) {
            if ($moduleName != '.' && $moduleName != '..' && $moduleName != 'admin') {
                if (is_dir($dir.'/'.$moduleName)){
                    if (is_file($dir.'/'.$moduleName.'/config.php')) {
                        $modules[$moduleName] = require($dir.'/'.$moduleName.'/config.php');
                    }
                }
            }
        }

        return $modules;
    }


    public function moduleIsActive($key)
    {
        return isset($this->active_modules[$key]);
    }

}