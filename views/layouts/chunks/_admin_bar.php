<style>
    #adminbar *{
        height:auto;
        width:auto;
        margin:0;
        padding:0;
        position:static;
        text-transform:none;
        letter-spacing:normal;
        line-height:1;
    }
    #adminbar :before, #adminbar :after{content:normal;}
    #adminbar a,#adminbar a:hover,#adminbar a img,#adminbar a img:hover{
        outline:none;border:none;text-decoration:none;background:none;
    }
    #adminbar{
        direction:ltr;
        background-color:#393874;
        color:#fff;font:normal 12px/28px Arial,Helvetica,sans-serif;
        height:28px;position:fixed;top:0;left:0;width:100%;z-index:99999;
    }
    #adminbar ul,#adminbar ul li{
        background:none;list-style:none;margin:0;padding:0;position:relative;z-index:99999;
    }
    #adminbar .quicklinks ul{
        text-align:left;
    }#adminbar .quicklinks ul li{
         float:left;
     }
    #adminbar .quicklinks>ul>li>a{
        /*border-right:1px solid #918383;*/
    }
    #adminbar .quicklinks>ul>li:last-child>a{
        border-right:none;
    }
    #adminbar .quicklinks>ul>li:hover>a{
        border-left-color:#707070;
    }
    #adminbar .quicklinks a,#adminbar .shortlink-input{
        color:#fff;height:28px;text-shadow:#555 0 -1px 0;display:block;font:normal 13px/28px Arial,Helvetica,sans-serif;padding:0 .85em;margin:0;
    }
    #adminbar .quicklinks a>span{
        line-height:28px;
    }
    #adminbar .quicklinks .menupop ul,#adminbar .shortlink-input{
        -moz-box-shadow:0 4px 8px rgba(0,0,0,0.1);-webkit-box-shadow:0 4px 8px rgba(0,0,0,0.1);box-shadow:0 4px 8px rgba(0,0,0,0.1);background:#fff;background:rgba(255,255,255,0.97);display:none;position:absolute;border:1px solid #dfdfdf;border-top:none;float:none;
    }
    #adminbar .selected .shortlink-input{
        display:block;
    }
    #adminbar .quicklinks .menupop ul li{
        float:none;
    }
    #adminbar .quicklinks .menupop ul li a,#adminbar .shortlink-input{
        color:#555;text-shadow:none;white-space:nowrap;min-width:140px;
    }
    #adminbar .shortlink-input{
        width:200px;
    }
    #adminbar .quicklinks .menupop ul li:hover>a{
        color:#fff;text-shadow:#666 0 -1px 0;
    }
    #adminbar .quicklinks li:hover>ul,#adminbar .quicklinks li.hover>ul{
        display:block;
    }
    #adminbar .quicklinks .menupop li:hover>ul,#adminbar .quicklinks .menupop li.hover>ul{
        margin-left:100%;margin-top:-28px;
    }
    #adminbar .quicklinks li:hover,#adminbar .quicklinks .selected{
        background:#cc0929;
    }
    #adminbar .quicklinks .menupop li:hover{
        background:#888;background:-moz-linear-gradient(bottom,#888,#9d9d9d);background:-webkit-gradient(linear,left bottom,left top,from(#888),to(#9d9d9d));
    }
    #adminbar .quicklinks a span#ab-awaiting-mod,#adminbar .quicklinks a span#ab-updates{
        background:#eee;color:#333;text-shadow:none;display:inline;padding:2px 5px;font-size:10px;font-weight:bold;-moz-border-radius:10px;-khtml-border-radius:10px;-webkit-border-radius:10px;border-radius:10px;
    }
    #adminbar .quicklinks a:hover span#ab-awaiting-mod,#adminbar .quicklinks a:hover span#ab-updates{
        background:#fff;color:#000;}#adminbar .quicklinks li#wp-admin-bar-my-account>a{border-left:none;
                                    }
    #adminbar .quicklinks li#wp-admin-bar-my-account-with-avatar>a img{width:16px;height:16px;display:inline;border:1px solid #999;vertical-align:middle;margin:-2px 23px 0 -5px;padding:0;background:#eee;float:none;}#adminbar .quicklinks li#wp-admin-bar-my-account-with-avatar ul{left:30px;}#adminbar .quicklinks li#wp-admin-bar-my-account-with-avatar ul ul{left:0;}#adminbar .quicklinks .menupop li a img.blavatar{vertical-align:middle;margin:0 8px 0 0;padding:0;}#adminbar #adminbarsearch{float:right;height:18px;padding:3px;margin:0;}#adminbar #adminbarsearch .adminbar-input{width:140px;height:auto;float:left;font:12px Arial,Helvetica,sans-serif;color:#555;text-shadow:0 1px 0 #fff;border:1px solid #626262;padding:2px 3px;margin:0 3px 0 0;background:#ddd;-moz-box-shadow:inset 2px 2px 1px #cdcdcd;-webkit-box-shadow:inset 2px 2px 1px #cdcdcd;box-shadow:inset 2px 2px 1px #cdcdcd;-webkit-border-radius:0;-khtml-border-radius:0;-moz-border-radius:0;border-radius:0;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;-ms-box-sizing:border-box;box-sizing:border-box;outline:none;}#adminbar #adminbarsearch .adminbar-button{font:bold 12px Arial,Helvetica,sans-serif;color:#444;text-shadow:0 1px 0 #eee;cursor:pointer;float:left;background:#aaa;background:-moz-linear-gradient(bottom,#aaa,#cecece);background:-webkit-gradient(linear,left bottom,left top,from(#aaa),to(#cecece));-webkit-border-radius:10px;-khtml-border-radius:10px;-moz-border-radius:10px;border-radius:10px;border:1px solid #626262;padding:2px 13px;margin:0;width:auto;height:auto;}#adminbar #adminbarsearch .adminbar-button:active{background:#a0a0a0;background:-moz-linear-gradient(bottom,#a0a0a0,#c1c1c1);background:-webkit-gradient(linear,left bottom,left top,from(#a0a0a0),to(#c1c1c1));-moz-box-shadow:inset 1px 1px 1px #9b9b9b;-webkit-box-shadow:inset 1px 1px 1px #9b9b9b;box-shadow:inset 1px 1px 1px #9b9b9b;}#adminbar #adminbarsearch .adminbar-button:hover{color:#000;}#adminbar #adminbarsearch .adminbar-button::-moz-focus-inner{border:none;}* html #adminbar{overflow:hidden;position:absolute;}* html #adminbar .quicklinks ul li a{float:left;}* html #adminbar .menupop a span{background-image:none;}

    .ab-top-secondary {
        background-color: #464646;
        background-image: -moz-linear-gradient(center bottom , #373737, #464646 5px);
        float: right;
    }
    .demo-navbar-fixed-top {
        margin-top: 25px;
    }
    body {
        margin-top: 25px;
    }

    .js_edit_mode_on, .js_edit_mode_on.js_edit_fix * {
        outline:3px solid yellow;
        cursor: pointer;
    }
    .js_edit_mode_on.empty_editable, .js_edit_mode_on.empty_editable * {
        border:3px solid yellow;
        cursor: pointer;
    }
    .js_edit_mode_on:hover, .js_edit_mode_on.js_edit_fix:hover * {
        outline:3px solid red;
    }

</style>

<?php
$script = /** @lang JavaScript */
    <<<SCRIPT
!function($){
    $(function(){

        var editableSetup = function(){
            var comments = $("*")
            .not('iframe')
            .not('frame')
            .contents()
            .filter(function(){
                return this.nodeType == 8;
            });
            
            comments.each(function(){
                var pref = 'we_editable_stopper';
                if(this.nodeValue.search(pref) === 0){
                    $(this).prev().addClass('js_edit_block_stopper')
                }
    
            });
            
            comments.each(function(){
                var pref = 'we_editable:';
                if(this.nodeValue.search(pref) === 0){
                    var data = JSON.parse(this.nodeValue.substr(pref.length));
                    var nodes;
                    if($(this).next().hasClass('js_edit_block_stopper')){
                        nodes = $(this).next();
                    } else {
                        nodes = $(this).nextUntil('.js_edit_block_stopper').next().andSelf()
                    }
                  
                    nodes.addClass('js_edit_db_text').data('edit_url', data['edit_url'])
                }
    
            });
            editableSetup = function(){};
        };


        $('.js_show_edit_blocks').on('click', function(){
            editableSetup();
        
            var that = $(this);

            if(that.data('edit_mode') == 'off'){
                $('.js_edit_db_text').addClass('js_edit_mode_on');
                $(this).html('Скрыть блоки');
                that.data('edit_mode', 'on');
            } else if(that.data('edit_mode') == 'on'){
                $('.js_edit_db_text').removeClass('js_edit_mode_on');
                $(this).html('Показать блоки');
                that.data('edit_mode', 'off');
            }

            return false;
        });

        $('body').on('click', '.js_edit_mode_on', function(){
            var that = $(this);
            window.open(that.data('edit_url'), '_blank')
                return false;
            })

        });
}(jQuery);
SCRIPT;

$this->registerJs($script, yii\web\View::POS_END);

?>

<!--<div id="adminbar" onmouseOver="this.style.width='100%'" onmouseOut="this.style.width='30px'">-->
<div id="adminbar">
    <div class="quicklinks" >
        <ul>
            <li>
                <a href="/">Главная</a>
            </li>
            <li>
                <a href="/admin/">Панель управления</a>
            </li>
            <li class="menupop" id="wp-admin-bar-appearance">
                <a href="/admin/"><span> + Добавить</span></a>
                <ul>
                    <li>
                        <a href="/admin/section/default/create/">Раздел</a>
                    </li>
                    <li>
                        <a href="/admin/block/default/create/">Блок</a>
                    </li>
                </ul>
            </li>

            <?php if(Yii::$app->controller->id == 'section') {?>
                <li>
                    <a target="_blank" href="/admin/section/">Все разделы</a>
                </li>
                <?php if(Yii::$app->controller->current_item) {?>
                    <li>
                        <a target="_blank" href="/admin/section/<?=Yii::$app->controller->current_item;?>/update">Редактировать раздел</a>
                    </li>
                <?php }?>
            <?php } ?>

            <?php if(Yii::$app->controller->id == 'news') {?>
                <li>
                    <a target="_blank" href="/admin/news/">Все новости</a>
                </li>
                <?php if(Yii::$app->controller->current_item) {?>
                    <li>
                        <a target="_blank" href="/admin/news/<?=Yii::$app->controller->current_item;?>/update">Редактировать новость</a>
                    </li>
                <?php }?>
            <?php }?>

            <?php if(Yii::$app->controller->id == 'promo') {?>
                <li>
                    <a target="_blank" href="/admin/promo/">Все акции</a>
                </li>
                <?php if(Yii::$app->controller->current_item) {?>
                    <li>
                        <a target="_blank" href="/admin/promo/<?=Yii::$app->controller->current_item;?>/update">Редактировать акцию</a>
                    </li>
                <?php }?>
            <?php }?>

            <?php if(Yii::$app->controller->id == 'room') {?>
                <li>
                    <a target="_blank" href="/admin/rooms/">Все номера</a>
                </li>
                <?php if(Yii::$app->controller->current_item) {?>
                    <li>
                        <a target="_blank" href="/admin/rooms/<?=Yii::$app->controller->current_item;?>/update">Редактировать номер</a>
                    </li>
                <?php }?>
            <?php }?>

            <?php if(Yii::$app->controller->id == 'post') {?>
                <li>
                    <a target="_blank" href="/admin/blog/post/">Все посты</a>
                </li>
                <?php if(Yii::$app->controller->current_item) {?>
                    <li>
                        <a target="_blank" href="/admin/blog/post/<?=Yii::$app->controller->current_item;?>/update">Редактировать пост</a>
                    </li>
                <?php }?>
            <?php }?>

            <?php if(Yii::$app->controller->id == 'catalog') {?>
                <li>
                    <a target="_blank" href="/admin/catalog/product/">Все Товары</a>
                </li>
                <?php if(Yii::$app->controller->current_item) {?>
                    <li>
                        <a target="_blank" href="/admin/catalog/product/<?=Yii::$app->controller->current_item;?>/update/">Редактировать товар</a>
                    </li>
                <?php }?>
            <?php }?>

            <li><a href="#" id="show-block__link" data-edit_mode="off" class="blocks-off js_show_edit_blocks">Показать блоки</a></li>

        </ul>
        <ul class="ab-top-secondary ab-top-menu">
            <li>
                <a href="/admin/logout/">Выход</a>
            </li>
        </ul>
    </div>
</div>