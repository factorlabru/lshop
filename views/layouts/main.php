<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<?=$this->render('@app/views/layouts/chunks/_head');?>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <?=\app\widgets\Menu::widget();?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<?=$this->render('@app/views/layouts/chunks/_footer');?>
<?php
if(Yii::$app->user->can('admin')) {
    echo $this->render('@app/views/layouts/chunks/_admin_bar');
} ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>